package ir.telecom.sensor.qdmon;

interface IMsdServiceCallback {
	void stateChanged(String reason);
	void internalError();
}
