package ir.telecom.sensor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import ir.telecom.sensor.toasty.Toasty;


public class CapInfoActivity extends BaseActivity {
    private Toolbar toolbar;
    DrawerLayout drawerLayout;
TextView tvAndroidVersion, tvRoot, tvHard, tvProc, tvCompatible;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cap_info);

        setToolbar();
        setDrawer(CapInfoActivity.this,toolbar);

        tvAndroidVersion =findViewById(R.id.tv_android_version);
        tvRoot =findViewById(R.id.tv_root);
        tvHard =findViewById(R.id.tv_hardware);
        tvProc =findViewById(R.id.tv_proc);
        tvCompatible =findViewById(R.id.tv_compatible);

        Map<String, String> cpuInfo = getCpuInfoMap();
        String root = "No";
        if (checkrootcommand() == 0)
            root = "Yes";
        tvRoot.setText( "Root: " + root);
        tvAndroidVersion.setText("Android: " + Build.VERSION.RELEASE);
        tvHard.setText("Hardware: " + cpuInfo.get("Hardware"));
        tvProc.setText("Processor: " + cpuInfo.get("Processor"));

        if (StartupActivity.isSNSNCompatible()) {
            tvCompatible.setText("Compatibility: " + "This device is compatible");
            tvCompatible.setTextColor(Color.parseColor("#2196F3"));
        }else {
            tvCompatible.setText("Compatibility: " + "This device is not compatible");
            tvCompatible.setTextColor(Color.parseColor("#FF4205"));
        }
    }

    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);

        toolbarDone.setVisibility(View.INVISIBLE);

        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });

    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(CapInfoActivity.this, DashboardActivity.class);
        startActivity(i);
        finish();
    }
    public int checkrootcommand() {

        Process exec = null;
        try {

            exec = Runtime.getRuntime().exec(new String[]{"su"});

            final OutputStreamWriter out = new OutputStreamWriter(exec.getOutputStream());
            out.write("exit");
            out.flush();

            Log.e(">> ROOT: ", "su command executed successfully");
            return 0; // returns zero when the command is executed successfully
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (exec != null) {
                try {
                    exec.destroy();
                } catch (Exception ignored) {
                }
            }
        }
        Log.e(">> ROOT: ", "su command failed");
        return 1; //returns one when the command execution fails
    }

    public static Map<String, String> getCpuInfoMap() {
        Map<String, String> map = new HashMap<String, String>();
        try {
            Scanner s = new Scanner(new File("/proc/cpuinfo"));
            while (s.hasNextLine()) {
                String[] vals = s.nextLine().split(": ");
                if (vals.length > 1) map.put(vals[0].trim(), vals[1].trim());
            }
        } catch (Exception e) {
            Log.e("getCpuInfoMap", Log.getStackTraceString(e));
        }
        return map;
    }
}
