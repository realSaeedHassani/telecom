package ir.telecom.sensor;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import ir.telecom.sensor.util.MSDServiceHelperCreator;

/**
 * Created by Bahar on 9/10/2018.
 */

public class KillService extends Service{

    protected MSDServiceHelperCreator msdServiceHelperCreator;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onTaskRemoved(Intent rootIntent) {

        SensorUpdateAlarmManager alarmManager = SensorUpdateAlarmManager.getInstance();
        alarmManager.cancelReminder(KillService.this);

        SendToServerAlarmManager alarmManager2 = SendToServerAlarmManager.getInstance();
        alarmManager2.cancelReminder(KillService.this);

        SensorInfoAlarmManager alarmManager3 = SensorInfoAlarmManager.getInstance();
        alarmManager3.cancelReminder(KillService.this);

        msdServiceHelperCreator = MSDServiceHelperCreator.getInstance(this.getApplicationContext());

        if (StartupActivity.isAppInitialized() && StartupActivity.isSNSNCompatible()) {
            msdServiceHelperCreator.getMsdServiceHelper().stopRecording();
            msdServiceHelperCreator.getMsdServiceHelper().stopService();
        }
        BaseActivity.exitFlag=true;
            //stop service
            stopSelf();
//        System.exit(0);

        }
}
