package ir.telecom.sensor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.geniusforapp.fancydialog.FancyAlertDialog;
import com.github.anastr.speedviewlib.DeluxeSpeedView;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import ir.telecom.sensor.model.CellInfo;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.qdmon.StateChangedReason;
import ir.telecom.sensor.ret.RequestAPI;
import ir.telecom.sensor.ret.ServiceGenerator;
import ir.telecom.sensor.ret.model.CatcherM;
import ir.telecom.sensor.ret.model.CellInfoM;
import ir.telecom.sensor.ret.model.SensorM;
import ir.telecom.sensor.ret.model.SessionInfoM;
import ir.telecom.sensor.ret.model.ThresholdValuesDto;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.MsdDatabaseManager;
import ir.telecom.sensor.util.MsdDialog;
import ir.telecom.sensor.util.PermissionChecker;
import ir.telecom.sensor.views.DashboardThreatChart;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardActivity extends BaseActivity {
    // Attributes
    /**
     * Device IMEI
     */
    private String iMEI;
    /**
     * Device IMEI Version
     */
    private String iMEIv;
    private String simOperator;
    private String simOperatorName;
    private String simSerial;
    private String simSubs;
    private int mnc;
    private int mcc, sid;
    private int cid;
    private int psc;
    private String IMSI;
    private int lac;
    private int networktype;

    public static int batteryLevel;
    private DashboardThreatChart layout;
    private ViewTreeObserver vto;
    private TextView txtImsiMonthCount;
    private TextView txtImsiWeekCount;
    private TextView txtImsiDayCount;
    private TextView txtImsiHourCount;
    DeluxeSpeedView speedometer;
    private DashboardThreatChart dtcImsiHour;
    private DashboardThreatChart dtcImsiDay;
    private DashboardThreatChart dtcImsiWeek;
    private DashboardThreatChart dtcImsiMonth;
    private TextView txtLastAnalysisTime;
    private TextView txtLastSyncTimeWithServer;

    Vector<TextView> threatImsiCounts;
    Toolbar toolbar;
    private boolean unknownOperator = false;
    private boolean isServiceBound = false;
    private boolean isActivityActive = false;
    TimerTask mTimerTask;
    Timer t = new Timer();
    private DrawerLayout drawerLayout;
    TextView textStartDegree, textEndDegree, txtOperator;
    CPhoneStateListener psListener;
    // Sharedpreferences setting
    public static final String SHPCONFIG = "shpConfig";
    public static final String T3212 = "T3212";
    public static final String croMin = "croMin";
    public static final String croMax = "croMax";
    public static final String deltaCmcp = "deltaCmcp";
    public static final String nNorm = "nNorm";
    public static final String deltaTch = "deltaTch";
    public static final String valid = "valid";
    TelephonyManager telephonyManager;

    private final SensorInfoAlarmManager infoalarmManager = SensorInfoAlarmManager.getInstance();


    // Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);

        setToolbar();
        setDrawer(DashboardActivity.this, toolbar);

        IntentFilter ifilterBattery = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = getApplicationContext().registerReceiver(new PowerConnectionReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        }, ifilterBattery);
        speedometer = findViewById(R.id.deluxeSpeedView);
        speedometer.setWithTremble(true);
        speedometer.setTrembleDegree(0.1f);
        textEndDegree = findViewById(R.id.txtSSCid);
        textStartDegree = findViewById(R.id.txtSSLac);
        txtOperator = findViewById(R.id.txtOperator);

        psListener = new CPhoneStateListener();
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        batteryLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        recordingButton = findViewById(R.id.stop_start_recording);
        txtImsiMonthCount = findViewById(R.id.txtDashboardImsiCatcherMonthCount);
        txtImsiWeekCount = findViewById(R.id.txtDashboardImsiCatcherWeekCount);
        txtImsiDayCount = findViewById(R.id.txtDashboardImsiCatcherDayCount);
        txtImsiHourCount = findViewById(R.id.txtDashboardImsiCatcherHourCount);
        txtLastAnalysisTime = findViewById(R.id.txtDashboardLastAnalysisTime);
        txtLastSyncTimeWithServer = findViewById(R.id.txtLastSyncTimeWithServer);
        recordingButton = findViewById(R.id.stop_start_recording);
        sendtoserverButton = findViewById(R.id.send_to_server);
        dtcImsiHour = findViewById(R.id.IMSICatcherChartHour);
        dtcImsiDay = findViewById(R.id.IMSICatcherChartDay);
        dtcImsiWeek = findViewById(R.id.IMSICatcherChartWeek);
        dtcImsiMonth = findViewById(R.id.IMSICatcherChartMonth);
        threatImsiCounts = new Vector<>();
        threatImsiCounts.add(txtImsiHourCount);
        threatImsiCounts.add(txtImsiDayCount);
        threatImsiCounts.add(txtImsiWeekCount);
        threatImsiCounts.add(txtImsiMonthCount);
        checkCompatibilityAndDisableFunctions();

        //setRecordingButton();
        recordingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPrefs2 = DashboardActivity.this.getSharedPreferences("unroot_app", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
                String check = sharedPrefs2.getString("unroot_app", "1");
                if (check.equals("1")) {
                    Log.e(">> CHECK: ", "1");
                    unroottogglerecording();
                } else {
                    if (snsnIncompatibilityReason == null) {
                        Log.e(">> CHECK: ", "2");
                        toggleRecording();
                    } else {
                        Log.e(">> CHECK: ", "3");
                        showSNSNFeaturesNotWorkingDialog(snsnIncompatibilityReason);
                    }
                }
            }
        });

        sendtoserverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //send to server
                try {
                    sendtoserver();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
            }
        });

        updateLastSyncTime();

    }


    protected void sendtoserver() throws NoSuchAlgorithmException, KeyManagementException {
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        RequestAPI service = ServiceGenerator.createService(RequestAPI.class,
                sharedPrefs.getString("protocol", "https"),
                sharedPrefs.getString("ip", "5.56.135.20") + ":" + sharedPrefs.getString("port", "8443")
        );
        String ip = sharedPrefs.getString("ip", "5.56.135.20");
        String port = sharedPrefs.getString("port", "8443");
        ServerInfo serverInfo = new ServerInfo(ip, port);

        Toasty.info(this, serverInfo.getIp() + ":" + serverInfo.getPort(), Toasty.LENGTH_LONG).show();
        ArrayList<SensorM> sensorArrayList = getNewSensorArrayListM();
        if (sensorArrayList.size() > 0) {
            Call<ResponseBody> callableResponse = service.sendSensorStatus(MsdConfig.getAppId(this), 1, sensorArrayList);
            dumpCallableResponse(callableResponse, 1, service);
        }


        ArrayList<CatcherM> catcherArrayLis = getNewCatcherArrayListM();
        if (catcherArrayLis.size() > 0) {
            Call<ResponseBody> callableResponse =
                    service.sendCatcher(MsdConfig.getAppId(this), 1, catcherArrayLis);
            dumpCallableResponse(callableResponse, 0, service);
        }
        /*
         * Send Cellinfo table...
         */

        ArrayList<CellInfoM> cellInfoArrayList = getNewCellInfoArrayListM();
        if (cellInfoArrayList.size() > 0) {
            Call<ResponseBody> callableResponse = service.sendCellinfoTable(MsdConfig.getAppId(this), 1, cellInfoArrayList);
            dumpCallableResponse(callableResponse, 0, service);
        }


        ArrayList<SessionInfoM> sessionInfoArrayList = getNewSessionInfoArrayListM();
        if (sessionInfoArrayList.size() > 0) {
            Call<ResponseBody> callableResponse = service.sendSessionInfo(MsdConfig.getAppId(this), 1, sessionInfoArrayList);
            dumpCallableResponse(callableResponse, 0, service);
        }
    }


    private <T> void dumpCallableResponse(Call<T> callableResponse, final int isSensorStatus, final RequestAPI service) {
        final Request request = callableResponse.request();
        try {
            Buffer buffer = new Buffer();
            String show = request.toString() + "headers: " + request.headers() + "\n";
            if (request.body() != null) {
                request.body().writeTo(buffer);
                show += "Body : " + buffer.readString(Charset.defaultCharset());
                Log.e(">> BODY: ", show);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        callableResponse.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                Toasty.success(DashboardActivity.this, "Response code: " + response.code(), Toast.LENGTH_LONG).show();
                try {
                    if (response.code() == 200) {
                        Toasty.success(DashboardActivity.this, "Send data successfully", Toast.LENGTH_LONG).show();
                        setLastSeenSharePre();
                        ResponseBody responseBody = (ResponseBody) response.body();
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        if (isSensorStatus == 1) {
                            if (jsonObject.getString("message").equals("1")) {
                                getConfig(service);
                            }
                        }
                    }
                    Log.e(">> RESPONSE: ", getStrFromResponseBody(response));
                } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                String str = "onFailure";
                if (t instanceof Exception)
                    str += t.getMessage();
                Toasty.error(DashboardActivity.this, str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getConfig(RequestAPI service) throws NoSuchAlgorithmException, KeyManagementException {
//        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        RequestAPI service = ServiceGetNewConfig.createService(RequestAPI.class,
//                sharedPrefs.getString("protocol", "https"),
//                sharedPrefs.getString("ip", "5.56.135.20") + ":" + sharedPrefs.getString("port", "8443")
//        );

        Call<ThresholdValuesDto> callableResponse = service.getConfig(MsdConfig.getAppId(this));
        setDataConfig(callableResponse);
    }

    private void setDataConfig(Call<ThresholdValuesDto> callableResponse) {
        final Request request = callableResponse.request();
        try {
            Buffer buffer = new Buffer();
            String show = request.toString() + "headers: " + request.headers() + "\n";
            if (request.body() != null) {
                request.body().writeTo(buffer);
                show += "body: " + buffer.readString(Charset.defaultCharset());
                Log.e(">> Config: ", show);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        callableResponse.enqueue(new Callback<ThresholdValuesDto>() {
            @Override
            public void onResponse(Call<ThresholdValuesDto> call, Response<ThresholdValuesDto> response) {
                Toasty.success(DashboardActivity.this, "Response code: " + response.code(), Toast.LENGTH_LONG).show();
                if (response.code() == 200) {
                    Toasty.success(DashboardActivity.this, "New config received", Toast.LENGTH_LONG).show();
                    saveNewConfig(response.body());
                }
            }

            @Override
            public void onFailure(Call<ThresholdValuesDto> call, Throwable t) {
                String str = "onFailure";
                if (t instanceof Exception)
                    str += t.getMessage();
                Toasty.error(DashboardActivity.this, str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveNewConfig(ThresholdValuesDto body) {
        SharedPreferences shpConfig = DashboardActivity.this.getSharedPreferences(SHPCONFIG,
                Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        Log.e(">> GET_CONFIG: ", body.getT3212() + " | " +
                body.getMin_Cell_Reselection_Offset() + " | " +
                body.getMax_Cell_Reselection_Offset() + " | " +
                body.getDelay_Between_Command_And_Complete() + " | " +
                body.getPaging_Group_Number() + " | " +
                body.getTraffic_Channel_Idle_Milliseconds());

        SharedPreferences.Editor editor = shpConfig.edit();
        editor.putString(T3212, String.valueOf(body.getT3212()));
        editor.putString(croMin, String.valueOf(body.getMin_Cell_Reselection_Offset()));
        editor.putString(croMax, String.valueOf(body.getMax_Cell_Reselection_Offset()));
        editor.putString(deltaCmcp, String.valueOf(body.getDelay_Between_Command_And_Complete()));
        editor.putString(nNorm, String.valueOf(body.getPaging_Group_Number()));
        editor.putString(deltaTch, String.valueOf(body.getTraffic_Channel_Idle_Milliseconds()));
        editor.putString(valid, "1");
        editor.commit();

        ContentValues insertValues = new ContentValues();
        insertValues.put("t3212_min", body.getT3212());
        insertValues.put("cro_min", body.getMin_Cell_Reselection_Offset());
        insertValues.put("cro_max", body.getMax_Cell_Reselection_Offset());
        insertValues.put("delta_cmcp", body.getDelay_Between_Command_And_Complete());
        insertValues.put("n_norm", body.getPaging_Group_Number());
        insertValues.put("delta_tch", body.getTraffic_Channel_Idle_Milliseconds());
        db.insert("config", null, insertValues);
    }


    private void setLastSeenSharePre() {
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM session_info ORDER BY timestamp DESC LIMIT 1;";
        @SuppressLint("Recycle")
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        SharedPreferences.Editor editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastSessionInfoSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }


        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM cell_info ORDER BY first_seen DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastCellInfoSentTime", cursor.getString(cursor.getColumnIndex("first_seen")));
            editor.apply();
        }

        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM catcher ORDER BY timestamp DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();
        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastCatcherSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }

        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM sensor_status ORDER BY timestamp DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastSensorSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }
    }


    public <T> String getStrFromResponseBody(Response<T> response) throws IOException {
        String body = "";
        if (response.body() instanceof ResponseBody)
            body = ((ResponseBody) response.body()).string();
        else if (response.body() == null)
            body = "";
        else
            body = response.body().toString();

        return String.format(Locale.US, "headers:\n %s, \nmessage \n: %s\nbody:\n %s\n", response.headers(), response.message(), body);
    }

    private ArrayList<SessionInfoM> getNewSessionInfoArrayListM() {
        ArrayList<SessionInfoM> sessionInfoArrayList;
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastSessionInfoSentTime = sharedPrefs.getString("lastSessionInfoSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        sessionInfoArrayList = msdOpenHelper.getUnsentSessionInfos(dbInstanse, lastSessionInfoSentTime);

        return sessionInfoArrayList;
    }

    private ArrayList<CellInfo> getNewCellInfoArrayList() {
        ArrayList<CellInfo> cellInfoArrayList = new ArrayList<>();
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        cellInfoArrayList = msdOpenHelper.getUnsentCellInfos(dbInstanse, lastCellInfoSentTime);

        return cellInfoArrayList;
    }

    private ArrayList<CellInfoM> getNewCellInfoArrayListM() {
        ArrayList<CellInfoM> cellInfoArrayList;
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        cellInfoArrayList = msdOpenHelper.getUnsentCellInfosM(dbInstanse, lastCellInfoSentTime);
        return cellInfoArrayList;
    }

    private ArrayList<CatcherM> getNewCatcherArrayListM() {

        ArrayList<CatcherM> catcherArrayList;
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastCatcherSentTime = sharedPrefs.getString("lastCatcherSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        catcherArrayList = msdOpenHelper.getUnsentCatchersM(dbInstanse, lastCatcherSentTime);
        return catcherArrayList;

    }

    private ArrayList<SensorM> getNewSensorArrayListM() {
        ArrayList<SensorM> sensorArrayList;
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastSensorSentTime = sharedPrefs.getString("lastSensorSentTime", "2017-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        sensorArrayList = msdOpenHelper.getUnsentSensorsM(dbInstanse, lastSensorSentTime);
        return sensorArrayList;
    }

    @SuppressLint("SetTextI18n")
    protected void toggleRecording() {
        Boolean isRecording = msdServiceHelperCreator.getMsdServiceHelper().isRecording();

        if (isRecording) {
            Log.e(">> Stop: ", "Dashboard toggleRecording()");
            stopRecording();
            recordingButton.setText("Start");
        } else {
            if (PermissionChecker.checkAndRequestPermissionForMsdService(this)) {
                startRecording();
                recordingButton.setText("Stop");
            }
        }
    }

    protected void unroottogglerecording() {
        SharedPreferences sharedPrefs2 = this.getSharedPreferences("unroot_start", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String isRecording = sharedPrefs2.getString("unroot_start", "1");
        if (isRecording.equals("1")) {
            infoalarmManager.cancelReminder(DashboardActivity.this);
            recordingButton.setText("Start");
            SharedPreferences sharedPrefs5 = this.getSharedPreferences("unroot_start", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor5 = sharedPrefs5.edit();
            editor5.putString("unroot_start", "0");
            editor5.commit();
        } else {
            infoalarmManager.setReminder(DashboardActivity.this);
            recordingButton.setText("Stop");
            SharedPreferences sharedPrefs5 = this.getSharedPreferences("unroot_start", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor5 = sharedPrefs5.edit();
            editor5.putString("unroot_start", "1");
            editor5.commit();
        }
    }


//    private void setRecordingButton() {
//
//        if (msdServiceHelperCreator.getMsdServiceHelper().isRecording()) {
//            recordingButton.setText("Stop");
//        } else {
//            recordingButton.setText("Start");
//        }
//
//    }

    private void checkCompatibilityAndDisableFunctions() {

        SQLiteDatabase db;
        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(this));
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(this);
        db = MsdDatabaseManager.getInstance().openDatabase();
        msoh.createtables(db);

        SharedPreferences sharedPrefs3 = this.getSharedPreferences("unroot_app", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor3 = sharedPrefs3.edit();
        editor3.putString("unroot_app", "0");
        editor3.commit();

        LinearLayout dashboardEventCharts = (LinearLayout) findViewById(R.id.dashboardChartSection);
        final String reason = StartupActivity.snsnIncompatibilityReason;
        if (reason != null) {
            SharedPreferences sharedPrefs = this.getSharedPreferences("frist_seen_session_info", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt("frist_seen_session_info", 0);
            editor.commit();
            SharedPreferences sharedPrefs4 = this.getSharedPreferences("unroot_app", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor4 = sharedPrefs4.edit();
            editor4.putString("unroot_app", "1");
            editor4.commit();

            SharedPreferences sharedPrefs5 = this.getSharedPreferences("unroot_start", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor5 = sharedPrefs5.edit();
            editor5.putString("unroot_start", "1");
            editor5.commit();

            infoalarmManager.setReminder(DashboardActivity.this);
//            txtLastAnalysisTime.setText(getString(R.string.compat_snsn_features_not_working_short_unroot));
            setViewAndChildrenEnabled(dashboardEventCharts, true);
            enableSNSNSpecificFunctionality();
        } else {
            setViewAndChildrenEnabled(dashboardEventCharts, true);

            enableSNSNSpecificFunctionality();
        }
    }

    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        if (view == null)
            return;
        if (enabled)
            view.setAlpha(1.0f);
        else
            view.setAlpha(0.8f);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);

        toolbarDone.setVisibility(View.INVISIBLE);
        toolbarTittle.setText("Sensor");


        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });

    }


//    @Override
//    //about recording
//    public boolean onCreateOptionsMenu(Menu _menu) {
//        // Inflate the menu items for use in the action bar
//        this.menu = _menu;
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main, menu);
//
//        MenuItem menuItem = menu.findItem(R.id.menu_action_scan);
//        if (msdServiceHelperCreator.getMsdServiceHelper().isRecording()) {
//            menuItem.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_record_disable, null));
//        } else {
//            menuItem.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_notrecord_disable, null));
//        }
//
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    protected void onStart() {
        super.onStart();

        layout = (DashboardThreatChart) findViewById(R.id.IMSICatcherChartMonth);
        vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                msdServiceHelperCreator.setRectWidth(layout.getMeasuredWidth() / 2);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityActive = false;
        stopTask();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityActive = true;


        refreshView();

        setRepeatingAsyncTask();

        updateLastAnalysis();
        updateLastSyncTime();

    }

    private void updateLastSyncTime() {
//        Toasty.info(this, "Start" , Toasty.LENGTH_LONG).show();
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String SensorLastSyncTime = sharedPrefs.getString("SensorLastSyncTime", "");
        String SessionInfoLastSyncTime = sharedPrefs.getString("SessionInfoLastSyncTime", "");
        String CatcherLastSyncTime = sharedPrefs.getString("CatcherLastSyncTime", "");
        String CellInfoLastSyncTime = sharedPrefs.getString("CellInfoLastSyncTime", "");

        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));

        Date SensorSyncTime = null;
        Date SessionSyncTime = null;
        Date CatcherSyncTime = null;
        Date CellInfoSyncTime = null;
        List<Date> dates = new ArrayList<Date>();
        try {
            if (SensorLastSyncTime.length() != 0) {
                SensorSyncTime = sourceFormat.parse(SensorLastSyncTime);

                dates.add(SensorSyncTime);
            }
            if (SessionInfoLastSyncTime.length() != 0) {
                SessionSyncTime = sourceFormat.parse(SessionInfoLastSyncTime);
                dates.add(SessionSyncTime);
            }
            if (CatcherLastSyncTime.length() != 0) {
                CatcherSyncTime = sourceFormat.parse(CatcherLastSyncTime);
                dates.add(CatcherSyncTime);
            }
            if (CellInfoLastSyncTime.length() != 0) {
                CellInfoSyncTime = sourceFormat.parse(CellInfoLastSyncTime);
                dates.add(CellInfoSyncTime);
            }
        } catch (ParseException e) {
//            Toasty.info(this, "Catch: " + e.getMessage() , Toasty.LENGTH_LONG).show();
        }

        Date minDate = getEarliestDate(dates);
        String result;
        if (minDate != null) {
            SimpleDateFormat desFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
            desFormat.setTimeZone(tz);
            result = desFormat.format(minDate);
        } else {
            result = "-";
        }
//        Toasty.info(this, "End" + result , Toasty.LENGTH_LONG).show();
        txtLastSyncTimeWithServer.setText(result);

    }

    public Date getEarliestDate(List<Date> dates) {

        List<Date> nonNullDates = new ArrayList<Date>();
//        Toasty.info(this, "End" + dates.toString() , Toasty.LENGTH_LONG).show();
        for (Date d : dates) {
            if (d != null) {
                nonNullDates.add(d);
            }
        }
        if (nonNullDates.size() != 0)
            return Collections.min(nonNullDates);
        else
            return null;
    }

    public void openDetailView(View view) {
        if (snsnIncompatibilityReason != null) {
            showSNSNFeaturesNotWorkingDialog(snsnIncompatibilityReason);
            return;
        }

        if (view.equals(findViewById(R.id.IMSICatcherCharts))) {
            Intent myIntent = new Intent(this, DetailChartActivity.class);
            myIntent.putExtra("ThreatType", view.getId());
            startActivity(myIntent);
        }
    }


    @Override
    public void stateChanged(StateChangedReason reason) {

        if (reason.equals(StateChangedReason.CATCHER_DETECTED) || reason.equals(StateChangedReason.SMS_DETECTED)) {
            refreshView();
        } else if (reason.equals(StateChangedReason.ANALYSIS_DONE)) {
            updateLastAnalysis();
            refreshView();
        } else if (reason.equals(StateChangedReason.NO_BASEBAND_DATA)) {
            txtLastAnalysisTime.setText(getString(R.string.compat_no_baseband_messages));
            txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_chartRed, null));
            // txtDashboardLastAnalysis.setVisibility(View.GONE);
        }

        super.stateChanged(reason);
    }

    @Override
    protected void refreshView() {
        // Redraw charts
        resetCharts();
        // Set texts
        resetThreatCounts();
    }


    private void resetThreatCounts() {
        SharedPreferences totalscore = this.getSharedPreferences("TotalScore", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String scoreText = totalscore.getString("TotalScore", "0");
        if (scoreText.equals("0.0")) {
            txtImsiMonthCount.setText(String.valueOf(0));
        } else if (!scoreText.equals("0.0")) {
            txtImsiMonthCount.setText(String.valueOf(msdServiceHelperCreator.getThreatsImsiMonthSum().length));
        }
        if (scoreText.equals("0.0")) {
            txtImsiWeekCount.setText(String.valueOf(0));
        } else if (!scoreText.equals("0.0")) {
            txtImsiWeekCount.setText(String.valueOf(msdServiceHelperCreator.getThreatsImsiWeekSum().length));
        }
        if (scoreText.equals("0.0")) {
            txtImsiDayCount.setText(String.valueOf(0));
        } else if (!scoreText.equals("0.0")) {
            txtImsiDayCount.setText(String.valueOf(msdServiceHelperCreator.getThreatsImsiDaySum().length));
        }
        if (scoreText.equals("0.0")) {
            txtImsiHourCount.setText(String.valueOf(0));
        } else if (!scoreText.equals("0.0")) {
            txtImsiHourCount.setText(String.valueOf(msdServiceHelperCreator.getThreatsImsiHourSum().length));
        }


        for (TextView tv : threatImsiCounts) {
            if (Integer.valueOf(tv.getText().toString()) > 0) {
                tv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_chartRed, null));
            } else {
                tv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_chartGreen, null));
            }
        }
    }

    private void resetCharts() {
        dtcImsiHour.invalidate();
        dtcImsiDay.invalidate();
        dtcImsiWeek.invalidate();
        dtcImsiMonth.invalidate();
    }


    private void updateLastAnalysis() {
//        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
//        Cursor cursor = null;
//        cursor = db.rawQuery("select timestamp  from catcher", null);
//        if(cursor !=null && cursor.moveToFirst()) {
////            SharedPreferences time =  this.getSharedPreferences("GetLastAnalysis", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
////            String timeText = time.getString("GetLastAnalysis","0");
//            cursor.moveToLast();
//            txtLastAnalysisTime.setText(convertDate(cursor.getString(0)));
//            txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_text, null));
//        }
//        else {
        // Set time of last measurement
        SharedPreferences sharedPrefs2 = this.getSharedPreferences("unroot_app", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String check = sharedPrefs2.getString("unroot_app", "1");
        long lastAnalysisTime = 0;
//            Log.i("lastAnalysisTime", "yeeees");
        if (msdServiceHelperCreator.getMsdServiceHelper().isConnected()) {
            lastAnalysisTime = msdServiceHelperCreator.getMsdServiceHelper().getLastAnalysisTimeMs();
//                Log.i("lastAnalysisTime", String.valueOf(lastAnalysisTime));
        }
        if (lastAnalysisTime > 0) {
//                Log.i("lastAnalysisTime", "yes");
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(lastAnalysisTime);
            txtLastAnalysisTime.setText(String.valueOf(DateFormat.getDateTimeInstance().format(c.getTime())));
            txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_text, null));
            //  txtDashboardLastAnalysis.setVisibility(View.VISIBLE);
        } else if (check.equals("1")) {
            //
            SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
            Cursor cursor = null;
            cursor = db.rawQuery("select timestamp  from session_info", null);
            if (cursor != null && cursor.moveToFirst()) {
////            SharedPreferences time =  this.getSharedPreferences("GetLastAnalysis", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
////            String timeText = time.getString("GetLastAnalysis","0");
                cursor.moveToLast();
                txtLastAnalysisTime.setText(convertDate(cursor.getString(0)));
                txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_text, null));
            }
        } else {
            //change for bahare
            SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            lastAnalysisTime = sharedPrefs.getLong("lastAnalysisTimeMs", 0);
            if (lastAnalysisTime > 0) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(lastAnalysisTime);
                txtLastAnalysisTime.setText(String.valueOf(DateFormat.getDateTimeInstance().format(c.getTime())));
                txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_text, null));
                //  txtDashboardLastAnalysis.setVisibility(View.VISIBLE);
            } else {
                txtLastAnalysisTime.setText("No analysis");
                txtLastAnalysisTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color.common_text, null));
                //  txtDashboardLastAnalysis.setVisibility(View.VISIBLE);
            }
//            txtDashboardLastAnalysis.setVisibility(View.GONE);
        }
//        }
    }

    public static Map<String, String> getCpuInfoMap() {
        Map<String, String> map = new HashMap<String, String>();
        try {
            Scanner s = new Scanner(new File("/proc/cpuinfo"));
            while (s.hasNextLine()) {
                String[] vals = s.nextLine().split(": ");
                if (vals.length > 1) map.put(vals[0].trim(), vals[1].trim());
            }
        } catch (Exception e) {Log.e("getCpuInfoMap",Log.getStackTraceString(e));}
        return map;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        MsdLog.d(">> DashboardActivity", "Received Permission request result; code: " + requestCode);
        if (requestCode == PermissionChecker.REQUEST_ACTIVE_TEST_PERMISSIONS) {
            if (grantResults.length > 0) {
                //find all neccessary permissions not granted
                List<String> notGrantedPermissions = new LinkedList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        notGrantedPermissions.add(permissions[i]);
                    }
                }

                if (notGrantedPermissions.isEmpty()) {
                    //Success: All neccessary permissions granted
                    // -> start Active Test!
                    //Log.i(TAG, "Active Test PERMISSIONS: ALL granted!");
                } else {

                    //ask again for all not granted permissions
                    boolean showDialog = false;
                    for (String notGrantedPermission : notGrantedPermissions) {
                        showDialog = showDialog || ActivityCompat.shouldShowRequestPermissionRationale(this, notGrantedPermission);
                    }

                    if (showDialog) {
//                        showDialogAskingForAllPermissionsActiveTest(getResources().getString(R.string.alert_active_test_permissions_not_granted));
                    } else {
                        // IF permission is denied (and "never ask again" is checked)
                        // Log.e(TAG, mTAG + ": Permission FAILURE: some permissions are not granted. Asking again.");
                        showDialogPersistentDeniedPermissions(getResources().getString(R.string.alert_active_test_permissions_not_granted_persistent));
                    }

                }

            }
        } else if (requestCode == PermissionChecker.REQUEST_NETWORK_ACTIVITY_PERMISSIONS) {
            if (grantResults.length > 0) {
                //find all neccessary permissions not granted
                List<String> notGrantedPermissions = new LinkedList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        notGrantedPermissions.add(permissions[i]);
                    }
                }

                if (notGrantedPermissions.isEmpty()) {
                    //Success: All neccessary permissions granted
                    Intent intent = new Intent(this, NetworkInfoActivity.class);
                    startActivity(intent);
                } else {
                    //ask again for all not granted permissions
                    boolean showDialog = false;
                    for (String notGrantedPermission : notGrantedPermissions) {
                        showDialog = showDialog || ActivityCompat.shouldShowRequestPermissionRationale(this, notGrantedPermission);
                    }

                    if (showDialog) {
                        showDialogAskingForAllPermissionsNetworkInfo(getResources().getString(R.string.alert_network_activity_permissions_not_granted));
                    } else {
                        // IF permission is denied (and "never ask again" is checked)
                        // Log.e(TAG, mTAG + ": Permission FAILURE: some permissions are not granted. Asking again.");
                        showDialogPersistentDeniedPermissionsNetworkInfo(getResources().getString(R.string.alert_network_activity_permissions_not_granted_persistent));
                    }

                }

            }
        } else if (requestCode == PermissionChecker.REQUEST_MSDSERVICE_PERMISSIONS) {
            if (grantResults.length > 0) {
                //find all neccessary permissions not granted
                List<String> notGrantedPermissions = new LinkedList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        notGrantedPermissions.add(permissions[i]);
                    }
                }

                if (notGrantedPermissions.isEmpty()) {
                    //Success: All neccessary permissions granted
                    if (snsnIncompatibilityReason == null)
                        startRecording();
                } else {

                    //ask again for all not granted permissions
                    boolean showDialog = false;
                    for (String notGrantedPermission : notGrantedPermissions) {
                        showDialog = showDialog || ActivityCompat.shouldShowRequestPermissionRationale(this, notGrantedPermission);
                    }

                    if (showDialog) {
                        showDialogAskingForAllPermissionsMsdService(getResources().getString(R.string.alert_msdservice_permissions_not_granted));
                    } else {
                        // IF permission is denied (and "never ask again" is checked)
                        // Log.e(TAG, mTAG + ": Permission FAILURE: some permissions are not granted. Asking again.");
                        showDialogPersistentDeniedPermissions(getResources().getString(R.string.alert_msdservice_permissions_not_granted_persistent));
                    }

                }

            }
        }

    }

    private void showDialogAskingForAllPermissionsMsdService(String message) {
        MsdDialog.makeConfirmationDialog(this, message,
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PermissionChecker.checkAndRequestPermissionForMsdService(DashboardActivity.this);
                    }
                }, null, false).show();
    }


    private void showDialogPersistentDeniedPermissions(String message) {

        MsdDialog.makeConfirmationDialog(this, message, null, null, false).show();

    }

    private void showDialogAskingForAllPermissionsNetworkInfo(String message) {
        MsdDialog.makeConfirmationDialog(this, message,
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PermissionChecker.checkAndRequestPermissionsForNetworkActivity(DashboardActivity.this);
                    }
                },
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DashboardActivity.this, NetworkInfoActivity.class);
                        startActivity(intent);
                    }
                }, false).show();
    }

    private void showDialogPersistentDeniedPermissionsNetworkInfo(String message) {
        MsdDialog.makeConfirmationDialog(this, message,
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DashboardActivity.this, NetworkInfoActivity.class);
                        startActivity(intent);
                    }
                },
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DashboardActivity.this, NetworkInfoActivity.class);
                        startActivity(intent);
                    }
                }, false).show();
    }

    private void textviewSetParams() {
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(this);
        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        //
        String[] scoreparams = msoh.select(db);
        float totalScore = 0;
        //
        String names[] = {"Inconsistent LAC", "Different ARFCNs", "Lonesome LA", "Non Neighbor", "High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher", "Low t3212", "Empty Paging", "Orphaned Channel", "Inconsistent Neighbor", "High Paging"};

        String param[] = {"P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12", "P13", "P14"};

        Boolean checked;
        SharedPreferences sharedPrefs = this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        TextView showParamsTextview1 = (TextView) findViewById(R.id.show_neighborparams_dashboard);
        TextView showParamsTextview = (TextView) findViewById(R.id.show_params_dashboard);
        String a = "";
        String b = "";
        Boolean settingSave = sharedPrefs.getBoolean("setting_save", false);


        TableLayout stk = (TableLayout) findViewById(R.id.table_main);
        stk.removeAllViews();

        TableRow tbrow0 = new TableRow(this);

        TextView tv0 = new TextView(this);
        tv0.setText("   Parameter   ");
        tv0.setTextColor(getResources().getColor(R.color.black));
        tv0.setBackgroundResource(R.drawable.border3);
        tbrow0.addView(tv0);

        TextView tv1 = new TextView(this);
        tv1.setText("   Parameter Name   ");
        tv1.setTextColor(getResources().getColor(R.color.black));
        tv1.setBackgroundResource(R.drawable.border3);
        tbrow0.addView(tv1);

        TextView tv2 = new TextView(this);
        tv2.setText("   Value   ");
        tv2.setTextColor(getResources().getColor(R.color.black));
        tv2.setBackgroundResource(R.drawable.border3);
        tbrow0.addView(tv2);
        stk.addView(tbrow0);


        if (settingSave == false) {

            for (int i = 0; i < 14; i++) {
                // a += names[i]+":\t" + scoreparams[i] + "\n";
                totalScore += Float.valueOf(scoreparams[i]);

                TableRow tbrow = new TableRow(this);
                tbrow.setBackgroundResource(R.drawable.border1);

                TextView t0v = new TextView(this);
                t0v.setText("  " + param[i] + "  ");
                t0v.setTextColor(getResources().getColor(R.color.black));
                t0v.setGravity(Gravity.CENTER);
                t0v.setBackgroundResource(R.drawable.border4);
                tbrow.addView(t0v);


                TextView t1v = new TextView(this);
                t1v.setText("  " + names[i] + "  ");
                t1v.setTextColor(getResources().getColor(R.color.black));
                t1v.setGravity(Gravity.CENTER);
                t1v.setBackgroundResource(R.drawable.border1);
                tbrow.addView(t1v);

                TextView t2v = new TextView(this);
                t2v.setText(scoreparams[i]);
                t2v.setTextColor(getResources().getColor(R.color.black));
                t2v.setGravity(Gravity.CENTER);
                t2v.setBackgroundResource(R.drawable.border2);
                tbrow.addView(t2v);
                stk.addView(tbrow);
            }
        } else {

            for (int i = 0; i < 14; i++) {
                checked = sharedPrefs.getBoolean(names[i], true);
                if (checked) {
                    totalScore += Float.valueOf(scoreparams[i]);

                    TableRow tbrow = new TableRow(this);
                    tbrow.setBackgroundResource(R.drawable.border1);

                    TextView t0v = new TextView(this);
                    t0v.setText("  " + param[i] + "  ");
                    t0v.setTextColor(getResources().getColor(R.color.black));
                    t0v.setGravity(Gravity.CENTER);
                    t0v.setBackgroundResource(R.drawable.border1);
                    tbrow.addView(t0v);

                    TextView t1v = new TextView(this);
                    t1v.setText("  " + names[i] + "  ");
                    t1v.setTextColor(getResources().getColor(R.color.black));
                    t1v.setGravity(Gravity.CENTER);
                    t1v.setBackgroundResource(R.drawable.border1);
                    tbrow.addView(t1v);

                    TextView t2v = new TextView(this);
                    t2v.setText(scoreparams[i]);
                    t2v.setTextColor(getResources().getColor(R.color.black));
                    t2v.setGravity(Gravity.CENTER);
                    t2v.setBackgroundResource(R.drawable.border2);
                    tbrow.addView(t2v);
                    stk.addView(tbrow);
                }
            }

        }


        final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
        final Button btn = (Button) findViewById(R.id.your_btn);
        final Button neighborbtn = (Button) findViewById(R.id.neighbor_btn);
        btn.setBackgroundColor(Color.GREEN);
        btn.startAnimation(animation);
        neighborbtn.setBackgroundColor(Color.GREEN);
        neighborbtn.startAnimation(animation);
        Float threshold = msoh.getThresholdConfig(db);

        //save total
        SharedPreferences totalscore1 = this.getSharedPreferences("TotalScore", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = totalscore1.edit();
        String score = Float.toString(totalScore);
        editor.putString("TotalScore", score);
        editor.commit();
        Cursor cursor = null;
        cursor = db.rawQuery("select timestamp  from catcher", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String time = convertDate(cursor.getString(0));

            if (totalScore > threshold) {
                a += " IMSI catcher detected at " + time + " \n Aggregate Score =" + totalScore + " \n Threshold Value= " + threshold;
                Drawable d = getResources().getDrawable(R.drawable.round_button_red);
                btn.setBackground(d);
            }
        } else {
            a += " No IMSI catcher detected" + " \n Aggregate Score =" + totalScore + " \n Threshold Value= " + threshold;
            Drawable d = getResources().getDrawable(R.drawable.round_button_green);
            btn.setBackground(d);
        }
        Cursor cursor2 = null;
        cursor2 = db.rawQuery("select timestamp,score from neighborcatcher", null);
        if (cursor2 != null && cursor2.moveToFirst()) {
            cursor.moveToLast();

            if (cursor2.getFloat(1) > threshold) {
                b += " IMSI catcher detected in around at" + cursor2.getFloat(0) + " Aggregate Score =" + cursor2.getFloat(1) + " \n Threshold Value= " + threshold;
                Drawable d = getResources().getDrawable(R.drawable.round_button_red);
                neighborbtn.setBackground(d);
            }
        } else {
            b += " No IMSI catcher detected in around \n Aggregate Score =0.0 \n Threshold Value= " + threshold;
            Drawable d = getResources().getDrawable(R.drawable.round_button_green);
            neighborbtn.setBackground(d);
        }

        showParamsTextview.setText(a);
        showParamsTextview1.setText(b);
    }

    /**
     * set a async to repeat every repeatTime
     */
    private void setRepeatingAsyncTask() {

        SharedPreferences sharedPrefs = DashboardActivity.this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        int repeatTime = Integer.parseInt(sharedPrefs.getString("dashboardUpdateInterval", "10"));
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        textviewSetParams();
                        updateLastAnalysis();
                        updateLastSyncTime();
                    }
                });
            }
        };

        t.schedule(mTimerTask, 0, (long) repeatTime * 1000);

    }

    public void stopTask() {

        if (mTimerTask != null) {
            mTimerTask.cancel();
        }
    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {


        FancyAlertDialog.Builder alert = new FancyAlertDialog.Builder(DashboardActivity.this)
                .setTextSubTitle("Exit")
                .setBody("Do you want to exit?")
                .setNegativeColor(R.color.dialog_cancel)
                .setNegativeButtonText("No")
                .setOnNegativeClicked(new FancyAlertDialog.OnNegativeClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButtonText("Yes")
                .setPositiveColor(R.color.dialog_okay)
                .setOnPositiveClicked(new FancyAlertDialog.OnPositiveClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        quitApplication();
                    }
                })
                .setBodyGravity(FancyAlertDialog.TextGravity.LEFT)
                .setTitleGravity(FancyAlertDialog.TextGravity.CENTER)
                .setSubtitleGravity(FancyAlertDialog.TextGravity.CENTER)
                .setCancelable(false)
                .build();
        alert.show();
    }

    public String convertDate(String timeStamp) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
//            Log.e("pari","error:"+e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);
        try {
            return destFormat.format(parsed);
        } catch (Exception ex) {
            return timeStamp;
        }

    }

    class PowerConnectionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
        }
    }

    public class CPhoneStateListener extends PhoneStateListener {
        public int signalStrengthValue;

        @SuppressLint("SetTextI18n")
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
//            signalStrengthValue = signalStrength.getGsmSignalStrength();
//            signalStrengthValue = (2 * signalStrengthValue) - 113; // -> dBm
//            Log.e(">> SP: ", String.valueOf(getSignalStrengthValue()));

            if (Settings.System.getInt(getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0) {
                speedometer.speedTo(-120);
                speedometer.setTrembleDegree(0);

            } else {
                List<Integer> param = getSignalStrengthValue(telephonyManager);
                if (param.size() > 0) {
                    speedometer.setTrembleDegree(0.1f);
                    speedometer.speedTo(param.get(0));
                    textEndDegree.setText("CID: " + param.get(1));
                    textStartDegree.setText("LAC: " + param.get(2));
                    switch (param.get(3)) {
                        case 35:
                            txtOperator.setText("Irancell");
                            break;
                        case 11:
                            txtOperator.setText("IR-MCI");
                            break;
                        case 20:
                            txtOperator.setText("Rightel");
                            break;
                        default:
                            txtOperator.setText(" ");
                            break;
                    }
                }
            }
        }

        List<Integer> getSignalStrengthValue(TelephonyManager tm) {
            int lCurrentApiVersion = android.os.Build.VERSION.SDK_INT;
            List<Integer> param = new ArrayList<>(4);
            List<android.telephony.CellInfo> cellInfoList = new ArrayList<>();
            try {
//            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
                } else {
                    cellInfoList = tm.getAllCellInfo();
                }
                if (cellInfoList != null) {
                    if (tm.getAllCellInfo().size() != 0) {
                        android.telephony.CellInfo info = tm.getAllCellInfo().get(0);

                        if (info instanceof CellInfoGsm) {
                            final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                            final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
//                    arfcn=identityGsm.getArfcn();
                            // Signal Strength
                            signalStrengthValue = gsm.getDbm(); // [dBm]
                            // Cell Identity
//                        mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
                            cid = identityGsm.getCid();
//                    mcc = identityGsm.getMcc();
                            mnc = identityGsm.getMnc();
                            lac = identityGsm.getLac();
                            param.add(signalStrengthValue);
                            param.add(cid);
                            param.add(lac);
                            param.add(mnc);

                        } else if (info instanceof CellInfoCdma) {
                            final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                            final CellIdentityCdma identityCdma = ((CellInfoCdma) info).getCellIdentity();
                            //arfcn
//                    arfcn=identityCdma.get();
                            // Signal Strength
                            signalStrengthValue = cdma.getDbm();
                            // Cell Identity
                            cid = identityCdma.getBasestationId();
                            mnc = identityCdma.getSystemId();
                            lac = identityCdma.getNetworkId();
                            sid = identityCdma.getSystemId();
                            param.add(signalStrengthValue);
                            param.add(cid);
                            param.add(lac);
                            param.add(mnc);
                        } else if (info instanceof CellInfoLte) {
                            final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                            final CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
                            // Signal Strength
                            signalStrengthValue = lte.getDbm();
                            // Cell Identity
                            mcc = identityLte.getMcc();
                            mnc = identityLte.getMnc();
                            cid = identityLte.getCi();
                            param.add(signalStrengthValue);
                            param.add(cid);
                            param.add(lac);
                            param.add(mnc);
                        } else if (lCurrentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2 && info instanceof CellInfoWcdma) {
                            final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
                            final CellIdentityWcdma identityWcdma = ((CellInfoWcdma) info).getCellIdentity();

                            // Signal Strength
                            signalStrengthValue = wcdma.getDbm();
                            // Cell Identity
                            lac = identityWcdma.getLac();
                            mcc = identityWcdma.getMcc();
                            mnc = identityWcdma.getMnc();
                            cid = identityWcdma.getCid();
                            psc = identityWcdma.getPsc();
                            param.add(signalStrengthValue);
                            param.add(cid);
                            param.add(lac);
                            param.add(mnc);
                        }
                    }
                }
            } catch (NullPointerException npe) {
//            Log.e(TAG, "sorry2");
            }
            if (param.size() != 0) {
                //Log.e(">> PARAM: ", String.valueOf(param));
            }
            return param;
        }


    }
}

