/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package ir.telecom.sensor.tableview.tableview;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.TableLayout;

import androidx.appcompat.widget.Toolbar;
import java.util.ArrayList;
import java.util.List;
import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.tableview.tableview.model.Cell;
import ir.telecom.sensor.tableview.tableview.model.ColumnHeader;
import ir.telecom.sensor.tableview.tableview.model.RowHeader;
import ir.telecom.sensor.util.MsdDatabaseManager;
public class TableViewModel {

    Intent intent;
    TableLayout table_layout ;
    private Toolbar toolbar;
    private boolean change=false;
    private String timeColNumber;
    private String tableName;
    MsdSQLiteOpenHelper msoh;
    SQLiteDatabase db;
    private static int COLUMN_SIZE = 500;
    private static int ROW_SIZE = 500;

    private Context mContext;

    public TableViewModel(Context context, Intent intent) {
        mContext = context;

        this.intent = intent;
        if(intent!=null){
            timeColNumber=intent.getStringExtra("timeColNumber");
            tableName=intent.getStringExtra("info");

        }else{
            timeColNumber="";
            tableName="";
        }
        msoh = new MsdSQLiteOpenHelper(mContext);
        db = MsdDatabaseManager.getInstance().openDatabase();
    }
    private List<RowHeader> getSimpleRowHeaderList() {


// Set rows.
        List<RowHeader> list = new ArrayList<>();
        ROW_SIZE =msoh.selectFullTables(db, tableName).getCount();
        for (int i = 0; i < ROW_SIZE; i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "" + i);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    public static List<RowHeader> getSimpleRowHeaderList(int startIndex) {
        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "row " + (startIndex + i));
            list.add(header);
        }

        return list;
    }
    /**
     * This is a dummy model list test some cases.
     */
    private List<ColumnHeader> getRandomColumnHeaderList() {

        // Set Columns.
        Cursor c;
        c = msoh.selectFullTables(db,tableName);
        COLUMN_SIZE = c.getColumnCount();



        List<ColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < COLUMN_SIZE; i++) {
            String title =c.getColumnName(i);
            ColumnHeader header = new ColumnHeader(String.valueOf(i), title);
            list.add(header);
        }
        return list;
    }
    /**
     * This is a dummy model list test some cases.
     */
    private List<List<Cell>> getCellListForSortingTest() {

        // Set all cells
        Cursor c;
        c = msoh.selectFullTables(db,tableName);
        ROW_SIZE = c.getCount();
        COLUMN_SIZE = c.getColumnCount();
        c.moveToFirst();

        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            for (int j = 0; j < COLUMN_SIZE; j++) {
                Cell cell = new Cell(String.valueOf(i), c.getString(j));
                cellList.add(cell);
            }
            list.add(cellList);
            c.moveToNext();
        }
        return list;
    }

    public List<List<Cell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<RowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<ColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }
}
