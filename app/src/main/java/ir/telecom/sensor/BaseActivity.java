package ir.telecom.sensor;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import ir.telecom.sensor.error.handling.ErrorControlObject;
import ir.telecom.sensor.qdmon.StateChangedReason;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MSDServiceHelperCreator;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.MsdDialog;
import ir.telecom.sensor.util.MsdLog;
import ir.telecom.sensor.util.Utils;

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "SNSN: BaseActivity";
    // Attributes
    protected MSDServiceHelperCreator msdServiceHelperCreator;
    protected TextView messageText;
    protected View messageLayout;
    protected Toast messageToast;
    protected Menu menu;
    protected Button recordingButton;
    protected Button sendtoserverButton;
    protected Boolean isInForeground = false;
    protected Handler handler;
    private Intent patchanalysisIntent;
    protected final int refresh_intervall = 1000;
    // Static variable so that it is common to all Activities of the App
    public static boolean exitFlag = false;
    protected String snsnIncompatibilityReason=null;
    private ErrorControlObject errorControlObject = new ErrorControlObject();
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    TextView toolbarTittle;
    TextView toolbarDone;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * ....................
         * >> Error Handling. |
         * ....................
         */
//        errorControlObject = new ErrorControlObject();
//        errorControlObject.setmCrashText(this.getLocalClassName());
//        errorControlObject.setmDestinationActivity(BaseActivity.class);
//        Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(this, errorControlObject));

//        Logger.addLogAdapter(new AndroidLogAdapter());

//        Logger.addLogAdapter(new DiskLogAdapter());
//        Logger.e(">> ERRRor");


        LayoutInflater inflater = getLayoutInflater();
        messageLayout = inflater.inflate(R.layout.custom_message_popdown,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        messageText = (TextView) messageLayout.findViewById(R.id.text);
        messageToast = new Toast(getApplicationContext());
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);



        // Get MsdService Helperf
        msdServiceHelperCreator = MSDServiceHelperCreator.getInstance(this.getApplicationContext());
        MsdLog.init(msdServiceHelperCreator.getMsdServiceHelper());
//        MsdLog.i("MSD", "MSD_ACTIVITY_CREATED: " + getClass().getCanonicalName());

        handler = new Handler();

    }

    @Override
    protected void onResume() {
        if (exitFlag) {
            finish();
//            System.exit(0);
            return;
        }
        msdServiceHelperCreator.setCurrentActivity(this);

        isInForeground = true;

        handler.postDelayed(runnable, refresh_intervall);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//          ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            requestPermissions();
        }

//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
////          ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
//            requestPermissions2();
//        }

//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//          ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
//            requestPermissions3();
//        }

        super.onResume();
    }


    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION ,android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

    }

    private void requestPermissions2() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE}, 2);

    }

    private void requestPermissions3() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 3);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
//        Log.i("TAG", "onRequestPermissionResult");
        if (requestCode == 1) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
//                Log.i("TAG", "User interaction was cancelled.");

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                boolean gps_enabled=false;
                boolean network_enabled=false;
                // Permission granted.
                try {
                    gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception ex) {
                }

                try {
                    network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch (Exception ex) {
                }

                if (!gps_enabled && !network_enabled) {
                    Toasty.error(this, "Location Service is not active. Please turn on gps or connect to network", Toast.LENGTH_LONG).show();

                }
            } else {
                Toasty.error(this,"this app need access to location data.",Toast.LENGTH_LONG).show();
                quitApplication();
            }
        }
        else if(requestCode == 2){

            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
//                Log.i("TAG", "User interaction was cancelled.");

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toasty.success(this,"Permission granted",Toast.LENGTH_LONG).show();

            } else {
                Toasty.warning(this,"this app need access to Read/write data in external storage.",Toast.LENGTH_LONG).show();
                quitApplication();
            }


        }
        else if(requestCode == 3){

            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
//                Log.i("TAG", "User interaction was cancelled.");

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toasty.success(this,"Permission granted",Toast.LENGTH_LONG).show();

            } else {
                Toasty.error(this,"this app need access to Read/write data in external storage.",Toast.LENGTH_LONG).show();
                quitApplication();
            }


        }
    }


    @Override
    protected void onPause() {
        isInForeground = false;
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void showCapInfo() {
        Intent intent = new Intent(this, CapInfoActivity.class);
        startActivity(intent);
    }
    protected void showNetworkInfo() {
            Intent intent = new Intent(this, NetworkInfoActivity.class);
            startActivity(intent);
            finish();
    }


    protected void startRecording() {
        msdServiceHelperCreator.getMsdServiceHelper().startRecording();
    }

    protected void stopRecording() {
        Log.e(">> Stop: ", "Recording stopped!");
        msdServiceHelperCreator.getMsdServiceHelper().stopRecording();
    }

    public MSDServiceHelperCreator getMsdServiceHelperCreator() {
        return msdServiceHelperCreator;
    }


    public void disableSNSNSpecificFunctionality(String snsnIncompatibilityReason){
        this.snsnIncompatibilityReason = snsnIncompatibilityReason;
    }

    public void enableSNSNSpecificFunctionality(){
        this.snsnIncompatibilityReason = null;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//
//            case R.id.menu_action_scan:
//                if (snsnIncompatibilityReason == null) {
//                    toggleRecording();
//                } else {
//                    showSNSNFeaturesNotWorkingDialog(snsnIncompatibilityReason);
//                }
//                break;
//            case R.id.menu_action_about:
//                showAbout();
//                break;
//            case R.id.menu_action_exit:
//                quitApplication();
//                break;
//            case R.id.menu_action_network_info:
//                if(snsnIncompatibilityReason == null) {
//                    showNetworkInfo();
//                }else{
//                    showSNSNFeaturesNotWorkingDialog(snsnIncompatibilityReason);
//                }
//                break;
//
////                add
//            case R.id.menu_action_select_params:
//                Intent intent = new Intent(this, MySettingActivity.class);
//                startActivity(intent);
//                break;
//
//
////            add
//            case R.id.menu_action_edit_config:
//                Intent intent1 = new Intent(this, EditConfigActivity.class);
//                startActivity(intent1);
//                break;
//
//            //            add
//            case R.id.menu_action_tables:
//                Intent intent2 = new Intent(this, ListOfTables.class);
//                startActivity(intent2);
//                break;
//
//
//
//            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
//                break;
//            default:
//                MsdLog.e("BaseActivity", "Invalid menu entry pressed,  id=" + item.getItemId());
//                break;
//        }
//
//        return true;
//    }

    private void showMessage(String message) {
        if (isInForeground) {
            messageText.setText(message);
            messageToast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, getActionBar().getHeight());
            messageToast.setDuration(Toast.LENGTH_LONG);
            messageToast.setView(messageLayout);
            messageToast.show();
        }
    }

    public void internalError(String errorMsg) {
//        MsdDialog.makeFatalConditionDialog(this, "A fatal error occured!", new OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                quitApplication();
//            }
//        }, errorMsg, false).show();
        //Log.d("internal error","fetal error");
    }
//A_6  check for state  and start to recording
    public void stateChanged(StateChangedReason reason) {
        if (reason.equals(StateChangedReason.RECORDING_STATE_CHANGED)) {

                if (msdServiceHelperCreator.getMsdServiceHelper().isRecording()) {
                    //recordingButton.setText("Stop");
                 //  showMessage(getResources().getString(R.string.message_recordingStarted));
                    Toasty.success(this,R.string.message_recordingStarted,Toast.LENGTH_LONG).show();
                } else {
                   //recordingButton.setText("Start");
                  // showMessage(getResources().getString(R.string.message_recordingStopped));
                    Toasty.success(this,R.string.message_recordingStopped,Toast.LENGTH_LONG).show();
                }
        }
    }

    public static String setAppId(Context context) {
        if (MsdConfig.getAppId(context).equals("")) {
            MsdConfig.setAppId(context, Utils.generateAppId());
        }
        return MsdConfig.getAppId(context);
    }

    protected Runnable runnable = new Runnable() {
        @Override
        public void run() {
            /* do what you need to do */
            refreshView();
            /* and here comes the "trick" */
            handler.postDelayed(runnable, refresh_intervall);
        }
    };

    protected void refreshView() {
    }

    private void setRecordingButton() {

            if (msdServiceHelperCreator.getMsdServiceHelper().isRecording()) {
                recordingButton.setText("Stop");
            } else {
                recordingButton.setText("Start");
            }

    }

    protected void quitApplication() {

//        MsdLog.i("MSD", "BaseActivity.quitApplication() called");

        SensorUpdateAlarmManager alarmManager = SensorUpdateAlarmManager.getInstance();
        alarmManager.cancelReminder(BaseActivity.this);

        SendToServerAlarmManager alarmManager2 = SendToServerAlarmManager.getInstance();
        alarmManager2.cancelReminder(BaseActivity.this);


        SensorInfoAlarmManager alarmManager3 =  SensorInfoAlarmManager.getInstance();
        alarmManager3.cancelReminder(BaseActivity.this);

        if (StartupActivity.isAppInitialized() && StartupActivity.isSNSNCompatible()) {
            Toasty.success(this,
                    "StartupActivity.isAppInitialized() && StartupActivity.isSNSNCompatible()").show();
            msdServiceHelperCreator.getMsdServiceHelper().stopRecording();
            msdServiceHelperCreator.getMsdServiceHelper().stopService();
        }
        // If we call System.exit() here from an activity launched by
        // DashboardActivity, the Android system will restart the App to resume
        // DashboardActivity (which is still on the activity stack). So
        // System.exit() has to be called from onResume() in DashboardActivity
        // instead. This is implemented via exitFlag, which is a static variable
        // of BaseActivity.
        exitFlag = true;
        finish();
      //  if (this.getClass() == DashboardActivity.class) {
//            System.exit(0);
       // }
    }

    public void showSNSNFeaturesNotWorkingDialog(String snsnIncompatibilityReason){
        if(snsnIncompatibilityReason.equals(getResources().getString(R.string.compat_no_baseband_messages_in_active_test))){
            showDialogWarningNoBasebandMessages();
        }
        else {
//            showDeviceIncompatibleDialog(snsnIncompatibilityReason);
        }
    }

    public void showDialogWarningNoBasebandMessages(){
        MsdDialog.makeConfirmationDialog(this, getResources().getString(R.string.compat_no_baseband_messages_warning),
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing here
                    }
                },
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        quitApplication();
                    }
                },
                null,
                getResources().getString(R.string.warning_button_proceed_anyway),
                getResources().getString(R.string.warning_button_quit),
                false
        ).show();
    }

    public void showDeviceIncompatibleDialog(String incompatibilityReason) {
        Utils.showDeviceIncompatibleDialog(this, incompatibilityReason+"\n"+this.getResources().getString(R.string.compat_snsn_features_not_working), new Runnable() {
            @Override
            public void run() {
                //do nothing here
            }
        });
    }


    /**
     * set drower to configure drower items
     */
    public void setDrawer(Context context, Toolbar toolbar) {
        navigationView = (NavigationView) ((Activity) context).findViewById(R.id.nav_view);
        drawerLayout = (DrawerLayout) ((Activity) context).findViewById(R.id.drawer_layout);

        View header = navigationView.getHeaderView(0);
        TextView sensorId = (TextView) header.findViewById(R.id.drawer_sensor_id);
        sensorId.setText(getResources().getText(R.string.actionBar_subTitle) + " " +setAppId(this));

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {

                    case R.id.dashboard:
                        Intent intent5 = new Intent(BaseActivity.this, DashboardActivity.class);
                        startActivity(intent5);
                        finish();
                        break;
                    case R.id.exit:
                        quitApplication();
                        break;

                    case R.id.detection:
                        Intent intent1 = new Intent(BaseActivity.this, EditConfigActivity.class);
                        startActivity(intent1);
                        finish();
                        break;

                    case R.id.manage_table:
                        Intent intent2 = new Intent(BaseActivity.this, ListOfTables2.class);
                        startActivity(intent2);
                        finish();
                        break;

                    case android.R.id.home:
                        NavUtils.navigateUpFromSameTask(BaseActivity.this);
                        break;
                    default:
                        break;
                }

                return false;
            }
        });
        actionBarDrawerToggle = new  ActionBarDrawerToggle((Activity) BaseActivity.this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);


    }

}
