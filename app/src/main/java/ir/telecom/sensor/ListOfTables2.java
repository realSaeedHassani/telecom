package ir.telecom.sensor;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.tableview.TableViewActivity;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdDatabaseManager;

public class ListOfTables2 extends BaseActivity {

    private Double longitude, latitude;
    private GpsTracker gpsTracker;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private MsdSQLiteOpenHelper msoh;
    private SQLiteDatabase db;
    private AVLoadingIndicatorView avl;
    private LinearLayout llTables;
    private LinearLayout llPWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_tables);
        msoh = new MsdSQLiteOpenHelper(this);
        db = MsdDatabaseManager.getInstance().openDatabase();
        avl = findViewById(R.id.avi);
        llTables = findViewById(R.id.ll_tables);
        llPWait = findViewById(R.id.ll_pwait);
        setToolbar();
        setDrawer(ListOfTables2.this, toolbar);
        if (checkRootCommand() == 0) {
            llTables.setVisibility(View.GONE);
            llPWait.setVisibility(View.VISIBLE);
            new LoadAsync().execute("Please wait...");
        }
    }

    private class LoadAsync extends AsyncTask<String, Void, Boolean> {
        protected void onPreExecute() {
            avl.show();
        }

        protected Boolean doInBackground(final String... args) {
            return convertTable();
        }

        protected void onPostExecute(final Boolean unused) {

            if (avl.isShown()) {
                avl.hide();
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                llTables.setVisibility(View.VISIBLE);
                llPWait.setVisibility(View.GONE);
            }

        }
    }


    public String convertDate(String timeStamp) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        return destFormat.format(parsed);
    }

    private boolean convertTable() {

        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        db.execSQL("DROP TABLE IF EXISTS cell_info_convert_time");
        db.execSQL("CREATE TABLE cell_info_convert_time  AS SELECT  *  FROM cell_info");
        Cursor cursor = db.rawQuery("SELECT id, first_seen, last_seen FROM cell_info_convert_time", null);

        List<String> lstID = new ArrayList<>();
        List<String> lstFirstSeen = new ArrayList<>();
        List<String> lstLastSeen = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                lstID.add(cursor.getString(cursor.getColumnIndex("id")));
                lstFirstSeen.add(cursor.getString(cursor.getColumnIndex("first_seen")));
                lstLastSeen.add(cursor.getString(cursor.getColumnIndex("last_seen")));
                cursor.moveToNext();
            }
        }

        for (int index = 0; index < lstFirstSeen.size(); ++index) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("first_seen", convertDate(lstFirstSeen.get(index)));
            contentValues.put("last_seen", convertDate(lstLastSeen.get(index)));
            db.update("cell_info_convert_time", contentValues, "id = " + lstID.get(index), null);
        }
        try {
            // Update session_info
            List<Integer> lstFlag;
            cursor = db.rawQuery("select timestamp, flag from session_info", null);
            List<String> lstTimestamp = new ArrayList<>();
            lstFlag = new ArrayList<>();
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    lstTimestamp.add(cursor.getString(cursor.getColumnIndex("timestamp")));
                    lstFlag.add(cursor.getInt(cursor.getColumnIndex("flag")));
                    cursor.moveToNext();
                }
            }

            for (int index = 0; index < lstTimestamp.size(); ++index) {
                if (lstFlag.size() > 0) {
                    if (lstFlag.get(index) == 0) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("timestamp", convertDate(lstTimestamp.get(index)));
                        contentValues.put("flag", 1);
                        db.update("session_info", contentValues, "flag = " + 0, null);
                    }
                }
            }
        } catch (Exception ex) {

        }
        return true;
    }

    public int checkRootCommand() {

        Process exec = null;
        try {

            exec = Runtime.getRuntime().exec(new String[]{"su"});

            final OutputStreamWriter out = new OutputStreamWriter(exec.getOutputStream());
            out.write("exit");
            out.flush();

            Log.i(">> ROOT: ", "su command executed successfully");
            return 0; // returns zero when the command is executed successfully
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (exec != null) {
                try {
                    exec.destroy();
                } catch (Exception ignored) {
                }
            }
        }
        return 1; //returns one when the command execution fails
    }

    public void onClick(View view) throws IOException {


        //save in excel

        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
        File file = new File(directory_path);
        if (!file.exists()) {
            file.mkdirs();
        }

        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), "msd.db", directory_path);

        //******


        String btnNaame = "";
        Intent intent = new Intent(this, TableViewActivity.class);
        switch (view.getId()) {
            case R.id.cell_info_btn: {
                btnNaame = "cell_info";
                if (getCountTable(btnNaame)) {
                    if (checkRootCommand() == 0)
                        intent.putExtra("info", "cell_info_convert_time");
                    else
                        intent.putExtra("info", "cell_info");
//                    intent.putExtra("info", "cell_info");
                    intent.putExtra("timeColNumber", "1");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.session_info_btn: {
                btnNaame = "session_info";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "session_info");
                    intent.putExtra("timeColNumber", "1");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.config_btn: {
                btnNaame = "config";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "config");
                    intent.putExtra("timeColNumber", "");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.catcher_btn: {
                btnNaame = "catcher_with_signal_power";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "catcher_with_signal_power");
                    intent.putExtra("timeColNumber", "5");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data").show();

                break;
            }
            case R.id.sensor_status_btn: {


                btnNaame = "sensor_status";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "sensor_status");
                    intent.putExtra("timeColNumber", "2");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.arfcn_list_btn: {


                btnNaame = "arfcn_list";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "arfcn_list");
                    intent.putExtra("timeColNumber", "");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.sensor_info_btn: {


                btnNaame = "sensor_info";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "sensor_info");
                    intent.putExtra("timeColNumber", "");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.neighbor_info_btn: {


                btnNaame = "neighbor_info";
                if (getCountTable(btnNaame)) {
                    intent.putExtra("info", "neighbor_info");
                    intent.putExtra("timeColNumber", "");
                    startActivity(intent);
                } else
                    Toasty.warning(this, "There is no data", Toast.LENGTH_LONG).show();
                break;
            }


            default:
                break;
        }

        //******save in excel


        sqliteToExcel.exportSingleTable(btnNaame, btnNaame + ".xls", new SQLiteToExcel.ExportListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onCompleted(String filePath) {
                //  Toast.makeText(ListOfTables.this,"Done! save table",Toast.LENGTH_LONG).show();
//                Log.d("list of table","Done! save table");
            }

            @Override
            public void onError(Exception e) {
                // Toast.makeText(ListOfTables.this,"Error!  save table",Toast.LENGTH_LONG).show();
//                Log.d("list of table","Error!  save table");
            }
        });
        //*********************************
//        getSupportFragmentManager().beginTransaction().add(R.id.activity_container, new
//                MainFragment(intent), MainFragment.class.getSimpleName()).commit();
    }


    boolean getCountTable(String table) {

        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + table;//" ORDER BY timestamp DESC LIMIT 1;";
        @SuppressLint("Recycle")
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            return true;
        }

        return false;
    }
//    public void getLocation(){
//        latitude = ImsiCatcher.getLatitude();
//        longitude = ImsiCatcher.getLongitude();
//    }

    public void getLocation() {
        gpsTracker = new GpsTracker(ListOfTables2.this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);

        toolbarDone.setText("Clear All Tables");
        toolbarTittle.setText("Tables");
        final String tableName[] = {"catcher", "cell_info", "sensor_status", "session_info", "si_loc",
                "serving_cell_info", "neighboring_cell_info", "arfcn_list", "arfcn_list", "location_info"
                , "paging_info", "rand_check", "sid_appid", "sqlite_sequence", "sensor_info", "neighbor_info", "myr1", "mya5", "myr1", "mycells_with_neig_arfcn", "mycells_with_neig_cell", "mycell_neig_count", "mylonesome_lacs"};

        final String tableNameRoot[] = {"catcher", "cell_info_convert_time", "sensor_status", "session_info", "si_loc",
                "serving_cell_info", "neighboring_cell_info", "arfcn_list", "arfcn_list", "location_info"
                , "paging_info", "rand_check", "sid_appid", "sqlite_sequence", "sensor_info", "neighbor_info", "myr1", "mya5", "myr1", "mycells_with_neig_arfcn", "mycells_with_neig_cell", "mycell_neig_count", "mylonesome_lacs"};


        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final String tableName[] = {"catcher", "cell_info", "sensor_status", "session_info", "si_loc",

                final String views[] = {"loneSome_lacs", "max_cipher", "best_cipher"
                        , "sessions_ciphered_perc", "si", "cells_with_neig_arfcn", "cells_with_neig_cell", "cell_neig_count", "cell_ref_by_neig_cell"
                        , "cell_ref_by_neig_count", "ci", "a2", "a4", "a5", "c1", "c2", "c4", "c5", "k1", "k2", "r1", "r2", "t1", "t3", "t4"};

                final long rowId[] = new long[48];
                for (int i = 0; i < 48; i++) {
                    rowId[i] = -1;
                }

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListOfTables2.this);
                alertDialogBuilder
                        .setMessage("Do you want to clear all tables?")
                        .setCancelable(false)
                        .setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        boolean flag = false;
                                        if (checkRootCommand() == 1) {
                                            for (int i = 0; i < tableName.length; i++) {
                                                rowId[i] = msoh.deleteTableRows(db, tableName[i]);
                                            }

                                            for (int j = 0; j < views.length; j++) {
                                                rowId[j + tableName.length] = msoh.dropView(db, views[j]);
                                            }
                                            for (int i = 0; i < rowId.length; i++) {
                                                if (rowId[i] == -1) {
                                                    flag = true;
                                                }
                                            }
                                        }
                                        // Execute for Root permission...
                                        else {
                                            for (int i = 0; i < tableNameRoot.length; i++) {
                                                rowId[i] = msoh.deleteTableRows(db, tableNameRoot[i]);
                                            }
                                            for (int j = 0; j < views.length; j++) {
                                                rowId[j + tableNameRoot.length] = msoh.dropView(db, views[j]);
                                            }
                                            for (int i = 0; i < rowId.length; i++) {
                                                if (rowId[i] == -1) {
                                                    flag = true;
                                                }
                                            }
                                        }
                                        if (!flag) {
                                            Toast.makeText(ListOfTables2.this, "All Tables Cleared", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });

    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ListOfTables2.this, DashboardActivity.class);
        startActivity(i);
        finish();

    }

}
