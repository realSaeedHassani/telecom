package ir.telecom.sensor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MSDServiceHelperCreator;
import ir.telecom.sensor.util.MsdDatabaseManager;
import ir.telecom.sensor.util.MsdLog;
import ir.telecom.sensor.util.PermissionChecker;


//pari
import android.telephony.CellInfo;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellIdentityLte;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoWcdma;

import java.util.List;

import android.telephony.*;
import android.os.*;
import android.widget.Toast;
//
import androidx.core.app.ActivityCompat;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by Pasargad-Pc on 10/14/2018.
 */

public class SensorInfoAlarmReceiver extends BroadcastReceiver {

    private static TelephonyManager tm;
    private static final String TAG = "SensorInfoAlmRcvr";
    Double longitude = 0.0;
    Double latitude = 0.0;
    GpsTracker gpsTracker;
    protected MSDServiceHelperCreator msdServiceHelperCreator;
    private Context context;
    SharedPreferences sharedPrefs;
    static int mcc = 0;
    static int mnc = 0;
    static int lac = 0;
    static int cid = 0;
    static int signalstrength = 0;
    static int sizeneighboring = 0;
    static int arfcn = 0;
    static int psc = 0;
    static int sid = 0;
    public List<CellInfo> cellInfoList;
    public List<NeighboringCellInfo> cellInfoList2;
    public int cellId = 0;
    public int rat = 0;
    SQLiteDatabase db;
    int neighbor = 0;

//    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//    final String myDate = sdf.format(new Date());


    /**
     * on Receive method
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        getLocation(context);

        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(context);
        db = MsdDatabaseManager.getInstance().openDatabase();


//        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//        final String myDate = sdf.format(new Date());


        final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf2.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        final String myDate = sdf2.format(new Date());

        Log.e(">> SAR: ", myDate);


        SharedPreferences sharedPrefs1 = context.getSharedPreferences("frist_seen_session_info", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        int seen_session = sharedPrefs1.getInt("frist_seen_session_info", 0);
         if (!isAirplaneModeOn(context)) {
            try
            {
                tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            loadCellInfo(tm);

            if (issamecellinfo()) {
                //update cell info

                msoh.updateCell(db, myDate);

            } else {
                msoh.insertCell(db, myDate, mcc, mnc, lac, cid, rat, arfcn, longitude, latitude, signalstrength, sizeneighboring);
                Cursor cursor = db.rawQuery("select id from cell_info ", null);
                if (cursor != null && cursor.moveToFirst()) {
                    cursor.moveToLast();
                    SharedPreferences sharedPrefs = context.getSharedPreferences("cellidarfcn", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putInt("cellidarfcn", cursor.getInt(0));
                    editor.commit();
                }
//
            }

//            msoh.insertSensorinfo(db, myDate2, longitude, latitude, mcc, mnc, lac, cid, signalstrength, sizeneighboring, arfcn, rat);
            if (seen_session == 0) {
                SharedPreferences sharedPrefs = context.getSharedPreferences("frist_seen_session_info", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putInt("frist_seen_session_info", 1);
                editor.commit();
                msoh.insertSession(db, myDate, mcc, mnc, lac, cid, sizeneighboring, arfcn, psc, rat, signalstrength, longitude, latitude, sizeneighboring);
            } else {
                if (!issamesessioninfo(db, mnc, mcc, lac, cid)) {
//            if (true) {
                    msoh.insertSession(db, myDate, mcc, mnc, lac, cid, sizeneighboring, arfcn, psc, rat, signalstrength, longitude, latitude, sizeneighboring);
                }
//           if(issamesessioninfo(db, mnc, mcc, lac, cid, arfcn))
//           {
//               msoh.updatesession();
//           }
//           else{
//               msoh.insertSession(db, myDate, mcc, mnc, lac, cid, sizeneighboring, arfcn, psc, rat, signalstrength, longitude, latitude,sizeneighboring);
//           }
            }

            loadneighCellInfo(tm);
        }catch(Exception ex){
            Log.e("Exception: ", ex.getMessage());}
//            loadCellInfo(tm);
//        loadCellInfo2(tm);
        } else {
            Toasty.error(context, "Your sim card is offline. Please turn on your sim card", Toast.LENGTH_LONG).show();
        }


//        loadneighCellInfo(tm);

//
    }


    public void getLocation(Context context) {
        gpsTracker = new GpsTracker(context);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    public boolean issamecellinfo() {

        Cursor cursor = db.rawQuery("select mcc,mnc,lac,cid,id from cell_info where mnc='" + mnc + "' and mcc='" + mcc + " ' and lac='" + lac + "' and cid='" + cid + "'", null);
        if (cursor != null && cursor.moveToFirst()) {
//            cursor.moveToLast();
//            if (mcc== cursor.getInt(0) && mnc== cursor.getInt(1) && lac== cursor.getInt(2) &&cid== cursor.getInt(3)  ) {
            SharedPreferences sharedPrefs = context.getSharedPreferences("cellid", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putInt("cellid", cursor.getInt(4));
            editor.commit();
            return true;
//            }
        } else
            return false;
    }


    public boolean isTableExists(Context context, String tableName, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                // Toast.makeText(context,"exist",Toast.LENGTH_SHORT).show();
                return true;
            }
            cursor.close();
        }
        //Toast.makeText(context,"not  exist",Toast.LENGTH_SHORT).show();
        return false;
    }

    public void loadCellInfo(TelephonyManager tm) {
        int lCurrentApiVersion = android.os.Build.VERSION.SDK_INT;

        try {
//            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            } else {
                cellInfoList = tm.getAllCellInfo();
            }
            if (tm.getAllCellInfo().size() > 0) {
                CellInfo info = tm.getAllCellInfo().get(0);
                sizeneighboring = cellInfoList.size();
                rat = networkTypeToNetworkGeneration(tm.getNetworkType());
                if (cellInfoList != null && cellInfoList.size() > 0) {
                    if (info instanceof CellInfoGsm) {
                        final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                        final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
                        if (android.os.Build.VERSION.SDK_INT >= 24) {
                            // Do something for nougat and above versions
                            arfcn = identityGsm.getArfcn();
                        } else {
                            // do something for phones running an SDK before lollipop
                            arfcn = 0;
                        }
//                    arfcn=identityGsm.getArfcn();
                        // Signal Strength
                        signalstrength = gsm.getDbm(); // [dBm]
                        // Cell Identity
//                        mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
                        cid = identityGsm.getCid();
                        mcc = identityGsm.getMcc();
                        mnc = identityGsm.getMnc();
                        lac = identityGsm.getLac();


                    } else if (info instanceof CellInfoCdma) {
                        final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                        final CellIdentityCdma identityCdma = ((CellInfoCdma) info).getCellIdentity();
                        //arfcn
//                    arfcn=identityCdma.get();
                        // Signal Strength
                        signalstrength = cdma.getDbm();
                        // Cell Identity
                        cid = identityCdma.getBasestationId();
                        mnc = identityCdma.getSystemId();
                        lac = identityCdma.getNetworkId();
                        sid = identityCdma.getSystemId();

                    } else if (info instanceof CellInfoLte) {
                        final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                        final CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
                        if (android.os.Build.VERSION.SDK_INT >= 24) {
                            // Do something for nougat and above versions
                            arfcn = identityLte.getEarfcn();
                        } else {
                            // do something for phones running an SDK before lollipop
                            arfcn = 0;
                        }
                        // Signal Strength
                        signalstrength = lte.getDbm();
                        // Cell Identity
                        mcc = identityLte.getMcc();
                        mnc = identityLte.getMnc();
                        cid = identityLte.getCi();

                    } else if (lCurrentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2 && info instanceof CellInfoWcdma) {
                        final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
                        final CellIdentityWcdma identityWcdma = ((CellInfoWcdma) info).getCellIdentity();
                        //ardcn
                        if (android.os.Build.VERSION.SDK_INT >= 24) {
                            // Do something for nougat and above versions
                            arfcn = identityWcdma.getUarfcn();
                        } else {
                            // do something for phones running an SDK before lollipop
                            arfcn = 0;
                        }

                        // Signal Strength
                        signalstrength = wcdma.getDbm();
                        // Cell Identity
                        lac = identityWcdma.getLac();
                        mcc = identityWcdma.getMcc();
                        mnc = identityWcdma.getMnc();
                        cid = identityWcdma.getCid();
                        psc = identityWcdma.getPsc();


                    } else {
//                    Log.i(TAG, "sorry");
                    }
                }
            }
        } catch (NullPointerException npe) {
//            Log.e(TAG, "sorry2");
        }
    }


    //neigh
    public void loadneighCellInfo(TelephonyManager tm) {

        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(context);
        db = MsdDatabaseManager.getInstance().openDatabase();


        @SuppressLint("SimpleDateFormat")
        final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf2.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        final String myDate = sdf2.format(new Date());


//        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//        final String myDate = sdf.format(new Date());


        int lCurrentApiVersion = android.os.Build.VERSION.SDK_INT;
        try {
//            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((EditConfigActivity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            } else {
                cellInfoList = tm.getAllCellInfo();
//                cellInfoList2 = tm.getNeighboringCellInfo();
            }
            if (cellInfoList != null & cellInfoList.size() > 1) {
                for (int i = 1; i < cellInfoList.size(); i++) {
                    CellInfo info = tm.getAllCellInfo().get(i);
//                    for (final CellInfo info : cellInfoList) {
//
                    if (info instanceof CellInfoGsm) {
//                        final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
//                        final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
//                        // Signal Strength
//                        signalstrength = gsm.getDbm(); // [dBm]
//                        // Cell Identity
////                        mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
//                        cid = identityGsm.getCid();
//                        mcc = identityGsm.getMcc();
//                        mnc = identityGsm.getMnc();
//                        lac = identityGsm.getLac();


                        final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                        final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
                        if (android.os.Build.VERSION.SDK_INT >= 24) {
                            // Do something for nougat and above versions
                            arfcn = identityGsm.getArfcn();
                        } else {
                            // do something for phones running an SDK before lollipop
                            arfcn = 0;
                        }
//                    arfcn=identityGsm.getArfcn();
                        // Signal Strength
                        signalstrength = gsm.getDbm(); // [dBm]
                        // Cell Identity
//                        mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
                        cid = identityGsm.getCid();
                        mcc = identityGsm.getMcc();
                        mnc = identityGsm.getMnc();
                        lac = identityGsm.getLac();

                    } else if (info instanceof CellInfoCdma) {
                        final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                        final CellIdentityCdma identityCdma = ((CellInfoCdma) info).getCellIdentity();
                        // Signal Strength
                        signalstrength = cdma.getDbm();
                        // Cell Identity
                        cid = identityCdma.getBasestationId();
                        mnc = identityCdma.getSystemId();
                        lac = identityCdma.getNetworkId();
                        sid = identityCdma.getSystemId();

                    } else if (lCurrentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2 && info instanceof CellInfoWcdma) {
                        final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
                        final CellIdentityWcdma identityWcdma = ((CellInfoWcdma) info).getCellIdentity();
                        //ardcn
//                            arfcn=identityWcdma.getUarfcn();
                        // Signal Strength
                        signalstrength = wcdma.getDbm();
                        // Cell Identity
                        lac = identityWcdma.getLac();
                        mcc = identityWcdma.getMcc();
                        mnc = identityWcdma.getMnc();
                        cid = identityWcdma.getCid();
                        psc = identityWcdma.getPsc();


                    } else if (info instanceof CellInfoLte) {
                        final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                        final CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
                        // Signal Strength
                        signalstrength = lte.getDbm();
                        // Cell Identity
                        mcc = identityLte.getMcc();
                        mnc = identityLte.getMnc();
                        cid = identityLte.getCi();

                    } else {
//                        Log.i(TAG, "sorry");
                    }

                    if (mcc == 0 && mnc == 0 && lac == 0) {
//                        msoh.insertNeighborinfo(db, myDate2, mcc, mnc, lac, cid, signalstrength, arfcn, cellId);
//                    msoh.insertCell(db, myDate, mcc, mnc, lac, cid, rat, arfcn, longitude, latitude, signalstrength);
                    } else {
//                        msoh.insertNeighborinfo(db, myDate2, mcc, mnc, lac, cid, signalstrength, arfcn, cellId);
                        if (issamecellinfo()) {
                            //update cell info
                            msoh.updateCell(db, myDate);
                        } else {
                            msoh.insertCell(db, myDate, mcc, mnc, lac, cid, rat, arfcn, longitude, latitude, signalstrength, sizeneighboring);
                            if (arfcn != 0) {
                                SharedPreferences sharedPrefs1 = context.getSharedPreferences("cellidarfcn", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
                                int arfcnid = sharedPrefs1.getInt("cellidarfcn", 0);
                                msoh.insertarfcn_list(db, arfcnid, arfcn);
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException npe) {
//            Log.e(TAG, "sorry2");
        }
    }


    public boolean issamesessioninfo(SQLiteDatabase db, int mnc, int mcc, int lac, int cid) {
        Cursor cursor = db.rawQuery("select mcc,mnc,lac,cid from session_info ", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            if (cursor.getInt(0) == mcc && cursor.getInt(1) == mnc && cursor.getInt(2) == lac && cursor.getInt(3) == cid) {
                return true;
            } else {
                return false;
            }
        } else
            return false;
    }
//public void estimate(){
//    Context context = this.getApplicationContext();
//    ActivityManager mgr = (ActivityManager)context.getSystemService(ACTIVITY_SERVICE);
//    List<RunningAppProcessInfo> processes = mgr.getRunningAppProcesses();
//    Log.e("DEBUG", "Running processes:");
//    for(Iterator i = processes.iterator(); i.hasNext(); )
//    {
//        RunningAppProcessInfo p = (RunningAppProcessInfo)i.next();
//        Log.e("DEBUG", "  process name: "+p.processName);
//        Log.e("DEBUG", "     pid: "+p.pid);
//        int[] pids = new int[1];
//        pids[0] = p.pid;
//        android.os.Debug.MemoryInfo[] MI = mgr.getProcessMemoryInfo(pids);
//        Log.e("memory","     dalvik private: " + MI[0].dalvikPrivateDirty);
//        Log.e("memory","     dalvik shared: " + MI[0].dalvikSharedDirty);
//        Log.e("memory","     dalvik pss: " + MI[0].dalvikPss);
//        Log.e("memory","     native private: " + MI[0].nativePrivateDirty);
//        Log.e("memory","     native shared: " + MI[0].nativeSharedDirty);
//        Log.e("memory","     native pss: " + MI[0].nativePss);
//        Log.e("memory","     other private: " + MI[0].otherPrivateDirty);
//        Log.e("memory","     other shared: " + MI[0].otherSharedDirty);
//        Log.e("memory","     other pss: " + MI[0].otherPss);
//
//        Log.e("memory","     total private dirty memory (KB): " + MI[0].getTotalPrivateDirty());
//        Log.e("memory","     total shared (KB): " + MI[0].getTotalSharedDirty());
//        Log.e("memory","     total pss: " + MI[0].getTotalPss());
//    }
//}

    public boolean isSimAvailable() {
        boolean isAvailable = false;
//        TelephonyManager telMgr = (TelephonyManager) Context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = tm.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT: //SimState = “No Sim Found!”;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = “Network Locked!”;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = “PIN Required to access SIM!”;
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = “PUK Required to access SIM!”; // Personal Unblocking Code
                break;
            case TelephonyManager.SIM_STATE_READY:
                isAvailable = true;
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = “Unknown SIM State!”;
                break;
        }
        return isAvailable;
    }


    private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;

    }

    public static int networkTypeToNetworkGeneration(int networkType) {
        if (networkType == 0)
            return 0;
        else if (  // networkType == TelephonyManager.NETWORK_TYPE_1xRTT // Moved to 3G !!
                networkType == TelephonyManager.NETWORK_TYPE_CDMA     // CDMA: Either IS95A or IS95B
                        || networkType == TelephonyManager.NETWORK_TYPE_EDGE
                        || networkType == TelephonyManager.NETWORK_TYPE_GPRS
                        || networkType == TelephonyManager.NETWORK_TYPE_GSM      // API >24
                        || networkType == TelephonyManager.NETWORK_TYPE_IDEN)
            return 2;
        else if (networkType == TelephonyManager.NETWORK_TYPE_1xRTT    // This is labelled by Google as 2G, but is really a 3G technology
                || networkType == TelephonyManager.NETWORK_TYPE_EHRPD    // CDMA: eHRPD
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_0
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_A
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_B
                || networkType == TelephonyManager.NETWORK_TYPE_HSDPA
                || networkType == TelephonyManager.NETWORK_TYPE_HSPA
                || networkType == TelephonyManager.NETWORK_TYPE_HSPAP    // API >12
                || networkType == TelephonyManager.NETWORK_TYPE_HSUPA
                || networkType == TelephonyManager.NETWORK_TYPE_TD_SCDMA // API >24
                || networkType == TelephonyManager.NETWORK_TYPE_UMTS)
            return 3;
        else if (networkType == TelephonyManager.NETWORK_TYPE_IWLAN    // API >24
                || networkType == TelephonyManager.NETWORK_TYPE_LTE)     // API >11
            // ToDo: IWLAN may be considered prone to other type of MiTM attack and might
            //          need to be treated differently...
            return 4;
        else {
            return 0;
        }
    }


}