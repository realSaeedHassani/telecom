package ir.telecom.sensor;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;

import ir.telecom.sensor.qdmon.StateChangedReason;
import ir.telecom.sensor.util.TimeSpace;
import ir.telecom.sensor.views.adapter.DetailChartGalleryAdapter;
import ir.telecom.sensor.views.adapter.ListViewImsiCatcherAdapter;

//bahare
public class DetailChartActivity extends BaseActivity {
    // Attributes
    private Spinner spinner;
    private ListView listView;
    private ImageView _imgThreatType;
    private TextView _txtThreatTypeImsiCatcherCount;
    private TextView _txtThreatTypeSilentSmsCount;
    private LinearLayout _llThreatTypeImsiCatcher;
    private LinearLayout _llThreatTypeSms;
    private ViewPager mPager;
    private Toolbar toolbar;
    private int _threatType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart_detail);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        _imgThreatType = (ImageView) findViewById(R.id.imgDetailChartThreatType);
        _txtThreatTypeImsiCatcherCount = (TextView) findViewById(R.id.txtDetailChartThreatTypeImsiCatcherCount);
        _txtThreatTypeSilentSmsCount = (TextView) findViewById(R.id.txtDetailChartThreatTypeSilentSmsCount);
        _llThreatTypeImsiCatcher = (LinearLayout) findViewById(R.id.llThreatTypeImsiCatcher);
        _llThreatTypeSms = (LinearLayout) findViewById(R.id.llThreatTypeSms);
        _threatType = getIntent().getIntExtra("ThreatType", R.id.IMSICatcherCharts);
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.vpDetailCharts);
        DetailChartGalleryAdapter mPagerAdapter = new DetailChartGalleryAdapter(getSupportFragmentManager(), this,
                getIntent().getIntExtra("ThreatType", R.id.IMSICatcherCharts));
        mPager.setAdapter(mPagerAdapter);

        setThreatTypeImageText();

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                spinner.setSelection(0);
                fillList(_threatType, position);
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
        });

        spinner = findViewById(R.id.spnDetailChart);
        listView = findViewById(R.id.lstDetailChart);
        setToolbar();

        configureSpinner(getIntent().getIntExtra("ThreatType", R.id.IMSICatcherCharts));
        fillList(getIntent().getIntExtra("ThreatType", R.id.IMSICatcherCharts), mPager.getCurrentItem());
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Start pager adapter with hour fragment
        this.mPager.setCurrentItem(3);

        resetListView();
    }

    private void configureSpinner(int id) {
//        if (id == R.id.SilentSMSCharts) {
//            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
//                    android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.event_types));
//            spinner.setAdapter(spinnerAdapter);
//        }
    }

    private void fillList(int id, int position) {
        long _startTime;
        long _endTime;

        switch (position) {
            case 3:
                _startTime = TimeSpace.Times.Hour.getStartTime();
                _endTime = TimeSpace.Times.Hour.getEndTime();
                break;
            case 2:
                _startTime = TimeSpace.Times.Day.getStartTime();
                _endTime = TimeSpace.Times.Day.getEndTime();
                break;
            case 1:
                _startTime = TimeSpace.Times.Week.getStartTime();
                _endTime = TimeSpace.Times.Week.getEndTime();
                break;
            case 0:
                _startTime = TimeSpace.Times.Month.getStartTime();
                _endTime = TimeSpace.Times.Month.getEndTime();
                break;
            default:
                _startTime = 0;
                _endTime = 0;
                break;
        }

        ListViewImsiCatcherAdapter listViewAdapter = new ListViewImsiCatcherAdapter(this,
                getMsdServiceHelperCreator().getMsdServiceHelper().getData().getImsiCatchers(_startTime, _endTime));
        listView.setAdapter(listViewAdapter);

        if (_txtThreatTypeImsiCatcherCount != null) {
            _txtThreatTypeImsiCatcherCount.setText(String.valueOf(getMsdServiceHelperCreator().
                    getMsdServiceHelper().getData().getImsiCatchers(_startTime, _endTime).size()));
        }
    }

    public int getThreatType() {
        return _threatType;
    }

    private void setThreatTypeImageText() {
        if (_threatType == R.id.IMSICatcherCharts) {
            _imgThreatType.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_content_imsi_event, null));
            _llThreatTypeImsiCatcher.setVisibility(View.VISIBLE);
            _llThreatTypeSms.setVisibility(View.GONE);
        }
    }

    @Override
    public void stateChanged(StateChangedReason reason) {
        super.stateChanged(reason);

        if (reason.equals(StateChangedReason.CATCHER_DETECTED) || reason.equals(StateChangedReason.SMS_DETECTED)) {
            resetListView();

        }
    }

    @Override
    public void refreshView() {
        mPager.getAdapter().notifyDataSetChanged();
    }

    private void resetListView() {
        fillList(_threatType, mPager.getCurrentItem());
        mPager.getAdapter().notifyDataSetChanged();
    }

    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);

        toolbarDone.setVisibility(View.INVISIBLE);
        toolbarTittle.setText("Detail Chart");

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed()
    {
        finish();
    }
}
