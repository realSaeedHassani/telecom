package ir.telecom.sensor;

public class EncryptedFileWriterError extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Throwable e = null;

    public EncryptedFileWriterError(String message, Throwable e) {
        super(message);
        this.e = e;
    }


    public EncryptedFileWriterError(String message) {
        super(message);
    }

    public Throwable getE() {
        return e;
    }

}