package ir.telecom.sensor.model;

import java.io.Serializable;

/**
 * Created by Bahar on 9/3/2018.
 */

public class CellInfo implements Serializable{

    private Long id;
    private String firstSeen;
    private String lastSeen;
    private short mcc;
    private short mnc;
    private int lac;
    private int cid;
    private short rat;
    private int bcch_arfcn;
    private short c1;
    private short c2;
    private int power_sum;
    private int power_count;
    private float gps_lon;
    private float gps_lat;
    private short msc_ver;
    private short combined;
    private short agch_blocks;
    private short pag_mframes;
    private short t3212;
    private short dtx;
    private short cro;
    private short temp_offset;
    private short pen_time;
    private short pwr_offset;
    private short gprs;
    private short ba_len;
    private short neigh_2;
    private short neigh_2b;
    private short neigh_2t;
    private short neigh_2q;
    private short neigh_5;
    private short neigh_5b;
    private short neigh_5t;
    private int count_si1;
    private int count_si2;
    private int count_si2b;
    private int count_si2t;
    private int count_si2q;
    private int count_si3;
    private int count_si4;
    private int count_si5;
    private int count_si5b;
    private int count_si5t;
    private int count_si6;
    private int count_si13;
    private String si1;
    private String si2;
    private String si2b;
    private String si2t;
    private String si2q;
    private String si3;
    private String si4;
    private String si5;
    private String si5b;
    private String si5t;
    private String si6;
    private String si13;

    public CellInfo(String firstSeen, String lastSeen, short mcc, short mnc, int lac, int cid, short rat, int bcch_arfcn, short c1, short c2, int power_sum, int power_count, float gps_lon, float gps_lat, short msc_ver, short combined, short agch_blocks, short pag_mframes, short t3212, short dtx, short cro, short temp_offset, short pen_time, short pwr_offset, short gprs, short ba_len, short neigh_2, short neigh_2b, short neigh_2t, short neigh_2q, short neigh_5, short neigh_5b, short neigh_5t, int count_si1, int count_si2, int count_si2b, int count_si2t, int count_si2q, int count_si3, int count_si4, int count_si5, int count_si5b, int count_si5t, int count_si6, int count_si13, String si1, String si2, String si2b, String si2t, String si2q, String si3, String si4, String si5, String si5b, String si5t, String si6, String si13) {
        this.firstSeen = firstSeen;
        this.lastSeen = lastSeen;
        this.mcc = mcc;
        this.mnc = mnc;
        this.lac = lac;
        this.cid = cid;
        this.rat = rat;
        this.bcch_arfcn = bcch_arfcn;
        this.c1 = c1;
        this.c2 = c2;
        this.power_sum = power_sum;
        this.power_count = power_count;
        this.gps_lon = gps_lon;
        this.gps_lat = gps_lat;
        this.msc_ver = msc_ver;
        this.combined = combined;
        this.agch_blocks = agch_blocks;
        this.pag_mframes = pag_mframes;
        this.t3212 = t3212;
        this.dtx = dtx;
        this.cro = cro;
        this.temp_offset = temp_offset;
        this.pen_time = pen_time;
        this.pwr_offset = pwr_offset;
        this.gprs = gprs;
        this.ba_len = ba_len;
        this.neigh_2 = neigh_2;
        this.neigh_2b = neigh_2b;
        this.neigh_2t = neigh_2t;
        this.neigh_2q = neigh_2q;
        this.neigh_5 = neigh_5;
        this.neigh_5b = neigh_5b;
        this.neigh_5t = neigh_5t;
        this.count_si1 = count_si1;
        this.count_si2 = count_si2;
        this.count_si2b = count_si2b;
        this.count_si2t = count_si2t;
        this.count_si2q = count_si2q;
        this.count_si3 = count_si3;
        this.count_si4 = count_si4;
        this.count_si5 = count_si5;
        this.count_si5b = count_si5b;
        this.count_si5t = count_si5t;
        this.count_si6 = count_si6;
        this.count_si13 = count_si13;
        this.si1 = si1;
        this.si2 = si2;
        this.si2b = si2b;
        this.si2t = si2t;
        this.si2q = si2q;
        this.si3 = si3;
        this.si4 = si4;
        this.si5 = si5;
        this.si5b = si5b;
        this.si5t = si5t;
        this.si6 = si6;
        this.si13 = si13;
    }

    public String getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(String firstSeen) {
        this.firstSeen = firstSeen;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public short getMcc() {
        return mcc;
    }

    public void setMcc(short mcc) {
        this.mcc = mcc;
    }

    public short getMnc() {
        return mnc;
    }

    public void setMnc(short mnc) {
        this.mnc = mnc;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public short getRat() {
        return rat;
    }

    public void setRat(short rat) {
        this.rat = rat;
    }

    public int getBcch_arfcn() {
        return bcch_arfcn;
    }

    public void setBcch_arfcn(int bcch_arfcn) {
        this.bcch_arfcn = bcch_arfcn;
    }

    public short getC1() {
        return c1;
    }

    public void setC1(short c1) {
        this.c1 = c1;
    }

    public short getC2() {
        return c2;
    }

    public void setC2(short c2) {
        this.c2 = c2;
    }

    public int getPower_sum() {
        return power_sum;
    }

    public void setPower_sum(int power_sum) {
        this.power_sum = power_sum;
    }

    public int getPower_count() {
        return power_count;
    }

    public void setPower_count(int power_count) {
        this.power_count = power_count;
    }

    public float getGps_lon() {
        return gps_lon;
    }

    public void setGps_lon(float gps_lon) {
        this.gps_lon = gps_lon;
    }

    public float getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(float gps_lat) {
        this.gps_lat = gps_lat;
    }

    public short getMsc_ver() {
        return msc_ver;
    }

    public void setMsc_ver(short msc_ver) {
        this.msc_ver = msc_ver;
    }

    public short getCombined() {
        return combined;
    }

    public void setCombined(short combined) {
        this.combined = combined;
    }

    public short getAgch_blocks() {
        return agch_blocks;
    }

    public void setAgch_blocks(short agch_blocks) {
        this.agch_blocks = agch_blocks;
    }

    public short getPag_mframes() {
        return pag_mframes;
    }

    public void setPag_mframes(short pag_mframes) {
        this.pag_mframes = pag_mframes;
    }

    public short getT3212() {
        return t3212;
    }

    public void setT3212(short t3212) {
        this.t3212 = t3212;
    }

    public short getDtx() {
        return dtx;
    }

    public void setDtx(short dtx) {
        this.dtx = dtx;
    }

    public short getCro() {
        return cro;
    }

    public void setCro(short cro) {
        this.cro = cro;
    }

    public short getTemp_offset() {
        return temp_offset;
    }

    public void setTemp_offset(short temp_offset) {
        this.temp_offset = temp_offset;
    }

    public short getPen_time() {
        return pen_time;
    }

    public void setPen_time(short pen_time) {
        this.pen_time = pen_time;
    }

    public short getPwr_offset() {
        return pwr_offset;
    }

    public void setPwr_offset(short pwr_offset) {
        this.pwr_offset = pwr_offset;
    }

    public short getGprs() {
        return gprs;
    }

    public void setGprs(short gprs) {
        this.gprs = gprs;
    }

    public short getBa_len() {
        return ba_len;
    }

    public void setBa_len(short ba_len) {
        this.ba_len = ba_len;
    }

    public short getNeigh_2() {
        return neigh_2;
    }

    public void setNeigh_2(short neigh_2) {
        this.neigh_2 = neigh_2;
    }

    public short getNeigh_2b() {
        return neigh_2b;
    }

    public void setNeigh_2b(short neigh_2b) {
        this.neigh_2b = neigh_2b;
    }

    public short getNeigh_2t() {
        return neigh_2t;
    }

    public void setNeigh_2t(short neigh_2t) {
        this.neigh_2t = neigh_2t;
    }

    public short getNeigh_2q() {
        return neigh_2q;
    }

    public void setNeigh_2q(short neigh_2q) {
        this.neigh_2q = neigh_2q;
    }

    public short getNeigh_5() {
        return neigh_5;
    }

    public void setNeigh_5(short neigh_5) {
        this.neigh_5 = neigh_5;
    }

    public short getNeigh_5b() {
        return neigh_5b;
    }

    public void setNeigh_5b(short neigh_5b) {
        this.neigh_5b = neigh_5b;
    }

    public short getNeigh_5t() {
        return neigh_5t;
    }

    public void setNeigh_5t(short neigh_5t) {
        this.neigh_5t = neigh_5t;
    }

    public int getCount_si1() {
        return count_si1;
    }

    public void setCount_si1(int count_si1) {
        this.count_si1 = count_si1;
    }

    public int getCount_si2() {
        return count_si2;
    }

    public void setCount_si2(int count_si2) {
        this.count_si2 = count_si2;
    }

    public int getCount_si2b() {
        return count_si2b;
    }

    public void setCount_si2b(int count_si2b) {
        this.count_si2b = count_si2b;
    }

    public int getCount_si2t() {
        return count_si2t;
    }

    public void setCount_si2t(int count_si2t) {
        this.count_si2t = count_si2t;
    }

    public int getCount_si2q() {
        return count_si2q;
    }

    public void setCount_si2q(int count_si2q) {
        this.count_si2q = count_si2q;
    }

    public int getCount_si3() {
        return count_si3;
    }

    public void setCount_si3(int count_si3) {
        this.count_si3 = count_si3;
    }

    public int getCount_si4() {
        return count_si4;
    }

    public void setCount_si4(int count_si4) {
        this.count_si4 = count_si4;
    }

    public int getCount_si5() {
        return count_si5;
    }

    public void setCount_si5(int count_si5) {
        this.count_si5 = count_si5;
    }

    public int getCount_si5b() {
        return count_si5b;
    }

    public void setCount_si5b(int count_si5b) {
        this.count_si5b = count_si5b;
    }

    public int getCount_si5t() {
        return count_si5t;
    }

    public void setCount_si5t(int count_si5t) {
        this.count_si5t = count_si5t;
    }

    public int getCount_si6() {
        return count_si6;
    }

    public void setCount_si6(int count_si6) {
        this.count_si6 = count_si6;
    }

    public int getCount_si13() {
        return count_si13;
    }

    public void setCount_si13(int count_si13) {
        this.count_si13 = count_si13;
    }

    public String getSi1() {
        return si1;
    }

    public void setSi1(String si1) {
        this.si1 = si1;
    }

    public String getSi2() {
        return si2;
    }

    public void setSi2(String si2) {
        this.si2 = si2;
    }

    public String getSi2b() {
        return si2b;
    }

    public void setSi2b(String si2b) {
        this.si2b = si2b;
    }

    public String getSi2t() {
        return si2t;
    }

    public void setSi2t(String si2t) {
        this.si2t = si2t;
    }

    public String getSi2q() {
        return si2q;
    }

    public void setSi2q(String si2q) {
        this.si2q = si2q;
    }

    public String getSi3() {
        return si3;
    }

    public void setSi3(String si3) {
        this.si3 = si3;
    }

    public String getSi4() {
        return si4;
    }

    public void setSi4(String si4) {
        this.si4 = si4;
    }

    public String getSi5() {
        return si5;
    }

    public void setSi5(String si5) {
        this.si5 = si5;
    }

    public String getSi5b() {
        return si5b;
    }

    public void setSi5b(String si5b) {
        this.si5b = si5b;
    }

    public String getSi5t() {
        return si5t;
    }

    public void setSi5t(String si5t) {
        this.si5t = si5t;
    }

    public String getSi6() {
        return si6;
    }

    public void setSi6(String si6) {
        this.si6 = si6;
    }

    public String getSi13() {
        return si13;
    }

    public void setSi13(String si13) {
        this.si13 = si13;
    }


    public String toJson() {
        return "{" +
                "\"firstSeen\"='" + firstSeen + '\'' +
                ", \"lastSeen\"='" + lastSeen +'\'' +
                ", \"mcc\"=" + mcc +
                ", \"mnc\"=" + mnc +
                ", \"mnc\"=" + mnc +
                ", \"lac\"=" + lac +
                ", \"cid\"=" + cid +
                ", \"rat\"=" + rat +
                ", \"bcch_arfcn\"=" + bcch_arfcn +
                ", \"c1\"=" + c1 +
                ", \"c2\"=" + c2 +
                ", \"power_sum\"=" + power_sum +
                ", \"power_count\"=" + power_count +
                ", \"gps_lon\"=" + gps_lon +
                ", \"gps_lat\"=" + gps_lat +
                ", \"msc_ver\"=" + msc_ver +
                ", \"combined\"=" + combined +
                ", \"agch_blocks\"=" + agch_blocks +
                ", \"pag_mframes\"=" + pag_mframes +
                ", \"t3212\"=" + t3212 +
                ", \"dtx\"=" + dtx +
                ", \"cro\"=" + cro +
                ", \"temp_offset\"=" + temp_offset +
                ", \"pen_time\"=" + pen_time +
                ", \"pwr_offset\"=" + pwr_offset +
                ", \"gprs\"=" + gprs +
                ", \"ba_len\"=" + ba_len +
                ", \"neigh_2\"=" + neigh_2 +
                ", \"neigh_2b\"=" + neigh_2b +
                ", \"neigh_2t\"=" + neigh_2t +
                ", \"neigh_2q\"=" + neigh_2q +
                ", \"neigh_5\"=" + neigh_5 +
                ", \"neigh_5b\"=" + neigh_5b +
                ", \"neigh_5t\"=" + neigh_5t +
                ", \"count_si1\"=" + count_si1 +
                ", \"count_si2\"=" + count_si2 +
                ", \"count_si2b\"=" + count_si2b +
                ", \"count_si2t\"=" + count_si2t +
                ", \"count_si2q\"=" + count_si2q +
                ", \"count_si3\"=" + count_si3 +
                ", \"count_si4\"=" + count_si4 +
                ", \"count_si5\"=" + count_si5 +
                ", \"count_si5b\"=" + count_si5b +
                ", \"count_si5t\"=" + count_si5t +
                ", \"count_si6\"=" + count_si6 +
                ", \"count_si13\"=" + count_si13 +
                ", \"si1\"=" + si1 +
                ", \"si2\"=" + si2 +
                ", \"si2b\"=" + si2b +
                ", \"si2t\"=" + si2t +
                ", \"si2q\"=" + si2q +
                ", \"si3\"=" + si3 +
                ", \"si4\"=" + si4 +
                ", \"si5\"=" + si5 +
                ", \"si5b\"=" + si5b +
                ", \"si5t\"=" + si5t +
                ", \"si6\"=" + si6 +
                ", \"si13\"='" + si13 + '\'' +
                '}';
    }


}
