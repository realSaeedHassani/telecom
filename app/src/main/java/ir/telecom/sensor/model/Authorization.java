package ir.telecom.sensor.model;

/**
 * Created by Aasghari on 11/28/2017.
 */
public enum Authorization {
    read_only,read_write,Read_create,Notify

}
