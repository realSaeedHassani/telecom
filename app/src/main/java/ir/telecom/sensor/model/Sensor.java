package ir.telecom.sensor.model;

import java.io.Serializable;

/**
 * Created by Bahar on 9/3/2018.
 */

public class Sensor implements Serializable {


    private String id;
    private String timestamp;
    private int batteryUsage;
    private double longitude;
    private double latitude;
    private boolean serviceStatus;
    private float cpuUsage;
    private float memoryUsage;
    private float diskUsage;


    public Sensor(String timestamp, int battery, double longitude, double latitude, boolean serviceStatus, float cpuUsage
            , float memoryUsage, float diskUsage) {
        this.timestamp = timestamp;
        this.batteryUsage = battery;
        this.longitude = longitude;
        this.latitude = latitude;
        this.serviceStatus = serviceStatus;
        this.cpuUsage = cpuUsage;
        this.memoryUsage = memoryUsage;
        this.diskUsage = diskUsage;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getBatteryUsage() {
        return batteryUsage;
    }

    public void setBatteryUsage(int battery) {
        this.batteryUsage = battery;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public boolean getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(boolean serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public float getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(float cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public double getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(float memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    public double getDiskUsage() {
        return diskUsage;
    }

    public void setDiskUsage(float diskUsage) {
        this.diskUsage = diskUsage;
    }

    public String toJson() {
        return "{" +
                "\"timestamp\"='" + timestamp + '\'' +
                ", \"batteryUsage\"=" + batteryUsage +
                ", \"longitude\"=" + longitude +
                ", \"latitude\"=" + latitude +
                ", \"serviceStatus\"=" + serviceStatus +
                ", \"cpuUsage\"=" + cpuUsage +
                ", \"memoryUsage\"=" + memoryUsage +
                ", \"diskUsage\"=" + diskUsage +
                '}';

    }
}
