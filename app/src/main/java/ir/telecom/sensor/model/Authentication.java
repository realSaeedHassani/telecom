package ir.telecom.sensor.model;

public enum Authentication {
    GSM_A3A8(1),
    UMTS_AKA(2);

    private int value;

    Authentication(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
