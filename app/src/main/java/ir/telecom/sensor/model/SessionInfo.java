package ir.telecom.sensor.model;

import java.io.Serializable;

/**
 * Created by Bahar on 9/3/2018.
 */

public class SessionInfo implements Serializable {


//    private int id;
//    private String timestamp;
//    private int rat;
//    private int domain;
//    private int mcc;
//    private int mnc;
//    private int lac;
//    private int cid;
//    private int arfcn;
//    private int psc;
//    private int cracked;
//    private int neighCount;
//    private int unenc;
//    private int unencRand;
//    private int enc;
//    private int encRand;
//    private int encNull;
//    private int encNullRand;
//    private int encSi;
//    private int encSiRand;
//    private int predict;
//    private int avgPower;
//    private int uplink_avail;
//    private int initial_seq;
//    private int cipherSeq;
//    private int auth;
//    private int authReqFn;
//    private int authRespFn;
//    private int authDelta;
//    private int cipherMissing;
//    private int cipherCompFirst;
//    private int cipherCompLast;
//    private int cipherCompCount;
//    private int cipherDelta;
//    private int cipher;
//    private int cmcImeisv;
//    private int integrity;
//    private int firstFn;
//    private int lastFn;
//    private int duration;
//    private int mobileOrig;
//    private int mobileTerm;
//    private int pagingMi;
//    private int tUnknown;
//    private int tDetach;
//    private int tLocupd;
//    private int luAcc;
//    private int luType;
//    private int luReject;
//    private int luRejCause;
//    private int luMcc;
//    private int luMnc;
//    private int luLac;
//    private int tAbort;
//    private int tRaupd;
//    private int tAttach;
//    private int attAcc;
//    private int tPdp;
//    private int pdpIp;
//    private int tCall;
//    private int tSms;
//    private int tSs;
//    private int tTmsiRealloc;
//    private int tRelease;
//    private int rrCause;
//    private int tGprs;
//    private int idenImsiAc;
//    private int idenImsiBc;
//    private int idenImeiAc;
//    private int idenImeiBc;
//    private int assign;
//    private int assignCmpl;
//    private int handover;
//    private int forcedHo;
//    private int aTimeslot;
//    private int aChanType;
//    private int aTsc;
//    private int aHopping;
//    private int aArfcn;
//    private int aHsn;
//    private int aMaio;
//    private int aMaLen;
//    private int aChanMode;
//    private int aMultirate;
//    private int callPresence;
//    private int serviceReq;
//    private int imsi;
//    private int imei;
//    private int tmsi;
//    private int newTmsi;
//    private int tlli;
//    private int msisdn;
//    private int msCipherMask;
//    private int ueCipherCap;
//    private int ueIntegrityCap;


    private Long id;
    private String timestamp;
    private short rat;
    private short domain;
    private short mcc;
    private short mnc;
    private int lac;
    private int cid;
    private int arfcn;
    private short psc;
    private short cracked;
    private short neigh_count;
    private short unenc;
    private short unenc_rand;
    private short enc;
    private short enc_rand;
    private short enc_null;
    private short enc_null_rand;
    private short enc_si;
    private short enc_si_rand;
    private short predict;
    private short avg_power;
    private short uplink_avail;
    private short initial_seq;
    private short cipher_seq;
    private short auth;
    private int auth_req_fn;
    private int auth_resp_fn;
    private int auth_delta;
    private short cipher_missing;
    private int cipher_comp_first;
    private int cipher_comp_last;
    private int cipher_comp_count;
    private int cipher_delta;
    private short cipher;
    private short cmc_imeisv;
    private short integrity;
    private int first_fn;
    private int last_fn;
    private int duration;
    private short mobile_orig;
    private short mobile_term;
    private short paging_mi;
    private short t_unknown;
    private short t_detach;
    private short t_locupd;
    private short lu_acc;
    private short lu_type;
    private short lu_reject;
    private short lu_rej_cause;
    private short lu_mcc;
    private short lu_mnc;
    private int lu_lac;
    private short t_abort;
    private short t_raupd;
    private short t_attach;
    private short att_acc;
    private short t_pdp;
    private String pdp_ip;
    private short t_call;
    private short t_sms;
    private short t_ss;
    private short t_tmsi_realloc;
    private short t_release;
    private short rr_cause;
    private short t_gprs;
    private short iden_imsi_ac;
    private short iden_imsi_bc;
    private short iden_imei_ac;
    private short iden_imei_bc;
    private short assign;
    private short assign_cmpl;
    private short handover;
    private short forced_ho;
    private short a_timeslot;
    private short a_chan_type;
    private short a_tsc;
    private short a_hopping;
    private short a_arfcn;
    private short a_hsn;
    private short a_maio;
    private short a_ma_len;
    private short a_chan_mode;
    private short a_multirate;
    private short call_presence;
    private short sms_presence;
    private short service_req;
    private String imsi;
    private String imei;
    private String tmsi;
    private String new_tmsi;
    private String tlli;
    private String msisdn;
    private short ms_cipher_mask;
    private short ue_cipher_cap;
    private short ue_integrity_cap;


    public SessionInfo(String timestamp, short rat, short domain, short mcc, short mnc, int lac, int cid, int arfcn, short psc, short cracked, short neigh_count, short unenc, short unenc_rand, short enc, short enc_rand, short enc_null, short enc_null_rand, short enc_si, short enc_si_rand, short predict, short avg_power, short uplink_avail, short initial_seq, short cipher_seq, short auth, int auth_req_fn, int auth_resp_fn, int auth_delta, short cipher_missing, int cipher_comp_first, int cipher_comp_last, int cipher_comp_count, int cipher_delta, short cipher, short cmc_imeisv, short integrity, int first_fn, int last_fn, int duration, short mobile_orig, short mobile_term, short paging_mi, short t_unknown, short t_detach, short t_locupd, short lu_acc, short lu_type, short lu_reject, short lu_rej_cause, short lu_mcc, short lu_mnc, int lu_lac, short t_abort, short t_raupd, short t_attach, short att_acc, short t_pdp, String pdp_ip, short tCall, short t_call, short t_sms, short t_ss, short t_tmsi_realloc, short t_release, short rr_cause, short t_gprs, short iden_imsi_ac, short iden_imsi_bc, short iden_imei_ac, short iden_imei_bc, short assign, short assign_cmpl, short handover, short forced_ho, short a_timeslot, short a_chan_type, short a_tsc, short a_hopping, short a_arfcn, short a_hsn, short a_maio, short a_ma_len, short a_chan_mode, short a_multirate, short call_presence, short sms_presence, short service_req, String imsi, String imei, String tmsi, String new_tmsi, String tlli, String msisdn, short ms_cipher_mask, short ue_cipher_cap, short ue_integrity_cap) {
        this.timestamp = timestamp;
        this.rat = rat;
        this.domain = domain;
        this.mcc = mcc;
        this.mnc = mnc;
        this.lac = lac;
        this.cid = cid;
        this.arfcn = arfcn;
        this.psc = psc;
        this.cracked = cracked;
        this.neigh_count = neigh_count;
        this.unenc = unenc;
        this.unenc_rand = unenc_rand;
        this.enc = enc;
        this.enc_rand = enc_rand;
        this.enc_null = enc_null;
        this.enc_null_rand = enc_null_rand;
        this.enc_si = enc_si;
        this.enc_si_rand = enc_si_rand;
        this.predict = predict;
        this.avg_power = avg_power;
        this.uplink_avail = uplink_avail;
        this.initial_seq = initial_seq;
        this.cipher_seq = cipher_seq;
        this.auth = auth;
        this.auth_req_fn = auth_req_fn;
        this.auth_resp_fn = auth_resp_fn;
        this.auth_delta = auth_delta;
        this.cipher_missing = cipher_missing;
        this.cipher_comp_first = cipher_comp_first;
        this.cipher_comp_last = cipher_comp_last;
        this.cipher_comp_count = cipher_comp_count;
        this.cipher_delta = cipher_delta;
        this.cipher = cipher;
        this.cmc_imeisv = cmc_imeisv;
        this.integrity = integrity;
        this.first_fn = first_fn;
        this.last_fn = last_fn;
        this.duration = duration;
        this.mobile_orig = mobile_orig;
        this.mobile_term = mobile_term;
        this.paging_mi = paging_mi;
        this.t_unknown = t_unknown;
        this.t_detach = t_detach;
        this.t_locupd = t_locupd;
        this.lu_acc = lu_acc;
        this.lu_type = lu_type;
        this.lu_reject = lu_reject;
        this.lu_rej_cause = lu_rej_cause;
        this.lu_mcc = lu_mcc;
        this.lu_mnc = lu_mnc;
        this.lu_lac = lu_lac;
        this.t_abort = t_abort;
        this.t_raupd = t_raupd;
        this.t_attach = t_attach;
        this.att_acc = att_acc;
        this.t_pdp = t_pdp;
        this.pdp_ip = pdp_ip;
        this.t_call = t_call;
        this.t_sms = t_sms;
        this.t_ss = t_ss;
        this.t_tmsi_realloc = t_tmsi_realloc;
        this.t_release = t_release;
        this.rr_cause = rr_cause;
        this.t_gprs = t_gprs;
        this.iden_imsi_ac = iden_imsi_ac;
        this.iden_imsi_bc = iden_imsi_bc;
        this.iden_imei_ac = iden_imei_ac;
        this.iden_imei_bc = iden_imei_bc;
        this.assign = assign;
        this.assign_cmpl = assign_cmpl;
        this.handover = handover;
        this.forced_ho = forced_ho;
        this.a_timeslot = a_timeslot;
        this.a_chan_type = a_chan_type;
        this.a_tsc = a_tsc;
        this.a_hopping = a_hopping;
        this.a_arfcn = a_arfcn;
        this.a_hsn = a_hsn;
        this.a_maio = a_maio;
        this.a_ma_len = a_ma_len;
        this.a_chan_mode = a_chan_mode;
        this.a_multirate = a_multirate;
        this.call_presence = call_presence;
        this.sms_presence = sms_presence;
        this.service_req = service_req;
        this.imsi = imsi;
        this.imei = imei;
        this.tmsi = tmsi;
        this.new_tmsi = new_tmsi;
        this.tlli = tlli;
        this.msisdn = msisdn;
        this.ms_cipher_mask = ms_cipher_mask;
        this.ue_cipher_cap = ue_cipher_cap;
        this.ue_integrity_cap = ue_integrity_cap;

    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public short getRat() {
        return rat;
    }

    public void setRat(short rat) {
        this.rat = rat;
    }

    public short getDomain() {
        return domain;
    }

    public void setDomain(short domain) {
        this.domain = domain;
    }

    public short getMcc() {
        return mcc;
    }

    public void setMcc(short mcc) {
        this.mcc = mcc;
    }

    public short getMnc() {
        return mnc;
    }

    public void setMnc(short mnc) {
        this.mnc = mnc;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getArfcn() {
        return arfcn;
    }

    public void setArfcn(int arfcn) {
        this.arfcn = arfcn;
    }

    public short getPsc() {
        return psc;
    }

    public void setPsc(short psc) {
        this.psc = psc;
    }

    public short getCracked() {
        return cracked;
    }

    public void setCracked(short cracked) {
        this.cracked = cracked;
    }

    public short getNeigh_count() {
        return neigh_count;
    }

    public void setNeigh_count(short neigh_count) {
        this.neigh_count = neigh_count;
    }

    public short getUnenc() {
        return unenc;
    }

    public void setUnenc(short unenc) {
        this.unenc = unenc;
    }

    public short getUnenc_rand() {
        return unenc_rand;
    }

    public void setUnenc_rand(short unenc_rand) {
        this.unenc_rand = unenc_rand;
    }

    public short getEnc() {
        return enc;
    }

    public void setEnc(short enc) {
        this.enc = enc;
    }

    public short getEnc_rand() {
        return enc_rand;
    }

    public void setEnc_rand(short enc_rand) {
        this.enc_rand = enc_rand;
    }

    public short getEnc_null() {
        return enc_null;
    }

    public void setEnc_null(short enc_null) {
        this.enc_null = enc_null;
    }

    public short getEnc_null_rand() {
        return enc_null_rand;
    }

    public void setEnc_null_rand(short enc_null_rand) {
        this.enc_null_rand = enc_null_rand;
    }

    public short getEnc_si() {
        return enc_si;
    }

    public void setEnc_si(short enc_si) {
        this.enc_si = enc_si;
    }

    public short getEnc_si_rand() {
        return enc_si_rand;
    }

    public void setEnc_si_rand(short enc_si_rand) {
        this.enc_si_rand = enc_si_rand;
    }

    public short getPredict() {
        return predict;
    }

    public void setPredict(short predict) {
        this.predict = predict;
    }

    public short getAvg_power() {
        return avg_power;
    }

    public void setAvg_power(short avg_power) {
        this.avg_power = avg_power;
    }

    public short getUplink_avail() {
        return uplink_avail;
    }

    public void setUplink_avail(short uplink_avail) {
        this.uplink_avail = uplink_avail;
    }

    public short getInitial_seq() {
        return initial_seq;
    }

    public void setInitial_seq(short initial_seq) {
        this.initial_seq = initial_seq;
    }

    public short getCipher_seq() {
        return cipher_seq;
    }

    public void setCipher_seq(short cipher_seq) {
        this.cipher_seq = cipher_seq;
    }

    public short getAuth() {
        return auth;
    }

    public void setAuth(short auth) {
        this.auth = auth;
    }

    public int getAuth_req_fn() {
        return auth_req_fn;
    }

    public void setAuth_req_fn(int auth_req_fn) {
        this.auth_req_fn = auth_req_fn;
    }

    public int getAuth_resp_fn() {
        return auth_resp_fn;
    }

    public void setAuth_resp_fn(int auth_resp_fn) {
        this.auth_resp_fn = auth_resp_fn;
    }

    public int getAuth_delta() {
        return auth_delta;
    }

    public void setAuth_delta(int auth_delta) {
        this.auth_delta = auth_delta;
    }

    public short getCipher_missing() {
        return cipher_missing;
    }

    public void setCipher_missing(short cipher_missing) {
        this.cipher_missing = cipher_missing;
    }

    public int getCipher_comp_first() {
        return cipher_comp_first;
    }

    public void setCipher_comp_first(int cipher_comp_first) {
        this.cipher_comp_first = cipher_comp_first;
    }

    public int getCipher_comp_last() {
        return cipher_comp_last;
    }

    public void setCipher_comp_last(int cipher_comp_last) {
        this.cipher_comp_last = cipher_comp_last;
    }

    public int getCipher_comp_count() {
        return cipher_comp_count;
    }

    public void setCipher_comp_count(int cipher_comp_count) {
        this.cipher_comp_count = cipher_comp_count;
    }

    public int getCipher_delta() {
        return cipher_delta;
    }

    public void setCipher_delta(int cipher_delta) {
        this.cipher_delta = cipher_delta;
    }

    public short getCipher() {
        return cipher;
    }

    public void setCipher(short cipher) {
        this.cipher = cipher;
    }

    public short getCmc_imeisv() {
        return cmc_imeisv;
    }

    public void setCmc_imeisv(short cmc_imeisv) {
        this.cmc_imeisv = cmc_imeisv;
    }

    public short getIntegrity() {
        return integrity;
    }

    public void setIntegrity(short integrity) {
        this.integrity = integrity;
    }

    public int getFirst_fn() {
        return first_fn;
    }

    public void setFirst_fn(int first_fn) {
        this.first_fn = first_fn;
    }

    public int getLast_fn() {
        return last_fn;
    }

    public void setLast_fn(int last_fn) {
        this.last_fn = last_fn;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public short getMobile_orig() {
        return mobile_orig;
    }

    public void setMobile_orig(short mobile_orig) {
        this.mobile_orig = mobile_orig;
    }

    public short getMobile_term() {
        return mobile_term;
    }

    public void setMobile_term(short mobile_term) {
        this.mobile_term = mobile_term;
    }

    public short getPaging_mi() {
        return paging_mi;
    }

    public void setPaging_mi(short paging_mi) {
        this.paging_mi = paging_mi;
    }

    public short getT_unknown() {
        return t_unknown;
    }

    public void setT_unknown(short t_unknown) {
        this.t_unknown = t_unknown;
    }

    public short getT_detach() {
        return t_detach;
    }

    public void setT_detach(short t_detach) {
        this.t_detach = t_detach;
    }

    public short getT_locupd() {
        return t_locupd;
    }

    public void setT_locupd(short t_locupd) {
        this.t_locupd = t_locupd;
    }

    public short getLu_acc() {
        return lu_acc;
    }

    public void setLu_acc(short lu_acc) {
        this.lu_acc = lu_acc;
    }

    public short getLu_type() {
        return lu_type;
    }

    public void setLu_type(short lu_type) {
        this.lu_type = lu_type;
    }

    public short getLu_reject() {
        return lu_reject;
    }

    public void setLu_reject(short lu_reject) {
        this.lu_reject = lu_reject;
    }

    public short getLu_rej_cause() {
        return lu_rej_cause;
    }

    public void setLu_rej_cause(short lu_rej_cause) {
        this.lu_rej_cause = lu_rej_cause;
    }

    public short getLu_mcc() {
        return lu_mcc;
    }

    public void setLu_mcc(short lu_mcc) {
        this.lu_mcc = lu_mcc;
    }

    public short getLu_mnc() {
        return lu_mnc;
    }

    public void setLu_mnc(short lu_mnc) {
        this.lu_mnc = lu_mnc;
    }

    public int getLu_lac() {
        return lu_lac;
    }

    public void setLu_lac(int lu_lac) {
        this.lu_lac = lu_lac;
    }

    public short getT_abort() {
        return t_abort;
    }

    public void setT_abort(short t_abort) {
        this.t_abort = t_abort;
    }

    public short getT_raupd() {
        return t_raupd;
    }

    public void setT_raupd(short t_raupd) {
        this.t_raupd = t_raupd;
    }

    public short getT_attach() {
        return t_attach;
    }

    public void setT_attach(short t_attach) {
        this.t_attach = t_attach;
    }

    public short getAtt_acc() {
        return att_acc;
    }

    public void setAtt_acc(short att_acc) {
        this.att_acc = att_acc;
    }

    public short getT_pdp() {
        return t_pdp;
    }

    public void setT_pdp(short t_pdp) {
        this.t_pdp = t_pdp;
    }

    public String getPdp_ip() {
        return pdp_ip;
    }

    public void setPdp_ip(String pdp_ip) {
        this.pdp_ip = pdp_ip;
    }

    public short getT_call() {
        return t_call;
    }

    public void setT_call(short t_call) {
        this.t_call = t_call;
    }

    public short getT_sms() {
        return t_sms;
    }

    public void setT_sms(short t_sms) {
        this.t_sms = t_sms;
    }

    public short getT_ss() {
        return t_ss;
    }

    public void setT_ss(short t_ss) {
        this.t_ss = t_ss;
    }

    public short getT_tmsi_realloc() {
        return t_tmsi_realloc;
    }

    public void setT_tmsi_realloc(short t_tmsi_realloc) {
        this.t_tmsi_realloc = t_tmsi_realloc;
    }

    public short getT_release() {
        return t_release;
    }

    public void setT_release(short t_release) {
        this.t_release = t_release;
    }

    public short getRr_cause() {
        return rr_cause;
    }

    public void setRr_cause(short rr_cause) {
        this.rr_cause = rr_cause;
    }

    public short getT_gprs() {
        return t_gprs;
    }

    public void setT_gprs(short t_gprs) {
        this.t_gprs = t_gprs;
    }

    public short getIden_imsi_ac() {
        return iden_imsi_ac;
    }

    public void setIden_imsi_ac(short iden_imsi_ac) {
        this.iden_imsi_ac = iden_imsi_ac;
    }

    public short getIden_imsi_bc() {
        return iden_imsi_bc;
    }

    public void setIden_imsi_bc(short iden_imsi_bc) {
        this.iden_imsi_bc = iden_imsi_bc;
    }

    public short getIden_imei_ac() {
        return iden_imei_ac;
    }

    public void setIden_imei_ac(short iden_imei_ac) {
        this.iden_imei_ac = iden_imei_ac;
    }

    public short getIden_imei_bc() {
        return iden_imei_bc;
    }

    public void setIden_imei_bc(short iden_imei_bc) {
        this.iden_imei_bc = iden_imei_bc;
    }

    public short getAssign() {
        return assign;
    }

    public void setAssign(short assign) {
        this.assign = assign;
    }

    public short getAssign_cmpl() {
        return assign_cmpl;
    }

    public void setAssign_cmpl(short assign_cmpl) {
        this.assign_cmpl = assign_cmpl;
    }

    public short getHandover() {
        return handover;
    }

    public void setHandover(short handover) {
        this.handover = handover;
    }

    public short getForced_ho() {
        return forced_ho;
    }

    public void setForced_ho(short forced_ho) {
        this.forced_ho = forced_ho;
    }

    public short getA_timeslot() {
        return a_timeslot;
    }

    public void setA_timeslot(short a_timeslot) {
        this.a_timeslot = a_timeslot;
    }

    public short getA_chan_type() {
        return a_chan_type;
    }

    public void setA_chan_type(short a_chan_type) {
        this.a_chan_type = a_chan_type;
    }

    public short getA_tsc() {
        return a_tsc;
    }

    public void setA_tsc(short a_tsc) {
        this.a_tsc = a_tsc;
    }

    public short getA_hopping() {
        return a_hopping;
    }

    public void setA_hopping(short a_hopping) {
        this.a_hopping = a_hopping;
    }

    public short getA_arfcn() {
        return a_arfcn;
    }

    public void setA_arfcn(short a_arfcn) {
        this.a_arfcn = a_arfcn;
    }

    public short getA_hsn() {
        return a_hsn;
    }

    public void setA_hsn(short a_hsn) {
        this.a_hsn = a_hsn;
    }

    public short getA_maio() {
        return a_maio;
    }

    public void setA_maio(short a_maio) {
        this.a_maio = a_maio;
    }

    public short getA_ma_len() {
        return a_ma_len;
    }

    public void setA_ma_len(short a_ma_len) {
        this.a_ma_len = a_ma_len;
    }

    public short getA_chan_mode() {
        return a_chan_mode;
    }

    public void setA_chan_mode(short a_chan_mode) {
        this.a_chan_mode = a_chan_mode;
    }

    public short getA_multirate() {
        return a_multirate;
    }

    public void setA_multirate(short a_multirate) {
        this.a_multirate = a_multirate;
    }

    public short getCall_presence() {
        return call_presence;
    }

    public void setCall_presence(short call_presence) {
        this.call_presence = call_presence;
    }

    public short getSms_presence() {
        return sms_presence;
    }

    public void setSms_presence(short sms_presence) {
        this.sms_presence = sms_presence;
    }

    public short getService_req() {
        return service_req;
    }

    public void setService_req(short service_req) {
        this.service_req = service_req;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTmsi() {
        return tmsi;
    }

    public void setTmsi(String tmsi) {
        this.tmsi = tmsi;
    }

    public String getNew_tmsi() {
        return new_tmsi;
    }

    public void setNew_tmsi(String new_tmsi) {
        this.new_tmsi = new_tmsi;
    }

    public String getTlli() {
        return tlli;
    }

    public void setTlli(String tlli) {
        this.tlli = tlli;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public short getMs_cipher_mask() {
        return ms_cipher_mask;
    }

    public void setMs_cipher_mask(short ms_cipher_mask) {
        this.ms_cipher_mask = ms_cipher_mask;
    }

    public short getUe_cipher_cap() {
        return ue_cipher_cap;
    }

    public void setUe_cipher_cap(short ue_cipher_cap) {
        this.ue_cipher_cap = ue_cipher_cap;
    }

    public short getUe_integrity_cap() {
        return ue_integrity_cap;
    }

    public void setUe_integrity_cap(short ue_integrity_cap) {
        this.ue_integrity_cap = ue_integrity_cap;
    }

    @Override
    public String toString() {
        return "SessionInfo{" +
                "id=" + id +
                ", timestamp='" + timestamp + '\'' +
                ", rat=" + rat +
                ", domain=" + domain +
                ", mcc=" + mcc +
                ", mnc=" + mnc +
                ", lac=" + lac +
                ", cid=" + cid +
                ", arfcn=" + arfcn +
                ", psc=" + psc +
                ", cracked=" + cracked +
                ", neigh_count=" + neigh_count +
                ", unenc=" + unenc +
                ", unenc_rand=" + unenc_rand +
                ", enc=" + enc +
                ", enc_rand=" + enc_rand +
                ", enc_null=" + enc_null +
                ", enc_null_rand=" + enc_null_rand +
                ", enc_si=" + enc_si +
                ", enc_si_rand=" + enc_si_rand +
                ", predict=" + predict +
                ", avg_power=" + avg_power +
                ", uplink_avail=" + uplink_avail +
                ", initial_seq=" + initial_seq +
                ", cipher_seq=" + cipher_seq +
                ", auth=" + auth +
                ", auth_req_fn=" + auth_req_fn +
                ", auth_resp_fn=" + auth_resp_fn +
                ", auth_delta=" + auth_delta +
                ", cipher_missing=" + cipher_missing +
                ", cipher_comp_first=" + cipher_comp_first +
                ", cipher_comp_last=" + cipher_comp_last +
                ", cipher_comp_count=" + cipher_comp_count +
                ", cipher_delta=" + cipher_delta +
                ", cipher=" + cipher +
                ", cmc_imeisv=" + cmc_imeisv +
                ", integrity=" + integrity +
                ", first_fn=" + first_fn +
                ", last_fn=" + last_fn +
                ", duration=" + duration +
                ", mobile_orig=" + mobile_orig +
                ", mobile_term=" + mobile_term +
                ", paging_mi=" + paging_mi +
                ", t_unknown=" + t_unknown +
                ", t_detach=" + t_detach +
                ", t_locupd=" + t_locupd +
                ", lu_acc=" + lu_acc +
                ", lu_type=" + lu_type +
                ", lu_reject=" + lu_reject +
                ", lu_rej_cause=" + lu_rej_cause +
                ", lu_mcc=" + lu_mcc +
                ", lu_mnc=" + lu_mnc +
                ", lu_lac=" + lu_lac +
                ", t_abort=" + t_abort +
                ", t_raupd=" + t_raupd +
                ", t_attach=" + t_attach +
                ", att_acc=" + att_acc +
                ", t_pdp=" + t_pdp +
                ", pdp_ip='" + pdp_ip + '\'' +
                ", t_call=" + t_call +
                ", t_sms=" + t_sms +
                ", t_ss=" + t_ss +
                ", t_tmsi_realloc=" + t_tmsi_realloc +
                ", t_release=" + t_release +
                ", rr_cause=" + rr_cause +
                ", t_gprs=" + t_gprs +
                ", iden_imsi_ac=" + iden_imsi_ac +
                ", iden_imsi_bc=" + iden_imsi_bc +
                ", iden_imei_ac=" + iden_imei_ac +
                ", iden_imei_bc=" + iden_imei_bc +
                ", assign=" + assign +
                ", assign_cmpl=" + assign_cmpl +
                ", handover=" + handover +
                ", forced_ho=" + forced_ho +
                ", a_timeslot=" + a_timeslot +
                ", a_chan_type=" + a_chan_type +
                ", a_tsc=" + a_tsc +
                ", a_hopping=" + a_hopping +
                ", a_arfcn=" + a_arfcn +
                ", a_hsn=" + a_hsn +
                ", a_maio=" + a_maio +
                ", a_ma_len=" + a_ma_len +
                ", a_chan_mode=" + a_chan_mode +
                ", a_multirate=" + a_multirate +
                ", call_presence=" + call_presence +
                ", sms_presence=" + sms_presence +
                ", service_req=" + service_req +
                ", imsi='" + imsi + '\'' +
                ", imei='" + imei + '\'' +
                ", tmsi='" + tmsi + '\'' +
                ", new_tmsi='" + new_tmsi + '\'' +
                ", tlli='" + tlli + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", ms_cipher_mask=" + ms_cipher_mask +
                ", ue_cipher_cap=" + ue_cipher_cap +
                ", ue_integrity_cap=" + ue_integrity_cap +
                '}';
    }


    public String toJson() {
        return "{" +
                "\"timestamp\"='" + timestamp + '\'' +
                ", \" rat\"=" + rat +
                ", \"domain\"=" + domain +
                ", \"mcc\"=" + mcc +
                ", \"mnc\"=" + mnc +
                ", \"lac\"=" + lac +
                ", \"cid\"=" + cid +
                ", \"arfcn\"=" + arfcn +
                ", \"psc\"=" + psc +
                ", \"cracked\"=" + cracked +
                ", \"neigh_count\"=" + neigh_count +
                ", \"unenc\"=" + unenc +
                ", \"unenc_rand\"=" + unenc_rand +
                ", \"enc\"=" + enc +
                ", \"enc_rand\"=" + enc_rand +
                ", \"enc_null\"=" + enc_null +
                ", \"enc_null_rand\"=" + enc_null_rand +
                ", \"enc_si\"=" + enc_si +
                ", \"enc_si_rand\"=" + enc_si_rand +
                ", \"predict\"=" + predict +
                ", \"avg_power\"=" + avg_power +
                ", \"uplink_avail\"=" + uplink_avail +
                ", \"initial_seq\"=" + initial_seq +
                ", \"cipher_seq\"=" + cipher_seq +
                ", \"auth\"=" + auth +
                ", \"auth_req_fn\"=" + auth_req_fn +
                ", \"auth_resp_fn\"=" + auth_resp_fn +
                ", \"auth_delta\"=" + auth_delta +
                ", \"cipher_missing\"=" + cipher_missing +
                ", \"cipher_comp_first\"=" + cipher_comp_first +
                ", \"cipher_comp_last\"=" + cipher_comp_last +
                ", \"cipher_comp_count\"=" + cipher_comp_count +
                ", \"cipher_delta\"=" + cipher_delta +
                ", \"cipher\"=" + cipher +
                ", \"cmc_imeisv\"=" + cmc_imeisv +
                ", \"integrity\"=" + integrity +
                ", \"first_fn\"=" + first_fn +
                ", \"last_fn\"=" + last_fn +
                ", \"duration\"=" + duration +
                ", \"mobile_orig\"=" + mobile_orig +
                ", \"mobile_term\"=" + mobile_term +
                ", \"paging_mi\"=" + paging_mi +
                ", \"t_unknown\"=" + t_unknown +
                ", \"t_detach\"=" + t_detach +
                ", \"t_locupd\"=" + t_locupd +
                ", \"lu_acc\"=" + lu_acc +
                ", \"lu_type\"=" + lu_type +
                ", \"lu_reject\"=" + lu_reject +
                ", \"lu_rej_cause\"=" + lu_rej_cause +
                ", \"lu_mcc\"=" + lu_mcc +
                ", \"lu_mnc\"=" + lu_mnc +
                ", \"lu_lac\"=" + lu_lac +
                ", \"t_abort\"=" + t_abort +
                ", \"t_raupd\"=" + t_raupd +
                ", \"t_attach\"=" + t_attach +
                ", \"att_acc\"=" + att_acc +
                ", \"t_pdp\"=" + t_pdp +
                ", \"pdp_ip\"='" + pdp_ip + '\'' +
                ", \"t_call\"=" + t_call +
                ", \"t_sms\"=" + t_sms +
                ", \"t_ss\"=" + t_ss +
                ", \"t_tmsi_realloc\"=" + t_tmsi_realloc +
                ", \"t_release\"=" + t_release +
                ", \"rr_cause\"=" + rr_cause +
                ", \"t_gprs\"=" + t_gprs +
                ", \"iden_imsi_ac\"=" + iden_imsi_ac +
                ", \"iden_imsi_bc\"=" + iden_imsi_bc +
                ", \"iden_imei_ac\"=" + iden_imei_ac +
                ", \"iden_imei_bc\"=" + iden_imei_bc +
                ", \"assign\"=" + assign +
                ", \"assign_cmpl\"=" + assign_cmpl +
                ", \"handover\"=" + handover +
                ", \"forced_ho\"=" + forced_ho +
                ", \"a_timeslot\"=" + a_timeslot +
                ", \"a_chan_type\"=" + a_chan_type +
                ", \"a_tsc\"=" + a_tsc +
                ", \"a_hopping\"=" + a_hopping +
                ", \"a_arfcn\"=" + a_arfcn +
                ", \"a_hsn\"=" + a_hsn +
                ", \"a_maio\"=" + a_maio +
                ", \"a_ma_len\"=" + a_ma_len +
                ", \"a_chan_mode\"=" + a_chan_mode +
                ", \"a_multirate\"=" + a_multirate +
                ", \"call_presence\"=" + call_presence +
                ", \"sms_presence\"=" + sms_presence +
                ", \"service_req\"=" + service_req +
                ", \"imsi\"='" + imsi + '\'' +
                ", \"imei\"='" + imei + '\'' +
                ", \"tmsi\"='" + tmsi + '\'' +
                ", \"new_tmsi\"='" + new_tmsi + '\'' +
                ", \"tlli\"='" + tlli + '\'' +
                ", \"msisdn\"='" + msisdn + '\'' +
                ", \"ms_cipher_mask\"=" + ms_cipher_mask +
                ", \"ue_cipher_cap\"=" + ue_cipher_cap +
                ", \"ue_integrity_cap\"=" + ue_integrity_cap +
                '}';
    }
}