package ir.telecom.sensor.model;

/**
 * Created by Bahar on 4/26/2018.
 * class NameValuePair to maintain web request pairs
 */
public class NameValuePair {

    private String name;
    private String value;

    public NameValuePair(String name, String value){

        this.name=name;
        this.value=value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
