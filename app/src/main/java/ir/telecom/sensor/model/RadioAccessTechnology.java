package ir.telecom.sensor.model;

public enum RadioAccessTechnology {
    GSM(0),
    UMTS(1),
    LTE(2);

    private int value;

    RadioAccessTechnology(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
