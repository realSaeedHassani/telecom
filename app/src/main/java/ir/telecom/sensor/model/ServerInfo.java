package ir.telecom.sensor.model;


import java.io.Serializable;

/**
 * ServerInfo---class for save server information
 *
 * @author Bahar
 */
public class ServerInfo implements Serializable {
    private String ip;
    private String port;

    /**
     * constructor
     */
    public ServerInfo(String ip, String port) {
        this.ip=ip;
        this.port=port;
    }

    /**
     * get port method
     *
     * @return port
     */
    public String getPort() {
        return port;
    }

    /**
     * set port method
     *
     * @param port
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * get ip method
     *
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * set ip method
     *
     * @param ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }


}

