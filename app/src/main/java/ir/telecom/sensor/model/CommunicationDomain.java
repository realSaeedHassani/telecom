package ir.telecom.sensor.model;

public enum CommunicationDomain {
    CIRCUIT_SWITCHED(0),
    PACKET_SWITCHED(1);

    private int value;

    CommunicationDomain(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
