package ir.telecom.sensor;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.util.MSDServiceHelperCreator;
import ir.telecom.sensor.util.MsdDatabaseManager;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by Bahar on 6/10/2018.
 * This receiver trigger from AlarmManager
 */
public class SensorUpdateAlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "SensorUpdateAlmRcvr";
    Double longitude = 0.0;
    Double latitude = 0.0;
    GpsTracker gpsTracker;
    protected MSDServiceHelperCreator msdServiceHelperCreator;
    boolean serviceStatus;
    private Context context;
    SharedPreferences sharedPrefs;


    /**
     * on Receive method
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
//        Log.d(TAG, "onReceive");

        this.context = context;
        sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        getLocation(context);

        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(context);
        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();



        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        final String myDate = sdf.format(new Date());

        float cpuUsage = (float)getCpuUsage();
        float memoryUsage = (float)getMemoryUsage();
        float diskUsage = (float) getDiskUsage();


        msdServiceHelperCreator = MSDServiceHelperCreator.getInstance(context);
        serviceStatus = msdServiceHelperCreator.getMsdServiceHelper().isRecording();
//        Log.i("recording",String.valueOf(msdServiceHelperCreator.getMsdServiceHelper().isRecording()));
        msoh.insertSensor(db, serviceStatus, myDate, DashboardActivity.batteryLevel, longitude, latitude
                , cpuUsage, memoryUsage, diskUsage);

    }


    public void getLocation(Context context) {
        gpsTracker = new GpsTracker(context);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        } else {
            gpsTracker.showSettingsAlert();
        }
    }



    public boolean isTableExists(Context context, String tableName, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                // Toast.makeText(context,"exist",Toast.LENGTH_SHORT).show();
                return true;
            }
            cursor.close();
        }
        //Toast.makeText(context,"not  exist",Toast.LENGTH_SHORT).show();
        return false;
    }




    private double getDiskUsage() {
        double used=phone_storage_used()/1024;
        double total=phone_storage_total()/1024;

        return (used/total)*100.0;
    }

    public long phone_storage_used(){

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * (stat.getBlockCountLong() - stat.getAvailableBlocksLong());
        }
        else {
            bytesAvailable = (long)stat.getBlockSize() * ((long)stat.getBlockCount() - (long)stat.getAvailableBlocks());
        }
        return bytesAvailable / (1024 * 1024);
    }

    public static long phone_storage_total(){

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getBlockCountLong();
        }
        else {
            bytesAvailable = (long)stat.getBlockSize() * (long)stat.getBlockCount();
        }
        return bytesAvailable / (1024 * 1024);
    }

    private double getMemoryUsage() {
        double totalRam;
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        totalRam = mi.totalMem;
        double percentAvail = mi.availMem / totalRam * 100.0;
        return percentAvail;
    }

    private double getCpuUsage() {
        double cpuusage;
        long work;
        long previousWork;
        long total;
        long previousTotal;
        try {
            String res = "";
            BufferedReader br;
            
            previousWork=sharedPrefs.getLong("previousWork",0);
            previousTotal=sharedPrefs.getLong("previousTotal",0);
            
            File pro = new File("/proc/stat");
            br = new BufferedReader(new FileReader(pro));
            String read;
            read = br.readLine();
            String[] values = read.split("[ ]+", 18);
//            Log.d("de", read);
//            for (int i = 0; i < values.length; i++)
//                Log.d("d e", values[i]);

            work = Long.parseLong(values[1]) + Long.parseLong(values[2]) + Long.parseLong(values[3]);
            total = work + Long.parseLong(values[4]) + Long.parseLong(values[5]) + Long.parseLong(values[6]) + Long.parseLong(values[7]);

            long dwork = work - previousWork;
            long dtotal = total - previousTotal;

//            Log.d("DE", String.format("%d %d", dwork, dtotal));

           
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putLong("previousTotal",total);
            editor.putLong("previousWork",work);
            editor.commit();


            cpuusage = restrictPercentage((dwork / (double) dtotal) * 100);
        }catch (Exception e){
//            Log.d("Exception",e.getMessage().toString());
            cpuusage=0;
        }
        return cpuusage;
    }


    public  double restrictPercentage(double percentage){
        if(percentage > 100)
            return 100;
        else if(percentage < 0)
            return 0;
        else return percentage;
    }
}
