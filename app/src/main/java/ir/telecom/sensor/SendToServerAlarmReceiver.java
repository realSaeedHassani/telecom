package ir.telecom.sensor;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import ir.telecom.sensor.model.CellInfo;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.ret.RequestAPI;
import ir.telecom.sensor.ret.ServiceGenerator;
import ir.telecom.sensor.ret.model.CatcherM;
import ir.telecom.sensor.ret.model.CellInfoM;
import ir.telecom.sensor.ret.model.SensorM;
import ir.telecom.sensor.ret.model.SessionInfoM;
import ir.telecom.sensor.ret.model.ThresholdValuesDto;
import ir.telecom.sensor.util.MSDServiceHelperCreator;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.MsdDatabaseManager;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bahar on 6/10/2018.
 * This receiver trigger from AlarmManager
 */
public class SendToServerAlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "SendToserverAlmRcvr";
    Double longitude, latitude;
    GpsTracker gpsTracker;
    protected MSDServiceHelperCreator msdServiceHelperCreator;
    boolean serviceStatus;
    SharedPreferences sharedPrefs;
    private Context context;
    // Sharedpreferences setting
    public static final String SHPCONFIG = "shpConfig";
    public static final String T3212 = "T3212";
    public static final String croMin = "croMin";
    public static final String croMax = "croMax";
    public static final String deltaCmcp = "deltaCmcp";
    public static final String nNorm = "nNorm";
    public static final String deltaTch = "deltaTch";
    public static final String valid = "valid";
    /**
     * on Receive method
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        RequestAPI service = null;
        try {
            service = ServiceGenerator.createService(RequestAPI.class,
                    sharedPrefs.getString("protocol", "https"),
                    sharedPrefs.getString("ip", "5.56.135.20") + ":" + sharedPrefs.getString("port", "8443")

            );
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        ArrayList<SensorM> sensorArrayList = getNewSensorArrayListM();
        if (sensorArrayList != null) {
            if (sensorArrayList.size() > 0) {
                Call<ResponseBody> callableResponse = service.sendSensorStatus(MsdConfig.getAppId(context), 1, sensorArrayList);
                dumpCallableResponse(callableResponse,1, service);
            }
        }

        ArrayList<CatcherM> catcherArrayLis = getNewCatcherArrayListM();
        if (catcherArrayLis != null) {
            if (catcherArrayLis.size() > 0) {
                Call<ResponseBody> callableResponse =
                        service.sendCatcher(MsdConfig.getAppId(context), 1, catcherArrayLis);
                dumpCallableResponse(callableResponse,0, service);
            }
        }

        /*
         * Send Cellinfo table...
         */
        ArrayList<CellInfoM> cellInfoArrayList = getNewCellInfoArrayListM();
        if (cellInfoArrayList != null) {
            if (cellInfoArrayList.size() > 0) {
                Call<ResponseBody> callableResponse = service.sendCellinfoTable(MsdConfig.getAppId(context), 1, cellInfoArrayList);
                dumpCallableResponse(callableResponse,0, service);
            }
        }

        ArrayList<SessionInfoM> sessionInfoArrayList = getNewSessionInfoArrayListM();
        if (sessionInfoArrayList != null) {
            if (sessionInfoArrayList.size() > 0) {
                Call<ResponseBody> callableResponse = service.sendSessionInfo(MsdConfig.getAppId(context), 1, sessionInfoArrayList);
                dumpCallableResponse(callableResponse,0, service);
            }
        }
    }

//    private <T> void dumpCallableResponse(Call<T> callableResponse) {
//        Request request = callableResponse.request();
//        try {
//            Buffer buffer = new Buffer();
//            String show = request.toString() + "headers: " + request.headers() + "\n";
//            if (request.body() != null) {
//                request.body().writeTo(buffer);
//                show += "Body : " + buffer.readString(Charset.defaultCharset());
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        callableResponse.enqueue(new Callback<T>() {
//            @Override
//            public void onResponse(Call<T> call, Response<T> response) {
//                if (response.code() == 200) {
//                    setLastSeenSharePre();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<T> call, Throwable t) {
//                String str = "onFailure";
//                if (t instanceof Exception)
//                    str += t.getMessage();
//                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private <T> void dumpCallableResponse(Call<T> callableResponse, final int isSensorStatus, final RequestAPI service) {
        final Request request = callableResponse.request();
        try {
            Buffer buffer = new Buffer();
            String show = request.toString() + "headers: " + request.headers() + "\n";
            if (request.body() != null) {
                request.body().writeTo(buffer);
                show += "Body : " + buffer.readString(Charset.defaultCharset());
                Log.e(">> BODY: ", show);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        callableResponse.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
//                Toasty.success(context, "Response code: " + response.code(), Toast.LENGTH_LONG).show();
                try {
                    if (response.code() == 200) {
//                        Toasty.success(DashboardActivity.this, "Send data successfully", Toast.LENGTH_LONG).show();
                        setLastSeenSharePre();
                        ResponseBody responseBody = (ResponseBody) response.body();
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        if (isSensorStatus == 1) {
                            if (jsonObject.getString("message").equals("1")){
                                getConfig(service);
                            }
                        }
                    }
//                    Log.e(">> RESPONSE: ", getStrFromResponseBody(response));
                } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                String str = "onFailure";
                if (t instanceof Exception)
                    str += t.getMessage();
            }
        });
    }

    private void getConfig(RequestAPI service) throws NoSuchAlgorithmException, KeyManagementException {
        Call<ThresholdValuesDto> callableResponse = service.getConfig(MsdConfig.getAppId(context));
        setDataConfig(callableResponse);
    }

    private void setDataConfig(Call<ThresholdValuesDto> callableResponse) {
        final Request request = callableResponse.request();
        try {
            Buffer buffer = new Buffer();
            String show = request.toString() + "headers: " + request.headers() + "\n";
            if (request.body() != null) {
                request.body().writeTo(buffer);
                show += "body: " + buffer.readString(Charset.defaultCharset());
                Log.e(">> Config: ", show);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        callableResponse.enqueue(new Callback<ThresholdValuesDto>() {
            @Override
            public void onResponse(Call<ThresholdValuesDto> call, Response<ThresholdValuesDto> response) {
//                Toasty.success(DashboardActivity.this, "Response code: " + response.code(), Toast.LENGTH_LONG).show();
                if (response.code() == 200) {
//                    Toasty.success(DashboardActivity.this, "New config received", Toast.LENGTH_LONG).show();
                    saveNewConfig(response.body());
                }
            }

            @Override
            public void onFailure(Call<ThresholdValuesDto> call, Throwable t) {
                String str = "onFailure";
                if (t instanceof Exception)
                    str += t.getMessage();
//                Toasty.error(DashboardActivity.this, str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveNewConfig(ThresholdValuesDto body) {
        SharedPreferences shpConfig = context.getSharedPreferences(SHPCONFIG,
                Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        Log.e(">> GET_CONFIG: ", body.getT3212() + " | " +
                body.getMin_Cell_Reselection_Offset() + " | " +
                body.getMax_Cell_Reselection_Offset() + " | " +
                body.getDelay_Between_Command_And_Complete() + " | " +
                body.getPaging_Group_Number() + " | " +
                body.getTraffic_Channel_Idle_Milliseconds());

        SharedPreferences.Editor editor = shpConfig.edit();
        editor.putString(T3212, String.valueOf(body.getT3212()));
        editor.putString(croMin, String.valueOf(body.getMin_Cell_Reselection_Offset()));
        editor.putString(croMax, String.valueOf(body.getMax_Cell_Reselection_Offset()));
        editor.putString(deltaCmcp, String.valueOf(body.getDelay_Between_Command_And_Complete()));
        editor.putString(nNorm, String.valueOf(body.getPaging_Group_Number()));
        editor.putString(deltaTch, String.valueOf(body.getTraffic_Channel_Idle_Milliseconds()));
        editor.putString(valid, "1");
        editor.commit();

        ContentValues insertValues = new ContentValues();
        insertValues.put("t3212_min", body.getT3212());
        insertValues.put("cro_min", body.getMin_Cell_Reselection_Offset());
        insertValues.put("cro_max", body.getMax_Cell_Reselection_Offset());
        insertValues.put("delta_cmcp", body.getDelay_Between_Command_And_Complete());
        insertValues.put("n_norm", body.getPaging_Group_Number());
        insertValues.put("delta_tch", body.getTraffic_Channel_Idle_Milliseconds());
        db.insert("config", null, insertValues);
    }


    private void setLastSeenSharePre() {
        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM session_info ORDER BY timestamp DESC LIMIT 1;";
        @SuppressLint("Recycle")
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        SharedPreferences.Editor editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastSessionInfoSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }


        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM cell_info ORDER BY first_seen DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastCellInfoSentTime", cursor.getString(cursor.getColumnIndex("first_seen")));
            editor.apply();
        }


        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM catcher ORDER BY timestamp DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();
        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastCatcherSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }


        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        selectQuery = "SELECT * FROM sensor_status ORDER BY timestamp DESC LIMIT 1;";
        cursor = dbInstanse.rawQuery(selectQuery, null);
        cursor.moveToLast();

        editor = sharedPrefs.edit();
        if (cursor.getCount() > 0) {
            editor.putString("lastSensorSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
            editor.apply();
        }
    }

    private ArrayList<SessionInfoM> getNewSessionInfoArrayListM() {
        ArrayList<SessionInfoM> sessionInfoArrayList;
        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastSessionInfoSentTime = sharedPrefs.getString("lastSessionInfoSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        sessionInfoArrayList = msdOpenHelper.getUnsentSessionInfos(dbInstanse, lastSessionInfoSentTime);

        return sessionInfoArrayList;
    }

    private ArrayList<CellInfoM> getNewCellInfoArrayListM() {
        ArrayList<CellInfoM> cellInfoArrayList;
        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        cellInfoArrayList = msdOpenHelper.getUnsentCellInfosM(dbInstanse, lastCellInfoSentTime);
        return cellInfoArrayList;
    }

    private ArrayList<CatcherM> getNewCatcherArrayListM() {

        ArrayList<CatcherM> catcherArrayList = null;
        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String lastCatcherSentTime = sharedPrefs.getString("lastCatcherSentTime", "2016-01-01 01:00:00");
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
        try {
            SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
            catcherArrayList = msdOpenHelper.getUnsentCatchersM(dbInstanse, lastCatcherSentTime);
        }
            catch (Exception ex){
            Log.e(">> Error: ", ex.getMessage() + " ");
            }
        return catcherArrayList;

    }

    private ArrayList<SensorM> getNewSensorArrayListM() {
        ArrayList<SensorM> sensorArrayList;
        try {
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            String lastSensorSentTime = sharedPrefs.getString("lastSensorSentTime", "2016-01-01 01:00:00");
            MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
            SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
            sensorArrayList = msdOpenHelper.getUnsentSensorsM(dbInstanse, lastSensorSentTime);
        } catch (NullPointerException e) {
            return null;
        }
        return sensorArrayList;
    }
    private ArrayList<CellInfo> getNewCellInfoArrayList() {
        ArrayList<CellInfo> cellInfoArrayList = new ArrayList<>();
        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        cellInfoArrayList = msdOpenHelper.getUnsentCellInfos(dbInstanse, lastCellInfoSentTime);

        return cellInfoArrayList;
    }

//    private ArrayList<Catcher> getNewCatcherArrayList() {
//
//        ArrayList<Catcher> catcherArrayList=new ArrayList<>();
//        String lastCatcherSentTime=sharedPrefs.getString("lastCatcherSentTime","2016-01-01 01:00:00");
//        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        catcherArrayList = msdOpenHelper.getUnsentCatchers(dbInstanse, lastCatcherSentTime);
//
//        return catcherArrayList;
//
//    }

//    private ArrayList<Sensor> getNewSensorArrayList() {
//        ArrayList<Sensor> sensorArrayList=new ArrayList<>();
//        String lastSensorSentTime=sharedPrefs.getString("lastSensorSentTime","2016-01-01 01:00:00");
//        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        sensorArrayList = msdOpenHelper.getUnsentSensors(dbInstanse, lastSensorSentTime);
//
//        return sensorArrayList;
//
//    }


    public boolean isTableExists(Context context, String tableName, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                // Toast.makeText(context,"exist",Toast.LENGTH_SHORT).show();
                return true;
            }
            cursor.close();
        }
        //Toast.makeText(context,"not  exist",Toast.LENGTH_SHORT).show();
        return false;
    }

    public void sendtoserverbtn() {
//        Log.d(TAG, "sendtoserverbtn");

        this.context = context;
        sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String ip = sharedPrefs.getString("ip", "5.56.135.20");
        String port = sharedPrefs.getString("port", "8443");
        ServerInfo serverInfo = new ServerInfo(ip, port);

        /*ArrayList<Sensor> sensorArrayList=getNewSensorArrayList();
        if(sensorArrayList.size()>0) {
            new SensorRequests(context, serverInfo).sendSensorData(sensorArrayList);
        }

        ArrayList<Catcher> catcherArrayList=getNewCatcherArrayList();
        if(catcherArrayList.size()>0) {
            new CatcherRequests(context, serverInfo).sendCatcherData(catcherArrayList);
        }

        ArrayList<CellInfo> cellInfoArrayList=getNewCellInfoArrayList();
        if(cellInfoArrayList.size()>0) {
            new CellInfoRequests(context, serverInfo).sendCellInfoData(cellInfoArrayList);
        }

        ArrayList<SessionInfo> sessionInfoArrayList=getNewSessionInfoArrayList();
        if(sessionInfoArrayList.size()>0) {
            new SessionInfoRequests(context, serverInfo).sendSessionInfoData(sessionInfoArrayList);
        }*/
    }


}


//package ir.telecom.sensor;
//
//import android.annotation.SuppressLint;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.util.ArrayList;
//
//import ir.telecom.sensor.model.Catcher;
//import ir.telecom.sensor.model.CellInfo;
//import ir.telecom.sensor.model.Sensor;
//import ir.telecom.sensor.model.ServerInfo;
//import ir.telecom.sensor.model.SessionInfo;
//import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
//import ir.telecom.sensor.ret.RequestAPI;
//import ir.telecom.sensor.ret.ServiceGenerator;
//import ir.telecom.sensor.ret.model.CatcherM;
//import ir.telecom.sensor.ret.model.CellInfoM;
//import ir.telecom.sensor.ret.model.SensorM;
//import ir.telecom.sensor.ret.model.SessionInfoM;
//import ir.telecom.sensor.server.CatcherRequests;
//import ir.telecom.sensor.server.CellInfoRequests;
//import ir.telecom.sensor.server.SensorRequests;
//import ir.telecom.sensor.server.SessionInfoRequests;
//import ir.telecom.sensor.toasty.Toasty;
//import ir.telecom.sensor.util.MSDServiceHelperCreator;
//import ir.telecom.sensor.util.MsdConfig;
//import ir.telecom.sensor.util.MsdDatabaseManager;
//import okhttp3.Request;
//import okhttp3.ResponseBody;
//import okio.Buffer;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
// * Created by Bahar on 6/10/2018.
// * This receiver trigger from AlarmManager
// */
//public class SendToServerAlarmReceiver extends BroadcastReceiver {
//
//    private static final String TAG = "SendToserverAlmRcvr";
//    Double longitude, latitude;
//    GpsTracker gpsTracker;
//    protected MSDServiceHelperCreator msdServiceHelperCreator;
//    boolean serviceStatus;
//    SharedPreferences sharedPrefs;
//    private Context context;
//
//    /**
//     * on Receive method
//     *
//     * @param context
//     * @param intent
//     */
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        Log.e(">> RECEIVER: ", "RECEIVER IS ON!");
//        Toasty.error(context, ">> RECEIVER is ON").show();
//        sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        RequestAPI service = null;
//        try {
//            service = ServiceGenerator.createService(RequestAPI.class, sharedPrefs.getString("protocol", "https"),
//                    sharedPrefs.getString("ip", "5.56.135.20") + ":" + sharedPrefs.getString("port", "8443"));
//
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        this.context = context;
//
//        String ip = sharedPrefs.getString("ip", "5.56.135.20");
//        String port = sharedPrefs.getString("port", "8443");
//        ServerInfo serverInfo = new ServerInfo(ip, port);
//
//        try {
//            ArrayList<SensorM> sensorArrayList = getNewSensorArrayListM();
//            if (sensorArrayList.size() > 0) {
////            new SensorRequests(this, serverInfo).sendSensorData(sensorArrayList);
//                Call<ResponseBody> callableResponse = service.sendSensorStatus(MsdConfig.getAppId(context), 1, sensorArrayList);
//                dumpCallableResponse(callableResponse);
//            }
//        } catch (NullPointerException e) {
//
//        }
//
//        ArrayList<CatcherM> catcherArrayLis = getNewCatcherArrayListM();
//        if (catcherArrayLis.size() > 0) {
////            new CatcherRequests(this, serverInfo).sendCatcherData(catcherArrayLis);
//            Call<ResponseBody> callableResponse =
//                    service.sendCatcher(MsdConfig.getAppId(context), 1, catcherArrayLis);
//            dumpCallableResponse(callableResponse);
//        }
//
//        // ___________ Traditional Request ____________________
////        ArrayList<CellInfo> cellInfo = getNewCellInfoArrayList();
////
////        for (CellInfo cellInfo : cellInfoArrayList) {
////            Log.e(">> Cell Info: ", cellInfo.toJson());
////        }
////        Log.e(">> Size: ", String.valueOf(cellInfoArrayList.size()));
////        if (cellInfoArrayList.size() > 0) {
////            new CellInfoRequests(this, serverInfo).sendCellInfoData(cellInfo);
////
////        }
//        // _________________________________________________
//
//
//        /*
//         * Send Cellinfo table...
//         */
//
//        ArrayList<CellInfoM> cellInfoArrayList = getNewCellInfoArrayListM();
//        if (cellInfoArrayList.size() > 0) {
//            Call<ResponseBody> callableResponse = service.sendCellinfoTable(MsdConfig.getAppId(context), 1, cellInfoArrayList);
//            dumpCallableResponse(callableResponse);
//        }
//
//        ArrayList<SessionInfoM> sessionInfoArrayList = getNewSessionInfoArrayListM();
//        if (sessionInfoArrayList.size() > 0) {
////            new SessionInfoRequests(this, serverInfo).sendSessionInfoData(sessionInfoArrayList);
//            Call<ResponseBody> callableResponse = service.sendSessionInfo(MsdConfig.getAppId(context), 1, sessionInfoArrayList);
//            dumpCallableResponse(callableResponse);
//        }
//
//
////        ArrayList<Sensor> sensorArrayList=getNewSensorArrayList();
////        if(sensorArrayList.size()>0) {
////            new SensorRequests(context, serverInfo).sendSensorData(sensorArrayList);
////        }
////
////        ArrayList<Catcher> catcherArrayList=getNewCatcherArrayList();
////        if(catcherArrayList.size()>0) {
////            new CatcherRequests(context, serverInfo).sendCatcherData(catcherArrayList);
////        }
////
////        ArrayList<CellInfo> cellInfoArrayList=getNewCellInfoArrayList();
////        if(cellInfoArrayList.size()>0) {
////            new CellInfoRequests(context, serverInfo).sendCellInfoData(cellInfoArrayList);
////        }
////
////        ArrayList<SessionInfo> sessionInfoArrayList=getNewSessionInfoArrayList();
////        if(sessionInfoArrayList.size()>0) {
////            new SessionInfoRequests(context, serverInfo).sendSessionInfoData(sessionInfoArrayList);
////        }
//
//    }
//
//    private <T> void dumpCallableResponse(Call<T> callableResponse) {
//        Request request = callableResponse.request();
//        try {
//            Buffer buffer = new Buffer();
//            String show = request.toString() + "headers: " + request.headers() + "\n";
//            if (request.body() != null) {
//                request.body().writeTo(buffer);
//                show += "Body : " + buffer.readString(Charset.defaultCharset());
//            }
//
////            updateResult(show + "\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        callableResponse.enqueue(new Callback<T>() {
//            @Override
//            public void onResponse(Call<T> call, Response<T> response) {
//                if (response.code() == 200) {
//                    setLastSeenSharePre();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<T> call, Throwable t) {
//                String str = "onFailure";
//                if (t instanceof Exception)
//                    str += ((Exception) t).getMessage();
//                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void setLastSeenSharePre() {
//        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        String selectQuery = "SELECT * FROM session_info ORDER BY timestamp DESC LIMIT 1;";
//        @SuppressLint("Recycle")
//        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
//        cursor.moveToLast();
//
//        SharedPreferences.Editor editor = sharedPrefs.edit();
//        if (cursor.getCount() > 0) {
//            editor.putString("lastSessionInfoSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
//            editor.apply();
//        }
//
//
//        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        selectQuery = "SELECT * FROM cell_info ORDER BY first_seen DESC LIMIT 1;";
//        cursor = dbInstanse.rawQuery(selectQuery, null);
//        cursor.moveToLast();
//
//        editor = sharedPrefs.edit();
//        if (cursor.getCount() > 0) {
//            editor.putString("lastCellInfoSentTime", cursor.getString(cursor.getColumnIndex("first_seen")));
//            editor.apply();
//        }
//
//
//        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        selectQuery = "SELECT * FROM catcher ORDER BY timestamp DESC LIMIT 1;";
//        cursor = dbInstanse.rawQuery(selectQuery, null);
//        cursor.moveToLast();
//        editor = sharedPrefs.edit();
//        if (cursor.getCount() > 0) {
//            editor.putString("lastCatcherSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
//            editor.apply();
//        }
//
//
//        dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        selectQuery = "SELECT * FROM sensor_status ORDER BY timestamp DESC LIMIT 1;";
//        cursor = dbInstanse.rawQuery(selectQuery, null);
//        cursor.moveToLast();
//
//        editor = sharedPrefs.edit();
//        if (cursor.getCount() > 0) {
//            editor.putString("lastSensorSentTime", cursor.getString(cursor.getColumnIndex("timestamp")));
//            editor.apply();
//        }
//    }
//
//    private ArrayList<SessionInfoM> getNewSessionInfoArrayListM() {
//        ArrayList<SessionInfoM> sessionInfoArrayList;
//        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        String lastSessionInfoSentTime = sharedPrefs.getString("lastSessionInfoSentTime", "2016-01-01 01:00:00");
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        sessionInfoArrayList = msdOpenHelper.getUnsentSessionInfos(dbInstanse, lastSessionInfoSentTime);
//
//        return sessionInfoArrayList;
//    }
//
//    private ArrayList<CellInfoM> getNewCellInfoArrayListM() {
//        ArrayList<CellInfoM> cellInfoArrayList;
//        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        cellInfoArrayList = msdOpenHelper.getUnsentCellInfosM(dbInstanse, lastCellInfoSentTime);
//        return cellInfoArrayList;
//    }
//
//    private ArrayList<CatcherM> getNewCatcherArrayListM() {
//
//        ArrayList<CatcherM> catcherArrayList;
//        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        String lastCatcherSentTime = sharedPrefs.getString("lastCatcherSentTime", "2016-01-01 01:00:00");
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        catcherArrayList = msdOpenHelper.getUnsentCatchersM(dbInstanse, lastCatcherSentTime);
//        return catcherArrayList;
//
//    }
//
//    private ArrayList<SensorM> getNewSensorArrayListM() {
//        ArrayList<SensorM> sensorArrayList;
//        try {
//            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//            String lastSensorSentTime = sharedPrefs.getString("lastSensorSentTime", "2016-01-01 01:00:00");
//            MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//            SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//            sensorArrayList = msdOpenHelper.getUnsentSensorsM(dbInstanse, lastSensorSentTime);
//        } catch (NullPointerException e) {
//            return null;
//        }
//        return sensorArrayList;
//    }
////    private ArrayList<SessionInfo> getNewSessionInfoArrayList() {
////        ArrayList<SessionInfo> sessionInfoArrayList=new ArrayList<>();
////        String lastSessionInfoSentTime=sharedPrefs.getString("lastSessionInfoSentTime","2016-01-01 01:00:00");
////        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
////        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
////        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
////        sessionInfoArrayList = msdOpenHelper.getUnsentSessionInfos(dbInstanse, lastSessionInfoSentTime);
////
////        return sessionInfoArrayList;
////    }
//
//    private ArrayList<CellInfo> getNewCellInfoArrayList() {
//        ArrayList<CellInfo> cellInfoArrayList = new ArrayList<>();
//        String lastCellInfoSentTime = sharedPrefs.getString("lastCellInfoSentTime", "2016-01-01 01:00:00");
//        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
//        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
//        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
//        cellInfoArrayList = msdOpenHelper.getUnsentCellInfos(dbInstanse, lastCellInfoSentTime);
//
//        return cellInfoArrayList;
//    }
//
////    private ArrayList<Catcher> getNewCatcherArrayList() {
////
////        ArrayList<Catcher> catcherArrayList=new ArrayList<>();
////        String lastCatcherSentTime=sharedPrefs.getString("lastCatcherSentTime","2016-01-01 01:00:00");
////        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
////        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
////        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
////        catcherArrayList = msdOpenHelper.getUnsentCatchers(dbInstanse, lastCatcherSentTime);
////
////        return catcherArrayList;
////
////    }
//
////    private ArrayList<Sensor> getNewSensorArrayList() {
////        ArrayList<Sensor> sensorArrayList=new ArrayList<>();
////        String lastSensorSentTime=sharedPrefs.getString("lastSensorSentTime","2016-01-01 01:00:00");
////        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
////        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
////        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
////        sensorArrayList = msdOpenHelper.getUnsentSensors(dbInstanse, lastSensorSentTime);
////
////        return sensorArrayList;
////
////    }
//
//
//    public boolean isTableExists(Context context, String tableName, SQLiteDatabase db) {
//        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
//        if (cursor != null) {
//            if (cursor.getCount() > 0) {
//                cursor.close();
//                // Toast.makeText(context,"exist",Toast.LENGTH_SHORT).show();
//                return true;
//            }
//            cursor.close();
//        }
//        //Toast.makeText(context,"not  exist",Toast.LENGTH_SHORT).show();
//        return false;
//    }
//
//    public void sendtoserverbtn() {
////        Log.d(TAG, "sendtoserverbtn");
//
//        this.context = context;
//        sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        String ip = sharedPrefs.getString("ip", "5.56.135.20");
//        String port = sharedPrefs.getString("port", "8443");
//        ServerInfo serverInfo = new ServerInfo(ip, port);
//
//        /*ArrayList<Sensor> sensorArrayList=getNewSensorArrayList();
//        if(sensorArrayList.size()>0) {
//            new SensorRequests(context, serverInfo).sendSensorData(sensorArrayList);
//        }
//
//        ArrayList<Catcher> catcherArrayList=getNewCatcherArrayList();
//        if(catcherArrayList.size()>0) {
//            new CatcherRequests(context, serverInfo).sendCatcherData(catcherArrayList);
//        }
//
//        ArrayList<CellInfo> cellInfoArrayList=getNewCellInfoArrayList();
//        if(cellInfoArrayList.size()>0) {
//            new CellInfoRequests(context, serverInfo).sendCellInfoData(cellInfoArrayList);
//        }
//
//        ArrayList<SessionInfo> sessionInfoArrayList=getNewSessionInfoArrayList();
//        if(sessionInfoArrayList.size()>0) {
//            new SessionInfoRequests(context, serverInfo).sendSessionInfoData(sessionInfoArrayList);
//        }*/
//    }
//
//
//}
