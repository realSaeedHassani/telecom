package ir.telecom.sensor;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Bahar on 6/10/2018.
 */

public class SendToServerAlarmManager {
    private static SendToServerAlarmManager myObj;


    /**
     * private constructor
     */
    private SendToServerAlarmManager() {
    }


    /**
     * static method to get instanse
     *
     * @return obj of AccountingAlarmManager
     */
    public static SendToServerAlarmManager getInstance() {
        if (myObj == null) {
            myObj = new SendToServerAlarmManager();
        }
        return myObj;
    }


    /**
     * set daily reminder
     *
     * @param context
     */
    public void setReminder(Context context) {

        SharedPreferences sharedPrefs =  context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        int interval=Integer.parseInt(sharedPrefs.getString("updateInterval","5"));

        Calendar calendar = Calendar.getInstance();
        Calendar setcalendar = Calendar.getInstance();

        setcalendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        setcalendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
        setcalendar.set(Calendar.SECOND,calendar.get(Calendar.SECOND));

        // cancel already scheduled reminders
        cancelReminder(context);


        ComponentName receiver = new ComponentName(context, SendToServerAlarmReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        Intent intent1 = new Intent(context, SendToServerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        android.app.AlarmManager am = (android.app.AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.setRepeating(android.app.AlarmManager.RTC_WAKEUP, setcalendar.getTimeInMillis()+(long) interval *1000, (long) interval *60*1000, pendingIntent);

    }



    /**
     * cancel reminder
     *
     * @param context
     */
    public void cancelReminder(Context context) {

//        Log.d("salam","cancel sendtoserver Reminder");

        ComponentName receiver = new ComponentName(context, SendToServerAlarmReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent1 = new Intent(context, SendToServerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        android.app.AlarmManager am = (android.app.AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }


}
