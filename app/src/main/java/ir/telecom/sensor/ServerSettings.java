package ir.telecom.sensor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdConfig;

public class ServerSettings extends BaseActivity {
    private MsdConfig msdConfig;
    EditText ip;
    EditText port;
    EditText updateInterval;
    Button btn;
    SharedPreferences sharedPrefs;
    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    RadioButton rb1;
    RadioButton rb2;
    EditText sensor_id;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_server_setting);
        initial();
        ip.requestFocus();


        setToolbar();
        setDrawer(ServerSettings.this,toolbar);

        sharedPrefs =  ServerSettings.this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);

        /**
         * Check if ip and port are saved , set them on edittexts
         */
        msdConfig=new MsdConfig();
        sensor_id.setText(msdConfig.getAppId(ServerSettings.this));

        String ip1 = sharedPrefs.getString("ip","");
        String port1 = sharedPrefs.getString("port","");
        String updateInterval1 = sharedPrefs.getString("updateInterval","5");
        String protocol1=sharedPrefs.getString("protocol","https");

        if (ip1.length() != 0) {
            ip.setText(ip1);
        }
        if (port1.length() != 0) {
            port.setText(port1);
        }

        if (updateInterval1.length() != 0) {
            updateInterval.setText(updateInterval1);
        }

        if(protocol1.equals("http")){
            rb1.setChecked(true);

        }else if(protocol1.equals("https")){
            rb2.setChecked(true);
        }else{
            rb2.setChecked(true);
        }



        port.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    String portStr = s.toString().trim();
                    if (portStr.contains(".") || Integer.parseInt(portStr) > 65535 || Integer.parseInt(portStr) == 0
                            || ((portStr.length() > 1) && (portStr.substring(0, 1)).equals("0"))) {
                        Toasty.error(ServerSettings.this, "Please input valid Port number", Toast.LENGTH_LONG)
                                .show();
                        port.setTextColor(Color.RED);

                    } else {
                        port.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });

        updateInterval.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String sessionRefereshTimeStr = s.toString().trim();
                if (sessionRefereshTimeStr.length() > 0) {
                    if (Integer.parseInt(sessionRefereshTimeStr) < 1 || Integer.parseInt(sessionRefereshTimeStr) > 600) {
                        Toasty.warning(ServerSettings.this, "Update interval time must be between 30 and 6000 minutes. Please select another", Toast.LENGTH_SHORT).show();
                        updateInterval.setTextColor(Color.RED);
                    } else {
                        updateInterval.setTextColor(Color.parseColor("#006978"));
                    }
                }
                flag=true;
            }
        });
    }

    /**
     * initial elements
     */
    private void initial() {

        sensor_id=(EditText) findViewById(R.id.sensor_id_edit_text);

        ip = (EditText) findViewById(R.id.ip_host_edittext);
        ip.setText("5.56.135.20");
        port = (EditText) findViewById(R.id.port_edittext);
        updateInterval=(EditText) findViewById(R.id.update_rate_edittext);
        rb1=(RadioButton) findViewById(R.id.http);
        rb2=(RadioButton) findViewById(R.id.https);
    }

    /**
     * Check IP is valid or not.
     *
     * @param ip
     * @return boolean valid parameter
     */
    public boolean validateIP(String ip) {
        boolean valid = true;
//        String[] split = ip.split("\\.");
//        for (int i = 0; i < 4; i++) {
//            if (Integer.parseInt(split[i]) > 255 || ((split[i].length() > 1) && (split[i].substring(0, 1)).equals("0"))) {
//                valid = false;
//            }
//        }
        return true;
    }

    /**
     * check Port
     *
     * @param port
     * @return
     */
    public boolean validatePort(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 65535 || Integer.parseInt(port) == 0) {
            valid = false;
        } else if ((port.length() > 1) && (port.substring(0, 1)).equals("0")) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }

    public boolean validateInterval(String value){
        boolean valid1;
        if (value.contains(".")) {
            valid1 = false;
        } else if (Integer.parseInt(value) > 600 || Integer.parseInt(value) < 1) {
            valid1 = false;
        } else {
            valid1 = true;
        }
        return valid1;
    }


    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);
        toolbarDone.setVisibility(View.VISIBLE);
        toolbarTittle.setText("Server Setting");

        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        String sensorIdStr=sensor_id.getText().toString();

                        final SharedPreferences.Editor editor = sharedPrefs.edit();
                        String ipStr=ip.getText().toString();//  getText(ServerSettings.this).toString();
                        String portStr=port.getText().toString();
                        String updateIntervalStr=updateInterval.getText().toString();
                        String protocol;
                        if(rb2.isChecked()){
                            protocol="https";
                        }else{
                            protocol="http";
                        }

                        if (ipStr.length() == 0 || portStr.length() == 0 || sensorIdStr.length()==0 ) {
                            Toasty.error(ServerSettings.this, "Please fill all the fields", Toast.LENGTH_LONG).show();
                        } else if (!validateIP(ipStr)) {
                            Toasty.error(ServerSettings.this, "IP is not valid", Toast.LENGTH_LONG).show();
                        } else if (!validatePort(portStr)) {
                            Toasty.error(ServerSettings.this, "Port is not valid", Toast.LENGTH_LONG).show();
                        } else if (!validateInterval(updateIntervalStr)) {
                            Toasty.error(ServerSettings.this, "Update interval time must be between 30 and 6000 minutes. Please select another", Toast.LENGTH_LONG).show();
                        } else {
                            editor.putString("ip",ipStr);
                            editor.putString("port",portStr);
                            editor.putString("updateInterval",updateIntervalStr);
                            editor.putString("protocol",protocol);
                            editor.commit();
                            if(flag){
                                SendToServerAlarmManager alarmManager = SendToServerAlarmManager.getInstance();
                                alarmManager.setReminder(ServerSettings.this);
                            }

                            MsdConfig.setAppId(ServerSettings.this,sensorIdStr);

                            Toasty.success(ServerSettings.this,"Chanses saved.",Toast.LENGTH_LONG).show();

                        }

            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });


    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ServerSettings.this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

}

