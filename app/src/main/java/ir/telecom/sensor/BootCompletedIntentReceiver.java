package ir.telecom.sensor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ir.telecom.sensor.qdmon.MsdService;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.Utils;

public class BootCompletedIntentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if (!MsdConfig.getFirstRun(context) && MsdConfig.getStartOnBoot(context)) {

                //do not start background service, if device is incompatible and firmware did not change
                String currentFirmware = Utils.getFirmwareInformation();
                String lastFirmware = MsdConfig.getLastFirmwareInformation(context);
                if (lastFirmware != null) {
                    if (lastFirmware.equals(currentFirmware) && MsdConfig.getDeviceIncompatible(context)) {
                        //do not start service
                        return;
                    }
                } else {
                    //not able to tell if there was a firmware update or not
                    if (MsdConfig.getDeviceIncompatible(context)) {
                        //do not start service
                        return;
                    }
                }

                //start service
                Intent i = new Intent(context, MsdService.class);
                i.setAction("android.intent.action.BOOT_COMPLETED");
                context.startService(i);
            }
        }
    }
}
