package ir.telecom.sensor.network;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import ir.telecom.sensor.model.ServerInfo;

/**
 * Created by Bahar on 4/30/2018.
 */

public class AsyncConnect extends AsyncTask<String, Integer, HTTPResponse> {

    private static final String TAG = AsyncConnect.class.getName();
    private AsyncConnect.ShowResponse delegate = null;
    private ServerInfo serverInfo;
    private HTTPRequest httpRequest;
    private String protocolName;


    public AsyncConnect(ShowResponse fromClass, String protocolName, ServerInfo serverInfo,
                        HTTPRequest httpRequest) {

        this.delegate = fromClass;
        this.serverInfo = serverInfo;
        this.httpRequest = httpRequest;
        this.protocolName = protocolName;
    }
//
//    public AsyncConnect(ShowResponse fromClass, ServerInfo serverInfo, HTTPRequest httpRequest) {
//
//        this(fromClass,"https",serverInfo,httpRequest);
//    }

    /**
     * onCancelled
     */
    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.d("asynctask","AsyncConnect cancelled");
    }

    @Override
    protected HTTPResponse doInBackground(String... params) {
        Log.d("connect","connect starts");
        HTTPConnection connection;

        if (protocolName.equals("http")) {
            connection = new HTTPConnection(serverInfo);
        } else {
            connection = new HTTPSConnection(serverInfo);
        }

        String httpResult;
        try {

         httpResult = connection.doRequest(httpRequest);



            writeToLogFile(String.valueOf(httpRequest));

            HTTPResponse response = new HTTPResponse(httpResult);
            //response.extractAttributes();
            return response;

        } catch (ConnFailException e) {
            return new ConnFailResponse(e.getMessage());
        }
    }
    public void writeToLogFile(String data)
    {
        Log.i("thread","log2");
        File dir = new File (Environment.getExternalStorageDirectory()+ "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "logdata.txt");

        if(!file.exists())
        {
            try {
                file.createNewFile();

            }
            catch (Exception e)
            {
                Log.d("Exception",e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut= new FileOutputStream(file,true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    @Override
    protected void onPostExecute(HTTPResponse response) {
        if(delegate!=null){
            delegate.getPostResponse(response);
        }
    }

    /**
     * Difine interface
     */
    public interface ShowResponse {

        public void getPostResponse(HTTPResponse response);

    }


}



