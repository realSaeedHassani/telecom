package ir.telecom.sensor.network;

/**
 * ConnFailResponse--- connection fail Response class
 *
 * @author Bahar
 */
public class ConnFailResponse extends HTTPResponse {

    private String message;

    /**
     *constructor
     */
    public ConnFailResponse(String httpResult) {

       super(httpResult);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public void extractAttributes() {
                message=httpResult;
    }
}
