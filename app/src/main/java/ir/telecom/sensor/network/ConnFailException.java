package ir.telecom.sensor.network;

/**
 * ConnFailException---class for connection failed exception
 */

public class ConnFailException extends Exception {

    private final String message;

    /**
     * constructor
     *
     * @param message
     */
    public ConnFailException(String message) {
        this.message=message;
    }

    /**
     * get message method
     *
     * @return String message
     */
    @Override
    public String getMessage() {
        return message;
    }


}
