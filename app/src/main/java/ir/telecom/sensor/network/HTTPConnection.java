package ir.telecom.sensor.network;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.apache.poi.ss.formula.functions.Na;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import ir.telecom.sensor.DashboardActivity;
import ir.telecom.sensor.model.NameValuePair;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.util.Utils;

/**
 * HTTPConnection---class to create httpconnection , implement GET and POST methods
 *
 * @author Bahar
 */

public class HTTPConnection {

    private static final String TAG =  HTTPConnection.class.getName();
    private ServerInfo serverInfo;
    protected String protocolName;

    /**
     * constructor
     */

    public HTTPConnection(ServerInfo serverInfo) {
        this.protocolName = "http";
        this.serverInfo = serverInfo;
    }

    /**
     * do Request method to create a Request
     *
     * @param httpRequest
     * @throws ConnFailException
     */
    public String doRequest(HTTPRequest httpRequest) throws ConnFailException {
        String httpResult="";
if(httpRequest!=null) {
    if (httpRequest.getCommand().equalsIgnoreCase("GET")) {
        httpResult = doGet(httpRequest);
    } else if (httpRequest.getCommand().equalsIgnoreCase("POST")) {
        httpResult = doPost(httpRequest);
    } else {
        httpResult = "";
    }
}
        return httpResult;
    }


    /**
     * create Http connection using GET method
     *
     * @param httpRequest
     * @return string buffer.toString()
     */

    public String doGet(HTTPRequest httpRequest) throws ConnFailException {


        String url = protocolName + "://" + serverInfo.getIp() + ":" + serverInfo.getPort() + httpRequest.getPath() + getGetDataString(httpRequest.getPairs());
        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = null;
            conn = (HttpURLConnection) urlObj.openConnection();
            conn.setConnectTimeout(20 * 1000);

            int responseCode = conn.getResponseCode();
            String line;
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = null;
            if (responseCode == HttpURLConnection.HTTP_OK) {
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                reader.close();
                conn.disconnect();
                return buffer.toString();
            } else {
                return "request failed error:" + responseCode;
            }

        } catch (MalformedURLException e) {
            throw new ConnFailException("Connection problem was occured.");
        } catch (java.net.SocketTimeoutException e) {
            throw new ConnFailException("The connection has timed out ");
        } catch (IOException e) {
            throw new ConnFailException("Connection problem was occured");
        } catch (Exception localException) {
            throw new ConnFailException("Connection problem was occured. ");
        }

    }

    private String getGetDataString(List<NameValuePair> pairs) {

        Log.e("getGetDataString: ", pairs.toString());
        for (NameValuePair n : pairs)
            Log.e(">> NamePairs: ", n.getName()+ " : " + n.getValue());
        if (pairs == null) {
            return "";
        } else {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (NameValuePair pair : pairs) {
                if (first) {
                    result.append("?");
                    first = false;
                } else {
                    result.append("&");
                }

                result.append(pair.getName());
                result.append("=");
                result.append(pair.getValue());
            }
            String r = result.toString();

            Log.e(">> getGetDataString: ", r);
            return r;
        }
    }

    /**
     * create Http connection using POST method
     *
     * @param httpRequest
     * @return
     * @throws Exception
     */

    public String doPost(HTTPRequest httpRequest) throws ConnFailException {
        String url = protocolName + "://" + serverInfo.getIp() + ":" + serverInfo.getPort() + httpRequest.getPath()+getGetDataString(httpRequest.getPairs());

        try {

            URL urlObj = new URL(url);
            Log.e(">> URL: "+TAG, urlObj.getFile());
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","*/*");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            Log.e(">> OutputStream: ", os.toString());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(httpRequest.getBody());


            Utils.writeToFile(httpRequest.getBody(), "HttpConnection.html");

            writer.flush();
            writer.close();

            os.close();

            int responseCode = conn.getResponseCode();
            System.out.println("URL: "+url);
            System.out.println(responseCode);
            System.out.println("###################");
            Log.d("response ", String.valueOf(responseCode));


            System.out.println("IN tHE IF");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            String line = "";

            while ((line = in.readLine()) != null) {
                System.out.println("READING RESPONSE BODY");
                sb.append(line);
                break;
            }

            in.close();
            System.out.println("RESPONSE MESSAGE: "+sb.toString());
            return sb.toString();

        } catch (MalformedURLException e) {
            throw new ConnFailException("Connection problem was occured.");
        } catch (java.net.SocketTimeoutException e) {
            throw new ConnFailException("The connection has timed out ");
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConnFailException("Connection problem was occured");
        } catch (Exception localException) {
            localException.printStackTrace();
            throw new ConnFailException("Connection problem was occured. ");
        }
    }

    private String getPostDataString(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        if (params == null) {
            return "";
        } else {

            for (NameValuePair pair : params) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }
            String r = result.toString();
            return r;
        }
    }


}

