package ir.telecom.sensor.network;


/**
 * HTTPResponse---class for http response
 *
 * @author Bahar
 */

public class HTTPResponse {

    protected boolean success;
    protected String httpResult;


    /**
     * constructor
     */
    public HTTPResponse(String httpResult) {
        this.httpResult=httpResult;
    }

    /**
     * get success attribute
     *
     * @return boolean success
     */
    public boolean getSuccess() {
        return success;
    }

    /**
     * set success attribute
     *
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * get httpResult method
     *
     * @return String
     */
    public String getHttpResult() {
        return httpResult;
    }

    /**
     * set httpResult method
     *
     * @return String
     */
    public void setHttpResult(String httpResult) {
        this.httpResult = httpResult;
    }
}

