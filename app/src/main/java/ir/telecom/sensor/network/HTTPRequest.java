package ir.telecom.sensor.network;


import java.util.List;

import ir.telecom.sensor.model.NameValuePair;

/**
 * HTTPRequest---class to create a request string for register,unregister,view Account
 *
 * @author Bahar
 */

public class HTTPRequest {

    protected String command;//get or post
    protected List<NameValuePair> pairs;
    protected String path;

    protected String body;
    //protected String sessionId;

    /**
     * constructor
     *
     * @param command
     * @param pairs
     * @param path
     */
    public HTTPRequest(String command,String body, List<NameValuePair> pairs, String path) {
        this.command = command;
        this.pairs = pairs;
        this.path = path;
        this.body=body;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<NameValuePair> getPairs() {
        return pairs;
    }

    public void setPairs(List<NameValuePair> pairs) {
        this.pairs = pairs;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


}

