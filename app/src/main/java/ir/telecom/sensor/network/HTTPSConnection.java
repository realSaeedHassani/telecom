package ir.telecom.sensor.network;


import android.util.Log;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import ir.telecom.sensor.model.ServerInfo;

/**
 * HTTPSConnection---class to https connection
 *
 * @author Bahar
 */

public class HTTPSConnection extends HTTPConnection {
    /**
     * constructor
     *
     * @param serverInfo
     */
    public HTTPSConnection(ServerInfo serverInfo) {
        super(serverInfo);
        try {
            TrustManager[] trustAllCerts = {new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {

                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs,
                                               String authType) {
                    //checkClientTrusted
                }

                public void checkServerTrusted(X509Certificate[] certs,
                                               String authType) {
                    //checkServerTrusted
                }
            }};
            SSLContext sc = SSLContext.getInstance("SSL");

            HostnameVerifier hv = new HostnameVerifier() {
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            };
            sc.init(null, trustAllCerts, new SecureRandom());

            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception localException) {
            Log.d("localException",localException.getMessage());
        }

        this.protocolName = "https";
    }

}

