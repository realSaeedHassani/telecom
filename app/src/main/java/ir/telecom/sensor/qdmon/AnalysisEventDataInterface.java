package ir.telecom.sensor.qdmon;

import java.util.Vector;

import ir.telecom.sensor.analysis.ImsiCatcher;

//parimah check how many use this.
public interface AnalysisEventDataInterface {

    public Vector<ImsiCatcher> getImsiCatchers(long startTime, long endTime);

}
