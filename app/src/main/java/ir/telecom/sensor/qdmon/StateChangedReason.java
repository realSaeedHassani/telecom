package ir.telecom.sensor.qdmon;
// why state of app chenge just becuase of attck or tesets
public enum StateChangedReason {
    RECORDING_STATE_CHANGED,
    CATCHER_DETECTED,
    SMS_DETECTED,
    SEC_METRICS_CHANGED,
    ANALYSIS_DONE,
    RAT_CHANGED,
    NO_BASEBAND_DATA
}