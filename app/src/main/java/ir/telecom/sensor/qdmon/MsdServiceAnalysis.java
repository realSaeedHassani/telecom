package ir.telecom.sensor.qdmon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ir.telecom.sensor.GpsChangedListener;
import ir.telecom.sensor.util.MsdDatabaseManager;


public class MsdServiceAnalysis implements GpsChangedListener {
    private static double mLongitude = 0.0;
    private static double mLatitude = 0.0;
    private static String TAG = "MsdServiceAnalysis";

    static int index = 0;
    private static Context mContext;

    private static int getLast(SQLiteDatabase db, String tableName) {
        try {
            Cursor c = db.rawQuery("SELECT * FROM " + tableName, null);
            int result = c.getCount();
            c.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalStateException("SQLException in getLast(" + tableName + ",): ", e);
        }
    }

    public static int runCatcherAnalysis(Context context, SQLiteDatabase db) throws Exception {
        mContext = context;
        int before, after;
        if (checkRootCommand() == 0) {
            convertTable();
        }
        before = getLast(db, "catcher");

        MsdSQLiteOpenHelper.readSQLAsset(context, db, "catcher_analysis.sql", false);
        MsdSQLiteOpenHelper.readSQLAsset(context, db, "add_sp2catcher.sql", false);

        // Back up Catcher info;
        addSignalPowerToChatcher(db);
        after = getLast(db, "catcher");
        if (after != before) {
            return after - before;
        }
        Log.e(">> Update IMSI: ", "*****");

        updatecatcher(context, db);
        return 0;
    }
    public static String convertDate(String timeStamp) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        return destFormat.format(parsed);
    }

    private static void convertTable() {

        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        // Update session_info
        List<Integer> lstFlag;
        Cursor cursor = db.rawQuery("select timestamp, flag from session_info", null);
        List<String> lstTimestamp = new ArrayList<>();
        lstFlag = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                lstTimestamp.add(cursor.getString(cursor.getColumnIndex("timestamp")));
                lstFlag.add(cursor.getInt(cursor.getColumnIndex("flag")));
                cursor.moveToNext();
            }
        }

        for (int index = 0; index < lstTimestamp.size(); ++index) {
            if (lstFlag.size() > 0) {
                if (lstFlag.get(index) == 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("timestamp", convertDate(lstTimestamp.get(index)));
                    contentValues.put("flag", 1);
                    db.update("session_info", contentValues, "flag = " + 0, null);
                }
            }
        }
    }

    public static int checkRootCommand() {

        Process exec = null;
        try {

            exec = Runtime.getRuntime().exec(new String[]{"su"});

            final OutputStreamWriter out = new OutputStreamWriter(exec.getOutputStream());
            out.write("exit");
            out.flush();

            Log.i(">> ROOT: ", "su command executed successfully");
            return 0; // returns zero when the command is executed successfully
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (exec != null) {
                try {
                    exec.destroy();
                } catch (Exception ignored) {
                }
            }
        }
        return 1; //returns one when the command execution fails
    }
    private static void addSignalPowerToChatcher(SQLiteDatabase db) {

//        db.execSQL("DELETE FROM catcher_with_signal_power " +
//                "WHERE cid = 0");
        db.execSQL(
                " UPDATE catcher_with_signal_power  \n" +
                        " SET pwr=" + getSignalPower() + "\n" +
                        " WHERE pwr = 0");

        try {
            if (isOnGps()) {
                Log.e(TAG, "*** GPS is on! ***");
                Log.e(TAG, mLongitude +
                        " / " + mLatitude);
                db.execSQL(
                        " update catcher_with_signal_power  \n" +
                                " set longitude=" + mLongitude +
                                ", latitude=" + mLatitude +
                                " where longitude = 0 and latitude = 0");
            } else {
                Log.e(TAG, "GPS is off!");
                db.execSQL(
                        " update catcher_with_signal_power  \n" +
                                " set longitude=" + 0 +
                                ", latitude=" + 0 +
                                " where longitude = 0 and latitude = 0");
            }
        } catch (Exception ex) {
            Log.e(TAG, ">> Error: " + ex.getMessage());
        }
    }
    /**
     * Check for Gps, ON/OFF
     */
    private static boolean isOnGps() {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    //    public  static void updatecatcher(Context context,SQLiteDatabase dbInstanse) {
//        int a2=0;
//                int a4=0;
//                int a5=0;
//        int k1=0;
//        int k2=0;
//        int c1=0;
//        int c2=0;
//        int c4=0;
//        int c5=0;
//        int t1=0;
//                int t3=0;
//                int t4=0;
//        int r1=0;
//        int r2=0;
//
//        SharedPreferences totalscore = context.getSharedPreferences("TotalScore", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        String scoreText = totalscore.getString("TotalScore", "0");
//        String names[] = {"Inconsistent LAC", "Different ARFCNs", "Lonesome LA", "Non Neighbor", "High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher", "Low t3212", "Empty Paging", "Orphaned Channel", "Inconsistent Neighbor", "High Paging"};
//        Cursor cursor = dbInstanse.rawQuery("select * from catcher", null);
//        if (cursor != null && cursor.moveToFirst()) {
//            cursor.moveToLast();
//
//            SharedPreferences paramscore = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//            if (paramscore.getBoolean(names[0], true)) {
//                a2 = cursor.getInt(cursor.getColumnIndex("a2"));
//            } else {
//                a2 = 0;
//            }
//            if (paramscore.getBoolean(names[1], true)) {
//                a4 = cursor.getInt(cursor.getColumnIndex("a4"));
//            } else {
//                a4 = 0;
//            }
//            if (paramscore.getBoolean(names[2], true)) {
//                a5 = cursor.getInt(cursor.getColumnIndex("a5"));
//            } else {
//                a5 = 0;
//            }
//            if (paramscore.getBoolean(names[3], true)) {
//                k1 = cursor.getInt(cursor.getColumnIndex("k1"));
//            } else {
//                k1 = 0;
//            }
//            if (paramscore.getBoolean(names[4], true)) {
//                k2 = cursor.getInt(cursor.getColumnIndex("k2"));
//            } else {
//                k2 = 0;
//            }
//            if (paramscore.getBoolean(names[5], true)) {
//                c1 = cursor.getInt(cursor.getColumnIndex("c1"));
//            } else {
//                c1 = 0;
//            }
//            if (paramscore.getBoolean(names[6], true)) {
//                c2 = cursor.getInt(cursor.getColumnIndex("c2"));
//            } else {
//                c2 = 0;
//            }
////        if(paramscore.getBoolean(names[7],true)) {
////            c3=cursor.getInt(cursor.getColumnIndex("c3"));
////        }
////        else
////        {
////            c3=0;
////        }
//            if (paramscore.getBoolean(names[7], true)) {
//                c4 = cursor.getInt(cursor.getColumnIndex("c4"));
//            } else {
//                c4 = 0;
//            }
//            if (paramscore.getBoolean(names[8], true)) {
//                c5 = cursor.getInt(cursor.getColumnIndex("c5"));
//            } else {
//                c5 = 0;
//            }
//            if (paramscore.getBoolean(names[9], true)) {
//                t1 = cursor.getInt(cursor.getColumnIndex("t1"));
//            } else {
//                t1 = 0;
//            }
//            if (paramscore.getBoolean(names[10], true)) {
//                t3 = cursor.getInt(cursor.getColumnIndex("t3"));
//            } else {
//                t3 = 0;
//            }
//            if (paramscore.getBoolean(names[11], true)) {
//                t4 = cursor.getInt(cursor.getColumnIndex("t4"));
//            } else {
//                t4 = 0;
//            }
//            if (paramscore.getBoolean(names[12], true)) {
//                r1 = cursor.getInt(cursor.getColumnIndex("r1"));
//            } else {
//                r1 = 0;
//            }
//            if (paramscore.getBoolean(names[13], true)) {
//                r2 = cursor.getInt(cursor.getColumnIndex("r2"));
//            } else {
//                r2 = 0;
//            }
//        }
//
//        ContentValues values;
//        values=new ContentValues();
//        values.put("a2", a2);
//        values.put("a4", a4);
//        values.put("a5", a5);
//        values.put("k1", k1);
//        values.put("k2", k2);
//        values.put("c1", c1);
//        values.put("c2", c2);
//        values.put("c5", c5);
//        values.put("t1", t1);
//        values.put("t3", t3);
//        values.put("t4", t4);
//        values.put("r1", r1);
//        values.put("r2", r2);
//        values.put("score",scoreText);
////        SharedPreferences sharedPrefs = context.getSharedPreferences("cellid", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
////        int cellid = sharedPrefs.getInt("cellid", 0);
////        String q= "Update TABLE if exists mylonesome_lacs";
////        db.execSQL(q);
////        db.execSQL("UPDATE cell_info SET last_seen='timestamp' WHERE id='cellid' ");
////        Cursor catcherid=dbInstanse.rawQuery("",null);
//        int catcherid=cursor.getInt(cursor.getColumnIndex("id"));
//        dbInstanse.update("catcher",values, "id="+catcherid, null);
//    }
    public static void updatecatcher(Context context, SQLiteDatabase dbInstanse) {
        int a2 = 0;
        int a4 = 0;
        int a5 = 0;
        int k1 = 0;
        int k2 = 0;
        int c1 = 0;
        int c2 = 0;
        int c4 = 0;
        int c5 = 0;
        int t1 = 0;
        int t3 = 0;
        int t4 = 0;
        int r1 = 0;
        int r2 = 0;

        SharedPreferences totalscore = context.getSharedPreferences("TotalScore", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String scoreText = totalscore.getString("TotalScore", "0");
//        String score = Float.toString(scoreText);
        Float f = Float.parseFloat(scoreText);
        String names[] = {"Inconsistent LAC", "Different ARFCNs", "Lonesome LA", "Non Neighbor", "High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher", "Low t3212", "Empty Paging", "Orphaned Channel", "Inconsistent Neighbor", "High Paging"};
//        Cursor cursor = dbInstanse.rawQuery("select * from catcher", null);
//        if (cursor != null && cursor.moveToFirst()) {
//            cursor.moveToLast();
//
//            SharedPreferences paramscore = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//            if (paramscore.getBoolean(names[0], true)) {
//                a2 = cursor.getInt(cursor.getColumnIndex("a2"));
//            } else {
//                a2 = 0;
//            }
//            if (paramscore.getBoolean(names[1], true)) {
//                a4 = cursor.getInt(cursor.getColumnIndex("a4"));
//            } else {
//                a4 = 0;
//            }
//            if (paramscore.getBoolean(names[2], true)) {
//                a5 = cursor.getInt(cursor.getColumnIndex("a5"));
//            } else {
//                a5 = 0;
//            }
//            if (paramscore.getBoolean(names[3], true)) {
//                k1 = cursor.getInt(cursor.getColumnIndex("k1"));
//            } else {
//                k1 = 0;
//            }
//            if (paramscore.getBoolean(names[4], true)) {
//                k2 = cursor.getInt(cursor.getColumnIndex("k2"));
//            } else {
//                k2 = 0;
//            }
//            if (paramscore.getBoolean(names[5], true)) {
//                c1 = cursor.getInt(cursor.getColumnIndex("c1"));
//            } else {
//                c1 = 0;
//            }
//            if (paramscore.getBoolean(names[6], true)) {
//                c2 = cursor.getInt(cursor.getColumnIndex("c2"));
//            } else {
//                c2 = 0;
//            }
////        if(paramscore.getBoolean(names[7],true)) {
////            c3=cursor.getInt(cursor.getColumnIndex("c3"));
////        }
////        else
////        {
////            c3=0;
////        }
//            if (paramscore.getBoolean(names[7], true)) {
//                c4 = cursor.getInt(cursor.getColumnIndex("c4"));
//            } else {
//                c4 = 0;
//            }
//            if (paramscore.getBoolean(names[8], true)) {
//                c5 = cursor.getInt(cursor.getColumnIndex("c5"));
//            } else {
//                c5 = 0;
//            }
//            if (paramscore.getBoolean(names[9], true)) {
//                t1 = cursor.getInt(cursor.getColumnIndex("t1"));
//            } else {
//                t1 = 0;
//            }
//            if (paramscore.getBoolean(names[10], true)) {
//                t3 = cursor.getInt(cursor.getColumnIndex("t3"));
//            } else {
//                t3 = 0;
//            }
//            if (paramscore.getBoolean(names[11], true)) {
//                t4 = cursor.getInt(cursor.getColumnIndex("t4"));
//            } else {
//                t4 = 0;
//            }
//            if (paramscore.getBoolean(names[12], true)) {
//                r1 = cursor.getInt(cursor.getColumnIndex("r1"));
//            } else {
//                r1 = 0;
//            }
//            if (paramscore.getBoolean(names[13], true)) {
//                r2 = cursor.getInt(cursor.getColumnIndex("r2"));
//            } else {
//                r2 = 1;
//            }
//        }
//
        ContentValues values;
        values = new ContentValues();
//        values.put("timestamp", "2018-12-11 12:45:30");
//        values.put("a2", 5);
//        values.put("a4", a4);
//        values.put("a5", a5);
//        values.put("k1", k1);
//        values.put("k2", k2);
//        values.put("c1", 2);
//        values.put("c2", c2);
//        values.put("c5", c5);
//        values.put("t1", t1);
//        values.put("t3", t3);
//        values.put("t4", t4);
//        values.put("r1", r1);
//        values.put("r2", r2);
//        values.put("score",7);
//        values.put("longitude",12);
//        values.put("latitude",13);
//        dbInstanse.insert("catcher",null,values);
//        SharedPreferences sharedPrefs = context.getSharedPreferences("cellid", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        int cellid = sharedPrefs.getInt("cellid", 0);
//        String q= "Update TABLE if exists mylonesome_lacs";
//        db.execSQL(q);
//        db.execSQL("UPDATE cell_info SET last_seen='timestamp' WHERE id='cellid' ");
//        Cursor catcherid=dbInstanse.rawQuery("",null);
        Cursor cursor = dbInstanse.rawQuery("SELECT * FROM catcher", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();

            SharedPreferences paramscore = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            if (paramscore.getBoolean(names[0], true)) {
                a2 = cursor.getInt(cursor.getColumnIndex("a2"));
            } else {
                a2 = 0;
            }
            if (paramscore.getBoolean(names[1], true)) {
                a4 = cursor.getInt(cursor.getColumnIndex("a4"));
            } else {
                a4 = 0;
            }
            if (paramscore.getBoolean(names[2], true)) {
                a5 = cursor.getInt(cursor.getColumnIndex("a5"));
            } else {
                a5 = 0;
            }
            if (paramscore.getBoolean(names[3], true)) {
                k1 = cursor.getInt(cursor.getColumnIndex("k1"));
            } else {
                k1 = 0;
            }
            if (paramscore.getBoolean(names[4], true)) {
                k2 = cursor.getInt(cursor.getColumnIndex("k2"));
            } else {
                k2 = 0;
            }
            if (paramscore.getBoolean(names[5], true)) {
                c1 = cursor.getInt(cursor.getColumnIndex("c1"));
            } else {
                c1 = 0;
            }
            if (paramscore.getBoolean(names[6], true)) {
                c2 = cursor.getInt(cursor.getColumnIndex("c2"));
            } else {
                c2 = 0;
            }
//        if(paramscore.getBoolean(names[7],true)) {
//            c3=cursor.getInt(cursor.getColumnIndex("c3"));
//        }
//        else
//        {
//            c3=0;
//        }
            if (paramscore.getBoolean(names[7], true)) {
                c4 = cursor.getInt(cursor.getColumnIndex("c4"));
            } else {
                c4 = 0;
            }
            if (paramscore.getBoolean(names[8], true)) {
                c5 = cursor.getInt(cursor.getColumnIndex("c5"));
            } else {
                c5 = 0;
            }
            if (paramscore.getBoolean(names[9], true)) {
                t1 = cursor.getInt(cursor.getColumnIndex("t1"));
            } else {
                t1 = 0;
            }
            if (paramscore.getBoolean(names[10], true)) {
                t3 = cursor.getInt(cursor.getColumnIndex("t3"));
            } else {
                t3 = 0;
            }
            if (paramscore.getBoolean(names[11], true)) {
                t4 = cursor.getInt(cursor.getColumnIndex("t4"));
            } else {
                t4 = 0;
            }
            if (paramscore.getBoolean(names[12], true)) {
                r1 = cursor.getInt(cursor.getColumnIndex("r1"));
            } else {
                r1 = 0;
            }
            if (paramscore.getBoolean(names[13], true)) {
                r2 = cursor.getInt(cursor.getColumnIndex("r2"));
            } else {
                r2 = 1;
            }
        }

//        values.put("timestamp", "2018-12-11 12:45:30");
        values.put("a2", a2);
        values.put("a4", a4);
        values.put("a5", a5);
        values.put("k1", k1);
        values.put("k2", k2);
        values.put("c1", a2);
        values.put("c2", c2);
        values.put("c5", c5);
        values.put("t1", t1);
        values.put("t3", t3);
        values.put("t4", t4);
        values.put("r1", r1);
        values.put("r2", r2);
        values.put("score", f);
//        values.put("longitude",12);
//        values.put("latitude",12);
//        dbInstanse.insert("catcher",null,values);

        int catcherid = cursor.getInt(cursor.getColumnIndex("id"));
//    Log.i("parimahh","catcherid");
        dbInstanse.update("catcher", values, "id=" + catcherid, null);
    }

    public static int getSignalPower() {
        TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        CellSignalStrengthGsm gsm = null;
        @SuppressLint("MissingPermission")
        CellInfo info = tm.getAllCellInfo().get(0);
        if (info instanceof CellInfoGsm)
            gsm = ((CellInfoGsm) info).getCellSignalStrength();
        return gsm.getDbm();
    }

    public static void setLocation(GpsChangedListener gpsChangedListener, double latitude, double longitude) {
        gpsChangedListener.setOnLocationChanged(latitude, longitude);
    }

    @Override
    public void setOnLocationChanged(double latitude, double longitude) {
        mLongitude = longitude;
        mLatitude = latitude;
    }
}
