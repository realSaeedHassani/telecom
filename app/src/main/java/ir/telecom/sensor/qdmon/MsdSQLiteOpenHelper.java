package ir.telecom.sensor.qdmon;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.io.*;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import ir.telecom.sensor.model.Catcher;
import ir.telecom.sensor.model.CellInfo;
import ir.telecom.sensor.ret.model.CatcherM;
import ir.telecom.sensor.ret.model.CellInfoM;
import ir.telecom.sensor.ret.model.SensorM;
import ir.telecom.sensor.ret.model.SessionInfoM;
import ir.telecom.sensor.util.MsdDatabaseManager;
import ir.telecom.sensor.util.Utils;

public class MsdSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "msd.db";
    private static final int DATABASE_VERSION = 24;
    private static final boolean verbose = false;
    private Context context;
    final static String fileName = "cell-info.txt";
    //    Environment.getExternalStorageDirectory() + "/sensor/logdata/");
    final static String path = Environment.getExternalStorageDirectory() + "";
//    File dir = new File (Environment.getExternalStorageDirectory()+ "/sensor/logdata/");
//    final static String TAG = FileHelper.class.getName();
//static Map<String, Class> typeMapper = new HashMap<>();

    static List<String> columns = new ArrayList<>();

    static {
        columns.add("first_seen");
        columns.add("last_seen");
        columns.add("mcc");
        columns.add("mnc");
        columns.add("lac");
        columns.add("cid");
        columns.add("rat");
        columns.add("bcch_arfcn");
        columns.add("c1");
        columns.add("c2");
        columns.add("power_sum");
        columns.add("power_count");
        columns.add("gps_lon");
        columns.add("gps_lat");
        columns.add("msc_ver");
        columns.add("combined");
        columns.add("agch_blocks");
        columns.add("pag_mframes");
        columns.add("t3212");
        columns.add("dtx");
        columns.add("cro");
        columns.add("temp_offset");
        columns.add("pen_time");
        columns.add("pwr_offset");
        columns.add("gprs");
        columns.add("ba_len");
        columns.add("neigh_2");
        columns.add("neigh_2b");
        columns.add("neigh_2t");
        columns.add("neigh_2q");
        columns.add("neigh_5");
        columns.add("neigh_5b");
        columns.add("neigh_5t");
        columns.add("count_si1");
        columns.add("count_si2");
        columns.add("count_si2b");
        columns.add("count_si2t");
        columns.add("count_si2q");
        columns.add("count_si3");
        columns.add("count_si4");
        columns.add("count_si5");
        columns.add("count_si5b");
        columns.add("count_si5t");
        columns.add("count_si6");
        columns.add("count_si13");
        columns.add("si1");
        columns.add("si2");
        columns.add("si2b");
        columns.add("si2t");
        columns.add("si2q");
        columns.add("si3");
        columns.add("si4");
        columns.add("si5");
        columns.add("si5b");
        columns.add("si5t");
        columns.add("si6");
        columns.add("si13");
    }

    public MsdSQLiteOpenHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public static void readSQLAsset(Context context, SQLiteDatabase db, String file, Boolean verbose) throws SQLException, IOException {
//        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, file);

        Long tmp = System.currentTimeMillis();
        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.readSQLAsset(" + file + ") called");

        try {
//            wl.acquire();
            db.beginTransaction();
            String sql = Utils.readFromAssets(context, file);
            if (verbose) {
                Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.readSQLAsset(" + file + "): " + sql);
            }
            for (String statement : sql.split(";")) {
                if (statement.trim().length() > 0 && !statement.trim().startsWith("/*!")) {
                    if (verbose) {
                        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.readSQLAsset(" + file + "): statement=" + statement);
                    }
                    long startTime = System.currentTimeMillis();
                    db.execSQL(statement);
                    if (verbose) {
                        long durationMs = System.currentTimeMillis() - startTime;
                        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.readSQLAsset(" + file + "): statement took " + durationMs);
                    }
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
//            wl.release();
        }
        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.readSQLAsset(" + file + ") done, took " + (System.currentTimeMillis() - tmp) + "ms");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.onCreate() called");
        try {
            readSQLAsset(context, db, "catcher_wsp_table.sql", verbose);
            readSQLAsset(context, db, "si.sql", verbose);
            readSQLAsset(context, db, "si_loc.sql", verbose);
            readSQLAsset(context, db, "cell_info.sql", verbose);
            readSQLAsset(context, db, "config.sql", verbose);
            readSQLAsset(context, db, "Telephonyparams.sql", false);
//            readSQLAsset(context, db, "Telephonyparams.sql", false);
            readSQLAsset(context, db, "analysis_tables.sql", verbose);
//            readSQLAsset(context, db, "neighbor.sql", false);
            readSQLAsset(context, db, "local.sqlx", verbose);
            readSQLAsset(context, db, "sensor.sql", false);
//            readSQLAsset(context, db, "trigger.sql", false);

        } catch (Exception e) {
            Log.e("MSD", "Failed to create database", e);
        }


        Log.i(MsdService.TAG, "MsdSQLiteOpenHelper.onCreate() done");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public float getThresholdConfig(SQLiteDatabase db) {
        Cursor c = db.rawQuery("select catcher_min_Score from  config", null);

        if (c != null) {
            c.moveToFirst();
        }
        return c.getFloat(0);
    }

    public Cursor selectFullTables(SQLiteDatabase db, String TName) {
        Cursor c = null;
        try {
            if (TName.equals("cell_info"))
                c = db.rawQuery("select * from  " + TName + " ORDER BY first_seen DESC", null);
            else if (TName.equals("config"))
                c = db.rawQuery("select * from  " + TName, null);
            else if (TName.equals("cell_info_convert_time"))
                c = db.rawQuery("select * from  " + TName, null);
            else
                c = db.rawQuery("select * from  " + TName + " ORDER BY timestamp DESC", null);
        }catch (Exception ex){
            c = db.rawQuery("select * from cell_info ", null);
        }
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }


    /**
     * get Sensor update interval
     *
     * @return int
     */
    public int getSensorUpdateInterval(SQLiteDatabase db) {

        String selectQuery = "SELECT sensor_update_interval FROM  config";
        Cursor cursor = db.rawQuery(selectQuery, null);
        int interval = 60;
        if (cursor.moveToFirst()) {
            do {

                interval = (cursor.getInt(cursor.getColumnIndex("sensor_update_interval")));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return interval;
    }


    //pari
    public int getSensorInfoInterval(SQLiteDatabase db) {
        SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        int interval = Integer.parseInt(sharedPrefs.getString("sensorinfoInterval", "60"));
        return interval;
    }

    //

    public String updateConfig(SQLiteDatabase db, String name[], String values[]) {
        ContentValues cv = new ContentValues();

        for (int i = 0; i < 13; i++) {
            if (i == 8 || i == 9) {
                cv.put(name[i], Float.valueOf(values[i]));
            } else
                cv.put(name[i], Integer.valueOf(values[i]));


        }


        db.update("config", cv, null, null);


        Cursor c = selectFullTables(db, "config");
        String a = String.valueOf(c.getFloat(0));
        return a;

    }

    public void insertarfcn_list(SQLiteDatabase db, int id, int arfcn) {

        ContentValues values;
        values = new ContentValues();
        values.put("id", id);
        values.put("source", "tele");
        values.put("arfcn", arfcn);
        db.insert("arfcn_list", null, values);
    }


    public void insertSensor(SQLiteDatabase db, boolean serviceStatus, String timestamp, int battery_level, Double longitude, Double latitude, float cpuUsage, float memoryUsage, float diskUsage) {
        ContentValues values;
        values = new ContentValues();
        values.put("service_status", serviceStatus);
        values.put("timestamp", getTime(timestamp));
        values.put("battery_level", battery_level);
        values.put("longitude", longitude);
        values.put("latitude", latitude);
        values.put("cpu_usage", cpuUsage);
        values.put("memory_usage", memoryUsage);
        values.put("disk_usage", diskUsage);
        db.insert("sensor_status", null, values);

    }

    /**
     * Convert date YYYY-MM-DDTHH:MM:SS.000+0000
     */
    private String getTime(String timestamp) {
        ;
        String[] sub = convertDate(timestamp).split("[T.]");
        return sub[0] + " " + sub[1];
    }

    public void insertSensorinfo(SQLiteDatabase db, String timestamp, Double longitude, Double latitude, int mcc, int mnc, int lac, int cid, int signalstrength, int sizeneighboring, int arfcn, int RAT) {
        ContentValues values;
        values = new ContentValues();

        values.put("timestamp", timestamp);
        values.put("longitude", longitude);
        values.put("latitude", latitude);
        values.put("Mcc", mcc);
        values.put("Mnc", mnc);
        values.put("Lac", lac);
        values.put("Cid", cid);
        values.put("SignalStrength ", signalstrength);
        values.put("SizeNeighboring", sizeneighboring);
        values.put("ARFCN", arfcn);
        values.put("RAT", RAT);
        db.insert("sensor_info", null, values);

    }

    public void insertNeighborinfo(SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int signalstrength, int arfcn, int cellid) {
        ContentValues values;
        values = new ContentValues();

        values.put("timestamp", timestamp);
        values.put("Mcc", mcc);
        values.put("Mnc", mnc);
        values.put("Lac", lac);
        values.put("Cid", cid);
        values.put("SignalStrength ", signalstrength);
        values.put("ARFCN", arfcn);
        values.put("cellId", cellid);
        db.insert("neighbor_info", null, values);
    }

    public void insertSession(SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int sizeneighboring, int arfcn, int psc, int rat, int signalstrenght, double longitude, double latitude, int neighborsize) {
        ContentValues values;
        values = new ContentValues();
        values.put("domain", 1);
        values.put("cracked", 0);
        values.put("unenc", 0);
        values.put("unenc_rand", 0);
        values.put("enc", 0);
        values.put("enc_rand", 0);
        values.put("enc_null", 0);
        values.put("enc_null_rand", 0);
        values.put("enc_si", 0);
        values.put("enc_si_rand", 0);
        values.put("predict", 0);
        values.put("uplink_avail", 0);
        values.put("initial_seq", 0);
        values.put("cipher_seq", 0);
        values.put("auth", 0);
        values.put("auth_req_fn", 0);
        values.put("auth_resp_fn", 0);
        values.put("auth_delta", 0);
        values.put("cipher_missing", 0);
        values.put("cipher_comp_first", 0);
        values.put("cipher_comp_last", 0);
        values.put("cipher_comp_count", 0);
        values.put("cipher_delta", 0);
        values.put("cipher", 0);
        values.put("cmc_imeisv", 0);
        values.put("integrity", 0);
        values.put("first_fn", 0);
        values.put("last_fn", 0);
        values.put("duration", 0);
        values.put("mobile_orig", 0);
        values.put("mobile_orig", 0);
        values.put("mobile_term", 0);
        values.put("paging_mi", 0);
        values.put("t_unknown", 0);
        values.put("t_detach", 0);
        values.put("t_locupd", 0);
        values.put("cipher_comp_first", 0);
        values.put("cipher_comp_last", 0);
        values.put("cipher_comp_count", 0);
        values.put("cipher_delta", 0);
        values.put("cipher", 0);
        values.put("cmc_imeisv", 0);
        values.put("integrity", 0);
        values.put("first_fn", 0);
        values.put("last_fn", 0);
        values.put("duration", 0);
        values.put("mobile_orig", 0);
        values.put("mobile_orig", 0);
        values.put("mobile_term", 0);
        values.put("paging_mi", 0);
        values.put("t_unknown", 0);
        values.put("t_detach", 0);
        values.put("handover", 0);
        values.put("assign_cmpl", 0);
        values.put("assign", 0);
        values.put("iden_imei_bc", 0);
        values.put(" iden_imei_ac", 0);
        values.put("iden_imsi_bc", 0);
        values.put("iden_imsi_ac", 0);
        values.put("rr_cause", 0);
        values.put("t_release", 0);
        values.put("t_tmsi_realloc", 0);
        values.put("t_ss", 0);
        values.put(" t_sms", 0);
        values.put("t_call", 0);
        values.put("pdp_ip", 0);
        values.put("t_pdp", 0);
        values.put("att_acc", 0);
        values.put("t_attach", 0);
        values.put("t_raupd", 0);
        values.put("t_abort", 0);
        values.put("lu_lac", 0);
        values.put(" lu_mnc", 0);
        values.put("lu_mcc", 0);
        values.put("lu_rej_cause", 0);
        values.put("lu_reject", 0);
        values.put("lu_type", 0);
        values.put("lu_acc", 0);
        values.put("ms_cipher_mask", 0);
        values.put("msisdn", 0);
        values.put(" tlli", 0);
        values.put("new_tmsi", 0);
        values.put("tmsi", 0);
        values.put("imei", 0);
        values.put("imsi", 0);
        values.put("service_req", 0);
        values.put(" sms_presence", 0);
        values.put("call_presence", 0);
        values.put(" a_multirate", 0);
        values.put("a_chan_mode", 0);
        values.put("a_ma_len", 0);
        values.put("a_ma_len", 0);
        values.put("a_maio", 0);
        values.put("a_hsn", 0);
        values.put("a_arfcn", 0);
        values.put("a_hopping", 0);
        values.put("a_tsc", 0);
        values.put("a_chan_type", 0);
        values.put("a_timeslot", 0);
        values.put("forced_ho", 0);
        values.put("ue_integrity_cap", 0);
        values.put("ue_cipher_cap", 0);
        values.put(" avg_power", signalstrenght);
        Log.e(">> insertSession: ", timestamp);
        values.put("timestamp", getTime(timestamp));
        values.put("Mcc", mcc);
        values.put("Mnc", mnc);
        values.put("Lac", lac);
        values.put("Cid", cid);
        values.put("neigh_count", sizeneighboring);
        values.put("arfcn", arfcn);
        values.put("psc", psc);
        values.put("rat", rat);
        db.insert("session_info", null, values);
        analysis(db, longitude, latitude, mnc, mcc, lac, cid, timestamp, neighborsize);
        try {
//            readSQLAsset(context, db, "telephonyview.sql", false);
            Log.e(">> TAG", "insertSession");
            readSQLAsset(context, db, "catcher_analysis.sql", false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void insertCell(SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int rat, int arfcn, double longitude, double latitude, int signalstrength, int neighbor) {
        ContentValues values;
        values = new ContentValues();
        Log.e(">> insertCell: ", timestamp);
        values.put("first_seen", getTime(timestamp));
        values.put("last_seen", getTime(timestamp));
        values.put(" bcch_arfcn", arfcn);
        values.put("c1", 0);
        values.put("c2", 0);
        values.put("power_sum", 0);
        values.put("power_count", 0);
        values.put("gps_lon", longitude);
        values.put("gps_lat", latitude);
        values.put(" msc_ver", 0);
        values.put("combined", 0);
        values.put("agch_blocks", 0);
        values.put("pag_mframes", 0);
        values.put("t3212", 0);
        values.put("dtx", 0);
        values.put("cro", 0);
        values.put("temp_offset", 0);
        values.put("pen_time", 0);
        values.put("pwr_offset", signalstrength);
        values.put("gprs", 0);
        values.put("ba_len", 0);
        values.put("neigh_2", neighbor);
        values.put("neigh_2b", 0);
        values.put("neigh_2t", 0);
        values.put("neigh_2q", 0);
        values.put("neigh_5", 0);
        values.put("neigh_5b", 0);
        values.put("neigh_5t", 0);
        values.put("count_si1", 0);
        values.put("count_si2", 0);
        values.put("count_si2b", 0);
        values.put("count_si2t", 0);
        values.put("count_si2q", 0);
        values.put("count_si3", 0);
        values.put("count_si4", 0);
        values.put("count_si5", 0);
        values.put("count_si5b", 0);
        values.put("count_si5t", 0);
        values.put("count_si6", 0);
        values.put("count_si13", 0);
        values.put("si1", 0);
        values.put("si2", 0);
        values.put("si2b", 0);
        values.put("si2t", 0);
        values.put("si2q", 0);
        values.put("si3", 0);
        values.put("si4", 0);
        values.put("si5", 0);
        values.put("si5b", 0);
        values.put("si5t", 0);
        values.put("si6", 0);
        values.put("si13", 0);
        values.put("Mcc", mcc);
        values.put("Mnc", mnc);
        values.put("Lac", lac);
        values.put("Cid", cid);
        values.put("rat", rat);
        db.insert("cell_info", null, values);
//        insertneighborcatchernroot(db,timestamp, mcc, mnc, lac,cid, rat,arfcn, longitude,latitude, signalstrength,neighbor);
        try {
//            readSQLAsset(context, db, "unrootneighbor.sql", false);
            readSQLAsset(context, db, "catcher_analysis.sql", false);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void updateCell(SQLiteDatabase db, String timestamp) {
        ContentValues values;
        values = new ContentValues();
        Log.e(">> updateCell: ", timestamp);
        values.put("last_seen", getTime(timestamp));
        SharedPreferences sharedPrefs = context.getSharedPreferences("cellid", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        int cellid = sharedPrefs.getInt("cellid", 0);
//        String q= "Update TABLE if exists mylonesome_lacs";
//        db.execSQL(q);
//        db.execSQL("UPDATE cell_info SET last_seen='timestamp' WHERE id='cellid' ");
        db.update("cell_info", values, "id=" + cellid, null);
    }

    public void insertneighborcatchernroot(SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int rat, int arfcn, double longitude, double latitude, int signalstrength, int neighbor) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT DISTINCT\n" +
                "        l.id,\n" +
                "        l.mcc,\n" +
                "        l.mnc,\n" +
                "        l.lac,\n" +
                "        l.cid\n" +
                "FROM cell_info AS l\n" +
                "WHERE NOT EXISTS (SELECT \n" +
                "        r.mcc,\n" +
                "        r.mnc,\n" +
                "        r.lac,\n" +
                "        r.cid\n" +
                "        FROM session_info As r\n" +
                "        WHERE   l.mcc = r.mcc AND\n" +
                "        l.mnc = r.mnc AND\n" +
                "        l.lac = r.lac AND\n" +
                "        l.cid = r.cid);\n" +
                "      ", null);

        if (cursor != null && cursor.moveToFirst()) {
            String q = "DROP TABLE if exists neighborcell";
            db.execSQL(q);
            String q4 = "CREATE TABLE neighborcell(id,mcc,mnc,lac,cid,arfcn,rat)";
            db.execSQL(q4);
            String neighborcell = "neighborcell";
            while (cursor == null) {
                String query3 = "INSERT INTO neighborcell VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','1')";
                db.execSQL(query3);
                cursor.moveToNext();
            }
            insertneighborviewunroot(neighborcell, db, timestamp, mcc, mnc, lac, cid, rat, arfcn, longitude, latitude, signalstrength, neighbor);
        }
    }

    public void insertneighborviewunroot(String tableName, SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int rat, int arfcn, double longitude, double latitude, int signalstrength, int neighbor) {
        insertr1(db);
        insertk1(db, neighbor);
        inserta4(db);
        inserta2(db);
        neighborcatcherunroot(db, timestamp, mcc, mnc, lac, cid, rat, arfcn, longitude, latitude, signalstrength);
    }

    public void createtables(SQLiteDatabase db) {
        String q = "DROP TABLE if exists mylonesome_lacs";
        db.execSQL(q);
        String q2 = "CREATE TABLE mylonesome_lacs(mcc, mnc,lac,cells)";
        db.execSQL(q2);
        String q3 = "DROP TABLE if exists mya5";
        db.execSQL(q3);
        String q4 = "CREATE TABLE mya5(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q4);
        String q5 = "DROP TABLE if exists mycells_neig_count";
        db.execSQL(q5);
        String q6 = "CREATE TABLE mycells_neig_count(id,cell_mcc,cell_mnc,cell_lac,cell_cid,cell_arfcn,neig_id,count)";
        db.execSQL(q6);
        String q7 = "DROP TABLE if exists mycells_with_neig_cell";
        db.execSQL(q7);
        String q8 = "CREATE TABLE mycells_with_neig_cell(id,cell_mcc,cell_mnc,cell_lac,cell_cid,cell_arfcn,neig_id)";
        db.execSQL(q8);
        String q9 = "DROP TABLE if exists mycells_with_neig_arfcn";
        db.execSQL(q9);
        String q10 = "CREATE TABLE mycells_with_neig_arfcn(last_seen,id,mcc,mnc,lac,cid,bcch_arfcn,neig_arfcn)";
        db.execSQL(q10);
        String q11 = "DROP TABLE if exists myr1";
        db.execSQL(q11);
        String q12 = "CREATE TABLE myr1(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q12);
        String q13 = "DROP TABLE if exists mya4";
        db.execSQL(q13);
        String q14 = "CREATE TABLE mya4(id,mcc,mnc,lac,cid,value,score)";
        db.execSQL(q14);
        String q15 = "DROP TABLE if exists myk1";
        db.execSQL(q15);
        String q16 = "CREATE TABLE myk1(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q16);
        String q17 = "DROP TABLE if exists mya2";
        db.execSQL(q17);
        String q18 = "CREATE TABLE mya2(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q18);
        String q19 = "DROP TABLE if exists mya1";
        db.execSQL(q19);
        String q20 = "CREATE TABLE mya1(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q20);
        String q21 = "DROP TABLE if exists myk2";
        db.execSQL(q21);
        String q22 = "CREATE TABLE myk2(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q22);
        String q23 = "DROP TABLE if exists myt1";
        db.execSQL(q23);
        String q24 = "CREATE TABLE myt1(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q24);
        String q25 = "DROP TABLE if exists myr2";
        db.execSQL(q25);
        String q26 = "CREATE TABLE myr2(id,mcc,mnc,lac,cid,score)";
        db.execSQL(q26);
    }

    public void analysis(SQLiteDatabase db, double longitude, double latitude, int mnc, int mcc, int lac, int cid, String timestamp, int neighborsize) {

        insertA5(db);
        insertk1(db, neighborsize);
        insertr1(db);
        inserta4(db);
        inserta2(db);
        insertCatcher(db, longitude, latitude, mnc, mcc, lac, cid, timestamp);

    }
//a5

    public void insertLoneSomeLAC(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("select mcc,mnc,lac,count(distinct cid) as cells from session_info where mcc > 0 and lac > 0 and cid > 0 and mcc < 1000 and mnc < 1000 group by mcc, mnc, lac having cells = 1", null);
        if (cursor != null && cursor.moveToFirst()) {
//
            cursor.moveToLast();
            String query3 = "INSERT INTO mylonesome_lacs VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "')";
            db.execSQL(query3);
//
        }
    }

    public void insertA5(SQLiteDatabase db) {
        insertLoneSomeLAC(db);
        Cursor cursor1 = null;
        Cursor cursor = null;
        if (isTableExists(context, "mylonesome_lacs", db)) {
            cursor1 = db.rawQuery("select mcc,mnc,lac  from mylonesome_lacs ", null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                cursor1.moveToLast();
                cursor = db.rawQuery("select id,mcc,mnc,lac,cid from session_info where mnc='" + cursor1.getInt(cursor1.getColumnIndex("mnc")) + "' and mcc='" + cursor1.getInt(cursor1.getColumnIndex("mcc")) + "' and lac='" + cursor1.getInt(cursor1.getColumnIndex("lac")) + "'", null);
                if (cursor != null && cursor.moveToFirst()) {
                    cursor.moveToLast();
                    String query3 = "INSERT INTO mya5 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','1')";
                    db.execSQL(query3);

                }
            }
        }
    }

    public void insertcells_neig_count(SQLiteDatabase db) {
        Cursor cursor = null;
        if (isTableExists(context, "mycells_with_neig_cell", db)) {
            cursor = db.rawQuery("SELECT *, count(*) as count FROM mycells_with_neig_cell GROUP BY id, cell_mcc, cell_mnc, cell_lac, cell_cid;", null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToLast();
                String query3 = "INSERT INTO mycells_neig_count VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "','" + cursor.getInt(7) + "')";
                db.execSQL(query3);
            }
        }

    }

    public void insertcells_with_neig_cell(SQLiteDatabase db) {
        if (isTableExists(context, "mycells_with_neig_arfcn", db)) {
            Cursor cursor = null;
            cursor = db.rawQuery("SELECT DISTINCT\n" +
                    "        c.id,\n" +
                    "        c.mcc as cell_mcc,\n" +
                    "        c.mnc as cell_mnc,\n" +
                    "        c.lac as cell_lac,\n" +
                    "        c.cid as cell_cid,\n" +
                    "        c.bcch_arfcn as cell_arfcn,\n" +
                    "        n.id as neig_id\n" +
                    "FROM mycells_with_neig_arfcn as c, cell_info as n, config\n" +
                    "ON\n" +
                    "\tn.mcc>0 AND n.lac>0 AND n.cid>0 AND\n" +
                    "\tc.neig_arfcn = n.bcch_arfcn AND\n" +
                    "\tabs(julianday(c.last_seen)-julianday(n.last_seen)) < (config.neig_max_delta / 86400.0)\n" +
                    "WHERE c.bcch_arfcn != c.neig_arfcn;", null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToLast();
                String query3 = "INSERT INTO mycells_with_neig_cell VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "')";
                db.execSQL(query3);

            }
        }
    }

    public void insertcells_with_neig_arfcn(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT\n" +
                "        ci.last_seen,\n" +
                "        ci.id,\n" +
                "        ci.mcc,\n" +
                "        ci.mnc,\n" +
                "        ci.lac,\n" +
                "        ci.cid,\n" +
                "        ci.bcch_arfcn,\n" +
                "        al.arfcn as neig_arfcn\n" +
                "FROM cell_info AS ci LEFT JOIN arfcn_list AS al\n" +
                "ON ci.id = al.id\n" +
                "WHERE ci.mcc > 0 AND ci.lac > 0 AND ci.cid > 0;", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
//
            String query3 = "INSERT INTO mycells_with_neig_arfcn VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "','" + cursor.getInt(7) + "')";
            db.execSQL(query3);

        }
    }

    //neighbor cell
    public void insertr1(SQLiteDatabase db) {
        insertcells_with_neig_arfcn(db);
        insertcells_with_neig_cell(db);
        insertcells_neig_count(db);
        Cursor cursor = null;
        if (isTableExists(context, "mycells_ref_by_neig_count", db) && isTableExists(context, "mycells_neig_count", db)) {
            cursor = db.rawQuery("SELECT \n" +
                    "        cn.id,\n" +
                    "        cn.cell_mcc AS mcc,\n" +
                    "        cn.cell_mnc AS mnc,\n" +
                    "        cn.cell_lac AS lac,\n" +
                    "        cn.cell_cid AS cid,\n" +
                    "\t\tCASE\n" +
                    "\t\t\tWHEN cn.count - crbn.count = 0 THEN 1.0\n" +
                    "\t\t\t\t\t\t\t\t\t\t   ELSE 0.0\n" +
                    "\t\tEND as score\n" +
                    "FROM mycells_neig_count as cn, mycells_ref_by_neig_count as crbn\n" +
                    "ON cn.id = crbn.id", null);
            if (cursor != null && cursor.moveToFirst()) {
                cursor.moveToLast();
                String query3 = "INSERT INTO myr1 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "')";
                db.execSQL(query3);

            }
        }
    }

    public void inserta4(SQLiteDatabase db) {
        Cursor cursor2 = null;
        cursor2 = db.rawQuery("SELECT\n" +
                "        l.id,\n" +
                "        l.mcc,\n" +
                "        l.mnc,\n" +
                "        l.lac,\n" +
                "        l.cid,\n" +
                "        count(*) AS value,\n" +
                "        CASE WHEN count(*) > 1 THEN 1 ELSE 0 END AS score\n" +
                "FROM cell_info AS l LEFT JOIN cell_info AS r\n" +
                "ON\n" +
                "        l.mcc = r.mcc AND\n" +
                "        l.mnc = r.mnc AND\n" +
                "        l.lac = r.lac AND\n" +
                "        l.cid = r.cid AND\n" +
                "        l.rat = r.rat AND\n" +
                "        l.bcch_arfcn != r.bcch_arfcn AND\n" +
                "        l.first_seen < r.first_seen\n" +
                "WHERE\n" +
                "        l.mcc > 0 AND\n" +
                "        l.lac > 0 AND\n" +
                "        l.cid > 0 AND\n" +
                "        r.bcch_arfcn != null\n" +
                "GROUP BY l.mcc, l.mnc, l.lac, l.cid;", null);
        if (cursor2 != null && cursor2.moveToFirst()) {
            cursor2.moveToLast();
            String query3 = "INSERT INTO mya4 VALUES('" + cursor2.getInt(0) + "','" + cursor2.getInt(1) + "','" + cursor2.getInt(2) + "','" + cursor2.getInt(3) + "','" + cursor2.getInt(4) + "','" + cursor2.getInt(5) + "','" + cursor2.getInt(6) + "')";
            db.execSQL(query3);


        }

    }

    public void insertk1(SQLiteDatabase db, int neighborsize) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT\n" +
                "\tid,\n" +
                "\tmcc,\n" +
                "\tmnc,\n" +
                "\tlac,\n" +
                "\tcid,\n" +
                "\tCASE\n" +
                "\t\tWHEN " + neighborsize + " > 1 THEN 0.0\n" +
                "\t\tELSE 1.0\n" +
                "\tEND as score\n" +
                "\tFROM cell_info\n", null);

        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String query3 = "INSERT INTO myk1 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "')";
            db.execSQL(query3);

        }

    }

    public void inserta2(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT\n" +
                "\tcell.id,\n" +
                "\tcell.mcc,\n" +
                "\tcell.mnc,\n" +
                "\tcell.lac,\n" +
                "\tcell.cid,\n" +
                "\t((count(*) - sum(case when cell.lac != neig.lac then 1 else 0 end)) = 0) * 0.5 as score\n" +
                "FROM\n" +
                "\tcell_info  AS cell,\n" +
                "\tarfcn_list AS al,\n" +
                "\tcell_info  AS neig,\n" +
                "\tconfig\n" +
                "ON\n" +
                "\tcell.id = al.id AND\n" +
                "\tal.arfcn = neig.bcch_arfcn AND\n" +
                "\tcell.bcch_arfcn != neig.bcch_arfcn AND\n" +
                "\tcell.mcc = neig.mcc AND\n" +
                "\tcell.mnc = neig.mnc AND\n" +
                "\tabs(strftime('%s', cell.last_seen) - strftime('%s', neig.last_seen)) < config.neig_max_delta\n" +
                "WHERE\n" +
                "\tcell.mcc > 0 AND\n" +
                "\tcell.lac > 0 AND\n" +
                "\tcell.cid > 0 AND\n" +
                "\tneig.mcc > 0 AND\n" +
                "\tneig.lac > 0 AND\n" +
                "\tneig.cid > 0\n" +
                "GROUP BY\n" +
                "\tcell.mcc,\n" +
                "\tcell.mnc,\n" +
                "\tcell.lac,\n" +
                "\tcell.cid\n" +
                "HAVING\n" +
                "\tcount(*) > 2;", null);

        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String query3 = "INSERT INTO mya2 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "')";
            db.execSQL(query3);
        }
    }

    public void insertk2(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("select\n" +
                "        id,\n" +
                "        mcc,\n" +
                "        mnc,\n" +
                "        lac,\n" +
                "        cid,\n" +
                "        cro as value,\n" +
                "        (cro > config.cro_max) as score\n" +
                "from cell_info, config\n" +
                "where mcc > 0 and lac > 0 and cid > 0 and cro > 0;\n", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String query3 = "INSERT INTO myk2 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "')";
            db.execSQL(query3);
        }
    }

    public void insertr2(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT\n" +
                "        id,\n" +
                "        mcc,\n" +
                "        mnc,\n" +
                "        cid,\n" +
                "        lac,\n" +
                "        ((9 - cell_info.agch_blocks - 6 * cell_info.combined) * cell_info.pag_mframes) > config.n_norm as score\n" +
                "FROM cell_info, config;", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String query3 = "INSERT INTO myr2 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "')";
            db.execSQL(query3);
        }
    }

    public void insertt1(SQLiteDatabase db) {
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT\n" +
                "        id,\n" +
                "        mcc,\n" +
                "        mnc,\n" +
                "        lac,\n" +
                "        cid,\n" +
                "        t3212 AS value,\n" +
                "\t\tCASE\n" +
                "\t\t\tWHEN t3212 < 5 THEN 1.5\n" +
                "\t\t\tWHEN t3212 < 9 THEN 0.7\n" +
                "\t\t\tELSE 0.0\n" +
                "\t\tEND AS score\n" +
                "FROM cell_info\n" +
                "WHERE t3212 > 0;", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            String query3 = "INSERT INTO myt1 VALUES('" + cursor.getInt(0) + "','" + cursor.getInt(1) + "','" + cursor.getInt(2) + "','" + cursor.getInt(3) + "','" + cursor.getInt(4) + "','" + cursor.getInt(5) + "','" + cursor.getInt(6) + "','" + cursor.getInt(6) + "')";
            db.execSQL(query3);
        }
    }


    public void insertCatcher(SQLiteDatabase db, double longitude, double latitude, int mnc, int mcc, int lac, int cid, String timestamp) {
        int score = 0;
        Cursor cursora5 = null;
        Cursor cursorr1 = null;
        Cursor cursora4 = null;
        Cursor cursork1 = null;
        Cursor cursora2 = null;

        if (isTableExists(context, "mya5", db)) {
            cursora5 = db.rawQuery("select score from mya5", null);
            cursora5.moveToLast();
            if (cursora5 != null && cursora5.moveToFirst()) {
                score = score + cursora5.getInt(0);
            }
        }
        if (isTableExists(context, "mya4", db)) {
            cursora4 = db.rawQuery("select score from mya4", null);
            cursora4.moveToLast();
            if (cursora4 != null && cursora4.moveToFirst()) {
                score = score + cursora4.getInt(0);
            }
        }

        if (isTableExists(context, "myr1", db)) {
            cursorr1 = db.rawQuery("select score from myr1", null);
            cursorr1.moveToLast();
            if (cursorr1 != null && cursorr1.moveToFirst()) {
                score = score + cursorr1.getInt(0);
            }
        }
        if (isTableExists(context, "mya2", db)) {
            cursora2 = db.rawQuery("select score from mya2", null);
            cursora2.moveToLast();
            if (cursora2 != null && cursora2.moveToFirst()) {
                score = score + cursora2.getInt(0);
            }
        }
        if (isTableExists(context, "myk1", db)) {
            cursork1 = db.rawQuery("select score from myk1", null);
            cursork1.moveToLast();
            if (cursork1 != null && cursork1.moveToFirst()) {
                score = score + cursork1.getInt(0);
            }
        }
        if (score > getThresholdConfig(db)) {
            ContentValues values;
            values = new ContentValues();
            if (isTableExists(context, "mya5", db) && cursora5 != null && cursora5.moveToFirst()) {
                values.put("a5", cursora5.getInt(0));
            }
//                values.put("k1", cursorr1.getInt(0));
            if (isTableExists(context, "mya4", db) && cursora4 != null && cursora4.moveToFirst()) {
                values.put("a4", cursora4.getInt(0));
            }
            if (isTableExists(context, "myr1", db) && cursorr1 != null && cursorr1.moveToFirst()) {
                values.put("r1", cursorr1.getInt(0));
            }
            if (isTableExists(context, "mya2", db) && cursora2 != null && cursora2.moveToFirst()) {
                values.put("a2", cursora2.getInt(0));
            }
            if (isTableExists(context, "myk1", db) && cursork1 != null && cursork1.moveToFirst()) {
                values.put("k1", cursork1.getInt(0));
            }
            values.put("longitude", longitude);
            values.put(" latitude", latitude);
            values.put("mcc", mcc);
            values.put("mnc", mnc);
            values.put("cid", cid);
            values.put("lac", lac);
//            values.put("pwr",1000);
            values.put("timestamp", getTime(timestamp));
            values.put("score", score);
            values.put("pwr", 1000);
            db.insert("catcher", null, values);
            Log.i("parimah", "catcher filled");
        }

    }

    public void neighborcatcher(SQLiteDatabase db, double longitude, double latitude, int mnc, int mcc, int lac, int cid, String timestamp, int neighborsize) {
        inserta2(db);
        inserta4(db);
        insertk1(db, neighborsize);
        insertk2(db);
        insertt1(db);
        insertr1(db);
        insertr2(db);
        neighborcatcher(db, longitude, latitude, mnc, mcc, lac, cid, timestamp);
    }

    public void neighborcatcherunroot(SQLiteDatabase db, String timestamp, int mcc, int mnc, int lac, int cid, int rat, int arfcn, double longitude, double latitude, int signalstrength) {
        float score = 0;
        Cursor cursora2 = null;
        Cursor cursora4 = null;
        Cursor cursork1 = null;
        Cursor cursork2 = null;
        Cursor cursort1 = null;
        Cursor cursorr1 = null;
        Cursor cursorr2 = null;


        if (isTableExists(context, "mya2", db)) {
            cursora2 = db.rawQuery("select score from mya2", null);
            cursora2.moveToLast();
            if (cursora2 != null && cursora2.moveToFirst()) {
                score = score + cursora2.getFloat(0);
            }
        }

        if (isTableExists(context, "mya4", db)) {
            cursora4 = db.rawQuery("select score from mya4", null);
            cursora4.moveToLast();
            if (cursora4 != null && cursora4.moveToFirst()) {
                score = score + cursora4.getFloat(0);
            }
        }
        if (isTableExists(context, "myk1", db)) {
            cursork1 = db.rawQuery("select score from myk1", null);
            cursork1.moveToLast();
            if (cursork1 != null && cursork1.moveToFirst()) {
                score = score + cursork1.getFloat(0);
            }
        }

        if (isTableExists(context, "myr1", db)) {
            cursorr1 = db.rawQuery("select score from myr1", null);
            cursorr1.moveToLast();
            if (cursorr1 != null && cursorr1.moveToFirst()) {
                score = score + cursorr1.getFloat(0);
            }
        }


        if (score > getThresholdConfig(db)) {
            ContentValues values;
            values = new ContentValues();

            if (isTableExists(context, "mya4", db) && cursora4 != null && cursora4.moveToFirst()) {
                values.put("a4", cursora4.getInt(0));
            }
            if (isTableExists(context, "myr1", db) && cursorr1 != null && cursorr1.moveToFirst()) {
                values.put("r1", cursorr1.getInt(0));
            }
            if (isTableExists(context, "mya2", db) && cursora2 != null && cursora2.moveToFirst()) {
                values.put("a2", cursora2.getInt(0));
            }
            if (isTableExists(context, "myk1", db) && cursork1 != null && cursork1.moveToFirst()) {
                values.put("k1", cursork1.getInt(0));
            }
            values.put("longitude", longitude);
            values.put(" latitude", latitude);
            values.put("mcc", mcc);
            values.put("mnc", mnc);
            values.put("cid", cid);
            values.put("lac", lac);

            values.put("timestamp", getTime(timestamp));
            values.put("score", score);
            db.insert("nieghborcatcher", null, values);
            Log.i("parimah", "neighborcatcher filled");
            SharedPreferences totalneighborscore = context.getSharedPreferences("neighborcatcher", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = totalneighborscore.edit();
//            String score = Float.toString(neighborcatcher);
            editor.putFloat("neighborcatcher", 0);
            editor.commit();
        }
    }

    public void neighborcatcher(SQLiteDatabase db, double longitude, double latitude, int mnc, int mcc, int lac, int cid, String timestamp) {
        float score = 0;
        Cursor cursora2 = null;
        Cursor cursora4 = null;
        Cursor cursork1 = null;
        Cursor cursork2 = null;
        Cursor cursort1 = null;
        Cursor cursorr1 = null;
        Cursor cursorr2 = null;


        if (isTableExists(context, "mya2", db)) {
            cursora2 = db.rawQuery("select score from mya2", null);
            cursora2.moveToLast();
            if (cursora2 != null && cursora2.moveToFirst()) {
                score = score + cursora2.getFloat(0);
            }
        }

        if (isTableExists(context, "mya4", db)) {
            cursora4 = db.rawQuery("select score from mya4", null);
            cursora4.moveToLast();
            if (cursora4 != null && cursora4.moveToFirst()) {
                score = score + cursora4.getFloat(0);
            }
        }
        if (isTableExists(context, "myk1", db)) {
            cursork1 = db.rawQuery("select score from myk1", null);
            cursork1.moveToLast();
            if (cursork1 != null && cursork1.moveToFirst()) {
                score = score + cursork1.getFloat(0);
            }
        }
        if (isTableExists(context, "myk2", db)) {
            cursork2 = db.rawQuery("select score from myk2", null);
            cursork2.moveToLast();
            if (cursork2 != null && cursork2.moveToFirst()) {
                score = score + cursork2.getFloat(0);
            }
        }
        if (isTableExists(context, "myt1", db)) {
            cursort1 = db.rawQuery("select score from myt1", null);
            cursort1.moveToLast();
            if (cursort1 != null && cursort1.moveToFirst()) {
                score = score + cursort1.getFloat(0);
            }
        }
        if (isTableExists(context, "myr1", db)) {
            cursorr1 = db.rawQuery("select score from myr1", null);
            cursorr1.moveToLast();
            if (cursorr1 != null && cursorr1.moveToFirst()) {
                score = score + cursorr1.getFloat(0);
            }
        }
        if (isTableExists(context, "myr2", db)) {
            cursorr2 = db.rawQuery("select score from myr2", null);
            cursorr2.moveToLast();
            if (cursorr2 != null && cursorr2.moveToFirst()) {
                score = score + cursorr2.getFloat(0);
            }
        }

        if (score > getThresholdConfig(db)) {
            ContentValues values;
            values = new ContentValues();

            if (isTableExists(context, "mya4", db) && cursora4 != null && cursora4.moveToFirst()) {
                values.put("a4", cursora4.getInt(0));
            }
            if (isTableExists(context, "myr1", db) && cursorr1 != null && cursorr1.moveToFirst()) {
                values.put("r1", cursorr1.getInt(0));
            }
            if (isTableExists(context, "mya2", db) && cursora2 != null && cursora2.moveToFirst()) {
                values.put("a2", cursora2.getInt(0));
            }
            if (isTableExists(context, "myk1", db) && cursork1 != null && cursork1.moveToFirst()) {
                values.put("k1", cursork1.getInt(0));
            }
            if (isTableExists(context, "myk2", db) && cursork2 != null && cursork2.moveToFirst()) {
                values.put("k2", cursork2.getInt(0));
            }
            if (isTableExists(context, "myt1", db) && cursort1 != null && cursort1.moveToFirst()) {
                values.put("t1", cursort1.getInt(0));
            }
            if (isTableExists(context, "myr2", db) && cursorr2 != null && cursorr2.moveToFirst()) {
                values.put("r2", cursorr2.getInt(0));
            }
            values.put("longitude", longitude);
            values.put(" latitude", latitude);
            values.put("mcc", mcc);
            values.put("mnc", mnc);
            values.put("cid", cid);
            values.put("lac", lac);

            values.put("timestamp", getTime(timestamp));
            values.put("score", score);
            db.insert("nieghborcatcher", null, values);
            Log.i("parimah", "neighborcatcher filled");
            SharedPreferences totalneighborscore = context.getSharedPreferences("neighborcatcher", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = totalneighborscore.edit();
//            String score = Float.toString(neighborcatcher);
            editor.putFloat("neighborcatcher", 0);
            editor.commit();
        }
    }

    public boolean isTableExists(Context context, String tableName, SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                //Toast.makeText(context,"exist",Toast.LENGTH_SHORT).show();
                return true;
            }
            cursor.close();
        }
        //Toast.makeText(context,"not  exist",Toast.LENGTH_SHORT).show();
        return false;
    }

//    public ArrayList<Sensor> getUnsentSensors(SQLiteDatabase dbInstanse, String lastSensorSentTime) {
//
//        String selectQuery = "SELECT * FROM sensor_status where Datetime(timestamp) > Datetime('" + lastSensorSentTime + "');";
//        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
//        ArrayList<Sensor> sensorArrayList = new ArrayList<>();
//
//
//        if (cursor.moveToFirst()) {
//            do {
//                System.out.println("SENSOR: " + cursor.getInt(cursor.getColumnIndex("battery_level")));
//                Sensor sensor = new Sensor(convertDate(cursor.getString(cursor.getColumnIndex("timestamp"))),
//                        cursor.getInt(cursor.getColumnIndex("battery_level")),
//                        cursor.getDouble(cursor.getColumnIndex("longitude")),
//                        cursor.getDouble(cursor.getColumnIndex("latitude")),
//                        (cursor.getInt(cursor.getColumnIndex("service_status")) > 0),
//                        cursor.getFloat(cursor.getColumnIndex("cpu_usage")),
//                        cursor.getFloat(cursor.getColumnIndex("memory_usage")),
//                        cursor.getFloat(cursor.getColumnIndex("disk_usage")));
//
//                sensorArrayList.add(sensor);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        return sensorArrayList;
//    }

    public ArrayList<SensorM> getUnsentSensorsM(SQLiteDatabase dbInstanse, String lastSensorSentTime) {
        Log.e(">> Last Sent Sensor: ", lastSensorSentTime);
        String selectQuery = "SELECT * FROM sensor_status where Datetime(timestamp) > Datetime('" + lastSensorSentTime + "');";
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        ArrayList<SensorM> sensorArrayList = new ArrayList<>();


        if (cursor.moveToFirst()) {
            do {

                SensorM sensor = new SensorM(convertDate(cursor.getString(cursor.getColumnIndex("timestamp"))),
                        cursor.getInt(cursor.getColumnIndex("battery_level")),
                        cursor.getDouble(cursor.getColumnIndex("longitude")),
                        cursor.getDouble(cursor.getColumnIndex("latitude")),
                        (cursor.getInt(cursor.getColumnIndex("service_status")) > 0),
                        cursor.getFloat(cursor.getColumnIndex("cpu_usage")),
                        cursor.getFloat(cursor.getColumnIndex("memory_usage")),
                        cursor.getFloat(cursor.getColumnIndex("disk_usage")));

                sensorArrayList.add(sensor);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return sensorArrayList;
    }


    public ArrayList<CatcherM> getUnsentCatchersM(SQLiteDatabase dbInstanse, String lastCatcherSentTime) {
        Log.e(">> Last Sent Catcher: ", lastCatcherSentTime);
        String selectQuery = "SELECT * FROM catcher_with_signal_power where Datetime(timestamp) > Datetime('" + lastCatcherSentTime + "');";
//        String selectQuery = "SELECT * FROM catcher where Datetime(timestamp) > Datetime('" + lastCatcherSentTime + "');";
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        ArrayList<CatcherM> catcherArrayList = new ArrayList<>();
        //
        float a2 = 0;
        float a4 = 0;
        float a5 = 0;
        float k1 = 0;
        float k2 = 0;
        float c1 = 0;
        float c2 = 0;
        float c4 = 0;
        float c5 = 0;
        float t1 = 0;
        float t3 = 0;
        float t4 = 0;
        float r1 = 0;
        float r2 = 0;
        String names[] = {"Inconsistent LAC", "Different ARFCNs", "Lonesome LA", "Non Neighbor", "High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher", "Low t3212", "Empty Paging", "Orphaned Channel", "Inconsistent Neighbor", "High Paging"};
        SharedPreferences paramscore = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            if (paramscore.getBoolean(names[0], true)) {
                a2 = cursor.getFloat(cursor.getColumnIndex("a2"));
            } else {
                a2 = 0;
            }
            if (paramscore.getBoolean(names[1], true)) {
                a4 = cursor.getFloat(cursor.getColumnIndex("a4"));
            } else {
                a4 = 0;
            }
            if (paramscore.getBoolean(names[2], true)) {
                a5 = cursor.getFloat(cursor.getColumnIndex("a5"));
            } else {
                a5 = 0;
            }
            if (paramscore.getBoolean(names[3], true)) {
                k1 = cursor.getFloat(cursor.getColumnIndex("k1"));
            } else {
                k1 = 0;
            }
            if (paramscore.getBoolean(names[4], true)) {
                k2 = cursor.getFloat(cursor.getColumnIndex("k2"));
            } else {
                k2 = 0;
            }
            if (paramscore.getBoolean(names[5], true)) {
                c1 = cursor.getFloat(cursor.getColumnIndex("c1"));
            } else {
                c1 = 0;
            }
            if (paramscore.getBoolean(names[6], true)) {
                c2 = cursor.getFloat(cursor.getColumnIndex("c2"));
            } else {
                c2 = 0;
            }
//        if(paramscore.getBoolean(names[7],true)) {
//            c3=cursor.getInt(cursor.getColumnIndex("c3"));
//        }
//        else
//        {
//            c3=0;
//        }
            if (paramscore.getBoolean(names[7], true)) {
                c4 = cursor.getFloat(cursor.getColumnIndex("c4"));
            } else {
                c4 = 0;
            }
            if (paramscore.getBoolean(names[8], true)) {
                c5 = cursor.getFloat(cursor.getColumnIndex("c5"));
            } else {
                c5 = 0;
            }
            if (paramscore.getBoolean(names[9], true)) {
                t1 = cursor.getFloat(cursor.getColumnIndex("t1"));
            } else {
                t1 = 0;
            }
            if (paramscore.getBoolean(names[10], true)) {
                t3 = cursor.getFloat(cursor.getColumnIndex("t3"));
            } else {
                t3 = 0;
            }
            if (paramscore.getBoolean(names[11], true)) {
                t4 = cursor.getFloat(cursor.getColumnIndex("t4"));
            } else {
                t4 = 0;
            }
            if (paramscore.getBoolean(names[12], true)) {
                r1 = cursor.getFloat(cursor.getColumnIndex("r1"));
            } else {
                r1 = 0;
            }
            if (paramscore.getBoolean(names[13], true)) {
                r2 = cursor.getFloat(cursor.getColumnIndex("r2"));
            } else {
                r2 = 0;
            }
//        if(paramscore.getBoolean(names[0],true)) {
//            f1=cursor.getInt(cursor.getColumnIndex("f1"));
//        }
//        else
//        {
//            f1=0;
//        }

        }

        assert cursor != null;
        if (cursor.moveToFirst()) {
            do {
                CatcherM catcher = new CatcherM(convertDate(cursor.getString(cursor.getColumnIndex("timestamp"))),
                        cursor.getInt(cursor.getColumnIndex("mcc")), cursor.getInt(cursor.getColumnIndex("mnc")),
                        cursor.getInt(cursor.getColumnIndex("lac")), cursor.getInt(cursor.getColumnIndex("cid")),
                        cursor.getInt(cursor.getColumnIndex("duration")), cursor.getFloat(cursor.getColumnIndex("a2")), cursor.getFloat(cursor.getColumnIndex("a4")), cursor.getFloat(cursor.getColumnIndex("a5")), cursor.getFloat(cursor.getColumnIndex("k1")), cursor.getFloat(cursor.getColumnIndex("k2")),
                        cursor.getFloat(cursor.getColumnIndex("c1")), cursor.getFloat(cursor.getColumnIndex("c2")), cursor.getFloat(cursor.getColumnIndex("c4")), cursor.getFloat(cursor.getColumnIndex("c5")), cursor.getFloat(cursor.getColumnIndex("t1")), cursor.getFloat(cursor.getColumnIndex("t3")), cursor.getFloat(cursor.getColumnIndex("t4")), cursor.getFloat(cursor.getColumnIndex("r1")), cursor.getFloat(cursor.getColumnIndex("r2")), cursor.getDouble(cursor.getColumnIndex("longitude")),
                        cursor.getDouble(cursor.getColumnIndex("latitude")),
                        (short) cursor.getInt(cursor.getColumnIndex("valid")),
                        cursor.getFloat(cursor.getColumnIndex("score")),
                        cursor.getInt(cursor.getColumnIndex("pwr")));

                catcherArrayList.add(catcher);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return catcherArrayList;
    }


    public ArrayList<CellInfo> getUnsentCellInfos(SQLiteDatabase dbInstanse, String lastCellInfoSentTime) {
        Log.e(">> Last Sent CellInfo: ", lastCellInfoSentTime);
        String selectQuery = "SELECT * FROM cell_info where Datetime(first_seen)> Datetime('" + lastCellInfoSentTime + "')";
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        ArrayList<CellInfo> cellInfoArrayList = new ArrayList<>();


        if (cursor.moveToFirst()) {

            do {

                CellInfo cellInfo = new CellInfo(cursor.getString(cursor.getColumnIndex("first_seen")),
                        convertDate(cursor.getString(cursor.getColumnIndex("last_seen"))),
                        (short) cursor.getInt(cursor.getColumnIndex("mcc")), (short) cursor.getInt(cursor.getColumnIndex("mnc")),
                        cursor.getInt(cursor.getColumnIndex("lac")), cursor.getInt(cursor.getColumnIndex("cid")),
                        (short) cursor.getInt(cursor.getColumnIndex("rat")), cursor.getInt(cursor.getColumnIndex("bcch_arfcn")),
                        (short) cursor.getInt(cursor.getColumnIndex("c1")), (short) cursor.getInt(cursor.getColumnIndex("c2")),
                        cursor.getInt(cursor.getColumnIndex("power_sum")), cursor.getInt(cursor.getColumnIndex("power_count")),
                        cursor.getFloat(cursor.getColumnIndex("gps_lon")), cursor.getFloat(cursor.getColumnIndex("gps_lat")),
                        (short) cursor.getInt(cursor.getColumnIndex("msc_ver")), (short) cursor.getInt(cursor.getColumnIndex("combined")),
                        (short) cursor.getInt(cursor.getColumnIndex("agch_blocks")), (short) cursor.getInt(cursor.getColumnIndex("pag_mframes")),
                        (short) cursor.getInt(cursor.getColumnIndex("t3212")), (short) cursor.getInt(cursor.getColumnIndex("dtx")),
                        (short) cursor.getInt(cursor.getColumnIndex("cro")), (short) cursor.getInt(cursor.getColumnIndex("temp_offset")),
                        (short) cursor.getInt(cursor.getColumnIndex("pen_time")), (short) cursor.getInt(cursor.getColumnIndex("pwr_offset")),
                        (short) cursor.getInt(cursor.getColumnIndex("gprs")), (short) cursor.getInt(cursor.getColumnIndex("ba_len")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_2")), (short) cursor.getInt(cursor.getColumnIndex("neigh_2b")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_2t")), (short) cursor.getInt(cursor.getColumnIndex("neigh_2q")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_5")), (short) cursor.getInt(cursor.getColumnIndex("neigh_5b")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_5t")), cursor.getInt(cursor.getColumnIndex("count_si1")),
                        cursor.getInt(cursor.getColumnIndex("count_si2")), cursor.getInt(cursor.getColumnIndex("count_si2b")),
                        cursor.getInt(cursor.getColumnIndex("count_si2t")), cursor.getInt(cursor.getColumnIndex("count_si2q")),
                        cursor.getInt(cursor.getColumnIndex("count_si3")), cursor.getInt(cursor.getColumnIndex("count_si4")),
                        cursor.getInt(cursor.getColumnIndex("count_si5")), cursor.getInt(cursor.getColumnIndex("count_si5b")),
                        cursor.getInt(cursor.getColumnIndex("count_si5t")), cursor.getInt(cursor.getColumnIndex("count_si6")),
                        cursor.getInt(cursor.getColumnIndex("count_si13")), cursor.getString(cursor.getColumnIndex("si1"))
                        , cursor.getString(cursor.getColumnIndex("si2")), cursor.getString(cursor.getColumnIndex("si2b")),
                        cursor.getString(cursor.getColumnIndex("si2t")), cursor.getString(cursor.getColumnIndex("si2q")),
                        cursor.getString(cursor.getColumnIndex("si3")), cursor.getString(cursor.getColumnIndex("si4")),
                        cursor.getString(cursor.getColumnIndex("si5")), cursor.getString(cursor.getColumnIndex("si5b")),
                        cursor.getString(cursor.getColumnIndex("si5t")), cursor.getString(cursor.getColumnIndex("si6")),
                        cursor.getString(cursor.getColumnIndex("si13")));


                cellInfoArrayList.add(cellInfo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cellInfoArrayList;
    }

    private void convertTable() {

        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
        db.execSQL("DROP TABLE IF EXISTS cell_info_convert_time");
        db.execSQL("CREATE TABLE cell_info_convert_time  AS SELECT  *  FROM cell_info");
        Cursor cursor = db.rawQuery("SELECT id, first_seen, last_seen FROM cell_info_convert_time", null);

        List<String> lstID = new ArrayList<>();
        List<String> lstFirstSeen = new ArrayList<>();
        List<String> lstLastSeen = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                lstID.add(cursor.getString(cursor.getColumnIndex("id")));
                lstFirstSeen.add(cursor.getString(cursor.getColumnIndex("first_seen")));
                lstLastSeen.add(cursor.getString(cursor.getColumnIndex("last_seen")));
                cursor.moveToNext();
            }
        }

        for (int index = 0; index < lstFirstSeen.size(); ++index) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("first_seen", convertDate(lstFirstSeen.get(index)));
            contentValues.put("last_seen", convertDate(lstLastSeen.get(index)));
            db.update("cell_info_convert_time", contentValues, "id = " + lstID.get(index), null);
        }

        // Update session_info
        List<Integer> lstFlag;
        cursor = db.rawQuery("select timestamp, flag from session_info", null);
        List<String> lstTimestamp = new ArrayList<>();
        lstFlag = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                lstTimestamp.add(cursor.getString(cursor.getColumnIndex("timestamp")));
                lstFlag.add(cursor.getInt(cursor.getColumnIndex("flag")));
                cursor.moveToNext();
            }
        }

        for (int index = 0; index < lstTimestamp.size(); ++index) {
            if (lstFlag.size() > 0) {
                if (lstFlag.get(index) == 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("timestamp", convertDate(lstTimestamp.get(index)));
                    contentValues.put("flag", 1);
                    db.update("session_info", contentValues, "flag = " + 0, null);
                }
            }
        }
    }

    public int checkRootCommand() {

        Process exec = null;
        try {

            exec = Runtime.getRuntime().exec(new String[]{"su"});

            final OutputStreamWriter out = new OutputStreamWriter(exec.getOutputStream());
            out.write("exit");
            out.flush();

            Log.i(">> ROOT: ", "su command executed successfully");
            return 0; // returns zero when the command is executed successfully
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (exec != null) {
                try {
                    exec.destroy();
                } catch (Exception ignored) {
                }
            }
        }
        return 1; //returns one when the command execution fails
    }

    public ArrayList<CellInfoM> getUnsentCellInfosM(SQLiteDatabase dbInstanse, String lastCellInfoSentTime) {

        Log.e(">> Last Sent CellInfo: ", lastCellInfoSentTime);
        String selectQuery = "SELECT * FROM cell_info where Datetime(first_seen)> Datetime('" + lastCellInfoSentTime + "')";
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        ArrayList<CellInfoM> cellInfoArrayList = new ArrayList<>();
        int root = checkRootCommand();
        if (cursor.moveToFirst()) {
            do {
                String firstSeen = convertDate(cursor.getString(cursor.getColumnIndex("first_seen")));
                String lastSeen  = convertDate(cursor.getString(cursor.getColumnIndex("last_seen")));

                if (root == 0) {
                 firstSeen = convertDateUTC(cursor.getString(cursor.getColumnIndex("first_seen")));
                 lastSeen  = convertDateUTC(cursor.getString(cursor.getColumnIndex("last_seen")));
                }

                CellInfoM cellInfo = new CellInfoM(firstSeen,
                        lastSeen,
                        (short) cursor.getInt(cursor.getColumnIndex("mcc")), (short) cursor.getInt(cursor.getColumnIndex("mnc")),
                        cursor.getInt(cursor.getColumnIndex("lac")), cursor.getInt(cursor.getColumnIndex("cid")),
                        (short) cursor.getInt(cursor.getColumnIndex("rat")), cursor.getInt(cursor.getColumnIndex("bcch_arfcn")),
                        (short) cursor.getInt(cursor.getColumnIndex("c1")), (short) cursor.getInt(cursor.getColumnIndex("c2")),
                        cursor.getInt(cursor.getColumnIndex("power_sum")), cursor.getInt(cursor.getColumnIndex("power_count")),
                        cursor.getFloat(cursor.getColumnIndex("gps_lon")), cursor.getFloat(cursor.getColumnIndex("gps_lat")),
                        (short) cursor.getInt(cursor.getColumnIndex("msc_ver")), (short) cursor.getInt(cursor.getColumnIndex("combined")),
                        (short) cursor.getInt(cursor.getColumnIndex("agch_blocks")), (short) cursor.getInt(cursor.getColumnIndex("pag_mframes")),
                        (short) cursor.getInt(cursor.getColumnIndex("t3212")), (short) cursor.getInt(cursor.getColumnIndex("dtx")),
                        (short) cursor.getInt(cursor.getColumnIndex("cro")), (short) cursor.getInt(cursor.getColumnIndex("temp_offset")),
                        (short) cursor.getInt(cursor.getColumnIndex("pen_time")), (short) cursor.getInt(cursor.getColumnIndex("pwr_offset")),
                        (short) cursor.getInt(cursor.getColumnIndex("gprs")), (short) cursor.getInt(cursor.getColumnIndex("ba_len")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_2")), (short) cursor.getInt(cursor.getColumnIndex("neigh_2b")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_2t")), (short) cursor.getInt(cursor.getColumnIndex("neigh_2q")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_5")), (short) cursor.getInt(cursor.getColumnIndex("neigh_5b")),
                        (short) cursor.getInt(cursor.getColumnIndex("neigh_5t")), cursor.getInt(cursor.getColumnIndex("count_si1")),
                        cursor.getInt(cursor.getColumnIndex("count_si2")), cursor.getInt(cursor.getColumnIndex("count_si2b")),
                        cursor.getInt(cursor.getColumnIndex("count_si2t")), cursor.getInt(cursor.getColumnIndex("count_si2q")),
                        cursor.getInt(cursor.getColumnIndex("count_si3")), cursor.getInt(cursor.getColumnIndex("count_si4")),
                        cursor.getInt(cursor.getColumnIndex("count_si5")), cursor.getInt(cursor.getColumnIndex("count_si5b")),
                        cursor.getInt(cursor.getColumnIndex("count_si5t")), cursor.getInt(cursor.getColumnIndex("count_si6")),
                        cursor.getInt(cursor.getColumnIndex("count_si13")), cursor.getString(cursor.getColumnIndex("si1")),
                        cursor.getString(cursor.getColumnIndex("si2")), cursor.getString(cursor.getColumnIndex("si2b")),
                        cursor.getString(cursor.getColumnIndex("si2t")), cursor.getString(cursor.getColumnIndex("si2q")),
                        cursor.getString(cursor.getColumnIndex("si3")), cursor.getString(cursor.getColumnIndex("si4")),
                        cursor.getString(cursor.getColumnIndex("si5")), cursor.getString(cursor.getColumnIndex("si5b")),
                        cursor.getString(cursor.getColumnIndex("si5t")), cursor.getString(cursor.getColumnIndex("si6")),
                        cursor.getString(cursor.getColumnIndex("si13")));
                cellInfoArrayList.add(cellInfo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return cellInfoArrayList;
    }


    public ArrayList<SessionInfoM> getUnsentSessionInfos(SQLiteDatabase dbInstanse, String lastSessionInfoSentTime) {

        Log.e(">> Last Session Info: ", lastSessionInfoSentTime);
        String selectQuery = "SELECT * FROM session_info where Datetime(timestamp) > Datetime('" + lastSessionInfoSentTime + "');";
        Cursor cursor = dbInstanse.rawQuery(selectQuery, null);
        ArrayList<SessionInfoM> sessionInfoArrayList = new ArrayList<>();


        if (cursor.moveToFirst()) {
            do {
                SessionInfoM sessionInfo = new SessionInfoM(convertDate(cursor.getString(cursor.getColumnIndex("timestamp"))),
                        (short) cursor.getInt(cursor.getColumnIndex("rat")), (short) cursor.getInt(cursor.getColumnIndex("domain")),
                        (short) cursor.getInt(cursor.getColumnIndex("mcc")), (short) cursor.getInt(cursor.getColumnIndex("mnc")),
                        cursor.getInt(cursor.getColumnIndex("lac")), cursor.getInt(cursor.getColumnIndex("cid")),
                        cursor.getInt(cursor.getColumnIndex("arfcn")), (short) cursor.getInt(cursor.getColumnIndex("psc")),
                        (short) cursor.getInt(cursor.getColumnIndex("cracked")), (short) cursor.getInt(cursor.getColumnIndex("neigh_count")),
                        (short) cursor.getInt(cursor.getColumnIndex("unenc")), (short) cursor.getInt(cursor.getColumnIndex("unenc_rand")),
                        (short) cursor.getInt(cursor.getColumnIndex("enc")), (short) cursor.getInt(cursor.getColumnIndex("enc_rand")),
                        (short) cursor.getInt(cursor.getColumnIndex("enc_null")), (short) cursor.getInt(cursor.getColumnIndex("enc_null_rand")),
                        (short) cursor.getInt(cursor.getColumnIndex("enc_si")), (short) cursor.getInt(cursor.getColumnIndex("enc_si_rand")),
                        (short) cursor.getInt(cursor.getColumnIndex("predict")), (short) cursor.getInt(cursor.getColumnIndex("avg_power")),
                        (short) cursor.getInt(cursor.getColumnIndex("uplink_avail")), (short) cursor.getInt(cursor.getColumnIndex("initial_seq")),
                        (short) cursor.getInt(cursor.getColumnIndex("cipher_seq")), (short) cursor.getInt(cursor.getColumnIndex("auth")),
                        cursor.getInt(cursor.getColumnIndex("auth_req_fn")), cursor.getInt(cursor.getColumnIndex("auth_resp_fn")),
                        cursor.getInt(cursor.getColumnIndex("auth_delta")), (short) cursor.getInt(cursor.getColumnIndex("cipher_missing")),
                        cursor.getInt(cursor.getColumnIndex("cipher_comp_first")), cursor.getInt(cursor.getColumnIndex("cipher_comp_last")),
                        cursor.getInt(cursor.getColumnIndex("cipher_comp_count")), cursor.getInt(cursor.getColumnIndex("cipher_delta")),
                        (short) cursor.getInt(cursor.getColumnIndex("cipher")), (short) cursor.getInt(cursor.getColumnIndex("cmc_imeisv")),
                        (short) cursor.getInt(cursor.getColumnIndex("integrity")), cursor.getInt(cursor.getColumnIndex("first_fn")),
                        cursor.getInt(cursor.getColumnIndex("last_fn")), cursor.getInt(cursor.getColumnIndex("duration")),
                        (short) cursor.getInt(cursor.getColumnIndex("mobile_orig")), (short) cursor.getInt(cursor.getColumnIndex("mobile_term")),
                        (short) cursor.getInt(cursor.getColumnIndex("paging_mi")), (short) cursor.getInt(cursor.getColumnIndex("t_unknown")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_detach")), (short) cursor.getInt(cursor.getColumnIndex("t_locupd")),
                        (short) cursor.getInt(cursor.getColumnIndex("lu_acc")), (short) cursor.getInt(cursor.getColumnIndex("lu_type")),
                        (short) cursor.getInt(cursor.getColumnIndex("lu_reject")), (short) cursor.getInt(cursor.getColumnIndex("lu_rej_cause")),
                        (short) cursor.getInt(cursor.getColumnIndex("lu_mcc")), (short) cursor.getInt(cursor.getColumnIndex("lu_mnc")),
                        cursor.getInt(cursor.getColumnIndex("lu_lac")), (short) cursor.getInt(cursor.getColumnIndex("t_abort")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_raupd")), (short) cursor.getInt(cursor.getColumnIndex("t_attach")),
                        (short) cursor.getInt(cursor.getColumnIndex("att_acc")), (short) cursor.getInt(cursor.getColumnIndex("t_pdp")),
                        cursor.getString(cursor.getColumnIndex("pdp_ip")), (short) cursor.getInt(cursor.getColumnIndex("t_call")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_call")), (short) cursor.getInt(cursor.getColumnIndex("t_sms")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_ss")), (short) cursor.getInt(cursor.getColumnIndex("t_tmsi_realloc")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_release")), (short) cursor.getInt(cursor.getColumnIndex("rr_cause")),
                        (short) cursor.getInt(cursor.getColumnIndex("t_gprs")), (short) cursor.getInt(cursor.getColumnIndex("iden_imsi_ac")),
                        (short) cursor.getInt(cursor.getColumnIndex("iden_imsi_bc")), (short) cursor.getInt(cursor.getColumnIndex("iden_imei_ac")),
                        (short) cursor.getInt(cursor.getColumnIndex("iden_imei_bc")), (short) cursor.getInt(cursor.getColumnIndex("assign")),
                        (short) cursor.getInt(cursor.getColumnIndex("assign_cmpl")), (short) cursor.getInt(cursor.getColumnIndex("handover")),
                        (short) cursor.getInt(cursor.getColumnIndex("forced_ho")), (short) cursor.getInt(cursor.getColumnIndex("a_timeslot")),
                        (short) cursor.getInt(cursor.getColumnIndex("a_chan_type")), (short) cursor.getInt(cursor.getColumnIndex("a_tsc")),
                        (short) cursor.getInt(cursor.getColumnIndex("a_hopping")), (short) cursor.getInt(cursor.getColumnIndex("a_arfcn")),
                        (short) cursor.getInt(cursor.getColumnIndex("a_hsn")), (short) cursor.getInt(cursor.getColumnIndex("a_maio")),
                        (short) cursor.getInt(cursor.getColumnIndex("a_ma_len")), (short) cursor.getInt(cursor.getColumnIndex("a_chan_mode")),
                        (short) cursor.getInt(cursor.getColumnIndex("a_multirate")), (short) cursor.getInt(cursor.getColumnIndex("call_presence")),
                        (short) cursor.getInt(cursor.getColumnIndex("sms_presence")), (short) cursor.getInt(cursor.getColumnIndex("service_req")),
                        cursor.getString(cursor.getColumnIndex("imsi")), cursor.getString(cursor.getColumnIndex("imei")),
                        cursor.getString(cursor.getColumnIndex("tmsi")), cursor.getString(cursor.getColumnIndex("new_tmsi")),
                        cursor.getString(cursor.getColumnIndex("tlli")), cursor.getString(cursor.getColumnIndex("msisdn")),
                        (short) cursor.getInt(cursor.getColumnIndex("ms_cipher_mask")), (short) cursor.getInt(cursor.getColumnIndex("ue_cipher_cap")),
                        (short) cursor.getInt(cursor.getColumnIndex("ue_integrity_cap")));
                sessionInfoArrayList.add(sessionInfo);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return sessionInfoArrayList;
    }

    public String[] select(SQLiteDatabase db) {
        String s[] = {"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
        Cursor cursor = null;
        cursor = db.rawQuery("select a2,a4,a5,k1,k2,c1,c2,c4,c5,t1,t3,t4,r1,r2 from catcher", null);
        if (cursor != null && cursor.moveToFirst()) {
            cursor.moveToLast();
            for (int i = 0; i < 14; i++) {
                float f = cursor.getFloat(i);
                s[i] = Float.toString(f);
            }
        }
        return s;
    }

    // Convert time in root access.
    private String convertDateUTC(String timeStamp) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
            Log.e("pari", "error:" + e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        destFormat.setTimeZone(tz);
        try {
            return destFormat.format(parsed);
        } catch (Exception ex) {
            return timeStamp;
        }
    }
    private String convertDate(String timeStamp) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        destFormat.setTimeZone(tz);
        try {
            return destFormat.format(parsed);
        } catch (Exception ex) {
            return getTime(convertDateUTC(timeStamp));
        }
    }

    public void InsertCatcher(SQLiteDatabase dbInstanse, Catcher catcher) {
        ContentValues values;
        values = new ContentValues();

        values.put("mcc", catcher.getMcc());
        values.put("mnc", catcher.getMnc());
        values.put("lac", catcher.getLac());
        values.put("cid", catcher.getCid());


        values.put("timestamp", catcher.getTimestamp());
        values.put("a2", catcher.getP1());
        values.put("a4", catcher.getP2());
        values.put("a5", catcher.getP3());
        values.put("k1", catcher.getP4());
        values.put("k2", catcher.getP5());
        values.put("c1", catcher.getP6());
        values.put("c2", catcher.getP7());
        values.put("c4", catcher.getP8());
        values.put("c5", catcher.getP9());
        values.put("t1", catcher.getP10());
        values.put("t3", catcher.getP11());
        values.put("t4", catcher.getP12());
        values.put("r1", catcher.getP13());
        values.put("r2", catcher.getP14());
        values.put("longitude", catcher.getLongitude());
        values.put("latitude", catcher.getLatitude());
        values.put("valid", catcher.getValid());
        values.put("score", catcher.getScore());
        values.put("pwr", 2000);
        dbInstanse.insert("catcher", null, values);
    }

    public long deleteTableRows(SQLiteDatabase db, String tableName) {
        long id;
        try {
            if (isTableExists(context, tableName, db)) {
                id = db.delete(tableName, "", null);
            } else {
                id = 0;
            }
        } catch (Exception e) {
            id = -1;
        }
        return id;
    }

    public long dropView(SQLiteDatabase db, String view) {
        long id;
        try {
            db.rawQuery("DROP VIEW if exists " + view, null);
            id = 0;
        } catch (Exception e) {
            id = -1;
        }
        return id;
    }

    public static void writeToLogFile2(String data) {
        Log.i("thread", "log2");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "upload.txt");

        if (!file.exists()) {
            try {
                file.createNewFile();

            } catch (Exception e) {
                Log.d("Exception", e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void writeToLogFile(String data) {
        Log.i("thread", "log2");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "logdata.txt");

        if (!file.exists()) {
            try {
                file.createNewFile();

            } catch (Exception e) {
                Log.d("Exception", e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public static String ReadFile(Context context) {
        File sdcard = new File(Environment.getExternalStorageDirectory() + "/Documents/");
//        File sdcard = Environment.getExternalStorageDirectory();
        Log.i("file", "cell");
        String line = null;
        ContentValues values;
        values = new ContentValues();
        SQLiteDatabase db;
        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(context);
        db = MsdDatabaseManager.getInstance().openDatabase();

        try {
            File file = new File(sdcard, "cell_info.csv");
            StringBuilder text = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder stringBuilder = new StringBuilder();
            bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                String[] tokens = line.split(",");
                for (int i = 1; i < tokens.length; i++) {
                    values.put(String.valueOf(columns.get(i - 1)), tokens[i]);
                }
                db.insert("cell_info", null, values);
            }
            line = stringBuilder.toString();

            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            Log.e("Exception", "File write failed: " + ex.toString());
        } catch (IOException ex) {
            Log.e("Exception", "File write failed: " + ex.toString());
        }
        return line;
    }

}