package ir.telecom.sensor.qdmon;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.util.Log;
//import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;

import ir.telecom.sensor.analysis.ImsiCatcher;
import ir.telecom.sensor.util.MsdDatabaseManager;


public class AnalysisEventData implements AnalysisEventDataInterface {
    private SQLiteDatabase db;
    private Context context;

    public AnalysisEventData(Context context) {
        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        this.db = MsdDatabaseManager.getInstance().openDatabase();
        this.context = context;
    }

    private static String[] catcher_cols =
            new String[]
                    {"strftime('%s',timestamp)",
                            "strftime('%s',timestamp) + duration/1000",
                            "id",
                            "mcc",
                            "mnc",
                            "lac",
                            "cid",
                            "latitude",
                            "longitude",
                            "valid",
                            "score",
                            "a1",
                            "a2",
                            "a4",
                            "a5",
                            "k1",
                            "k2",
                            "c1",
                            "c2",
                            "c3",
                            "c4",
                            "c5",
                            "t1",
                            "t3",
                            "t4",
                            "r1",
                            "r2",
                            "f1",
                            "pwr"};
    //************************************** return "ImsiCatcher" object *********************************************************

    static private ImsiCatcher catcherFromCursor(Cursor c, Context context) {
        return new ImsiCatcher
                (c.getLong(0) * 1000L - 16200,    // startTime
                        c.getLong(1) * 1000L - 16200,    // endTime
                        c.getInt(2),            // id
                        c.getInt(3),            // mcc
                        c.getInt(4),            // mnc
                        c.getInt(5),            // lac
                        c.getInt(6),            // cid
                        c.getDouble(7),        // latitude
                        c.getDouble(8),        // longitude
                        true,        // valid
                        c.getDouble(10),        // score
                        c.getDouble(11),        // a1
                        c.getDouble(12),        // a2
                        c.getDouble(13),        // a4
                        c.getDouble(14),        // a5
                        c.getDouble(15),        // k1
                        c.getDouble(16),        // k2
                        c.getDouble(17),        // c1
                        c.getDouble(18),        // c2
                        c.getDouble(19),        // c3
                        c.getDouble(20),        // c4
                        c.getDouble(21),        // c5
                        c.getDouble(22),        // t1
                        c.getDouble(23),        // t3
                        c.getDouble(24),        // t4
                        c.getDouble(25),        // r1
                        c.getDouble(26),        // r2
                        c.getDouble(27),        // f1
                        c.getDouble(28),        // pwr
                        context);
    }


    //*********************************************return "ImsiCatcher" from table "catcher" by start and end time *************************************************
    @Override
    public Vector<ImsiCatcher> getImsiCatchers(long startTime, long endTime) {
        ImsiCatcher catcher;
        Vector<ImsiCatcher> result = new Vector<>();
        Cursor c = null;
        try {
            c = db.query("catcher_with_signal_power", catcher_cols, "strftime('%s',timestamp) >= ? AND strftime('%s',timestamp) <= ?",
                    new String[]{Long.toString(startTime / 1000+ 16200), Long.toString(endTime / 1000+ 16200)}, null, null, null);

            if (c.moveToFirst()) {
                do {
                    catcher = catcherFromCursor(c, context);
                    result.add(catcher);
                } while (c.moveToNext());
            }
        } catch (SQLiteDatabaseLockedException e) {
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return result;
    }
    public String convertDate(String timeStamp) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
        }

        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(parsed);
        return result;
    }

    private static String getDate(long time) {
        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        ;
        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(tz);
        java.util.Date currentTimeZone = new java.util.Date(time * 1000);
        return sdf.format(currentTimeZone);
    }
}
