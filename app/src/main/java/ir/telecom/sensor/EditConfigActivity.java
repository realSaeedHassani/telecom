package ir.telecom.sensor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.database.sqlite.SQLiteDatabase;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.MsdDatabaseManager;

public class EditConfigActivity extends BaseActivity {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private EditText e[];
    private String name[];
    private String values[];
    private MsdConfig msdConfig;
    //    EditText sensor_id;
    private boolean flag;
    EditText dashboardUpdateInterval;
    EditText sensorinfoInterval;
    SharedPreferences sharedPrefs;

    public static final String SHPCONFIG = "shpConfig";
    public static final String T3212 = "T3212";
    public static final String croMin = "croMin";
    public static final String croMax = "croMax";
    public static final String deltaCmcp = "deltaCmcp";
    public static final String nNorm = "nNorm";
    public static final String deltaTch = "deltaTch";
    public static final String valid = "valid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_config);

        setToolbar();
        setDrawer(EditConfigActivity.this, toolbar);
        sharedPrefs = EditConfigActivity.this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);


        //final int checked[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//        sensor_id=(EditText) findViewById(R.id.sensor_id_edit_text);
        dashboardUpdateInterval = (EditText) findViewById(R.id.dashboard_update_interval);
        sensorinfoInterval = (EditText) findViewById(R.id.sensor_info_interval);
        EditText cro_max = (EditText) findViewById(R.id.cro_max_edit_text);
        EditText cro_min = (EditText) findViewById(R.id.cro_min_edit_text);
        EditText t3212_rr = (EditText) findViewById(R.id.t3212_min_edit_text);
        EditText delta_tch = (EditText) findViewById(R.id.delta_tch_edit_text);
        EditText delta_cmcp = (EditText) findViewById(R.id.delta_cmcp_edit_text);
        EditText sensor_update_interval = (EditText) findViewById(R.id.sensor_update_interval);
        EditText n_norm = (EditText) findViewById(R.id.n_norm_edit_text);
        EditText delta_arfcn = (EditText) findViewById(R.id.delta_arfcn_edit_text);
        EditText min_pag1_rate = (EditText) findViewById(R.id.min_page1_rate_edit_text);
        EditText catcher_min_score = (EditText) findViewById(R.id.catcher_min_score_edit_text);
        EditText loc_max_delta = (EditText) findViewById(R.id.loc_max_delta_edit_text);
        EditText cell_info_max_delta = (EditText) findViewById(R.id.cell_info_max_delta_edit_text);
        EditText neig_max_delta = (EditText) findViewById(R.id.neig_max_delta_edit_text);

        e = new EditText[]{sensor_update_interval, cro_max, cro_min, t3212_rr, delta_tch, delta_cmcp, n_norm, delta_arfcn,
                min_pag1_rate, catcher_min_score, loc_max_delta, cell_info_max_delta, neig_max_delta};

        name = new String[]{"sensor_update_interval", "cro_max", "cro_min", "t3212_min", "delta_tch", "delta_cmcp", "n_norm", "delta_arfcn",
                "min_pag1_rate", "catcher_min_score", "loc_max_delta", "cell_info_max_delta", "neig_max_delta"};

        //bahare
        values = new String[13];

        e[0].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                flag = true;

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 60 || Integer.parseInt(editStr) < 1) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[0].setTextColor(Color.RED);

                    } else {
                        e[0].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        e[1].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 60 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[1].setTextColor(Color.RED);

                    } else {
                        e[1].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        e[2].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 40 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[2].setTextColor(Color.RED);

                    } else {
                        e[2].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        e[3].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 10 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[3].setTextColor(Color.RED);

                    } else {
                        e[3].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[4].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 8000 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[4].setTextColor(Color.RED);

                    } else {
                        e[4].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[5].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 2000 || Integer.parseInt(editStr) < 0) {
                        Toasty.error(EditConfigActivity.this, "Please input valid  number", Toast.LENGTH_LONG)
                                .show();
                        e[5].setTextColor(Color.RED);

                    } else {
                        e[5].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[6].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 80 || Integer.parseInt(editStr) < 0) {
                        Toasty.error(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[6].setTextColor(Color.RED);

                    } else {
                        e[6].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[7].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 60 || Integer.parseInt(editStr) < 0) {
                        Toasty.error(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[7].setTextColor(Color.RED);

                    } else {
                        e[7].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[8].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (Float.parseFloat(editStr) > 5.0 || Float.parseFloat(editStr) < 0.0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[8].setTextColor(Color.RED);

                    } else {
                        e[8].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[9].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();

                    if (Float.parseFloat(editStr) > 15.0 || Float.parseFloat(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[9].setTextColor(Color.RED);

                    } else {
                        e[9].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[10].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 600 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid  number", Toast.LENGTH_LONG)
                                .show();
                        e[10].setTextColor(Color.RED);

                    } else {
                        e[10].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[11].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 1800 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[11].setTextColor(Color.RED);

                    } else {
                        e[11].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });
        e[12].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    String editStr = s.toString().trim();
                    if (editStr.contains(".") || Integer.parseInt(editStr) > 600 || Integer.parseInt(editStr) < 0) {
                        Toasty.warning(EditConfigActivity.this, "Please input valid number", Toast.LENGTH_LONG)
                                .show();
                        e[12].setTextColor(Color.RED);

                    } else {
                        e[12].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method
            }
        });

        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(EditConfigActivity.this);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
        Cursor c = msdOpenHelper.selectFullTables(dbInstanse, "config");
        for (int i = 0; i < 13; i++) {
            String value = c.getString(i);
            e[i].setText(value);
        }
        String dashboardUpdateInterval1 = sharedPrefs.getString("dashboardUpdateInterval", "10");
        if (dashboardUpdateInterval1.length() != 0) {
            dashboardUpdateInterval.setText(dashboardUpdateInterval1);
        }
        String sensorinfoInterval1 = sharedPrefs.getString("sensorinfoInterval", "60");
        if (sensorinfoInterval1.length() != 0) {
            sensorinfoInterval.setText(sensorinfoInterval1);
        }

        SharedPreferences myShpConfig = EditConfigActivity.this.getSharedPreferences(SHPCONFIG, Context.MODE_PRIVATE);
        Log.e(">> CFG: ", myShpConfig.getString(valid, "0"));
        if (myShpConfig.getString(valid, "0").equals("1")) {
            Log.e(">> New Config: ", "New config received.");
            cro_max.setText(myShpConfig.getString(croMax, "0"));
            cro_min.setText(myShpConfig.getString(croMin, "0"));
            t3212_rr.setText(myShpConfig.getString(T3212, "0"));
            delta_tch.setText(myShpConfig.getString(deltaTch, "0"));
            delta_cmcp.setText(myShpConfig.getString(deltaCmcp, "0"));
            n_norm.setText(myShpConfig.getString(nNorm, "0"));
        }
    }

    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = toolbar.findViewById(R.id.toolbarDone);
        toolbarDone.setVisibility(View.VISIBLE);
        toolbarTittle.setText("Configuration Settings");

        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String update = e[0].getText().toString();
                String crowMax = e[1].getText().toString();
                String crowMin = e[2].getText().toString();
                String t3212 = e[3].getText().toString();
                String deltaTch = e[4].getText().toString();
                String deltaCmcp = e[5].getText().toString();
                String nNorm = e[6].getText().toString();
                String deltaArfcn = e[7].getText().toString();
                String minPage1 = e[8].getText().toString();
                String catcherMin = e[9].getText().toString();
                String locMax = e[10].getText().toString();
                String cellInfo = e[11].getText().toString();
                String neigMax = e[12].getText().toString();
                String dashboardUpdateIntervalStr = dashboardUpdateInterval.getText().toString();
                String sensorinfoIntervalStr = sensorinfoInterval.getText().toString();
                if (update.length() == 0 || crowMax.length() == 0 || crowMin.length() == 0 || t3212.length() == 0 || deltaTch.length() == 0 || deltaCmcp.length() == 0 ||
                        nNorm.length() == 0 || deltaArfcn.length() == 0 || minPage1.length() == 0 || catcherMin.length() == 0 || locMax.length() == 0
                        || cellInfo.length() == 0 || neigMax.length() == 0 || dashboardUpdateIntervalStr.length() == 0 || sensorinfoIntervalStr.length() == 0) {
                    Toasty.error(EditConfigActivity.this, "Please fill all fields.", Toast.LENGTH_LONG).show();
                } else if (!validateUpdate(update)) {
                    Toasty.error(EditConfigActivity.this, "Sensor update interval is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateCrowMax(crowMax)) {
                    Toasty.error(EditConfigActivity.this, " Max Cell reselection offset is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateCrowMin(crowMin)) {
                    Toasty.error(EditConfigActivity.this, "MainCell reselection offset is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateT3212(t3212)) {
                    Toasty.error(EditConfigActivity.this, "Cell reselect timeout is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateDeltaTch(deltaTch)) {
                    Toasty.error(EditConfigActivity.this, "Traffic channel idle milliseconds is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateDeltaCmcp(deltaCmcp)) {
                    Toasty.error(EditConfigActivity.this, "Delay between command and complete  is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateNNorn(nNorm)) {
                    Toasty.error(EditConfigActivity.this, "Paging groups number is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateDeltaArfcn(deltaArfcn)) {
                    Toasty.error(EditConfigActivity.this, "Arfcn difference in cells is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateMinPage1(minPage1)) {
                    Toasty.error(EditConfigActivity.this, "Type 1 paging rate is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateCatcherMin(catcherMin)) {
                    Toasty.error(EditConfigActivity.this, "IMSI catcher score is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateLocMaxDelta(locMax)) {
                    Toasty.error(EditConfigActivity.this, "Location delta time  is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateCellInfo(cellInfo)) {
                    Toasty.error(EditConfigActivity.this, "Cell_info delta time is not valid", Toast.LENGTH_LONG).show();
                } else if (!validateNeigMax(neigMax)) {
                    Toasty.error(EditConfigActivity.this, "Neighbor info delta time is not valid", Toast.LENGTH_LONG).show();
                } else {

                    for (int i = 0; i < 13; i++) {
                        if (!(e[i].getText().toString().equals(""))) {
                            values[i] = e[i].getText().toString();
                        }
                    }

                    MsdSQLiteOpenHelper msoh = new MsdSQLiteOpenHelper(EditConfigActivity.this);
                    SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
                    String a = msoh.updateConfig(db, name, values);

                    if (flag) {
                        SensorUpdateAlarmManager alarmManager = SensorUpdateAlarmManager.getInstance();
                        alarmManager.setReminder(EditConfigActivity.this);
                    }

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString("dashboardUpdateInterval", dashboardUpdateIntervalStr);
                    editor.commit();


                    SharedPreferences.Editor editor1 = sharedPrefs.edit();
                    editor1.putString("sensorinfoInterval", sensorinfoIntervalStr);
                    editor1.commit();

                    Toasty.success(EditConfigActivity.this, "Done! saves Changed ", Toast.LENGTH_SHORT).show();


                }

            }
        });


        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });


    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(EditConfigActivity.this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

    public boolean validateUpdate(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 60 || Integer.parseInt(port) < 1) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateCrowMax(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 60 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }

    public boolean validateCrowMin(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 40 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateT3212(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 10 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateDeltaTch(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 8000 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateDeltaCmcp(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 2000 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateNNorn(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 80 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateDeltaArfcn(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 60 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateMinPage1(String port) {

        boolean valid;
        if (Float.parseFloat(port) > 5.0 || Float.parseFloat(port) < 0.0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateCatcherMin(String port) {

        boolean valid;
        if (Float.parseFloat(port) > 15.0 || Float.parseFloat(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateLocMaxDelta(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 600 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }


    public boolean validateCellInfo(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 1800 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }

    public boolean validateNeigMax(String port) {

        boolean valid;
        if (port.contains(".")) {
            valid = false;
        } else if (Integer.parseInt(port) > 600 || Integer.parseInt(port) < 0) {
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }
}
