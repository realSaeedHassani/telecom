package ir.telecom.sensor.views.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;

import ir.telecom.sensor.DashboardActivity;
import ir.telecom.sensor.DetailChartActivity;
import ir.telecom.sensor.R;
import ir.telecom.sensor.analysis.ImsiCatcher;

//bahare
public class ListViewImsiCatcherAdapter extends ArrayAdapter<ImsiCatcher> {
    // Attributes
    private final Context context;
    private final Vector<ImsiCatcher> values;
    private DetailChartActivity host;

    public ListViewImsiCatcherAdapter(Context context, Vector<ImsiCatcher> values) {
        super(context, R.layout.custom_row_layout_sms, values);
        this.context = context;
        this.values = values;
        this.host = (DetailChartActivity) context;
    }

    private String addScore(String text, String scoreName, double score) {

//        String[] check={"0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
//        SharedPreferences sharedPrefs =  context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
//        editor = sharedPrefs.edit();
//        for(int j=0; j<14;j++){
//            boolean flag=sharedPrefs.getBoolean(names[j],false);
//            if(flag){
//                check[j]="1";
//            }
//        }
        if (score > 0.0) {
            String scoreText = String.format("%.1f", score);
            return text + ", " + scoreName + "=" + scoreText;
        }
        return text;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SharedPreferences time = context.getSharedPreferences("GetLastAnalysis", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = time.edit();

        String names[] = {"Inconsistent LAC", "Different ARFCNs", "Lonesome LA", "Non Neighbor", "High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher", "Low t3212", "Empty Paging", "Orphaned Channel", "Inconsistent Neighbor", "High Paging"};
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.custom_row_layout_imsicatcher, parent, false);

        ImsiCatcher catcher = values.get(position);

        SharedPreferences totalscore = context.getSharedPreferences("TotalScore", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        String scoreText = String.format("%.2f", catcher.getScore());

        SharedPreferences paramscore = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        if (paramscore.getBoolean(names[0], true)) {
            scoreText = addScore(scoreText, "P1", catcher.getA2());
        }

        if (paramscore.getBoolean(names[1], true)) {
            scoreText = addScore(scoreText, "P2", catcher.getA4());
        }
        if (paramscore.getBoolean(names[2], true)) {
            scoreText = addScore(scoreText, "P3", catcher.getA5());
        }
        if (paramscore.getBoolean(names[3], true)) {
            scoreText = addScore(scoreText, "P4", catcher.getK1());
        }
        if (paramscore.getBoolean(names[4], true)) {
            scoreText = addScore(scoreText, "P5", catcher.getK2());
        }
        if (paramscore.getBoolean(names[5], true)) {
            scoreText = addScore(scoreText, "P6", catcher.getC1());
        }
        if (paramscore.getBoolean(names[6], true)) {
            scoreText = addScore(scoreText, "P7", catcher.getC2());
        }
        if (paramscore.getBoolean(names[7], true)) {
            scoreText = addScore(scoreText, "P8", catcher.getC4());
        }
        if (paramscore.getBoolean(names[8], true)) {
            scoreText = addScore(scoreText, "P9", catcher.getC5());
        }
        if (paramscore.getBoolean(names[9], true)) {
            scoreText = addScore(scoreText, "P10", catcher.getT1());
        }
        if (paramscore.getBoolean(names[10], true)) {
            scoreText = addScore(scoreText, "P11", catcher.getT3());
        }
        if (paramscore.getBoolean(names[11], true)) {
            scoreText = addScore(scoreText, "P12", catcher.getT4());
        }
        if (paramscore.getBoolean(names[12], true)) {
            scoreText = addScore(scoreText, "P13", catcher.getR1());
        }
        if (paramscore.getBoolean(names[13], true)) {
            scoreText = addScore(scoreText, "P14", catcher.getR2());
        }

        ((TextView) rowView.findViewById(R.id.txtImsiRowScoreValue)).setText(scoreText);

        // Set time
        Timestamp stamp = new Timestamp(values.get(position).getStartTime());

        ((TextView) rowView.findViewById(R.id.txtImsiRowTimeValue)).setText(getDate(stamp.getTime()/1000 - 16200) );

        editor.putString("GetLastAnalysis", DateFormat.getDateTimeInstance().format(stamp.getTime()));
        editor.commit();
        // Set position
        ((TextView) rowView.findViewById(R.id.txtImsiRowPositionValue)).setText(values.get(position).getLocation());

        // Set cell id
        ((TextView) rowView.findViewById(R.id.txtImsiRowCellIdValue)).setText(String.valueOf(values.get(position).getFullCellID()));

        return rowView;
    }
    private   String getDate(long time) {
        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(tz);
        java.util.Date currentTimeZone = new java.util.Date(time * 1000);
        return sdf.format(currentTimeZone);
    }
}

