package ir.telecom.sensor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdDatabaseManager;

public class ShowTablesRows extends BaseActivity {

    Intent intent;
    TableLayout table_layout ;
    private Toolbar toolbar;
    private boolean change=false;
    private String timeColNumber;
    private String tableName;
    MsdSQLiteOpenHelper msoh;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tables);



        Cursor c;
        intent = getIntent();
        if(intent!=null){
            timeColNumber=intent.getStringExtra("timeColNumber");
            tableName=intent.getStringExtra("info");

        }else{
            timeColNumber="";
            tableName="";
        }


        table_layout = (TableLayout) findViewById(R.id.tableLayout1);
        setToolbar();

        msoh = new MsdSQLiteOpenHelper(this);
        db = MsdDatabaseManager.getInstance().openDatabase();

        Log.e(">> Table name: ","*** " +  tableName);
        c = msoh.selectFullTables(db,tableName);

        int rows = c.getCount();
        int cols = c.getColumnCount();


        TableRow row1 = new TableRow(this);
        row1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));

        for (int j = 0; j < cols; j++)
        {
            String tempString=c.getColumnName(j);
            if(tableName.equals("catcher")){

                if (tempString.equals("a1") || tempString.equals("c3") || tempString.equals("f1")){
                    j++;
                    tempString=c.getColumnName(j);
                }

                switch(tempString){
                    case "a2":{
                        tempString="P1";
                        break;
                    }
                    case "a4":{
                        tempString="P2";
                        break;
                    }
                    case "a5":{
                        tempString="P3";
                        break;
                    }
                    case "k1":{
                        tempString="P4";
                        break;
                    }

                    case "k2":{
                        tempString="P5";
                        break;
                    }

                    case "c1":{
                        tempString="P6";
                        break;
                    }

                    case "c2":{
                        tempString="P7";
                        break;
                    }

                    case "c4":{
                        tempString="P8";
                        break;
                    }

                    case "c5":{
                        tempString="P9";
                        break;
                    }

                    case "t1":{
                        tempString="P10";
                        break;
                    }

                    case "t3":{
                        tempString="P11";
                        break;
                    }

                    case "t4":{
                        tempString="P12";
                        break;
                    }

                    case "r1":{
                        tempString="P13";
                        break;
                    }

                    case "r2":{
                        tempString="P14";
                        break;
                    }

                }
            }
            TextView tv = new TextView(this);
            SpannableString spanString = new SpannableString(tempString);
            spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
            spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
            tv.setText(spanString);
            tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT));
            tv.setGravity(Gravity.CENTER);
            tv.setTextSize(18);
            tv.setPadding(0, 5, 10, 5);
            row1.addView(tv);
        }
        table_layout.addView(row1);
        c.moveToFirst();

        if (rows != 0) {
            for (int i = 0; i < rows; i++) {

                TableRow row = new TableRow(this);
                row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));

                // inner for loop
                for (int j = 0; j < cols; j++) {

                    if(tableName.equals("catcher")){
                        if(j==7 || j==14 || j==23){
                            j++;
                        }
                    }

                    TextView tv = new TextView(this);
                    tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT));
                    tv.setGravity(Gravity.CENTER);
                    tv.setTextSize(18);
                    tv.setPadding(0, 5, 10, 5);

                    if(timeColNumber!=null && timeColNumber.length()>0) {
                        if (j == Integer.valueOf(timeColNumber)) {
                            tv.setText(convertDate(c.getString(j)));
                            if(intent.getStringExtra("info").equals("cell_info") && !change){
                                timeColNumber=String.valueOf(Integer.valueOf(timeColNumber)+1);
                                change=true;
                            }
                        } else {
                            tv.setText(c.getString(j));
                        }

                    }else{
                        tv.setText(c.getString(j));
                        }


                    row.addView(tv);
                }

                c.moveToNext();

                if(intent.getStringExtra("info").equals("cell_info") && change){
                    timeColNumber=String.valueOf(Integer.valueOf(timeColNumber)-1);
                    change=false;
                }

                table_layout.addView(row);

            }
        }
        else
        {
            Toasty.error(this,"No data ",Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);
        toolbarTittle.setText(tableName);

        toolbarDone.setText("Clear");
        if(tableName.equals("config")){
            toolbarDone.setVisibility(View.INVISIBLE);
        }

        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ShowTablesRows.this);
                alertDialogBuilder
                        .setMessage("Do you want to clear this table?")
                        .setCancelable(false)
                        .setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        long rowId=msoh.deleteTableRows(db,tableName);
                                        if(rowId!= -1) {
                                            table_layout.removeAllViews();
                                        }

                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });




        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        finish();

    }

    public  String  convertDate(String timeStamp) {
        Log.e(">> ShowTablesRows_0: ", timeStamp);


//        Log.e("time",timeStamp);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
//            Log.e("pari","error:"+e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("Asia/Tehran");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(parsed);
        Log.e(">> ShowTablesRows_1: ", result);
        return result;
    }
}
