package ir.telecom.sensor;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.geniusforapp.fancydialog.FancyAlertDialog;

import net.skoumal.emulatordetector.EmulatorDetector;

import androidx.appcompat.app.AppCompatActivity;
import ir.telecom.sensor.error.handling.ErrorControlObject;

/**
 * SplashScreen
 * Class to show splash screen at the begining
 *
 * @author Bahar
 */
public class SplashScreen extends AppCompatActivity {
    private ErrorControlObject errorControlObject = new ErrorControlObject();

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /*
         * ....................
         * >> Error Handling. |
         * ....................
         */
//        errorControlObject.setmCrashText("Oops!!! \nClass Name:"+this.getLocalClassName());
//        errorControlObjaect.setmDestinationActivity(SplashScreen.class);
//        Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(this, errorControlObject));

        super.onCreate(savedInstanceState);
        //  Fabric.with(this, new Crashlytics());


        setContentView(R.layout.activity_splash_screen);
        /**
         * ........................................
         * @programmer Saeed                      .
         * @supervisor Seyed-Mostafa              .
         *                                        .
         * @comment
         * ........................................
         * >> Run only real devices, not emulators.
         * ........................................
         */
        if (!EmulatorDetector.isEmulator()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT < 17) {
                        String release = Build.VERSION.RELEASE;

                        FancyAlertDialog.Builder alert = new FancyAlertDialog.Builder(SplashScreen.this)
                                .setTextSubTitle(" (Android " + release + ")")
                                .setBody("This app is not running on android " + release + " or lower\n\n" +
                                        "Your android version is: " + release + "\n" +
                                        "Minimum android required: 4.2")
                                .setPositiveButtonText("Ok")
                                .setPositiveColor(R.color.dialog_okay)
                                .setOnPositiveClicked(new FancyAlertDialog.OnPositiveClicked() {
                                    @Override
                                    public void OnClick(View view, Dialog dialog) {
//                                        System.exit(0);
                                    }
                                })
                                .setBodyGravity(FancyAlertDialog.TextGravity.LEFT)
                                .setTitleGravity(FancyAlertDialog.TextGravity.CENTER)
                                .setSubtitleGravity(FancyAlertDialog.TextGravity.CENTER)
                                .setCancelable(false)
                                .build();
                        alert.show();
                    } else {
                        Intent intent = new Intent(SplashScreen.this, StartupActivity.class);
                        startActivity(intent);
                        SplashScreen.this.finish();
                    }
                }
            }, 3000);
        }
    }

}