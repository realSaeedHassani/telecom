package ir.telecom.sensor;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;

import ir.telecom.sensor.error.handling.ErrorControlObject;
import ir.telecom.sensor.error.handling.UncaughtException;
import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.qdmon.MsdServiceCallback;
import ir.telecom.sensor.qdmon.MsdServiceHelper;
import ir.telecom.sensor.qdmon.StateChangedReason;
import ir.telecom.sensor.util.Constants;
import ir.telecom.sensor.util.MsdDatabaseManager;

//import de.srlabs.sensor.upload.DumpFile;

/**
 * This Activity can be opened from an Android Notification after a fatal error
 * occurred. It works independently from the main App (and therefore in another
 * process).
 * <p>
 * It provides the possibility to upload the debug logfile for the crash to
 * report the bug.
 */
public class CrashUploadActivity extends Activity implements MsdServiceCallback {
    private static String TAG = "CrashUploadActivity";
    public static String EXTRA_ERROR_TEXT = "ERROR_TEXT";
    public static String EXTRA_ERROR_ID = "ERROR_ID";
    private String errorText;
    private long fileId;
    private MsdServiceHelper helper;
    private boolean triggerUploadingPending = false;
    private ErrorControlObject errorControlObject = new ErrorControlObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * ....................
         * >> Error Handling. |
         * ....................
         */
        errorControlObject.setmCrashText(this.getLocalClassName());
        errorControlObject.setmDestinationActivity(CrashUploadActivity.class);
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(this, errorControlObject));

        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(CrashUploadActivity.this));
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        errorText = extras.getString(EXTRA_ERROR_TEXT);
        fileId = extras.getLong(EXTRA_ERROR_ID);
    }


    @Override
    public void stateChanged(StateChangedReason reason) {
        if (triggerUploadingPending && helper.isConnected()) {
            triggerUploadingPending = false;
            cancelNotification();
            quitApplication();
        }
    }

    @Override
    public void internalError(String msg) {
    }

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_ID_INTERNAL_ERROR);
    }

    protected void quitApplication() {
//        finish();
//        System.exit(0);
    }
}
