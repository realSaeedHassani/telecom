package ir.telecom.sensor.server;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ir.telecom.sensor.model.CellInfo;
import ir.telecom.sensor.model.NameValuePair;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.network.AsyncConnect;
import ir.telecom.sensor.network.ConnFailResponse;
import ir.telecom.sensor.network.HTTPRequest;
import ir.telecom.sensor.network.HTTPResponse;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.Utils;

/**
 * Created by Bahar on 9/4/2018.
 */

public class CellInfoRequests implements AsyncConnect.ShowResponse {

    private static final String TAG = CellInfoRequests.class.getName();
    private Context context;
    private ServerInfo serverInfo;
    private CellInfo lastCellInfoOfList;


    /**
     * constructor
     *
     * @param context
     * @param serverInfo
     */

    public CellInfoRequests(Context context, ServerInfo serverInfo) {
        this.context = context;
        this.serverInfo = serverInfo;
    }


    /**
     * send sensor date to server
     */
    public void sendCellInfoData(ArrayList<CellInfo> cellInfo) {
        if (isOnline()) {
            HTTPRequest sendSessionInfoRequest = null;
            try {
                sendSessionInfoRequest = generateSendCellInfoRequest(cellInfo);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            String protocol = sharedPrefs.getString("protocol", "https");
            AsyncConnect asyncConnect = new AsyncConnect(CellInfoRequests.this, protocol, serverInfo, sendSessionInfoRequest);
            asyncConnect.execute();
        } else {
            Toast.makeText(context, "There is no internet connection to send data to server", Toast.LENGTH_LONG).show();
        }
    }

    private HTTPRequest generateSendCellInfoRequest(ArrayList<CellInfo> cellInfo) throws InvocationTargetException, IllegalAccessException, JSONException {

        if (cellInfo != null && cellInfo.size() > 0) {
            lastCellInfoOfList = cellInfo.get(cellInfo.size() - 1);
        }
        JSONArray jsonArray = new JSONArray();
//        JSONObject jsonArray = new JSONObject();
        //      Method[] methods = CellInfo.class.getDeclaredMethods();
//writeToLogFile(cellInfo.get(i));
        for (int i = 0; i < cellInfo.size(); i++) {
            writeToLogFile2(String.valueOf(i));
            writeToLogFile2(cellInfo.get(i).toJson());
            JSONObject jsonParam = new JSONObject(cellInfo.get(i).toJson());
//            for (Method method : methods) {
//                String methodName = method.getName();
//                String perfix = methodName.substring(0, 3);
//                if (perfix.equals("get")) {
//
//                    String upAttributeName = methodName.substring(3);
//                    StringBuilder sb = new StringBuilder(upAttributeName);
//                    sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
//                    String attributeName = sb.toString();
//                    jsonParam.put(attributeName, String.valueOf(method.invoke(cellInfo.get(i))));
//                }
//            }
            jsonArray.put(jsonParam);
        }
        String jsonBody = jsonArray.toString();

        writeToLogFile(jsonBody);

        Utils.writeToFile(jsonBody, "CellInfoRequests");

        NameValuePair pair = new NameValuePair("list", "1");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(pair);

        for (NameValuePair n : pairs)

            Log.e(">> NameValuePair: ", n.getName() + " = " + n.getValue());


        Log.e(">> Body: ", jsonBody);

        MsdConfig msdConfig = new MsdConfig();
        String sensor_id = msdConfig.getAppId(context);

        HTTPRequest httpRrequest = new HTTPRequest("POST", jsonBody, pairs, "/sensors/" + sensor_id + "/cell-info");
        return httpRrequest;
    }


    @Override
    public void getPostResponse(HTTPResponse response) {

        if (response instanceof ConnFailResponse) {

            ConnFailResponse connFailResponse = (ConnFailResponse) response;
            connFailResponse.extractAttributes();
            Toast.makeText(context, connFailResponse.getMessage(), Toast.LENGTH_LONG).show();

        } else {
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            if (lastCellInfoOfList != null) {
                editor.putString("lastCellInfoSentTime", convertDate(lastCellInfoOfList.getFirstSeen()));
            }

//            long newTimeMilliseconds = System.currentTimeMillis();
//            android.text.format.DateFormat df = new android.text.format.DateFormat();
//            String newDate= (String) df.format("yyyy-MM-dd HH:mm:ss", new Date(newTimeMilliseconds));

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            final String newDate = sdf.format(new Date());

            editor.putString("CellInfoLastSyncTime", newDate);

            editor.commit();

            Toast.makeText(context, "cellinfo table sent", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Check the device is connected to internet or not.
     *
     * @return boolean shows divice is connected or not
     */
    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {

            isAvailable = true;
        }
        return isAvailable;
    }

    public String convertDate(String timeStamp) {
        Log.e(">> CellINFO_0: ", timeStamp);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
//            Log.e("pari","error:"+e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("UTC");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);
        Log.e(">> CellINFO_1: ", destFormat.format(parsed));
        return destFormat.format(parsed);
    }

    public void writeToLogFile(String data) {
//        Log.i("thread","log2");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "jsoncell_info.txt");

        if (!file.exists()) {
            try {
                file.createNewFile();

            } catch (Exception e) {
//                Log.d("Exception",e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void writeToLogFile2(String data) {
//        Log.i("thread","log2");
        File dir = new File(Environment.getExternalStorageDirectory() + "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "arraycell_info.txt");

        if (!file.exists()) {
            try {
                file.createNewFile();

            } catch (Exception e) {
//                Log.d("Exception",e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
