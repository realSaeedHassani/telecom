package ir.telecom.sensor.server;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ir.telecom.sensor.model.NameValuePair;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.model.SessionInfo;
import ir.telecom.sensor.network.AsyncConnect;
import ir.telecom.sensor.network.ConnFailResponse;
import ir.telecom.sensor.network.HTTPRequest;
import ir.telecom.sensor.network.HTTPResponse;
import ir.telecom.sensor.qdmon.MsdService;
import ir.telecom.sensor.util.MsdConfig;

/**
 * Created by Bahar on 9/3/2018.
 */

public class SessionInfoRequests implements AsyncConnect.ShowResponse {

    private Context context;
    private ServerInfo serverInfo;
    private SessionInfo lastSessionInfoOfList;


    /**
     * constructor
     *
     * @param context
     * @param serverInfo
     */

    public SessionInfoRequests(Context context, ServerInfo serverInfo) {
        this.context = context;
        this.serverInfo = serverInfo;
    }

    /**
     * send sensor date to server
     */
    public void sendSessionInfoData(ArrayList<SessionInfo> sessionInfo) {
        if (isOnline()) {
            HTTPRequest sendSessionInfoRequest = null;
            try {
                sendSessionInfoRequest = generateSendSessionInfoRequest(sessionInfo);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            String protocol = sharedPrefs.getString("protocol", "https");

            AsyncConnect asyncConnect = new AsyncConnect(SessionInfoRequests.this, protocol, serverInfo, sendSessionInfoRequest);
            asyncConnect.execute();
        } else {
            Toast.makeText(context, "There is no internet connection to send data to server", Toast.LENGTH_LONG).show();
        }
    }

    private HTTPRequest generateSendSessionInfoRequest(ArrayList<SessionInfo> sessionInfo) throws InvocationTargetException, IllegalAccessException, JSONException {
//        MsdService.writeToLogFile2("SESSIONINFOOBJECT: "+sessionInfo.get(0).toString()+"\r\n");
        if (sessionInfo != null && sessionInfo.size()>0) {
            lastSessionInfoOfList = sessionInfo.get(sessionInfo.size() - 1);
        }
        JSONArray jsonArray = new JSONArray();
        //Method[] methods = SessionInfo.class.getDeclaredMethods();

        for (int i = 0; i < sessionInfo.size(); i++) {

            JSONObject jsonParam = new JSONObject(sessionInfo.get(i).toJson());

//            MsdService.writeToLogFile2("METHOD SIZE: "+methods.length+"\r\n");
//            for (Method method : methods) {
//                MsdService.writeToLogFile2("method name: "+method.getName()+"\r\n");
//                String methodName = method.getName();
//                String perfix = methodName.substring(0, 3);
//                if (perfix.equals("get")) {
//
//                    String upAttributeName = methodName.substring(3);
//                    StringBuilder sb = new StringBuilder(upAttributeName);
//                    sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
//                    String attributeName = sb.toString();
//                    MsdService.writeToLogFile2("att name: "+attributeName+"\r\n");
//                    MsdService.writeToLogFile2("BEFORE CALLING GET: "+ method.invoke(sessionInfo.get(i))+"\r\n");
//                    jsonParam.put(attributeName, String.valueOf(method.invoke(sessionInfo.get(i))));
//                    MsdService.writeToLogFile2("AFTER CALLING GET - JSON PARAM: "+ jsonParam.toString()+"\r\n");
//                }
//            }

            jsonArray.put(jsonParam);
        }
        String jsonBody = jsonArray.toString();
//        MsdService.writeToLogFile2("GENERATED JSON: "+jsonBody+"\r\n");
//        Log.i("json",jsonBody);
//        writeToLogFile(String.valueOf(jsonBody));
        NameValuePair pair = new NameValuePair("list", "1");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(pair);


        MsdConfig msdConfig = new MsdConfig();
        String sensor_id = msdConfig.getAppId(context);

        HTTPRequest httpRrequest = new HTTPRequest("POST", jsonBody, pairs, "/sensors/" + sensor_id + "/session-info");
        return httpRrequest;


    }
    public void writeToLogFile(String data)
    {
//        Log.i("thread","log2");
        File dir = new File (Environment.getExternalStorageDirectory()+ "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "logdata2.txt");

        if(!file.exists())
        {
            try {
                file.createNewFile();

            }
            catch (Exception e)
            {
//                Log.d("Exception",e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut= new FileOutputStream(file,true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    @Override
    public void getPostResponse(HTTPResponse response) {
        if (response instanceof ConnFailResponse) {

            ConnFailResponse connFailResponse = (ConnFailResponse) response;
            connFailResponse.extractAttributes();
            Toast.makeText(context, connFailResponse.getMessage(), Toast.LENGTH_LONG).show();

        } else {
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            if (lastSessionInfoOfList != null) {
                editor.putString("lastSessionInfoSentTime", convertDate(lastSessionInfoOfList.getTimestamp()));
            }
//            long newTimeMilliseconds = System.currentTimeMillis();
//            android.text.format.DateFormat df = new android.text.format.DateFormat();
//            String newDate= (String) df.format("yyyy-MM-dd HH:mm:ss", new Date(newTimeMilliseconds));

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            final String newDate = sdf.format(new Date());

            editor.putString("SessionInfoLastSyncTime", newDate);

            editor.commit();

            Toast.makeText(context, "Session Info table is sent", Toast.LENGTH_LONG).show();
        }



    }

    /**
     * Check the device is connected to internet or not.
     *
     * @return boolean shows divice is connected or not
     */
    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {

            isAvailable = true;
        }
        return isAvailable;
    }

    public  String  convertDate(String timeStamp) {
        Log.e(">> SessionInfoReq--0: ", timeStamp);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
//            Log.e("pari","error:"+e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("UTC");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(parsed);
        Log.e(">> SessionInfoReq--1: ", result);
        return result;
    }
}
