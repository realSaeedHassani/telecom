package ir.telecom.sensor.server;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ir.telecom.sensor.model.NameValuePair;
import ir.telecom.sensor.model.Sensor;
import ir.telecom.sensor.model.ServerInfo;
import ir.telecom.sensor.network.AsyncConnect;
import ir.telecom.sensor.network.ConnFailResponse;
import ir.telecom.sensor.network.HTTPRequest;
import ir.telecom.sensor.network.HTTPResponse;
import ir.telecom.sensor.util.MsdConfig;

/**
 * Created by Bahar on 9/3/2018.
 */

public class SensorRequests implements AsyncConnect.ShowResponse {

    private Context context;
    private ServerInfo serverInfo;
    private Sensor lastSensorOfList;


    /**
     * constructor
     *
     * @param context
     * @param serverInfo
     */

    public SensorRequests(Context context, ServerInfo serverInfo) {
        this.context = context;
        this.serverInfo = serverInfo;
    }

    /**
     * send sensor date to server
     */
    public void sendSensorData(ArrayList<Sensor> sensor) {
        if (isOnline()) {
            HTTPRequest sendSensorRequest = null;
            try {
                sendSensorRequest = generateSendSensorRequest(sensor);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            String protocol = sharedPrefs.getString("protocol", "https");

            AsyncConnect asyncConnect = new AsyncConnect(SensorRequests.this, protocol, serverInfo, sendSensorRequest);
            asyncConnect.execute();
        } else {
            Toast.makeText(context, "There is no internet connection to send data to server", Toast.LENGTH_LONG).show();
        }
    }

    private HTTPRequest generateSendSensorRequest(ArrayList<Sensor> sensor) throws InvocationTargetException, IllegalAccessException, JSONException {

        if (sensor != null && sensor.size()>0) {
            lastSensorOfList = sensor.get(sensor.size() - 1);
        }
        JSONArray jsonArray = new JSONArray();
//        Method[] methods = Sensor.class.getDeclaredMethods();

        for (int i = 0; i < sensor.size(); i++) {
            JSONObject jsonParam = new JSONObject(sensor.get(i).toJson());
//            for (Method method : methods) {
//                String methodName = method.getName();
//                String perfix = methodName.substring(0, 3);
//                if (perfix.equals("get")) {
//
//                    String upAttributeName = methodName.substring(3);
//                    StringBuilder sb = new StringBuilder(upAttributeName);
//                    sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
//                    String attributeName = sb.toString();
//                    jsonParam.put(attributeName, String.valueOf(method.invoke(sensor.get(i))));
//                }
//            }
            jsonArray.put(jsonParam);
        }
        String jsonBody = jsonArray.toString();


        NameValuePair pair = new NameValuePair("list", "1");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(pair);


        MsdConfig msdConfig = new MsdConfig();
        String sensor_id = msdConfig.getAppId(context);

        HTTPRequest httpRrequest = new HTTPRequest("POST", jsonBody, pairs, "/sensors/" + sensor_id + "/sensor-status");
        return httpRrequest;
    }


    @Override
    public void getPostResponse(HTTPResponse response) {

        if (response instanceof ConnFailResponse) {

            ConnFailResponse connFailResponse = (ConnFailResponse) response;
            connFailResponse.extractAttributes();
            Toast.makeText(context, connFailResponse.getMessage(), Toast.LENGTH_LONG).show();

        } else {
            SharedPreferences sharedPrefs = context.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            if (lastSensorOfList != null) {
                editor.putString("lastSensorSentTime", convertDate(lastSensorOfList.getTimestamp()));
            }

//            long newTimeMilliseconds = System.currentTimeMillis();
//            android.text.format.DateFormat df = new android.text.format.DateFormat();
//            String newDate= (String) df.format("yyyy-MM-dd HH:mm:ss", new Date(newTimeMilliseconds));

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            final String newDate = sdf.format(new Date());


            editor.putString("SensorLastSyncTime", newDate);

            editor.commit();

            Toast.makeText(context, "Sensor table is sent", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Check the device is connected to internet or not.
     *
     * @return boolean shows divice is connected or not
     */
    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {

            isAvailable = true;
        }
        return isAvailable;
    }

    public  String  convertDate(String timeStamp) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
        Date parsed = null; // => Date is in UTC now
        try {
            parsed = sourceFormat.parse(timeStamp);
        } catch (ParseException e) {
//            Log.e("pari","error:"+e.getMessage());
        }

        TimeZone tz = TimeZone.getTimeZone("UTC");
        //SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(parsed);
        return result;
    }
}
