package ir.telecom.sensor.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.Vector;

import ir.telecom.sensor.BaseActivity;
import ir.telecom.sensor.StartupActivity;
import ir.telecom.sensor.analysis.ImsiCatcher;
import ir.telecom.sensor.qdmon.MsdServiceCallback;
import ir.telecom.sensor.qdmon.MsdServiceHelper;
import ir.telecom.sensor.qdmon.StateChangedReason;

public class MSDServiceHelperCreator implements MsdServiceCallback {
    // Attributes
    private static MSDServiceHelperCreator _instance = null;
    private MsdServiceHelper msdServiceHelper;
    private int rectWidth;
    private Activity currentActivity;
    private boolean autostartRecordingPending = false;


    // Methods
    private MSDServiceHelperCreator(Context context, boolean autostartRecording) {
        msdServiceHelper = new MsdServiceHelper(context, this);
        this.autostartRecordingPending = autostartRecording;
    }
    // A_1 start auto recording at start page
    // start to auto recording and get instance

    public static MSDServiceHelperCreator getInstance(Context context) {
        if (_instance == null) {
            boolean autostartRecording = StartupActivity.isSNSNCompatible() && (PermissionChecker.isAccessingFineLocationAllowed(context) || PermissionChecker.isAccessingCoarseLocationAllowed(context));
            _instance = new MSDServiceHelperCreator(context, autostartRecording);
        }
        return _instance;
    }

    public static MSDServiceHelperCreator getInstance() {
        return _instance;
    }

    public MsdServiceHelper getMsdServiceHelper() {
        return msdServiceHelper;
    }

    // get event and convert to avvary for upload
    //convert to array
    private boolean[] vectorToUploadedBooleanArray(Vector<ImsiCatcher> events) {
        boolean[] result = new boolean[events.size()];
        for (int i = 0; i < events.size(); i++)
            result[i] = true;
        return result;
    }

    public boolean[] getThreatsImsiMonthSum() {
        return vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(TimeSpace.getTimeSpaceMonth().getStartTime(),
                TimeSpace.getTimeSpaceMonth().getEndTime()));
    }

    public boolean[][] getThreatsImsiMonth() {
        boolean[][] imsiMonth = new boolean[4][];
        long calEnd = TimeSpace.getTimeSpaceMonth().getEndTime();
        long timeSpan = (TimeSpace.getTimeSpaceMonth().getEndTime() - TimeSpace.getTimeSpaceMonth().getStartTime()) / 4;

        for (int i = 0; i < imsiMonth.length; i++) {
            imsiMonth[i] = vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(calEnd - timeSpan, calEnd));
            calEnd -= timeSpan;
        }
        return imsiMonth;
    }

    public boolean[] getThreatsImsiWeekSum() {
        return vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(TimeSpace.getTimeSpaceWeek().getStartTime(),
                TimeSpace.getTimeSpaceWeek().getEndTime()));
    }

    public boolean[][] getThreatsImsiWeek() {
        boolean[][] imsiWeek = new boolean[7][];
        long calEnd = TimeSpace.getTimeSpaceWeek().getEndTime();
        long timeSpan = (TimeSpace.getTimeSpaceWeek().getEndTime() - TimeSpace.getTimeSpaceWeek().getStartTime()) / 7;

        for (int i = 0; i < imsiWeek.length; i++) {
            imsiWeek[i] = vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(calEnd - timeSpan, calEnd));
            calEnd -= timeSpan;
        }

        return imsiWeek;
    }

    public boolean[][] getThreatsImsiDay() {
        boolean[][] imsiDay = new boolean[6][];
        long calEnd = TimeSpace.getTimeSpaceDay().getEndTime();
        long timeSpan = (TimeSpace.getTimeSpaceDay().getEndTime() - TimeSpace.getTimeSpaceDay().getStartTime()) / 6;

        for (int i = 0; i < imsiDay.length; i++) {
            imsiDay[i] = vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(calEnd - timeSpan, calEnd));
            calEnd = (calEnd - timeSpan);
        }
        return imsiDay;
    }

    public boolean[] getThreatsImsiDaySum() {
        return vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(TimeSpace.getTimeSpaceDay().getStartTime(),
                TimeSpace.getTimeSpaceDay().getEndTime()));
    }

    public boolean[][] getThreatsImsiHour() {
        boolean[][] imsiHour = new boolean[12][];
        long calEnd = TimeSpace.getTimeSpaceHour().getEndTime()/1000;
        long timeSpan = (TimeSpace.getTimeSpaceHour().getEndTime()/1000 - TimeSpace.getTimeSpaceHour().getStartTime()/1000)/12;
        for (int i = 0; i < imsiHour.length; i++) {
            imsiHour[i] = vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(calEnd *1000- timeSpan*1000,
                    calEnd*1000));
            calEnd = (calEnd - timeSpan);
        }

        return imsiHour;
    }

    public boolean[] getThreatsImsiHourSum() {
        return vectorToUploadedBooleanArray(msdServiceHelper.getData().getImsiCatchers(TimeSpace.getTimeSpaceHour().getStartTime(),
                TimeSpace.getTimeSpaceHour().getEndTime()));
    }


    public void setRectWidth(int rectWidth) {
        this.rectWidth = rectWidth;
    }

    public int getRectWidth() {
        return rectWidth;
    }

    public void setCurrentActivity(Activity activity) {
        this.currentActivity = activity;
    }

    @Override
    // state change to start recording
    public void stateChanged(StateChangedReason reason) {
        Log.e(">> State: ", "stateChanged");
        if (autostartRecordingPending && msdServiceHelper.isConnected()) {
            if (!msdServiceHelper.isRecording())
                msdServiceHelper.startRecording();
            autostartRecordingPending = false;
        }
        try {
            ((BaseActivity) currentActivity).stateChanged(reason);
        } catch (Exception e) {
            // TODO: Log output here?
        }
    }

    @Override
    public void internalError(String msg) {
        if (currentActivity instanceof BaseActivity) {
            ((BaseActivity) currentActivity).internalError(msg);
        }
    }
}
