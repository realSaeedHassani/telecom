package ir.telecom.sensor.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ir.telecom.sensor.R;
import okhttp3.Request;
import okio.Buffer;

public class Utils {

    private final static String TAG = "SNOOP";
    private final static String mTAG = "Utils";

    /**
     * Generates a new random app ID. Currently the App id consists of 8
     * hexadecimal digits generated based on the Android SecureRandom class.
     *
     * @return
     */
    @SuppressLint("TrulyRandom")
    public static String generateAppId() {
        SecureRandom sr = new SecureRandom();
        byte[] random = new byte[4];
        sr.nextBytes(random);
        return String.format("%02x%02x%02x%02x", random[0], random[1], random[2], random[3]);
    }

    public static String formatTimestamp(long millis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date(millis);
        return dateFormat.format(date);
    }


    //****************************************************************************************************
    /*
    // ----------------------------------------------------------------------
    // Here we are interested in the technology and not the speed, so we
    // consider everything with 2,2.5,2.75 as "2G". Same for 3G.
    // The following were extracted from Google sources in getNetworkClass(),
    // from TelephonyManager.java.
    // ----------------------------------------------------------------------
			//2G
			NETWORK_TYPE_1xRTT      *       // This is labelled by Google as 2G, but is really a 3G technology
			NETWORK_TYPE_CDMA               // CDMA: Either IS95A or IS95B
			NETWORK_TYPE_EDGE
			NETWORK_TYPE_GPRS
			NETWORK_TYPE_GSM        *       // API >24
			NETWORK_TYPE_IDEN       *
			//3G
			NETWORK_TYPE_EHRPD      *       // CDMA: eHRPD
			NETWORK_TYPE_EVDO_0     *
			NETWORK_TYPE_EVDO_A     *
			NETWORK_TYPE_EVDO_B     *
			NETWORK_TYPE_HSDPA
			NETWORK_TYPE_HSPA
			NETWORK_TYPE_HSPAP              // API >12
			NETWORK_TYPE_HSUPA
			NETWORK_TYPE_TD_SCDMA   *       // API >24
			NETWORK_TYPE_UMTS
			//4G
			NETWORK_TYPE_IWLAN      *       // API >24
			NETWORK_TYPE_LTE                // API >11
    // ----------------------------------------------------------------------
    // Where * has been added 2016-12-13.
    // ----------------------------------------------------------------------

	 * Determines the network generation based on the networkType retrieved via telephonyManager.getNetworkType()
	 * @param networkType
	 * @return
	 * 0: Invalid value
	 * 2: GSM
	 * 3: 3G
	 * 4: LTE
	 */
    //convert to network generation by data that get from telephonymanager.getnetworktype
    public static int networkTypeToNetworkGeneration(int networkType) {
        if (networkType == 0)
            return 0;
        else if (  // networkType == TelephonyManager.NETWORK_TYPE_1xRTT // Moved to 3G !!
                networkType == TelephonyManager.NETWORK_TYPE_CDMA     // CDMA: Either IS95A or IS95B
                        || networkType == TelephonyManager.NETWORK_TYPE_EDGE
                        || networkType == TelephonyManager.NETWORK_TYPE_GPRS
                        || networkType == TelephonyManager.NETWORK_TYPE_GSM      // API >24
                        || networkType == TelephonyManager.NETWORK_TYPE_IDEN)
            return 2;
        else if (networkType == TelephonyManager.NETWORK_TYPE_1xRTT    // This is labelled by Google as 2G, but is really a 3G technology
                || networkType == TelephonyManager.NETWORK_TYPE_EHRPD    // CDMA: eHRPD
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_0
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_A
                || networkType == TelephonyManager.NETWORK_TYPE_EVDO_B
                || networkType == TelephonyManager.NETWORK_TYPE_HSDPA
                || networkType == TelephonyManager.NETWORK_TYPE_HSPA
                || networkType == TelephonyManager.NETWORK_TYPE_HSPAP    // API >12
                || networkType == TelephonyManager.NETWORK_TYPE_HSUPA
                || networkType == TelephonyManager.NETWORK_TYPE_TD_SCDMA // API >24
                || networkType == TelephonyManager.NETWORK_TYPE_UMTS)
            return 3;
        else if (networkType == TelephonyManager.NETWORK_TYPE_IWLAN    // API >24
                || networkType == TelephonyManager.NETWORK_TYPE_LTE)     // API >11
            // ToDo: IWLAN may be considered prone to other type of MiTM attack and might
            //          need to be treated differently...
            return 4;
        else {
            return 0;
        }
    }

    //*************************************************************************************************

// read from a file that assingend
    public static String readFromAssets(Context context, String fileName) throws IOException {
        InputStream inputStream = context.getAssets().open(fileName);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException ee) {
//            Log.e(TAG, mTAG + ": IOException in readFromAssets():\n" + ee.toString());
        }
        return byteArrayOutputStream.toString();
    }



    //***************************************************************************************

    public static void showDeviceIncompatibleDialog(Activity activity, String incompatibilityReason, final Runnable callback) {
        String dialogMessage =
                activity.getResources().getString(R.string.alert_deviceCompatibility_header) + " " + incompatibilityReason ;
        final SpannableString spannableString = new SpannableString(dialogMessage);
        Linkify.addLinks(spannableString,Linkify.ALL);

        Dialog dialog = MsdDialog.makeOKButtonOnlyConfirmationDialog(activity, spannableString, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.run();
                    }
                });
        dialog.show();
        ((TextView)dialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * Gets the device major number of the diag device from /proc/devices (or
     * null if the Kernel does not contain a diag driver).
     *
     * @return
     */
    @SuppressLint("DefaultLocale")
    public static Integer getDiagDeviceNodeMajor() {
        BufferedReader r = null;
        try {
            r = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/devices")));
            while (true) {
                String line = r.readLine();
                writeToLogFile(line);
                if (line == null) // EOF
                    return null;
                line = line.trim();
                String[] line_elements = line.split("\\s+");

                if (line_elements.length != 2)
                    continue;
                if (line_elements[1].trim().toLowerCase().equals("dia")) {
                    return Integer.parseInt(line_elements[0].trim());
                }
            }
        } catch (IOException e) {
            return null;
        } finally {
            try {
                if (r != null)
                    r.close();
            } catch (IOException ee) {
//                Log.e(TAG, mTAG + ": IOException in getDiagDeviceNodeMajor():\n" + ee.toString());
            }
        }
    }

    public static void writeToLogFile(String data)
    {
//        Log.i("thread","log2");
        File dir = new File (Environment.getExternalStorageDirectory()+ "/sensor/logdata/");
        dir.mkdirs();
        File file = new File(dir, "compalible.txt");

        if(!file.exists())
        {
            try {
                file.createNewFile();

            }
            catch (Exception e)
            {
//                Log.d("Exception",e.getMessage().toString());
            }
        }

        try {
            FileOutputStream fOut= new FileOutputStream(file,true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    /**
     * Create the diag device (if it doesn't already exist).
     *
     * @return Error message as String or null if successful
     */
    public static String createDiagDevice() {
        File diagDevice = new File("/dev/diag");
        if (diagDevice.exists())
            return null;
        Integer diagDeviceMajor = Utils.getDiagDeviceNodeMajor();
        if (diagDeviceMajor == null) {
            return "Diag device does not exist and /proc/devices does not contain any entry for 'dia'";
        }
        // Try both standard mknod and busybox mknod
        String mknodCmd = "mknod /dev/diag c " + diagDeviceMajor + " 0 || busybox mknod /dev/diag c " + diagDeviceMajor + " 0";
        String suBinary = DeviceCompatibilityChecker.getSuBinary();
        String cmd[] = {suBinary, "-c", mknodCmd};
        Process mknod;
        try {
            mknod = Runtime.getRuntime().exec(cmd);
        } catch (IOException e1) {
//            Log.e(TAG, mTAG + ": IOException in createDiagDevice():\n" + e1.toString());
            return e1.getMessage();
        }
        try {
            mknod.waitFor();
        } catch (InterruptedException ee) {
//            Log.e(TAG, mTAG + ": InterruptedException in createDiagDevice():\n" + ee.toString());
        }
        if (!diagDevice.exists()) {
            return "Failed to create diag device: " + mknodCmd;
        }
        return null;
    }

    /**
     * Check the status of /dev/diag with:
     * # \ls -alZ /dev/diag
     * # crw-rw---- system   qcom_diag   u:object_r:diag_device:s0 diag
     *
     * @return string
     */
    public static String checkDiag() {
        Process checkdiag;
        String result;
        String checkCmd = "/system/bin/ls -alZ /dev/diag";
        String suBinary = DeviceCompatibilityChecker.getSuBinary();
        String cmd[] = {suBinary, "-c", checkCmd};

        try {
            checkdiag = Runtime.getRuntime().exec(cmd);
            BufferedReader bis = new BufferedReader(new InputStreamReader(checkdiag.getInputStream()));
            result = bis.readLine();
            checkdiag.destroy();
        } catch (Exception ee) {
//            Log.e(TAG, mTAG + ":checkDiag() Exception: " + ee);
            return "Error: no diag info";
        }
//        Log.i(TAG, "DIAG: " + "\"" + result + "\"");
        return result;
    }


    /**
     * Check if device comes with a Qualcomm MSM chip
     *
     * @return
     */
    public static boolean isDeviceMSM() {
        String msms;
        try {
            msms = MsdLog.osgetprop("ro.baseband") +
                    MsdLog.osgetprop("ro.board.platform") +
                    MsdLog.osgetprop("ro.boot.baseband");
        } catch (Exception e) {
//            Log.e(TAG, mTAG + "Exception in isDeviceMSM(): " + e);
            return true; // We are friendly to bad properties of unknown devices...
        }
        msms = msms.toLowerCase(Locale.US);
        CharSequence cs1 = "msm";
        return msms.contains(cs1);
    }

    /**
     * Get the epoch time of the firmware build date
     *
     * @return
     */
    private static String getFirmwareBuildDate() {
        String epochBuildTime;
        try {
            epochBuildTime = MsdLog.osgetprop("ro.build.date.utc");
        } catch (Exception e) {
//            Log.e(TAG, mTAG + " Exception in getFirmwareBuildDate(): " + e);
            return null;
        }
        return epochBuildTime;
    }

    /**
     * Get the output of cat /proc/version
     *
     * @return
     */
    private static String getProcVersionInfo() {
        String result;
        String infoFile = "/proc/version";

        try {
            FileReader reader = new FileReader(infoFile);
            BufferedReader bis = new BufferedReader(reader);
            result = bis.readLine();
        } catch (Exception ee) {
//            Log.e(TAG, mTAG + ":getProcVersionInfo() Exception: " + ee);
            return null;
        }
//        Log.i(TAG, "/proc/version: " + "\"" + result + "\"");
        return result;
    }

    /**
     * Create a string containing lots of information about the current firmware
     * This can be used to check, whether the firmware changed
     *
     * @return
     */
    public static String getFirmwareInformation() {
        return "" + getFirmwareBuildDate() + " | " + getProcVersionInfo();
    }

    public static void writeToFile(String data, String filename) {
        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        ("/Sensor-test/"
                        );

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, filename);

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
        }
    }

    public static String bodyToString(final Request request){

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
