package ir.telecom.sensor.util;

public class Constants {

    public static final int NOTIFICATION_ID_FOREGROUND_SERVICE = 1;
    public static final int NOTIFICATION_ID_INTERNAL_ERROR = 2;
    public static final int NOTIFICATION_ID_IMSI = 5;
    public static final int NOTIFICATION_ID_EXPECTED_ERROR = 6;
    //for last analysis
    public static final long ANALYSIS_INTERVAL_MS = 60000;

    public static final int LOC_MAX_DELTA = 600;

}
