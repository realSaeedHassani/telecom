package ir.telecom.sensor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
//pari
import java.util.*;

import android.telephony.*;
import android.widget.Toast;
//

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.github.anastr.speedviewlib.DeluxeSpeedView;

import java.lang.reflect.Method;
import java.text.DateFormat;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.MsdDatabaseManager;
import ir.telecom.sensor.util.PermissionChecker;

public class NetworkInfoActivity extends BaseActivity {


    private int phoneId = -1;

    private String cellInfo;

    private String dataState;

    private String dataStateShort;
    /**
     * Network Operator Name
     */
    private String StringNetworkType;
    private String networkName;
    private String mncMcc;
    private String simCountry;
    private String phoneType;
    private int DataState;

    /**
     * Device IMEI
     */
    private String iMEI;
    /**
     * Device IMEI Version
     */
    private String iMEIv;
    private String simOperator;
    private String simOperatorName;
    private String simSerial;
    private String simSubs;
    private int mnc;
    private int mcc, sid;
    private int cid;
    private int psc;
    private String IMSI;
    private int lac;
    private int networktype;
//    private int PrimaryScramblingCode;

    private String dataActivityType;

    private String dataActivityTypeShort;
    private boolean roaming;
    private String StringRoaming;
    private String StringDataState;

//    DeluxeSpeedView speedometer;
    TextView textStartDegree, textEndDegree, txtOperator;


    private final String TAG = "SNSN: NetworkActivity";
    protected final int refreshInterval = 60 * 1000;
    private Context context;
    private Handler handler = new Handler();
    private NetworkInfoRunnable networkInfoRunnable = new NetworkInfoRunnable();
    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private static TelephonyManager tm;
    TelephonyManager telephonyManager;
//    CPhoneStateListener psListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
//        setContentView(R.layout.activity_network_info);

        context = this.getApplicationContext();
        updateNetworkInfo();
        handler.postDelayed(networkInfoRunnable, refreshInterval);

        setToolbar();
        setDrawer(NetworkInfoActivity.this, toolbar);


//        speedometer = findViewById(R.id.deluxeSpeedView);
//        speedometer.setWithTremble(true);
//        speedometer.setTrembleDegree(0.1f);
//        textEndDegree = findViewById(R.id.txtSSCid);
//        textStartDegree = findViewById(R.id.txtSSLac);
//        txtOperator = findViewById(R.id.txtOperator);
//
//        psListener = new CPhoneStateListener();
//        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        telephonyManager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

    }


    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = toolbar.findViewById(R.id.toolbarDone);

        toolbarDone.setVisibility(View.INVISIBLE);
        toolbarTittle.setText("Network Information");


        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });

    }

    @Override
    protected void onResume() {
        updateNetworkInfo();
        handler.postDelayed(networkInfoRunnable, refreshInterval);
        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(networkInfoRunnable);
        super.onPause();
    }


    class NetworkInfoRunnable implements Runnable {
        @Override
        public void run() {
            updateNetworkInfo();
            handler.postDelayed(this, refreshInterval);
        }
    }


    private void updateNetworkInfo() {
        tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(context));
        SQLiteDatabase db = MsdDatabaseManager.getInstance().openDatabase();
//
        if (!isAirplaneModeOn(context)) {

            //  TMSI
            setCurrentTMSI(db);

            //  USIM
            setUSIM(db);

            //  Set transaction
//            if (PermissionChecker.isAccessingPhoneStateAllowed(context)) {
            setTransaction(db);
//            } else {
//                MsdLog.w(TAG, "Setting Transaction information not allowed - User did not grant READ_PHONE_STATE permission.");
//            }
        } else {
            Toasty.error(context, "Your Sim Card is offline.", Toast.LENGTH_LONG).show();
            Toasty.warning(context, "Please turn on your Sim Card", Toast.LENGTH_LONG).show();
        }

        MsdDatabaseManager.getInstance().closeDatabase();
    }

    private void setCurrentTMSI(SQLiteDatabase db) {
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
//        } else {
//                iMEI = tm.getDeviceId();
        iMEI = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        iMEIv = "0";//tm.getDeviceSoftwareVersion();
//        }
        phoneId = tm.getPhoneType();
        roaming = tm.isNetworkRoaming();
        if (roaming)
            StringRoaming = "True";
        else
            StringRoaming = "False";
        switch (phoneId) {

            case TelephonyManager.PHONE_TYPE_NONE:
            case TelephonyManager.PHONE_TYPE_SIP:
            case TelephonyManager.PHONE_TYPE_GSM:
                phoneType = "GSM";
                mncMcc = tm.getNetworkOperator();
                simOperator = tm.getSimOperator();
//                mcc = Integer.parseInt(tm.getSimOperator().substring(0, 3));
//                mnc = Integer.parseInt(tm.getSimOperator().substring(3, 5));
                if (mncMcc != null && mncMcc.length() >= 5) {
                    mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
                    mnc = Integer.parseInt(tm.getNetworkOperator().substring(3, 5));
                }
                GsmCellLocation gsmCellLocation = (GsmCellLocation) tm.getCellLocation();
                if (gsmCellLocation != null) {
                    cid = gsmCellLocation.getCid();
                    lac = gsmCellLocation.getLac();
                    psc = gsmCellLocation.getPsc();
                    List<CellInfo> cellInfoList = tm.getAllCellInfo();
                }
                break;

            case TelephonyManager.PHONE_TYPE_CDMA:
                phoneType = "CDMA";
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) tm.getCellLocation();
                if (cdmaCellLocation != null) {
                    cid = cdmaCellLocation.getBaseStationId();
                    lac = cdmaCellLocation.getNetworkId();
                    sid = cdmaCellLocation.getSystemId();
                    //     psc = cdmaCellLocation.get;
                    try {
                        Class<?> c = Class.forName("android.os.SystemProperties");
                        Method get = c.getMethod("get", String.class);
                        String homeOperator = ((String) get.invoke(c, "ro.cdma.home.operator.numeric"));
                        mcc = Integer.parseInt(homeOperator.substring(0, 3));
                        mnc = Integer.parseInt(homeOperator.substring(3, 5));
                    } catch (Exception e) {
                    }
                }
                break;
        }

//        public String getCellInfo(CellInfo cellInfo) {
//            String additional_info;
//        CellInfo cellInfoList = tm.getCellInfo();
////        CellInfo cellInfo= ;
//            if (cellInfo instanceof CellInfoGsm) {
//                CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
//                CellIdentityGsm cellIdentityGsm = cellInfoGsm.getCellIdentity();
//                additional_info = "cell identity " + cellIdentityGsm.getCid() + "\n"
//                        + "Mobile country code " + cellIdentityGsm.getMcc() + "\n"
//                        + "Mobile network code " + cellIdentityGsm.getMnc() + "\n"
//                        + "local area " + cellIdentityGsm.getLac() + "\n";
//            } else if (cellInfo instanceof CellInfoLte) {
//                CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
//                CellIdentityLte cellIdentityLte = cellInfoLte.getCellIdentity();
//                additional_info = "cell identity " + cellIdentityLte.getCid() + "\n"
//                        + "Mobile country code " + cellIdentityLte.getMcc() + "\n"
//                        + "Mobile network code " + cellIdentityLte.getMnc() + "\n"
//                        + "physical cell " + cellIdentityLte.getPci() + "\n"
//                        + "Tracking area code " + cellIdentityLte.getTac() + "\n";
//            } else if (cellInfo instanceof CellInfoWcdma){
//                CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
//                CellIdentityWcdma cellIdentityWcdma = cellInfoWcdma.getCellIdentity();
//                additional_info = "cell identity " + cellIdentityWcdma.getCid() + "\n"
//                        + "Mobile country code " + cellIdentityWcdma.getMcc() + "\n"
//                        + "Mobile network code " + cellIdentityWcdma.getMnc() + "\n"
//                        + "local area " + cellIdentityWcdma.getLac() + "\n";
//            }
////            return additional_info;
////        }


        networktype = tm.getNetworkType();
        switch (networktype) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                StringNetworkType = "2G";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                /**
                 From this link https://en.wikipedia.org/wiki/Evolution-Data_Optimized ..NETWORK_TYPE_EVDO_0 & NETWORK_TYPE_EVDO_A
                 EV-DO is an evolution of the CDMA2000 (IS-2000) standard that supports high data rates.

                 Where CDMA2000 https://en.wikipedia.org/wiki/CDMA2000 .CDMA2000 is a family of 3G[1] mobile technology standards for sending voice,
                 data, and signaling data between mobile phones and cell sites.
                 */
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                //Log.d("Type", "3g");
                //For 3g HSDPA , HSPAP(HSPA+) are main  networktype which are under 3g Network
                //But from other constants also it will 3g like HSPA,HSDPA etc which are in 3g case.
                //Some cases are added after  testing(real) in device with 3g enable data
                //and speed also matters to decide 3g network type
                //https://en.wikipedia.org/wiki/4G#Data_rate_comparison
                StringNetworkType = "3G";
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                //No specification for the 4g but from wiki
                //I found(LTE (Long-Term Evolution, commonly marketed as 4G LTE))
                //https://en.wikipedia.org/wiki/LTE_(telecommunication)
                StringNetworkType = "4G";
                break;
            default:
                StringNetworkType = "Not found";
                break;
        }
        // SIM Information
        simCountry = tm.getSimCountryIso();
        // Get the operator code of the active SIM (MCC + MNC)
        simOperator = tm.getSimOperator();
        simOperatorName = tm.getSimOperatorName();
        simSerial = "0";//tm.getSimSerialNumber();
        IMSI = "0";// tm.getSubscriberId();
        DataState = tm.getDataState();
        switch (DataState) {
            case TelephonyManager.DATA_DISCONNECTED:
                StringDataState = "Disconnected";
                break;
            case TelephonyManager.DATA_CONNECTING:
                StringDataState = "Connecting";
                break;
            case TelephonyManager.DATA_CONNECTED:
                StringDataState = "Connected";
                break;
            case TelephonyManager.DATA_SUSPENDED:
                StringDataState = "Suspended";
                break;
        }


        String tmsiText = "-";
        TextView tmsi = (TextView) findViewById(R.id.networkInfoCurrentTmsi);

        Cursor query = db.rawQuery
                ("SELECT max(id), ifnull(ifnull(new_tmsi, tmsi), '???') FROM session_info " +
                        "WHERE domain = 0 AND (tmsi NOT NULL OR new_tmsi NOT NULL) ORDER BY id", null);

        if (query.moveToFirst()) {
            tmsiText = query.getString(1);
        }
        tmsi.setText(tmsiText);
    }

    private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;

    }


    public boolean isSimAvailable() {
        boolean isAvailable = false;
//        TelephonyManager telMgr = (TelephonyManager) Context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = tm.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT: //SimState = “No Sim Found!”;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = “Network Locked!”;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = “PIN Required to access SIM!”;
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = “PUK Required to access SIM!”; // Personal Unblocking Code
                break;
            case TelephonyManager.SIM_STATE_READY:
                isAvailable = true;
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = “Unknown SIM State!”;
                break;
        }
        return isAvailable;
    }


    private void setUSIM(SQLiteDatabase db) {

        String usimText = context.getResources().getString(R.string.network_info_usim_unknown);
        TextView usim = (TextView) findViewById(R.id.networkInfoCurrentUSIM);

        Cursor query = db.rawQuery
                ("SELECT ifnull(max(auth), 0) FROM session_info WHERE domain = 0 AND auth > 0 AND (rat = 1 OR auth > 1)", null);

        if (query.moveToFirst()) {
            int auth = query.getInt(0);
            switch (auth) {
                case 1:
                    // Only authentications with A3/A8
                    usimText = context.getResources().getString(R.string.network_info_usim_not_present);
                    break;
                case 2:
                    // UMTS AKA authentications happened
                    usimText = context.getResources().getString(R.string.network_info_usim_present);
                    break;
            }
        }

        usim.setText(usimText);
    }

    private void setTextView(int ID, String value) {
        TextView view = (TextView) findViewById(ID);
        if (value == null) {
            view.setText("-");
        } else {
            view.setText(value);
        }
        view.invalidate();
    }

    private void setVisibility(boolean visible, int ID) {
        TextView view = (TextView) findViewById(ID);
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void setVisibility2(boolean visible, int ID1, int ID2) {
        setVisibility(visible, ID1);
        setVisibility(visible, ID2);
    }

    private String toAuthString(int value) {
        switch (value) {
            case 0:
                return context.getResources().getString(R.string.common_none);
            case 1:
                return "GSM A3/A8";
            case 2:
                return "UMTS AKA";
            default:
                return "-";
        }
    }

    private String toRATString(int value) {
        switch (value) {
            case 0:
                return "2G";
            case 1:
                return "3G";
            case 2:
                return "LTE";
            default:
                return "unknown";
        }
    }

    private String toCipherString(int rat, int value) {
        if (value > 4) {
            return "-";
        }
        switch (rat) {
            case 0: // 2G
                return "A5/" + Integer.toString(value);
            case 1: // 3G
                return "UEA/" + Integer.toString(value);
            case 2: // LTE
                return "EEA/" + Integer.toString(value);
            default:
                return context.getResources().getString(R.string.common_invalid);
        }
    }

    private String toGprsCipherString(int value) {
        return "GEA/" + value;
    }

    private String toIntegrityString(int rat, int ia) {
        switch (rat) {
            case 0: // 2G
                return "- (" + ia + ")";
            case 1: // 3G
                return "UIA/" + ia;
            case 2: // LTE
                return "EIA/" + ia;
            default:
                return context.getResources().getString(R.string.common_invalid);
        }
    }

    private String toDirectionString(int mo, int mt) {
        if (mo > 0 && mt > 0) {
            return context.getResources().getString(R.string.common_invalid) + "(MO+MT)";
        } else if (mt > 0) {
            return context.getResources().getString(R.string.network_info_mobile_terminated);
        } else if (mo > 0) {
            return context.getResources().getString(R.string.network_info_mobile_originated);
        } else {
            return context.getResources().getString(R.string.common_none);
        }
    }

    private String toPagingString(int mi) {
        switch (mi) {
            case 0:
                return null;
            case 1:
                return "IMSI";
            case 2:
                return "IMEI";
            case 3:
                return "IMEISV";
            case 4:
                return "TMSI";
            default:
                return context.getResources().getString(R.string.common_invalid);
        }
    }

    private String toTypeString(int locupd, int abort, int call, int sms) {

        String result = "";

        if (locupd > 0) {
            result += "location update ";
        }

        if (abort > 0) {
            result += "aborted ";
        }

        if (call > 0) {
            result += "call ";
        }

        if (sms > 0) {
            result += "sms ";
        }
        return result;
    }

    private String toLUResultString(int lu_acc, int lu_reject) {
        if (lu_acc > 0 && lu_reject > 0) {
            return context.getResources().getString(R.string.common_invalid);
        } else if (lu_acc > 0) {
            return context.getResources().getString(R.string.network_info_lu_accepted);
        } else if (lu_reject > 0) {
            return context.getResources().getString(R.string.network_info_lu_rejected);
        } else {
            return null;
        }
    }

    private String toLUTypeString(int lu_type) {
        switch (lu_type) {
            case 0:
                return "normal";
            case 1:
                return "periodic";
            case 2:
                return "IMSI attach";
            case 3:
                return "reserved";
            default:
                return "-";
        }
    }

    /**
     * REQUIRED PERMISSION:
     * READ_PHONE_STATE
     */
    private void setTransaction(SQLiteDatabase db) {

        long timestamp;
        int rat;
// mcc, mnc, lac, cid,
        int rnc, arfcn;
        int auth, cipher, integrity, duration;
        int mo, mt, paging_mi, t_locupd, lu_acc, lu_type, lu_reject, rej_cause;
        int lu_mcc, lu_mnc, lu_lac, t_abort, t_call, t_sms, id;
        int cipher_gprs = 0;
        boolean gprs_cipher_info_available = false;
        String msisdn;
//                IMSI;
//        TelephonyManager telephonyManager;

        String q =
                "SELECT max(id) as id, strftime('%s', timestamp) as timestamp, " +
                        "rat, mcc, mnc, lac, cid, arfcn, auth, cipher, integrity, " +
                        "duration, mobile_orig, mobile_term, paging_mi, " +
                        "t_locupd, lu_acc, lu_type, lu_reject, lu_rej_cause, " +
                        "lu_mcc, lu_mnc, lu_lac, t_abort, t_call, t_sms, msisdn " +
                        "from session_info where domain = 0 order by timestamp";

//        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        IMSI = telephonyManager.getSubscriberId();

        Cursor query = db.rawQuery(q, null);
        if (query.moveToFirst()) {
            timestamp = query.getLong(query.getColumnIndexOrThrow("timestamp"));
            id = query.getInt(query.getColumnIndexOrThrow("id"));
            rat = query.getInt(query.getColumnIndexOrThrow("rat"));
//            mcc = query.getInt(query.getColumnIndexOrThrow("mcc"));
//            mnc = query.getInt(query.getColumnIndexOrThrow("mnc"));
//            lac = query.getInt(query.getColumnIndexOrThrow("lac"));
//            cid = query.getInt(query.getColumnIndexOrThrow("cid"));
            rnc = cid >> 16;
            arfcn = query.getInt(query.getColumnIndexOrThrow("arfcn"));
            auth = query.getInt(query.getColumnIndexOrThrow("auth"));
            cipher = query.getInt(query.getColumnIndexOrThrow("cipher"));
            integrity = query.getInt(query.getColumnIndexOrThrow("integrity"));
            duration = query.getInt(query.getColumnIndexOrThrow("duration"));
            mo = query.getInt(query.getColumnIndexOrThrow("mobile_orig"));
            mt = query.getInt(query.getColumnIndexOrThrow("mobile_term"));
            paging_mi = query.getInt(query.getColumnIndexOrThrow("paging_mi"));
            t_locupd = query.getInt(query.getColumnIndexOrThrow("t_locupd"));
            t_abort = query.getInt(query.getColumnIndexOrThrow("t_abort"));
            t_call = query.getInt(query.getColumnIndexOrThrow("t_call"));
            t_sms = query.getInt(query.getColumnIndexOrThrow("t_sms"));
            msisdn = query.getString(query.getColumnIndexOrThrow("msisdn"));
            lu_acc = query.getInt(query.getColumnIndexOrThrow("lu_acc"));
            lu_type = query.getInt(query.getColumnIndexOrThrow("lu_type"));
            lu_reject = query.getInt(query.getColumnIndexOrThrow("lu_reject"));
            rej_cause = query.getInt(query.getColumnIndexOrThrow("lu_rej_cause"));
            lu_mcc = query.getInt(query.getColumnIndexOrThrow("lu_mcc"));
            lu_mnc = query.getInt(query.getColumnIndexOrThrow("lu_mnc"));
            lu_lac = query.getInt(query.getColumnIndexOrThrow("lu_lac"));

            if (rat == 0) { // Query GPRS Cipher when using 2G
                String q_gprs =
                        "SELECT max(id) as id, strftime('%s', timestamp) as timestamp, " +
                                "cipher from session_info where rat = 0 and domain = 1 " +
                                "and t_attach and att_acc order by timestamp";
                Cursor query_gprs = db.rawQuery(q_gprs, null);
                if (query_gprs.moveToFirst()) {
                    long gprs_timestamp = query_gprs.getLong(query_gprs.getColumnIndexOrThrow("timestamp"));
                    // Only show GPRS session if time difference between GSM session and GPRS session is below 24h
                    if (Math.abs(timestamp - gprs_timestamp) < 24 * 3600) {
                        cipher_gprs = query_gprs.getInt(query_gprs.getColumnIndexOrThrow("cipher"));
                        gprs_cipher_info_available = true;
                    }
                }
            }

            // Set timestamp
            setTextView(R.id.networkInfoTimestamp, String.valueOf(DateFormat.getDateTimeInstance().format(timestamp * 1000L)));
            setTextView(R.id.networkInfoCurrentInternalID, Integer.toString(id));
            setTextView(R.id.networkInfoCurrentRAT, toRATString(rat));
            setTextView(R.id.networkInfoCurrentMCC, Integer.toString(mcc));
            setTextView(R.id.networkInfoCurrentMNC, Integer.toString(mnc));
            setTextView(R.id.networkInfoCurrentLAC, Integer.toString(lac));
            setTextView(R.id.networkInfoCurrentCID, Integer.toString(cid));
            setTextView(R.id.networkInfoCurrentPSC, Integer.toString(psc));
            setTextView(R.id.networkInfoCurrentRNC, Integer.toString(rnc));
            setTextView(R.id.networkInfoCurrentARFCN, Integer.toString(arfcn));
            setTextView(R.id.networkInfoCurrentAuth, toAuthString(auth));
            setTextView(R.id.networkInfoCurrentCipher, toCipherString(rat, cipher));

            // GPRS cipher
            gprs_cipher_info_available = false; // Disable display of Cipher (GPRS) for now
            setVisibility2(gprs_cipher_info_available, R.id.networkInfoCurrentCipherGprs, R.id.txtCipherGprs);
            if (gprs_cipher_info_available) {
                setTextView(R.id.networkInfoCurrentCipherGprs, toGprsCipherString(cipher_gprs));
            }
            setTextView(R.id.networkInfoCurrentIntegrity, toIntegrityString(rat, integrity));
            setTextView(R.id.networkInfoCurrentDuration, Integer.toString(duration) + " ms");
            setTextView(R.id.networkInfoCurrentDirection, toDirectionString(mo, mt));
            setTextView(R.id.networkInfoCurrentPaging, toPagingString(paging_mi));

            setTextView(R.id.networkInfoCurrentType, StringNetworkType);
            setTextView(R.id.networkInfoPhoneType, phoneType);
            setTextView(R.id.networkInfoNetworkName, simOperatorName);
            setTextView(R.id.networkInfoSimSerial, simSerial);
            setTextView(R.id.networkInfoCounrty, simCountry);

            setTextView(R.id.networkInfoRoaming, StringRoaming);
            setTextView(R.id.networkInfoDataState, StringDataState);
            setTextView(R.id.networkInfoCurrentMSISDN, msisdn);

            setTextView(R.id.networkInfoPreviousMCC, Integer.toString(lu_mcc));
            setTextView(R.id.networkInfoPreviousMNC, Integer.toString(lu_mnc));
            setTextView(R.id.networkInfoPreviousLAC, Integer.toString(lu_lac));
            setTextView(R.id.networkInfoLUResult, toLUResultString(lu_acc, lu_reject));
            setTextView(R.id.networkInfoLUType, toLUTypeString(lu_type));
            setTextView(R.id.networkInfoLURejectCause, Integer.toString(rej_cause));

            setTextView(R.id.networkInfoCurrentImsi, IMSI);
            setTextView(R.id.networkInfoCurrentImei, iMEI);

            // Disable invalid elements

            // paging
            setVisibility2(paging_mi != 0, R.id.txtPaging, R.id.networkInfoCurrentPaging);

            // location update
            setVisibility(t_locupd == 1, R.id.networkInfoLupd);
            setVisibility2(t_locupd == 1, R.id.txtLUType, R.id.networkInfoLUType);
            setVisibility2(t_locupd == 1, R.id.txtLUResult, R.id.networkInfoLUResult);
            setVisibility2(t_locupd == 1 && lu_reject == 1, R.id.txtLURejectCause, R.id.networkInfoLURejectCause);
            setVisibility2(t_locupd == 1 && lu_acc == 1, R.id.txtPreviousMCC, R.id.networkInfoPreviousMCC);
            setVisibility2(t_locupd == 1 && lu_acc == 1, R.id.txtPreviousMNC, R.id.networkInfoPreviousMNC);
            setVisibility2(t_locupd == 1 && lu_acc == 1, R.id.txtPreviousLAC, R.id.networkInfoPreviousLAC);

            // MSISDN
            setVisibility2(t_call == 1 || t_sms == 1, R.id.txtMSISDN, R.id.networkInfoCurrentMSISDN);
            // MCC, MNC, LAC or CID may not be present
            setVisibility2(mcc > 0, R.id.txtMCC, R.id.networkInfoCurrentMCC);
            setVisibility2(mnc > 0, R.id.txtMNC, R.id.networkInfoCurrentMNC);
            setVisibility2(lac > 0, R.id.txtLAC, R.id.networkInfoCurrentLAC);
            setVisibility2(cid > 0, R.id.txtCID, R.id.networkInfoCurrentCID);
            setVisibility2(psc > 0, R.id.txtPSC, R.id.networkInfoCurrentPSC);
            setVisibility2(rnc > 0, R.id.txtRNC, R.id.networkInfoCurrentRNC);
            setVisibility2(arfcn > 0, R.id.txtARFCN, R.id.networkInfoCurrentARFCN);

            // Duration may be 0
            setVisibility2(duration > 0, R.id.txtDuration, R.id.networkInfoCurrentDuration);
        }
    }

    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(NetworkInfoActivity.this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

//    public class CPhoneStateListener extends PhoneStateListener {
//        public int signalStrengthValue;
//
//        @SuppressLint("SetTextI18n")
//        @Override
//        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
//            super.onSignalStrengthsChanged(signalStrength);
////            signalStrengthValue = signalStrength.getGsmSignalStrength();
////            signalStrengthValue = (2 * signalStrengthValue) - 113; // -> dBm
////            Log.e(">> SP: ", String.valueOf(getSignalStrengthValue()));
//
//            if (Settings.System.getInt(getContentResolver(),
//                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0) {
//                speedometer.speedTo(-120);
//                speedometer.setTrembleDegree(0);
//
//            } else {
//                List<Integer> param = getSignalStrengthValue(telephonyManager);
//                if (param.size() > 0) {
//                    speedometer.setTrembleDegree(0.1f);
//                    speedometer.speedTo(param.get(0));
//                    textEndDegree.setText("CID: " + param.get(1));
//                    textStartDegree.setText("LAC: " + param.get(2));
//                    switch (param.get(3)) {
//                        case 35:
//                            txtOperator.setText("Irancell");
//                            break;
//                        case 11:
//                            txtOperator.setText("IR-MCI");
//                            break;
//                        case 20:
//                            txtOperator.setText("Rightel");
//                            break;
//                        default:
//                            txtOperator.setText(" ");
//                            break;
//                    }
//                }
//            }
//        }
//
//        List<Integer> getSignalStrengthValue(TelephonyManager tm) {
//            int lCurrentApiVersion = android.os.Build.VERSION.SDK_INT;
//            List<Integer> param = new ArrayList<>(4);
//            List<CellInfo> cellInfoList = new ArrayList<>();
//            try {
////            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
////                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
//                } else {
//                    cellInfoList = tm.getAllCellInfo();
//                }
//                if (cellInfoList != null) {
//                    if (tm.getAllCellInfo().size() != 0) {
//                        CellInfo info = tm.getAllCellInfo().get(0);
//
//                        if (info instanceof CellInfoGsm) {
//                            final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
//                            final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
////                    arfcn=identityGsm.getArfcn();
//                            // Signal Strength
//                            signalStrengthValue = gsm.getDbm(); // [dBm]
//                            // Cell Identity
////                        mcc = Integer.parseInt(tm.getNetworkOperator().substring(0, 3));
//                            cid = identityGsm.getCid();
////                    mcc = identityGsm.getMcc();
//                            mnc = identityGsm.getMnc();
//                            lac = identityGsm.getLac();
//                            param.add(signalStrengthValue);
//                            param.add(cid);
//                            param.add(lac);
//                            param.add(mnc);
//
//                        } else if (info instanceof CellInfoCdma) {
//                            final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
//                            final CellIdentityCdma identityCdma = ((CellInfoCdma) info).getCellIdentity();
//                            //arfcn
////                    arfcn=identityCdma.get();
//                            // Signal Strength
//                            signalStrengthValue = cdma.getDbm();
//                            // Cell Identity
//                            cid = identityCdma.getBasestationId();
//                            mnc = identityCdma.getSystemId();
//                            lac = identityCdma.getNetworkId();
//                            sid = identityCdma.getSystemId();
//                            param.add(signalStrengthValue);
//                            param.add(cid);
//                            param.add(lac);
//                            param.add(mnc);
//                        } else if (info instanceof CellInfoLte) {
//                            final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
//                            final CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
//                            // Signal Strength
//                            signalStrengthValue = lte.getDbm();
//                            // Cell Identity
//                            mcc = identityLte.getMcc();
//                            mnc = identityLte.getMnc();
//                            cid = identityLte.getCi();
//                            param.add(signalStrengthValue);
//                            param.add(cid);
//                            param.add(lac);
//                            param.add(mnc);
//                        } else if (lCurrentApiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2 && info instanceof CellInfoWcdma) {
//                            final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
//                            final CellIdentityWcdma identityWcdma = ((CellInfoWcdma) info).getCellIdentity();
//
//                            // Signal Strength
//                            signalStrengthValue = wcdma.getDbm();
//                            // Cell Identity
//                            lac = identityWcdma.getLac();
//                            mcc = identityWcdma.getMcc();
//                            mnc = identityWcdma.getMnc();
//                            cid = identityWcdma.getCid();
//                            psc = identityWcdma.getPsc();
//                            param.add(signalStrengthValue);
//                            param.add(cid);
//                            param.add(lac);
//                            param.add(mnc);
//                        }
//                    }
//                }
//            } catch (NullPointerException npe) {
////            Log.e(TAG, "sorry2");
//            }
//            if (param.size() != 0) {
//                //Log.e(">> PARAM: ", String.valueOf(param));
//            }
//            return param;
//        }
//
//
//    }
}
