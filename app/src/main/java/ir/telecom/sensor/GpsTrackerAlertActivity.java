package ir.telecom.sensor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ir.telecom.sensor.error.handling.ErrorControlObject;
import ir.telecom.sensor.error.handling.UncaughtException;


/**
 * Created by Bahar on 7/21/2018.
 */
public class GpsTrackerAlertActivity extends AppCompatActivity {
    private String title;
    private String message;
    private TextView alertTitle;
    private TextView alertMessage;
    private Button alertCancelButton;
    private Button alertOkButton;
    static boolean active = false;
    public static Activity gpsTrackerAlertActivity;
    private ErrorControlObject errorControlObject = new ErrorControlObject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
         * ....................
         * >> Error Handling. |
         * ....................
         */
//        errorControlObject.setmCrashText( this.getLocalClassName() );
//        errorControlObject.setmDestinationActivity(GpsTrackerAlertActivity.class);
//        Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(this, errorControlObject));

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.gps_tracker_alert);
        this.setFinishOnTouchOutside(false);

        title=getIntent().getExtras().getString("title");
        message=getIntent().getExtras().getString("message");
        gpsTrackerAlertActivity=this;

        alertTitle=(TextView)findViewById(R.id.alert_title);
        alertMessage=(TextView)findViewById(R.id.alert_message);
        alertOkButton=(Button) findViewById(R.id.alert_ok_button);
        alertCancelButton=(Button) findViewById(R.id.alert_cancel_button);

        if(title!=null) {
            alertTitle.setText(title);
        }

        if(message!=null){
            alertMessage.setText(message);
        }

        alertCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                GpsTrackerAlertActivity.this.startActivity(intent);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }
}
