package ir.telecom.sensor;

/**
 * Created by Bahar on 2/10/2018.
 */

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import ir.telecom.sensor.toasty.Toasty;

/**
 * IP input control
 *
 * @author Shela
 */
public class IPEditText extends LinearLayout {

    private EditText mFirstIP;
    private EditText mSecondIP;
    private EditText mThirdIP;
    private EditText mFourthIP;

    private String mText1;
    private String mText2;
    private String mText3;
    private String mText4;

    private static final String INPUT_VALID_IP = "Please input valid IP address";

    public IPEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        /**
         * Initialize
         */
        View view = LayoutInflater.from(context).inflate(
                R.layout.custom_my_edittext, this);
        mFirstIP = (EditText) findViewById(R.id.ip_first);
        mSecondIP = (EditText) findViewById(R.id.ip_second);
        mThirdIP = (EditText) findViewById(R.id.ip_third);
        mFourthIP = (EditText) findViewById(R.id.ip_fourth);

        operatingEditText(context);
    }

    /**
     * Get the content of EditText.
     *
     * @param context
     */
    private void operatingEditText(final Context context) {
//        mFirstIP.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before,
//                                      int count) {
//                /**
//                 * Whenever the EditText is filled with 3 digit, the cursor will move to next EditText.
//                 */
//                if (s != null && s.length() > 0) {
//
//                    if (s.toString().trim().contains(".")) {
//                        mText1 = s.toString().replaceAll("\\.", "");
//                        mFirstIP.setText(mText1);
//                    } else {
//                        mText1 = s.toString().trim();
//                    }
//
//                    if(mText1.length()==1){
//                        mFirstIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                    }
//
//                    if (mText1.length() > 1 && !mText1.trim().contains(".")) {
//                        if (Integer.parseInt(mText1) > 255 || (mText1.substring(0, 1)).equals("0")) {
//                            Toasty.warning(context, INPUT_VALID_IP, Toast.LENGTH_SHORT).show();
//                            mFirstIP.setTextColor(Color.RED);
//                            //return;
//                        } else {
//                            mFirstIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                        }
//                    }
//
//                    if (s.length() > 2 || s.toString().trim().contains(".")) {
//                        mSecondIP.setFocusable(true);
//                        mSecondIP.requestFocus();
//                    }
//                } else {
//                    mText1 = "";
//                }
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//                //do nothing because some codes are written in onTextChanged method
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                //do nothing because some codes are written in onTextChanged method
//
//            }
//        });

        mSecondIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * Check if the IP address is valid.
                 * set focus to next EditText.
                 */
                if (s != null && s.length() > 0) {

                    if (s.toString().trim().contains(".")) {
                        mText2 = s.toString().replaceAll("\\.", "");
                        mSecondIP.setText(mText2);
                    } else {
                        mText2 = s.toString().trim();
                    }

                    if(mText2.length()==1){
                        mSecondIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }

                    if (mText2.length() > 1 && !mText2.trim().contains(".")) {
                        if (Integer.parseInt(mText2) > 255 || (mText2.substring(0, 1)).equals("0")) {
                            Toasty.warning(context, INPUT_VALID_IP,
                                    Toast.LENGTH_SHORT).show();
                            mSecondIP.setTextColor(Color.RED);
                            //return;
                        } else {
                            mSecondIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        }
                    }

                    if (s.length() > 2 || s.toString().trim().contains(".")) {
                        mThirdIP.setFocusable(true);
                        mThirdIP.requestFocus();
                    }
                } else {
                    mText2 = "";
                }

                /**
                 *  When user deletes the input, set focus to previous EditText
                 */
                if (s != null && s.length() == 0 && start == 0) {
                    mFirstIP.setFocusable(true);
                    mFirstIP.requestFocus();
                    mFirstIP.setSelection(mFirstIP.getText().length());
                }

            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                //do nothing because some codes are written in onTextChanged method

            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method

            }
        });


        mSecondIP.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_DEL) && mSecondIP.getText().toString().length() == 0) {
                    mFirstIP.setFocusable(true);
                    mFirstIP.requestFocus();
                    mFirstIP.setSelection(mFirstIP.getText().length());
                }
                return false;
            }
        });

        mThirdIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * Check if the IP address is valid.
                 * set focus to next EditText.
                 */
                if (s != null && s.length() > 0) {

                    if (s.toString().trim().contains(".")) {
                        mText3 = s.toString().replaceAll("\\.", "");
                        mThirdIP.setText(mText3);
                    } else {
                        mText3 = s.toString().trim();
                    }
                    if(mText3.length()==1){
                        mThirdIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }
                    if (mText3.length() > 1 && !mText3.trim().contains(".")) {
                        if (Integer.parseInt(mText3) > 255 || (mText3.substring(0, 1)).equals("0")) {
                            Toasty.warning(context, INPUT_VALID_IP,
                                    Toast.LENGTH_SHORT).show();
                            mThirdIP.setTextColor(Color.RED);
                            //return;
                        } else {
                            mThirdIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        }
                    }

                    if (s.length() > 2 || s.toString().trim().contains(".")) {
                        mFourthIP.setFocusable(true);
                        mFourthIP.requestFocus();
                    }
                } else {
                    mText3 = "";
                }

                /**
                 * When user deletes the input, set focus to previous EditText
                 */
                if (s != null && start == 0 && s.length() == 0) {
                    mSecondIP.setFocusable(true);
                    mSecondIP.requestFocus();
                    mSecondIP.setSelection(mSecondIP.getText().length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                //do nothing because some codes are written in onTextChanged method

            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method

            }
        });

        mThirdIP.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_DEL) && (mThirdIP.getText().toString().length() == 0)) {
                    mSecondIP.setFocusable(true);
                    mSecondIP.requestFocus();
                    mSecondIP.setSelection(mSecondIP.getText().length());
                }
                return false;
            }
        });

        mFourthIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * Check if the IP address is valid.
                 * set focus to next EditText.
                 */
                if (s != null && s.length() > 0) {

                    if (s.toString().trim().contains(".")) {
                        mText4 = s.toString().replaceAll("\\.", "");
                        mFourthIP.setText(mText4);
                    }else{
                        mText4 = s.toString().trim();
                    }

                    if(mText4.length()==1){
                        mFourthIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    }

                    if (mText4.length() > 1) {
                        if (Integer.parseInt(mText4) > 255 || (mText4.substring(0, 1)).equals("0")) {
                            Toasty.warning(context, INPUT_VALID_IP, Toast.LENGTH_SHORT).show();
                            mFourthIP.setTextColor(Color.RED);
                            //return;
                        } else {
                            mFourthIP.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        }
                    }

                    if (s.toString().trim().contains(".")) {
                        mFourthIP.setFocusable(true);
                        mFourthIP.requestFocus();
                        if(s.length()!=0 ) {
                            mFourthIP.setSelection(mFourthIP.getText().length());
                        }
                    }

                } else {
                    mText4 = "";
                }

                /**
                 * When user deletes the input, set focus to previous EditText
                 */
                if (s != null && start == 0 && s.length() == 0) {
                    mThirdIP.setFocusable(true);
                    mThirdIP.requestFocus();
                    mThirdIP.setSelection(mThirdIP.getText().length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                //do nothing because some codes are written in onTextChanged method
            }

            @Override
            public void afterTextChanged(Editable s) {
                //do nothing because some codes are written in onTextChanged method

            }
        });

        mFourthIP.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_DEL) && (mFourthIP.getText().toString().length() == 0)) {
                    mThirdIP.setFocusable(true);
                    mThirdIP.requestFocus();
                    mThirdIP.setSelection(mThirdIP.getText().length());
                }
                return false;
            }
        });
    }

    /**
     * return IP value
     *
     * @param context
     * @return String IP
     */
    public String getText(Context context) {

        if(TextUtils.isEmpty(mText1)){
            mText1 = mFirstIP.getText().toString().trim();
        }
        if (TextUtils.isEmpty(mText2)) {
            mText2 = mSecondIP.getText().toString().trim();
        }
        if (TextUtils.isEmpty(mText3)) {
            mText3 = mThirdIP.getText().toString().trim();
        }
        if (TextUtils.isEmpty(mText4)) {
            mText4 = mFourthIP.getText().toString().trim();}


//          if (TextUtils.isEmpty(mText1) || TextUtils.isEmpty(mText2) || TextUtils.isEmpty(mText3) || TextUtils.isEmpty(mText4)) {
//            return "";
//        }
        return mText1 ;//+ "." + mText2 + "." + mText3 + "." + mText4;
    }

    /**
     * set ip to edittexts
     *
     * @param ip
     */
    public void setText(String ip) {
        String[] split = ip.split("\\.");
        mFirstIP.setText(split[0]);
        mSecondIP.setText(split[1]);
        mThirdIP.setText(split[2]);
        mFourthIP.setText(split[3]);
    }

}
