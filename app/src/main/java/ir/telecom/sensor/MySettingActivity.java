package ir.telecom.sensor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import ir.telecom.sensor.toasty.Toasty;

public class MySettingActivity extends BaseActivity {

    private static int count;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private Editor editor;
    CheckBox a2,a4,a5,c1,c2,c4,c5,k1,k2,r1,r2,t1,t3,t4;

    //final String names[]={"a2","a4","a5","c1","c2","c4","c5","k1","k2","r1","r2","t1","t3","t4"};

    String names[] = {"Inconsistent LAC", "Different ARFCNs","Lonesome LA", "Non Neighbor","High Offset", "Encryption Downgrade", "Delayed Ack", "ID Request", "Low Cipher","Low t3212", "Empty Paging", "Orphaned Channel","Inconsistent Neighbor", "High Paging"};

    CheckBox[] cb={ a2,a4,a5,k1,k2,c1,c2,c4,c5,t1,t3,t4,r1,r2};
    CheckBox cb1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_setting);
        setToolbar();
        setDrawer(MySettingActivity.this,toolbar);


        cb1=(CheckBox) findViewById(R.id.select_all);
        cb[0]= (CheckBox) findViewById(R.id.Inconsistent_LAC);
        cb[1]= (CheckBox) findViewById(R.id.Different_ARFCNs);
        cb[2]= (CheckBox) findViewById(R.id.Lonesome_LA);
        cb[3]= (CheckBox) findViewById(R.id.Non_Neighbor);
        cb[4]= (CheckBox) findViewById(R.id.High_Offset);
        cb[5]= (CheckBox) findViewById(R.id.Encryption_Downgrade);
        cb[6]= (CheckBox) findViewById(R.id.Delayed_Ack);
        cb[7]= (CheckBox) findViewById(R.id.ID_Request);
        cb[8]= (CheckBox) findViewById(R.id.Low_Cipher);
        cb[9]= (CheckBox) findViewById(R.id.Low_t3212);
        cb[10]= (CheckBox) findViewById(R.id.Empty_Paging);
        cb[11]= (CheckBox) findViewById(R.id.Orphaned_Channel);
        cb[12]= (CheckBox) findViewById(R.id.Inconsistent_Neighbor);
        cb[13]= (CheckBox) findViewById(R.id.High_Paging);


        //delete for upload
        SharedPreferences sharedPrefs =  this.getSharedPreferences("sensorparameter", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
        editor = sharedPrefs.edit();

        boolean settingSave=sharedPrefs.getBoolean("setting_save", false);
        if(!settingSave){
            cb1.setChecked(true);
            for(int j=0; j<14;j++) {
                cb[j].setChecked(true);

            }
        }else{
            for(int j=0; j<14;j++) {
                boolean flag = sharedPrefs.getBoolean(names[j], true);
                if (flag) {
                    cb[j].setChecked(true);
                }
            }
        }



        cb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked=((CheckBox) view).isChecked();
                if(checked){
                    for(int i=0;i<14;i++)
                    {
                        cb[i].setChecked(true);

                    }
                }
                else{
                    for(int i=0;i<14;i++)
                    {
                        cb[i].setChecked(false);

                    }

                }
            }
        });

    }


    /**
     * Initial and set toolbar
     */
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTittle = (TextView) toolbar.findViewById(R.id.toolbarTitle);
        toolbarDone = (TextView) toolbar.findViewById(R.id.toolbarDone);
        toolbarDone.setVisibility(View.VISIBLE);
        toolbarTittle.setText("Parameters");

        toolbarDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                editor.putBoolean("setting_save",true);

                for(int i=0;i<14;i++)
                {
                    if(cb[i].isChecked())
                        editor.putBoolean(names[i],true);
                    else
                        editor.putBoolean(names[i],false);
                }


                editor.commit();
                Toasty.success(MySettingActivity.this,"chenges saved",Toast.LENGTH_LONG).show();
            }
        });


        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.setDrawerListener(actionBarDrawerToggle);
            }
        });


    }


    /**
     * on back pressed
     */
    @Override
    public void onBackPressed() {
        Intent i = new Intent(MySettingActivity.this, DashboardActivity.class);
        startActivity(i);
        finish();
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        alertDialogBuilder
//                .setMessage("Do you want to exit?")
//                .setCancelable(false)
//                .setPositiveButton("yes",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                quitApplication();
//                            }
//                        })
//                .setNegativeButton("No",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();

    }

}
