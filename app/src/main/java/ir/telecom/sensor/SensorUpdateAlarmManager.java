package ir.telecom.sensor;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Calendar;

import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.util.MsdDatabaseManager;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Bahar on 6/10/2018.
 */

public class SensorUpdateAlarmManager {
    private static SensorUpdateAlarmManager myObj;


    /**
     * private constructor
     */
    private SensorUpdateAlarmManager() {
    }


    /**
     * static method to get instanse
     *
     * @return obj of AccountingAlarmManager
     */
    public static SensorUpdateAlarmManager getInstance() {
        if (myObj == null) {
            myObj = new SensorUpdateAlarmManager();
        }
        return myObj;
    }


    /**
     * set daily reminder
     *
     * @param context
     */
    public void setReminder(Context context) {

//        int interval = Integer.parseInt("1");

        MsdSQLiteOpenHelper msdOpenHelper = new MsdSQLiteOpenHelper(context);
        SQLiteDatabase dbInstanse = MsdDatabaseManager.getInstance().openDatabase();
         int interval = msdOpenHelper.getSensorUpdateInterval(dbInstanse);

        Calendar calendar = Calendar.getInstance();
        Calendar setcalendar = Calendar.getInstance();

        setcalendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        setcalendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
        setcalendar.set(Calendar.SECOND,calendar.get(Calendar.SECOND));

        // cancel already scheduled reminders
        cancelReminder(context);


        ComponentName receiver = new ComponentName(context, SensorUpdateAlarmReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        Intent intent1 = new Intent(context, SensorUpdateAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        android.app.AlarmManager am = (android.app.AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.setRepeating(android.app.AlarmManager.RTC_WAKEUP, setcalendar.getTimeInMillis()+(long) interval *1000, (long) interval *1000, pendingIntent);

    }



    /**
     * cancel reminder
     *
     * @param context
     */
    public void cancelReminder(Context context) {

//        Log.d("salam","cancel sensorUpdate Reminder");

        ComponentName receiver = new ComponentName(context, SensorUpdateAlarmReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent1 = new Intent(context, SensorUpdateAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        android.app.AlarmManager am = (android.app.AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }


}
