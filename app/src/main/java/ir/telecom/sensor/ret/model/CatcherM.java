package ir.telecom.sensor.ret.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatcherM {

    @Expose
    @SerializedName("latitude")
    private double latitude;
    @Expose
    @SerializedName("mnc")
    private int mnc;
    @Expose
    @SerializedName("longitude")
    private double longitude;
    @Expose
    @SerializedName("p11")
    private float p11;
    @Expose
    @SerializedName("p12")
    private float p12;
    @Expose
    @SerializedName("p13")
    private float p13;
    @Expose
    @SerializedName("p14")
    private float p14;
    @Expose
    @SerializedName("duration")
    private int duration;
    @Expose
    @SerializedName("timestamp")
    private String timestamp;
    @Expose
    @SerializedName("p8")
    private float p8;
    @Expose
    @SerializedName("lac")
    private int lac;
    @Expose
    @SerializedName("p9")
    private float p9;
    @Expose
    @SerializedName("cid")
    private int cid;
    @Expose
    @SerializedName("p10")
    private float p10;
    @Expose
    @SerializedName("score")
    private double score;
    @Expose
    @SerializedName("power")
    private int pwr;
    @Expose
    @SerializedName("p1")
    private float p1;
    @Expose
    @SerializedName("mcc")
    private int mcc;
    @Expose
    @SerializedName("p2")
    private float p2;
    @Expose
    @SerializedName("p3")
    private float p3;
    @Expose
    @SerializedName("p4")
    private float p4;
    @Expose
    @SerializedName("p5")
    private float p5;
    @Expose
    @SerializedName("p6")
    private float p6;
    @Expose
    @SerializedName("valid")
    private int valid;
    @Expose
    @SerializedName("p7")
    private float p7;

    public CatcherM(String timestamp, int mcc, int mnc, int lac, int cid, int duration, float a2, float a4, float a5, float k1, float k2, float c1, float c2, float c4, float c5, float t1, float t3, float t4, float r1, float r2, double longitude, double latitude, short valid, float score, int
                    power) {
        this.timestamp = timestamp;
        this.mcc = mcc;
        this.mnc = mnc;
        this.lac = lac;
        this.cid = cid;
        this.duration = duration;
        this.p1 = a2;
        this.p2 = a4;
        this.p3 = a5;
        this.p4 = k1;
        this.p5 = k2;
        this.p6 = c1;
        this.p7 = c2;
        this.p8 = c4;
        this.p9 = c5;
        this.p10 = t1;
        this.p11 = t3;
        this.p12 = t4;
        this.p13 = r1;
        this.p14 = r2;
        this.longitude = longitude;
        this.latitude = latitude;
        this.valid = valid;
        this.score = score;
        this.pwr = power;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public float getP11() {
        return p11;
    }

    public void setP11(float p11) {
        this.p11 = p11;
    }

    public float getP12() {
        return p12;
    }

    public void setP12(float p12) {
        this.p12 = p12;
    }

    public float getP13() {
        return p13;
    }

    public void setP13(float p13) {
        this.p13 = p13;
    }

    public float getP14() {
        return p14;
    }

    public void setP14(float p14) {
        this.p14 = p14;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public float getP8() {
        return p8;
    }

    public void setP8(float p8) {
        this.p8 = p8;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public float getP9() {
        return p9;
    }

    public void setP9(float p9) {
        this.p9 = p9;
    }
    public int getPower() {
        return pwr;
    }

    public void setPower(int power) {
        this.pwr = power;
    }
    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public double getP10() {
        return p10;
    }

    public void setP10(float p10) {
        this.p10 = p10;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public float getP3() {
        return p3;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public float getP4() {
        return p4;
    }

    public void setP4(float p4) {
        this.p4 = p4;
    }

    public float getP5() {
        return p5;
    }

    public void setP5(float p5) {
        this.p5 = p5;
    }

    public float getP6() {
        return p6;
    }

    public void setP6(float p6) {
        this.p6 = p6;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public float getP7() {
        return p7;
    }

    public void setP7(float p7) {
        this.p7 = p7;
    }
}
//
//public class CatcherM implements Serializable {
//    private Long id;
//    private String timestamp;
//    private int mcc;
//    private int mnc;
//    private int lac;
//    private int cid;
//    private int duration;
//    private float p1;
//    private float p2;
//    private float p3;
//    private float p4;
//    private float p5;
//    private float p6;
//    private float p7;
//    private float p8;
//    private float p9;
//    private float p10;
//    private float p11;
//    private float p12;
//    private float p13;
//    private float p14;
//    private double longitude;
//    private double latitude;
//    private short valid;
//    private float score;
//
//    public CatcherM(String timestamp, int mcc, int mnc, int lac, int cid, int duration, float a2, float a4, float a5, float k1, float k2, float c1, float c2, float c4, float c5, float t1, float t3, float t4, float r1, float r2, double longitude, double latitude, short valid, float score) {
//        this.timestamp = timestamp;
//        this.mcc = mcc;
//        this.mnc = mnc;
//        this.lac = lac;
//        this.cid = cid;
//        this.duration = duration;
//        this.p1 = a2;
//        this.p2 = a4;
//        this.p3= a5;
//        this.p4 = k1;
//        this.p5 = k2;
//        this.p6 = c1;
//        this.p7 = c2;
//        this.p8 = c4;
//        this.p9 = c5;
//        this.p10 = t1;
//        this.p11 = t3;
//        this.p12 = t4;
//        this.p13 = r1;
//        this.p14 = r2;
//        this.longitude = longitude;
//        this.latitude = latitude;
//        this.valid = valid;
//        this.score = score;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public int getMcc() {
//        return mcc;
//    }
//
//    public void setMcc(int mcc) {
//        this.mcc = mcc;
//    }
//
//    public int getMnc() {
//        return mnc;
//    }
//
//    public void setMnc(int mnc) {
//        this.mnc = mnc;
//    }
//
//    public int getLac() {
//        return lac;
//    }
//
//    public void setLac(int lac) {
//        this.lac = lac;
//    }
//
//    public int getCid() {
//        return cid;
//    }
//
//    public void setCid(int cid) {
//        this.cid = cid;
//    }
//
//    public int getDuration() {
//        return duration;
//    }
//
//    public void setDuration(int duration) {
//        this.duration = duration;
//    }
//
//    public float getP1() {
//        return p1;
//    }
//
//    public void setP1(float p1) {
//        this.p1 = p1;
//    }
//
//    public float getP2() {
//        return p2;
//    }
//
//    public void setP2(float p2) {
//        this.p2 = p2;
//    }
//
//    public float getP3() {
//        return p3;
//    }
//
//    public void setP3(float p3) {
//        this.p3 = p3;
//    }
//
//    public float getP4() {
//        return p4;
//    }
//
//    public void setP4(float p4) {
//        this.p4 = p4;
//    }
//
//    public float getP5() {
//        return p5;
//    }
//
//    public void setP5(float p5) {
//        this.p5 = p5;
//    }
//
//    public float getP6() {
//        return p6;
//    }
//
//    public void setP6(float p6) {
//        this.p6 = p6;
//    }
//
//    public float getP7() {
//        return p7;
//    }
//
//    public void setP7(float p7) {
//        this.p7 = p7;
//    }
//
//    public float getP8() {
//        return p8;
//    }
//
//    public void setP8(float p8) {
//        this.p8 = p8;
//    }
//
//    public float getP9() {
//        return p9;
//    }
//
//    public void setP9(float p9) {
//        this.p9 = p9;
//    }
//
//    public float getP10() {
//        return p10;
//    }
//
//    public void setP10(float p10) {
//        this.p10 = p10;
//    }
//
//    public float getP11() {
//        return p11;
//    }
//
//    public void setP11(float p11) {
//        this.p11 = p11;
//    }
//
//    public float getP12() {
//        return p12;
//    }
//
//    public void setP12(float p12) {
//        this.p12 = p12;
//    }
//
//    public float getP13() {
//        return p13;
//    }
//
//    public void setP13(float p13) {
//        this.p13 = p13;
//    }
//
//    public float getP14() {
//        return p14;
//    }
//
//    public void setP14(float p14) {
//        this.p14 = p14;
//    }
//
//    public double getLongitude() {
//        return longitude;
//    }
//
//    public void setLongitude(double longitude) {
//        this.longitude = longitude;
//    }
//
//    public double getLatitude() {
//        return latitude;
//    }
//
//    public void setLatitude(double latitude) {
//        this.latitude = latitude;
//    }
//
//    public short getValid() {
//        return valid;
//    }
//
//    public void setValid(short valid) {
//        this.valid = valid;
//    }
//
//    public float getScore() {
//        return score;
//    }
//
//    public void setScore(float score) {
//        this.score = score;
//    }
//
//
//    public String toJson() {
//        return "{" +
//                "\"timestamp\"='" + timestamp + '\'' +
//                ", \"mcc\"=" + mcc +
//                ", \"mnc\"=" + mnc +
//                ", \"lac\"=" + lac +
//                ", \"lac\"=" + lac +
//                ", \"cid\"=" + cid +
//                ", \"duration\"=" + duration +
//                ", \"p1\"=" + p1 +
//                ", \"p2\"=" + p2 +
//                ", \"p3\"=" + p3 +
//                ", \"p4\"=" + p4 +
//                ", \"p5\"=" + p5 +
//                ", \"p6\"=" + p6 +
//                ", \"p7\"=" + p7 +
//                ", \"p8\"=" + p8 +
//                ", \"p9\"=" + p9 +
//                ", \"p10\"=" + p10 +
//                ", \"p11\"=" + p11 +
//                ", \"p12\"=" + p12 +
//                ", \"p13\"=" + p13 +
//                ", \"p14\"=" + p14 +
//                ", \"longitude\"=" + longitude +
//                ", \"latitude\"=" + latitude +
//                ", \"valid\"=" + valid +
//                ", \"score\"=" + score +
//                '}';
//    }
//
//
//}
