package ir.telecom.sensor.ret.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionInfoM {
    @Expose
    @SerializedName("assign")
    private short assign;
    @Expose
    @SerializedName("auth_req_fn")
    private int auth_req_fn;
    @Expose
    @SerializedName("cipher")
    private short cipher;
    @Expose
    @SerializedName("cipher_comp_first")
    private int cipher_comp_first;
    @Expose
    @SerializedName("arfcn")
    private int arfcn;
    @Expose
    @SerializedName("predict")
    private short predict;
    @Expose
    @SerializedName("t_locupd")
    private short t_locupd;
    @Expose
    @SerializedName("duration")
    private int duration;
    @Expose
    @SerializedName("iden_imei_ac")
    private short iden_imei_ac;
    @Expose
    @SerializedName("service_req")
    private short service_req;
    @Expose
    @SerializedName("handover")
    private short handover;
    @Expose
    @SerializedName("iden_imsi_bc")
    private short iden_imsi_bc;
    @Expose
    @SerializedName("a_chan_mode")
    private short a_chan_mode;
    @Expose
    @SerializedName("rr_cause")
    private short rr_cause;
    @Expose
    @SerializedName("cipher_comp_count")
    private int cipher_comp_count;
    @Expose
    @SerializedName("t_ss")
    private short t_ss;
    @Expose
    @SerializedName("msisdn")
    private String msisdn;
    @Expose
    @SerializedName("enc")
    private short enc;
    @Expose
    @SerializedName("a_hopping")
    private short a_hopping;
    @Expose
    @SerializedName("cipher_delta")
    private int cipher_delta;
    @Expose
    @SerializedName("t_abort")
    private short t_abort;
    @Expose
    @SerializedName("new_tmsi")
    private String new_tmsi;
    @Expose
    @SerializedName("ue_cipher_cap")
    private short ue_cipher_cap;
    @Expose
    @SerializedName("lu_lac")
    private int lu_lac;
    @Expose
    @SerializedName("rat")
    private short rat;
    @Expose
    @SerializedName("mnc")
    private short mnc;
    @Expose
    @SerializedName("cracked")
    private short cracked;
    @Expose
    @SerializedName("domain")
    private short domain;
    @Expose
    @SerializedName("a_hsn")
    private short a_hsn;
    @Expose
    @SerializedName("mobile_term")
    private short mobile_term;
    @Expose
    @SerializedName("avg_power")
    private short avg_power;
    @Expose
    @SerializedName("sms_presence")
    private short sms_presence;
    @Expose
    @SerializedName("t_sms")
    private short t_sms;
    @Expose
    @SerializedName("timestamp")
    private String timestamp;
    @Expose
    @SerializedName("enc_null")
    private short enc_null;
    @Expose
    @SerializedName("a_maio")
    private short a_maio;
    @Expose
    @SerializedName("lac")
    private int lac;
    @Expose
    @SerializedName("forced_ho")
    private short forced_ho;
    @Expose
    @SerializedName("lu_mnc")
    private short lu_mnc;
    @Expose
    @SerializedName("t_attach")
    private short t_attach;
    @Expose
    @SerializedName("neigh_count")
    private short neigh_count;
    @Expose
    @SerializedName("iden_imsi_ac")
    private short iden_imsi_ac;
    @Expose
    @SerializedName("enc_si")
    private short enc_si;
    @Expose
    @SerializedName("enc_rand")
    private short enc_rand;
    @Expose
    @SerializedName("enc_si_rand")
    private short enc_si_rand;
    @Expose
    @SerializedName("tlli")
    private String tlli;
    @Expose
    @SerializedName("last_fn")
    private int last_fn;
    @Expose
    @SerializedName("t_gprs")
    private short t_gprs;
    @Expose
    @SerializedName("cipher_missing")
    private short cipher_missing;
    @Expose
    @SerializedName("t_raupd")
    private short t_raupd;
    @Expose
    @SerializedName("a_multirate")
    private short a_multirate;
    @Expose
    @SerializedName("lu_rej_cause")
    private short lu_rej_cause;
    @Expose
    @SerializedName("a_chan_type")
    private short a_chan_type;
    @Expose
    @SerializedName("auth")
    private short auth;
    @Expose
    @SerializedName("cipher_comp_last")
    private int cipher_comp_last;
    @Expose
    @SerializedName("pdp_ip")
    private String pdp_ip;
    @Expose
    @SerializedName("imsi")
    private String imsi;
    @Expose
    @SerializedName("a_arfcn")
    private short a_arfcn;
    @Expose
    @SerializedName("lu_type")
    private short lu_type;
    @Expose
    @SerializedName("tmsi")
    private String tmsi;
    @Expose
    @SerializedName("call_presence")
    private short call_presence;
    @Expose
    @SerializedName("cipher_seq")
    private short cipher_seq;
    @Expose
    @SerializedName("auth_delta")
    private int auth_delta;
    @Expose
    @SerializedName("t_tmsi_realloc")
    private short t_tmsi_realloc;
    @Expose
    @SerializedName("uplink_avail")
    private short uplink_avail;
    @Expose
    @SerializedName("ue_integrity_cap")
    private short ue_integrity_cap;
    @Expose
    @SerializedName("cid")
    private int cid;
    @Expose
    @SerializedName("ms_cipher_mask")
    private short ms_cipher_mask;
    @Expose
    @SerializedName("t_call")
    private short t_call;
    @Expose
    @SerializedName("lu_acc")
    private short lu_acc;
    @Expose
    @SerializedName("imei")
    private String imei;
    @Expose
    @SerializedName("paging_mi")
    private short paging_mi;
    @Expose
    @SerializedName("t_release")
    private short t_release;
    @Expose
    @SerializedName("enc_null_rand")
    private short enc_null_rand;
    @Expose
    @SerializedName("assign_cmpl")
    private short assign_cmpl;
    @Expose
    @SerializedName("t_detach")
    private short t_detach;
    @Expose
    @SerializedName("t_pdp")
    private short t_pdp;
    @Expose
    @SerializedName("cmc_imeisv")
    private short cmc_imeisv;
    @Expose
    @SerializedName("t_unknown")
    private short t_unknown;
    @Expose
    @SerializedName("lu_mcc")
    private short lu_mcc;
    @Expose
    @SerializedName("initial_seq")
    private short initial_seq;
    @Expose
    @SerializedName("iden_imei_bc")
    private short iden_imei_bc;
    @Expose
    @SerializedName("unenc_rand")
    private short unenc_rand;
    @Expose
    @SerializedName("psc")
    private short psc;
    @Expose
    @SerializedName("lu_reject")
    private short lu_reject;
    @Expose
    @SerializedName("a_timeslot")
    private short a_timeslot;
    @Expose
    @SerializedName("unenc")
    private short unenc;
    @Expose
    @SerializedName("att_acc")
    private short att_acc;
    @Expose
    @SerializedName("a_tsc")
    private short a_tsc;
    @Expose
    @SerializedName("a_ma_len")
    private short a_ma_len;
    @Expose
    @SerializedName("integrity")
    private short integrity;
    @Expose
    @SerializedName("mcc")
    private short mcc;
    @Expose
    @SerializedName("auth_resp_fn")
    private int auth_resp_fn;
    @Expose
    @SerializedName("mobile_orig")
    private short mobile_orig;
    @Expose
    @SerializedName("first_fn")
    private int first_fn;

    public SessionInfoM(String timestamp, short rat, short domain, short mcc, short mnc, int lac, int cid, int arfcn, short psc, short cracked, short neigh_count, short unenc, short unenc_rand, short enc, short enc_rand, short enc_null, short enc_null_rand, short enc_si, short enc_si_rand, short predict, short avg_power, short uplink_avail, short initial_seq, short cipher_seq, short auth, int auth_req_fn, int auth_resp_fn, int auth_delta, short cipher_missing, int cipher_comp_first, int cipher_comp_last, int cipher_comp_count, int cipher_delta, short cipher, short cmc_imeisv, short integrity, int first_fn, int last_fn, int duration, short mobile_orig, short mobile_term, short paging_mi, short t_unknown, short t_detach, short t_locupd, short lu_acc, short lu_type, short lu_reject, short lu_rej_cause, short lu_mcc, short lu_mnc, int lu_lac, short t_abort, short t_raupd, short t_attach, short att_acc, short t_pdp, String pdp_ip, short tCall, short t_call, short t_sms, short t_ss, short t_tmsi_realloc, short t_release, short rr_cause, short t_gprs, short iden_imsi_ac, short iden_imsi_bc, short iden_imei_ac, short iden_imei_bc, short assign, short assign_cmpl, short handover, short forced_ho, short a_timeslot, short a_chan_type, short a_tsc, short a_hopping, short a_arfcn, short a_hsn, short a_maio, short a_ma_len, short a_chan_mode, short a_multirate, short call_presence, short sms_presence, short service_req, String imsi, String imei, String tmsi, String new_tmsi, String tlli, String msisdn, short ms_cipher_mask, short ue_cipher_cap, short ue_integrity_cap) {
        this.timestamp = timestamp;
        this.rat = rat;
        this.domain = domain;
        this.mcc = mcc;
        this.mnc = mnc;
        this.lac = lac;
        this.cid = cid;
        this.arfcn = arfcn;
        this.psc = psc;
        this.cracked = cracked;
        this.neigh_count = neigh_count;
        this.unenc = unenc;
        this.unenc_rand = unenc_rand;
        this.enc = enc;
        this.enc_rand = enc_rand;
        this.enc_null = enc_null;
        this.enc_null_rand = enc_null_rand;
        this.enc_si = enc_si;
        this.enc_si_rand = enc_si_rand;
        this.predict = predict;
        this.avg_power = avg_power;
        this.uplink_avail = uplink_avail;
        this.initial_seq = initial_seq;
        this.cipher_seq = cipher_seq;
        this.auth = auth;
        this.auth_req_fn = auth_req_fn;
        this.auth_resp_fn = auth_resp_fn;
        this.auth_delta = auth_delta;
        this.cipher_missing = cipher_missing;
        this.cipher_comp_first = cipher_comp_first;
        this.cipher_comp_last = cipher_comp_last;
        this.cipher_comp_count = cipher_comp_count;
        this.cipher_delta = cipher_delta;
        this.cipher = cipher;
        this.cmc_imeisv = cmc_imeisv;
        this.integrity = integrity;
        this.first_fn = first_fn;
        this.last_fn = last_fn;
        this.duration = duration;
        this.mobile_orig = mobile_orig;
        this.mobile_term = mobile_term;
        this.paging_mi = paging_mi;
        this.t_unknown = t_unknown;
        this.t_detach = t_detach;
        this.t_locupd = t_locupd;
        this.lu_acc = lu_acc;
        this.lu_type = lu_type;
        this.lu_reject = lu_reject;
        this.lu_rej_cause = lu_rej_cause;
        this.lu_mcc = lu_mcc;
        this.lu_mnc = lu_mnc;
        this.lu_lac = lu_lac;
        this.t_abort = t_abort;
        this.t_raupd = t_raupd;
        this.t_attach = t_attach;
        this.att_acc = att_acc;
        this.t_pdp = t_pdp;
        this.pdp_ip = pdp_ip;
        this.t_call = t_call;
        this.t_sms = t_sms;
        this.t_ss = t_ss;
        this.t_tmsi_realloc = t_tmsi_realloc;
        this.t_release = t_release;
        this.rr_cause = rr_cause;
        this.t_gprs = t_gprs;
        this.iden_imsi_ac = iden_imsi_ac;
        this.iden_imsi_bc = iden_imsi_bc;
        this.iden_imei_ac = iden_imei_ac;
        this.iden_imei_bc = iden_imei_bc;
        this.assign = assign;
        this.assign_cmpl = assign_cmpl;
        this.handover = handover;
        this.forced_ho = forced_ho;
        this.a_timeslot = a_timeslot;
        this.a_chan_type = a_chan_type;
        this.a_tsc = a_tsc;
        this.a_hopping = a_hopping;
        this.a_arfcn = a_arfcn;
        this.a_hsn = a_hsn;
        this.a_maio = a_maio;
        this.a_ma_len = a_ma_len;
        this.a_chan_mode = a_chan_mode;
        this.a_multirate = a_multirate;
        this.call_presence = call_presence;
        this.sms_presence = sms_presence;
        this.service_req = service_req;
        this.imsi = imsi;
        this.imei = imei;
        this.tmsi = tmsi;
        this.new_tmsi = new_tmsi;
        this.tlli = tlli;
        this.msisdn = msisdn;
        this.ms_cipher_mask = ms_cipher_mask;
        this.ue_cipher_cap = ue_cipher_cap;
        this.ue_integrity_cap = ue_integrity_cap;

    }
//    public SessionInfoM(String timestamp, short rat, short domain, short mcc, short mnc, int lac, int cid,
//                        int arfcn, short psc, short cracked, short neigh_count, short unenc,
//                        short unenc_rand, short enc, short enc_rand, short enc_null,
//                        short enc_null_rand, short enc_si, short enc_si_rand,
//                        short predict, short avg_power, short uplink_avail,
//                        short initial_seq, short cipher_seq, short auth, int auth_req_fn,
//                        int auth_resp_fn, int auth_delta, short cipher_missing,
//                        int cipher_comp_first, int cipher_comp_last, int cipher_comp_count,
//                        int cipher_delta, short cipher, short cmc_imeisv, short integrity,
//                        int first_fn, int last_fn, int duration, short mobile_orig,
//                        short mobile_term, short paging_mi, short t_unknown, short t_detach,
//                        short t_locupd, short lu_acc, short lu_type, short lu_reject,
//                        short lu_rej_cause, short lu_mcc, short lu_mnc, int lu_lac,
//                        short t_abort, short t_raupd, short t_attach, short att_acc,
//                        short t_pdp, String pdp_ip, short t_call, short t_call1,
//                        short t_sms, short t_ss, short t_tmsi_realloc, short t_release,
//                        short rr_cause, short t_gprs, short iden_imsi_ac, short iden_imsi_bc,
//                        short iden_imei_ac, short iden_imei_bc, short assign, short assign_cmpl,
//                        short handover, short forced_ho, short a_timeslot, short a_chan_type,
//                        short a_tsc, short a_hopping, short a_arfcn, short a_hsn, short a_maio,
//                        short a_ma_len, short a_chan_mode, short a_multirate, short call_presence,
//                        short sms_presence, short service_req, String imsi, String imei, String tmsi,
//                        String new_tmsi, String tlli, String msisdn, short ms_cipher_mask,
//                        short ue_cipher_cap, short ue_integrity_cap) {
//        this.timestamp=timestamp;this.rat = rat;this.domain =domain;
//        this.mcc = mcc, this.mnc short mnc, int lac, int cid,
//    }
//

//    public int getAssign() {
//        return assign;
//    }
//
//    public void setAssign(int assign) {
//        this.assign = assign;
//    }
//
//    public int getAuth_req_fn() {
//        return auth_req_fn;
//    }
//
//    public void setAuth_req_fn(int auth_req_fn) {
//        this.auth_req_fn = auth_req_fn;
//    }
//
//    public int getCipher() {
//        return cipher;
//    }
//
//    public void setCipher(int cipher) {
//        this.cipher = cipher;
//    }
//
//    public int getCipher_comp_first() {
//        return cipher_comp_first;
//    }
//
//    public void setCipher_comp_first(int cipher_comp_first) {
//        this.cipher_comp_first = cipher_comp_first;
//    }
//
//    public int getArfcn() {
//        return arfcn;
//    }
//
//    public void setArfcn(int arfcn) {
//        this.arfcn = arfcn;
//    }
//
//    public int getPredict() {
//        return predict;
//    }
//
//    public void setPredict(int predict) {
//        this.predict = predict;
//    }
//
//    public int getT_locupd() {
//        return t_locupd;
//    }
//
//    public void setT_locupd(int t_locupd) {
//        this.t_locupd = t_locupd;
//    }
//
//    public int getDuration() {
//        return duration;
//    }
//
//    public void setDuration(int duration) {
//        this.duration = duration;
//    }
//
//    public int getIden_imei_ac() {
//        return iden_imei_ac;
//    }
//
//    public void setIden_imei_ac(int iden_imei_ac) {
//        this.iden_imei_ac = iden_imei_ac;
//    }
//
//    public int getService_req() {
//        return service_req;
//    }
//
//    public void setService_req(int service_req) {
//        this.service_req = service_req;
//    }
//
//    public int getHandover() {
//        return handover;
//    }
//
//    public void setHandover(int handover) {
//        this.handover = handover;
//    }
//
//    public int getIden_imsi_bc() {
//        return iden_imsi_bc;
//    }
//
//    public void setIden_imsi_bc(int iden_imsi_bc) {
//        this.iden_imsi_bc = iden_imsi_bc;
//    }
//
//    public int getA_chan_mode() {
//        return a_chan_mode;
//    }
//
//    public void setA_chan_mode(int a_chan_mode) {
//        this.a_chan_mode = a_chan_mode;
//    }
//
//    public int getRr_cause() {
//        return rr_cause;
//    }
//
//    public void setRr_cause(int rr_cause) {
//        this.rr_cause = rr_cause;
//    }
//
//    public int getCipher_comp_count() {
//        return cipher_comp_count;
//    }
//
//    public void setCipher_comp_count(int cipher_comp_count) {
//        this.cipher_comp_count = cipher_comp_count;
//    }
//
//    public int getT_ss() {
//        return t_ss;
//    }
//
//    public void setT_ss(int t_ss) {
//        this.t_ss = t_ss;
//    }
//
//    public String getMsisdn() {
//        return msisdn;
//    }
//
//    public void setMsisdn(String msisdn) {
//        this.msisdn = msisdn;
//    }
//
//    public int getEnc() {
//        return enc;
//    }
//
//    public void setEnc(int enc) {
//        this.enc = enc;
//    }
//
//    public int getA_hopping() {
//        return a_hopping;
//    }
//
//    public void setA_hopping(int a_hopping) {
//        this.a_hopping = a_hopping;
//    }
//
//    public int getCipher_delta() {
//        return cipher_delta;
//    }
//
//    public void setCipher_delta(int cipher_delta) {
//        this.cipher_delta = cipher_delta;
//    }
//
//    public int getT_abort() {
//        return t_abort;
//    }
//
//    public void setT_abort(int t_abort) {
//        this.t_abort = t_abort;
//    }
//
//    public String getNew_tmsi() {
//        return new_tmsi;
//    }
//
//    public void setNew_tmsi(String new_tmsi) {
//        this.new_tmsi = new_tmsi;
//    }
//
//    public int getUe_cipher_cap() {
//        return ue_cipher_cap;
//    }
//
//    public void setUe_cipher_cap(int ue_cipher_cap) {
//        this.ue_cipher_cap = ue_cipher_cap;
//    }
//
//    public int getLu_lac() {
//        return lu_lac;
//    }
//
//    public void setLu_lac(int lu_lac) {
//        this.lu_lac = lu_lac;
//    }
//
//    public short getRat() {
//        return rat;
//    }
//
//    public void setRat(short rat) {
//        this.rat = rat;
//    }
//
//    public int getMnc() {
//        return mnc;
//    }
//
//    public void setMnc(int mnc) {
//        this.mnc = mnc;
//    }
//
//    public int getCracked() {
//        return cracked;
//    }
//
//    public void setCracked(int cracked) {
//        this.cracked = cracked;
//    }
//
//    public int getDomain() {
//        return domain;
//    }
//
//    public void setDomain(short domain) {
//        this.domain = domain;
//    }
//
//    public int getA_hsn() {
//        return a_hsn;
//    }
//
//    public void setA_hsn(int a_hsn) {
//        this.a_hsn = a_hsn;
//    }
//
//    public int getMobile_term() {
//        return mobile_term;
//    }
//
//    public void setMobile_term(int mobile_term) {
//        this.mobile_term = mobile_term;
//    }
//
//    public int getAvg_power() {
//        return avg_power;
//    }
//
//    public void setAvg_power(int avg_power) {
//        this.avg_power = avg_power;
//    }
//
//    public int getSms_presence() {
//        return sms_presence;
//    }
//
//    public void setSms_presence(int sms_presence) {
//        this.sms_presence = sms_presence;
//    }
//
//    public int getT_sms() {
//        return t_sms;
//    }
//
//    public void setT_sms(int t_sms) {
//        this.t_sms = t_sms;
//    }
//
//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public int getEnc_null() {
//        return enc_null;
//    }
//
//    public void setEnc_null(int enc_null) {
//        this.enc_null = enc_null;
//    }
//
//    public int getA_maio() {
//        return a_maio;
//    }
//
//    public void setA_maio(int a_maio) {
//        this.a_maio = a_maio;
//    }
//
//    public int getLac() {
//        return lac;
//    }
//
//    public void setLac(int lac) {
//        this.lac = lac;
//    }
//
//    public int getForced_ho() {
//        return forced_ho;
//    }
//
//    public void setForced_ho(int forced_ho) {
//        this.forced_ho = forced_ho;
//    }
//
//    public int getLu_mnc() {
//        return lu_mnc;
//    }
//
//    public void setLu_mnc(int lu_mnc) {
//        this.lu_mnc = lu_mnc;
//    }
//
//    public int getT_attach() {
//        return t_attach;
//    }
//
//    public void setT_attach(int t_attach) {
//        this.t_attach = t_attach;
//    }
//
//    public int getNeigh_count() {
//        return neigh_count;
//    }
//
//    public void setNeigh_count(int neigh_count) {
//        this.neigh_count = neigh_count;
//    }
//
//    public int getIden_imsi_ac() {
//        return iden_imsi_ac;
//    }
//
//    public void setIden_imsi_ac(int iden_imsi_ac) {
//        this.iden_imsi_ac = iden_imsi_ac;
//    }
//
//    public int getEnc_si() {
//        return enc_si;
//    }
//
//    public void setEnc_si(int enc_si) {
//        this.enc_si = enc_si;
//    }
//
//    public int getEnc_rand() {
//        return enc_rand;
//    }
//
//    public void setEnc_rand(int enc_rand) {
//        this.enc_rand = enc_rand;
//    }
//
//    public int getEnc_si_rand() {
//        return enc_si_rand;
//    }
//
//    public void setEnc_si_rand(int enc_si_rand) {
//        this.enc_si_rand = enc_si_rand;
//    }
//
//    public String getTlli() {
//        return tlli;
//    }
//
//    public void setTlli(String tlli) {
//        this.tlli = tlli;
//    }
//
//    public int getLast_fn() {
//        return last_fn;
//    }
//
//    public void setLast_fn(int last_fn) {
//        this.last_fn = last_fn;
//    }
//
//    public int getT_gprs() {
//        return t_gprs;
//    }
//
//    public void setT_gprs(int t_gprs) {
//        this.t_gprs = t_gprs;
//    }
//
//    public int getCipher_missing() {
//        return cipher_missing;
//    }
//
//    public void setCipher_missing(int cipher_missing) {
//        this.cipher_missing = cipher_missing;
//    }
//
//    public int getT_raupd() {
//        return t_raupd;
//    }
//
//    public void setT_raupd(int t_raupd) {
//        this.t_raupd = t_raupd;
//    }
//
//    public int getA_multirate() {
//        return a_multirate;
//    }
//
//    public void setA_multirate(int a_multirate) {
//        this.a_multirate = a_multirate;
//    }
//
//    public int getLu_rej_cause() {
//        return lu_rej_cause;
//    }
//
//    public void setLu_rej_cause(int lu_rej_cause) {
//        this.lu_rej_cause = lu_rej_cause;
//    }
//
//    public int getA_chan_type() {
//        return a_chan_type;
//    }
//
//    public void setA_chan_type(int a_chan_type) {
//        this.a_chan_type = a_chan_type;
//    }
//
//    public int getAuth() {
//        return auth;
//    }
//
//    public void setAuth(int auth) {
//        this.auth = auth;
//    }
//
//    public int getCipher_comp_last() {
//        return cipher_comp_last;
//    }
//
//    public void setCipher_comp_last(int cipher_comp_last) {
//        this.cipher_comp_last = cipher_comp_last;
//    }
//
//    public String getPdp_ip() {
//        return pdp_ip;
//    }
//
//    public void setPdp_ip(String pdp_ip) {
//        this.pdp_ip = pdp_ip;
//    }
//
//    public String getImsi() {
//        return imsi;
//    }
//
//    public void setImsi(String imsi) {
//        this.imsi = imsi;
//    }
//
//    public int getA_arfcn() {
//        return a_arfcn;
//    }
//
//    public void setA_arfcn(int a_arfcn) {
//        this.a_arfcn = a_arfcn;
//    }
//
//    public int getLu_type() {
//        return lu_type;
//    }
//
//    public void setLu_type(int lu_type) {
//        this.lu_type = lu_type;
//    }
//
//    public String getTmsi() {
//        return tmsi;
//    }
//
//    public void setTmsi(String tmsi) {
//        this.tmsi = tmsi;
//    }
//
//    public int getCall_presence() {
//        return call_presence;
//    }
//
//    public void setCall_presence(int call_presence) {
//        this.call_presence = call_presence;
//    }
//
//    public int getCipher_seq() {
//        return cipher_seq;
//    }
//
//    public void setCipher_seq(int cipher_seq) {
//        this.cipher_seq = cipher_seq;
//    }
//
//    public int getAuth_delta() {
//        return auth_delta;
//    }
//
//    public void setAuth_delta(int auth_delta) {
//        this.auth_delta = auth_delta;
//    }
//
//    public int getT_tmsi_realloc() {
//        return t_tmsi_realloc;
//    }
//
//    public void setT_tmsi_realloc(int t_tmsi_realloc) {
//        this.t_tmsi_realloc = t_tmsi_realloc;
//    }
//
//    public int getUplink_avail() {
//        return uplink_avail;
//    }
//
//    public void setUplink_avail(int uplink_avail) {
//        this.uplink_avail = uplink_avail;
//    }
//
//    public int getUe_integrity_cap() {
//        return ue_integrity_cap;
//    }
//
//    public void setUe_integrity_cap(int ue_integrity_cap) {
//        this.ue_integrity_cap = ue_integrity_cap;
//    }
//
//    public int getCid() {
//        return cid;
//    }
//
//    public void setCid(int cid) {
//        this.cid = cid;
//    }
//
//    public int getMs_cipher_mask() {
//        return ms_cipher_mask;
//    }
//
//    public void setMs_cipher_mask(int ms_cipher_mask) {
//        this.ms_cipher_mask = ms_cipher_mask;
//    }
//
//    public int getT_call() {
//        return t_call;
//    }
//
//    public void setT_call(int t_call) {
//        this.t_call = t_call;
//    }
//
//    public int getLu_acc() {
//        return lu_acc;
//    }
//
//    public void setLu_acc(int lu_acc) {
//        this.lu_acc = lu_acc;
//    }
//
//    public String getImei() {
//        return imei;
//    }
//
//    public void setImei(String imei) {
//        this.imei = imei;
//    }
//
//    public int getPaging_mi() {
//        return paging_mi;
//    }
//
//    public void setPaging_mi(int paging_mi) {
//        this.paging_mi = paging_mi;
//    }
//
//    public int getT_release() {
//        return t_release;
//    }
//
//    public void setT_release(int t_release) {
//        this.t_release = t_release;
//    }
//
//    public int getEnc_null_rand() {
//        return enc_null_rand;
//    }
//
//    public void setEnc_null_rand(int enc_null_rand) {
//        this.enc_null_rand = enc_null_rand;
//    }
//
//    public int getAssign_cmpl() {
//        return assign_cmpl;
//    }
//
//    public void setAssign_cmpl(int assign_cmpl) {
//        this.assign_cmpl = assign_cmpl;
//    }
//
//    public int getT_detach() {
//        return t_detach;
//    }
//
//    public void setT_detach(int t_detach) {
//        this.t_detach = t_detach;
//    }
//
//    public int getT_pdp() {
//        return t_pdp;
//    }
//
//    public void setT_pdp(int t_pdp) {
//        this.t_pdp = t_pdp;
//    }
//
//    public int getCmc_imeisv() {
//        return cmc_imeisv;
//    }
//
//    public void setCmc_imeisv(int cmc_imeisv) {
//        this.cmc_imeisv = cmc_imeisv;
//    }
//
//    public int getT_unknown() {
//        return t_unknown;
//    }
//
//    public void setT_unknown(int t_unknown) {
//        this.t_unknown = t_unknown;
//    }
//
//    public int getLu_mcc() {
//        return lu_mcc;
//    }
//
//    public void setLu_mcc(int lu_mcc) {
//        this.lu_mcc = lu_mcc;
//    }
//
//    public int getInitial_seq() {
//        return initial_seq;
//    }
//
//    public void setInitial_seq(int initial_seq) {
//        this.initial_seq = initial_seq;
//    }
//
//    public int getIden_imei_bc() {
//        return iden_imei_bc;
//    }
//
//    public void setIden_imei_bc(int iden_imei_bc) {
//        this.iden_imei_bc = iden_imei_bc;
//    }
//
//    public int getUnenc_rand() {
//        return unenc_rand;
//    }
//
//    public void setUnenc_rand(int unenc_rand) {
//        this.unenc_rand = unenc_rand;
//    }
//
//    public int getPsc() {
//        return psc;
//    }
//
//    public void setPsc(int psc) {
//        this.psc = psc;
//    }
//
//    public int getLu_reject() {
//        return lu_reject;
//    }
//
//    public void setLu_reject(int lu_reject) {
//        this.lu_reject = lu_reject;
//    }
//
//    public int getA_timeslot() {
//        return a_timeslot;
//    }
//
//    public void setA_timeslot(int a_timeslot) {
//        this.a_timeslot = a_timeslot;
//    }
//
//    public int getUnenc() {
//        return unenc;
//    }
//
//    public void setUnenc(int unenc) {
//        this.unenc = unenc;
//    }
//
//    public int getAtt_acc() {
//        return att_acc;
//    }
//
//    public void setAtt_acc(int att_acc) {
//        this.att_acc = att_acc;
//    }
//
//    public int getA_tsc() {
//        return a_tsc;
//    }
//
//    public void setA_tsc(int a_tsc) {
//        this.a_tsc = a_tsc;
//    }
//
//    public int getA_ma_len() {
//        return a_ma_len;
//    }
//
//    public void setA_ma_len(int a_ma_len) {
//        this.a_ma_len = a_ma_len;
//    }
//
//    public int getIntegrity() {
//        return integrity;
//    }
//
//    public void setIntegrity(int integrity) {
//        this.integrity = integrity;
//    }
//
//    public int getMcc() {
//        return mcc;
//    }
//
//    public void setMcc(int mcc) {
//        this.mcc = mcc;
//    }
//
//    public int getAuth_resp_fn() {
//        return auth_resp_fn;
//    }
//
//    public void setAuth_resp_fn(int auth_resp_fn) {
//        this.auth_resp_fn = auth_resp_fn;
//    }
//
//    public int getMobile_orig() {
//        return mobile_orig;
//    }
//
//    public void setMobile_orig(int mobile_orig) {
//        this.mobile_orig = mobile_orig;
//    }
//
//    public int getFirst_fn() {
//        return first_fn;
//    }
//
//    public void setFirst_fn(int first_fn) {
//        this.first_fn = first_fn;
//    }

//    private Long id;
//    private String timestamp;
//    private short rat;
//    private short domain;
//    private short mcc;
//    private short mnc;
//    private int lac;
//    private int cid;
//    private int arfcn;
//    private short psc;
//    private short cracked;
//    private short neigh_count;
//    private short unenc;
//    private short unenc_rand;
//    private short enc;
//    private short enc_rand;
//    private short enc_null;
//    private short enc_null_rand;
//    private short enc_si;
//    private short enc_si_rand;
//    private short predict;
//    private short avg_power;
//    private short uplink_avail;
//    private short initial_seq;
//    private short cipher_seq;
//    private short auth;
//    private int auth_req_fn;
//    private int auth_resp_fn;
//    private int auth_delta;
//    private short cipher_missing;
//    private int cipher_comp_first;
//    private int cipher_comp_last;
//    private int cipher_comp_count;
//    private int cipher_delta;
//    private short cipher;
//    private short cmc_imeisv;
//    private short integrity;
//    private int first_fn;
//    private int last_fn;
//    private int duration;
//    private short mobile_orig;
//    private short mobile_term;
//    private short paging_mi;
//    private short t_unknown;
//    private short t_detach;
//    private short t_locupd;
//    private short lu_acc;
//    private short lu_type;
//    private short lu_reject;
//    private short lu_rej_cause;
//    private short lu_mcc;
//    private short lu_mnc;
//    private int lu_lac;
//    private short t_abort;
//    private short t_raupd;
//    private short t_attach;
//    private short att_acc;
//    private short t_pdp;
//    private String pdp_ip;
//    private short t_call;
//    private short t_sms;
//    private short t_ss;
//    private short t_tmsi_realloc;
//    private short t_release;
//    private short rr_cause;
//    private short t_gprs;
//    private short iden_imsi_ac;
//    private short iden_imsi_bc;
//    private short iden_imei_ac;
//    private short iden_imei_bc;
//    private short assign;
//    private short assign_cmpl;
//    private short handover;
//    private short forced_ho;
//    private short a_timeslot;
//    private short a_chan_type;
//    private short a_tsc;
//    private short a_hopping;
//    private short a_arfcn;
//    private short a_hsn;
//    private short a_maio;
//    private short a_ma_len;
//    private short a_chan_mode;
//    private short a_multirate;
//    private short call_presence;
//    private short sms_presence;
//    private short service_req;
//    private String imsi;
//    private String imei;
//    private String tmsi;
//    private String new_tmsi;
//    private String tlli;
//    private String msisdn;
//    private short ms_cipher_mask;
//    private short ue_cipher_cap;
//    private short ue_integrity_cap;
//
//    public SessionInfoM(String timestamp, short rat, short domain, short mcc, short mnc, int lac, int cid, int arfcn, short psc, short cracked, short neigh_count, short unenc, short unenc_rand, short enc, short enc_rand, short enc_null, short enc_null_rand, short enc_si, short enc_si_rand, short predict, short avg_power, short uplink_avail, short initial_seq, short cipher_seq, short auth, int auth_req_fn, int auth_resp_fn, int auth_delta, short cipher_missing, int cipher_comp_first, int cipher_comp_last, int cipher_comp_count, int cipher_delta, short cipher, short cmc_imeisv, short integrity, int first_fn, int last_fn, int duration, short mobile_orig, short mobile_term, short paging_mi, short t_unknown, short t_detach, short t_locupd, short lu_acc, short lu_type, short lu_reject, short lu_rej_cause, short lu_mcc, short lu_mnc, int lu_lac, short t_abort, short t_raupd, short t_attach, short att_acc, short t_pdp, String pdp_ip, short tCall, short t_call, short t_sms, short t_ss, short t_tmsi_realloc, short t_release, short rr_cause, short t_gprs, short iden_imsi_ac, short iden_imsi_bc, short iden_imei_ac, short iden_imei_bc, short assign, short assign_cmpl, short handover, short forced_ho, short a_timeslot, short a_chan_type, short a_tsc, short a_hopping, short a_arfcn, short a_hsn, short a_maio, short a_ma_len, short a_chan_mode, short a_multirate, short call_presence, short sms_presence, short service_req, String imsi, String imei, String tmsi, String new_tmsi, String tlli, String msisdn, short ms_cipher_mask, short ue_cipher_cap, short ue_integrity_cap) {
//        this.timestamp = timestamp;
//        this.rat = rat;
//        this.domain = domain;
//        this.mcc = mcc;
//        this.mnc = mnc;
//        this.lac = lac;
//        this.cid = cid;
//        this.arfcn = arfcn;
//        this.psc = psc;
//        this.cracked = cracked;
//        this.neigh_count = neigh_count;
//        this.unenc = unenc;
//        this.unenc_rand = unenc_rand;
//        this.enc = enc;
//        this.enc_rand = enc_rand;
//        this.enc_null = enc_null;
//        this.enc_null_rand = enc_null_rand;
//        this.enc_si = enc_si;
//        this.enc_si_rand = enc_si_rand;
//        this.predict = predict;
//        this.avg_power = avg_power;
//        this.uplink_avail = uplink_avail;
//        this.initial_seq = initial_seq;
//        this.cipher_seq = cipher_seq;
//        this.auth = auth;
//        this.auth_req_fn = auth_req_fn;
//        this.auth_resp_fn = auth_resp_fn;
//        this.auth_delta = auth_delta;
//        this.cipher_missing = cipher_missing;
//        this.cipher_comp_first = cipher_comp_first;
//        this.cipher_comp_last = cipher_comp_last;
//        this.cipher_comp_count = cipher_comp_count;
//        this.cipher_delta = cipher_delta;
//        this.cipher = cipher;
//        this.cmc_imeisv = cmc_imeisv;
//        this.integrity = integrity;
//        this.first_fn = first_fn;
//        this.last_fn = last_fn;
//        this.duration = duration;
//        this.mobile_orig = mobile_orig;
//        this.mobile_term = mobile_term;
//        this.paging_mi = paging_mi;
//        this.t_unknown = t_unknown;
//        this.t_detach = t_detach;
//        this.t_locupd = t_locupd;
//        this.lu_acc = lu_acc;
//        this.lu_type = lu_type;
//        this.lu_reject = lu_reject;
//        this.lu_rej_cause = lu_rej_cause;
//        this.lu_mcc = lu_mcc;
//        this.lu_mnc = lu_mnc;
//        this.lu_lac = lu_lac;
//        this.t_abort = t_abort;
//        this.t_raupd = t_raupd;
//        this.t_attach = t_attach;
//        this.att_acc = att_acc;
//        this.t_pdp = t_pdp;
//        this.pdp_ip = pdp_ip;
//        this.t_call = t_call;
//        this.t_sms = t_sms;
//        this.t_ss = t_ss;
//        this.t_tmsi_realloc = t_tmsi_realloc;
//        this.t_release = t_release;
//        this.rr_cause = rr_cause;
//        this.t_gprs = t_gprs;
//        this.iden_imsi_ac = iden_imsi_ac;
//        this.iden_imsi_bc = iden_imsi_bc;
//        this.iden_imei_ac = iden_imei_ac;
//        this.iden_imei_bc = iden_imei_bc;
//        this.assign = assign;
//        this.assign_cmpl = assign_cmpl;
//        this.handover = handover;
//        this.forced_ho = forced_ho;
//        this.a_timeslot = a_timeslot;
//        this.a_chan_type = a_chan_type;
//        this.a_tsc = a_tsc;
//        this.a_hopping = a_hopping;
//        this.a_arfcn = a_arfcn;
//        this.a_hsn = a_hsn;
//        this.a_maio = a_maio;
//        this.a_ma_len = a_ma_len;
//        this.a_chan_mode = a_chan_mode;
//        this.a_multirate = a_multirate;
//        this.call_presence = call_presence;
//        this.sms_presence = sms_presence;
//        this.service_req = service_req;
//        this.imsi = imsi;
//        this.imei = imei;
//        this.tmsi = tmsi;
//        this.new_tmsi = new_tmsi;
//        this.tlli = tlli;
//        this.msisdn = msisdn;
//        this.ms_cipher_mask = ms_cipher_mask;
//        this.ue_cipher_cap = ue_cipher_cap;
//        this.ue_integrity_cap = ue_integrity_cap;
//
//    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public short getRat() {
        return rat;
    }

    public void setRat(short rat) {
        this.rat = rat;
    }

    public short getDomain() {
        return domain;
    }

    public void setDomain(short domain) {
        this.domain = domain;
    }

    public short getMcc() {
        return mcc;
    }

    public void setMcc(short mcc) {
        this.mcc = mcc;
    }

    public short getMnc() {
        return mnc;
    }

    public void setMnc(short mnc) {
        this.mnc = mnc;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getArfcn() {
        return arfcn;
    }

    public void setArfcn(int arfcn) {
        this.arfcn = arfcn;
    }

    public short getPsc() {
        return psc;
    }

    public void setPsc(short psc) {
        this.psc = psc;
    }

    public short getCracked() {
        return cracked;
    }

    public void setCracked(short cracked) {
        this.cracked = cracked;
    }

    public short getNeigh_count() {
        return neigh_count;
    }

    public void setNeigh_count(short neigh_count) {
        this.neigh_count = neigh_count;
    }

    public short getUnenc() {
        return unenc;
    }

    public void setUnenc(short unenc) {
        this.unenc = unenc;
    }

    public short getUnenc_rand() {
        return unenc_rand;
    }

    public void setUnenc_rand(short unenc_rand) {
        this.unenc_rand = unenc_rand;
    }

    public short getEnc() {
        return enc;
    }

    public void setEnc(short enc) {
        this.enc = enc;
    }

    public short getEnc_rand() {
        return enc_rand;
    }

    public void setEnc_rand(short enc_rand) {
        this.enc_rand = enc_rand;
    }

    public short getEnc_null() {
        return enc_null;
    }

    public void setEnc_null(short enc_null) {
        this.enc_null = enc_null;
    }

    public short getEnc_null_rand() {
        return enc_null_rand;
    }

    public void setEnc_null_rand(short enc_null_rand) {
        this.enc_null_rand = enc_null_rand;
    }

    public short getEnc_si() {
        return enc_si;
    }

    public void setEnc_si(short enc_si) {
        this.enc_si = enc_si;
    }

    public short getEnc_si_rand() {
        return enc_si_rand;
    }

    public void setEnc_si_rand(short enc_si_rand) {
        this.enc_si_rand = enc_si_rand;
    }

    public short getPredict() {
        return predict;
    }

    public void setPredict(short predict) {
        this.predict = predict;
    }

    public short getAvg_power() {
        return avg_power;
    }

    public void setAvg_power(short avg_power) {
        this.avg_power = avg_power;
    }

    public short getUplink_avail() {
        return uplink_avail;
    }

    public void setUplink_avail(short uplink_avail) {
        this.uplink_avail = uplink_avail;
    }

    public short getInitial_seq() {
        return initial_seq;
    }

    public void setInitial_seq(short initial_seq) {
        this.initial_seq = initial_seq;
    }

    public short getCipher_seq() {
        return cipher_seq;
    }

    public void setCipher_seq(short cipher_seq) {
        this.cipher_seq = cipher_seq;
    }

    public short getAuth() {
        return auth;
    }

    public void setAuth(short auth) {
        this.auth = auth;
    }

    public int getAuth_req_fn() {
        return auth_req_fn;
    }

    public void setAuth_req_fn(int auth_req_fn) {
        this.auth_req_fn = auth_req_fn;
    }

    public int getAuth_resp_fn() {
        return auth_resp_fn;
    }

    public void setAuth_resp_fn(int auth_resp_fn) {
        this.auth_resp_fn = auth_resp_fn;
    }

    public int getAuth_delta() {
        return auth_delta;
    }

    public void setAuth_delta(int auth_delta) {
        this.auth_delta = auth_delta;
    }

    public short getCipher_missing() {
        return cipher_missing;
    }

    public void setCipher_missing(short cipher_missing) {
        this.cipher_missing = cipher_missing;
    }

    public int getCipher_comp_first() {
        return cipher_comp_first;
    }

    public void setCipher_comp_first(int cipher_comp_first) {
        this.cipher_comp_first = cipher_comp_first;
    }

    public int getCipher_comp_last() {
        return cipher_comp_last;
    }

    public void setCipher_comp_last(int cipher_comp_last) {
        this.cipher_comp_last = cipher_comp_last;
    }

    public int getCipher_comp_count() {
        return cipher_comp_count;
    }

    public void setCipher_comp_count(int cipher_comp_count) {
        this.cipher_comp_count = cipher_comp_count;
    }

    public int getCipher_delta() {
        return cipher_delta;
    }

    public void setCipher_delta(int cipher_delta) {
        this.cipher_delta = cipher_delta;
    }

    public short getCipher() {
        return cipher;
    }

    public void setCipher(short cipher) {
        this.cipher = cipher;
    }

    public short getCmc_imeisv() {
        return cmc_imeisv;
    }

    public void setCmc_imeisv(short cmc_imeisv) {
        this.cmc_imeisv = cmc_imeisv;
    }

    public short getIntegrity() {
        return integrity;
    }

    public void setIntegrity(short integrity) {
        this.integrity = integrity;
    }

    public int getFirst_fn() {
        return first_fn;
    }

    public void setFirst_fn(int first_fn) {
        this.first_fn = first_fn;
    }

    public int getLast_fn() {
        return last_fn;
    }

    public void setLast_fn(int last_fn) {
        this.last_fn = last_fn;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public short getMobile_orig() {
        return mobile_orig;
    }

    public void setMobile_orig(short mobile_orig) {
        this.mobile_orig = mobile_orig;
    }

    public short getMobile_term() {
        return mobile_term;
    }

    public void setMobile_term(short mobile_term) {
        this.mobile_term = mobile_term;
    }

    public short getPaging_mi() {
        return paging_mi;
    }

    public void setPaging_mi(short paging_mi) {
        this.paging_mi = paging_mi;
    }

    public short getT_unknown() {
        return t_unknown;
    }

    public void setT_unknown(short t_unknown) {
        this.t_unknown = t_unknown;
    }

    public short getT_detach() {
        return t_detach;
    }

    public void setT_detach(short t_detach) {
        this.t_detach = t_detach;
    }

    public short getT_locupd() {
        return t_locupd;
    }

    public void setT_locupd(short t_locupd) {
        this.t_locupd = t_locupd;
    }

    public short getLu_acc() {
        return lu_acc;
    }

    public void setLu_acc(short lu_acc) {
        this.lu_acc = lu_acc;
    }

    public short getLu_type() {
        return lu_type;
    }

    public void setLu_type(short lu_type) {
        this.lu_type = lu_type;
    }

    public short getLu_reject() {
        return lu_reject;
    }

    public void setLu_reject(short lu_reject) {
        this.lu_reject = lu_reject;
    }

    public short getLu_rej_cause() {
        return lu_rej_cause;
    }

    public void setLu_rej_cause(short lu_rej_cause) {
        this.lu_rej_cause = lu_rej_cause;
    }

    public short getLu_mcc() {
        return lu_mcc;
    }

    public void setLu_mcc(short lu_mcc) {
        this.lu_mcc = lu_mcc;
    }

    public short getLu_mnc() {
        return lu_mnc;
    }

    public void setLu_mnc(short lu_mnc) {
        this.lu_mnc = lu_mnc;
    }

    public int getLu_lac() {
        return lu_lac;
    }

    public void setLu_lac(int lu_lac) {
        this.lu_lac = lu_lac;
    }

    public short getT_abort() {
        return t_abort;
    }

    public void setT_abort(short t_abort) {
        this.t_abort = t_abort;
    }

    public short getT_raupd() {
        return t_raupd;
    }

    public void setT_raupd(short t_raupd) {
        this.t_raupd = t_raupd;
    }

    public short getT_attach() {
        return t_attach;
    }

    public void setT_attach(short t_attach) {
        this.t_attach = t_attach;
    }

    public short getAtt_acc() {
        return att_acc;
    }

    public void setAtt_acc(short att_acc) {
        this.att_acc = att_acc;
    }

    public short getT_pdp() {
        return t_pdp;
    }

    public void setT_pdp(short t_pdp) {
        this.t_pdp = t_pdp;
    }

    public String getPdp_ip() {
        return pdp_ip;
    }

    public void setPdp_ip(String pdp_ip) {
        this.pdp_ip = pdp_ip;
    }

    public short getT_call() {
        return t_call;
    }

    public void setT_call(short t_call) {
        this.t_call = t_call;
    }

    public short getT_sms() {
        return t_sms;
    }

    public void setT_sms(short t_sms) {
        this.t_sms = t_sms;
    }

    public short getT_ss() {
        return t_ss;
    }

    public void setT_ss(short t_ss) {
        this.t_ss = t_ss;
    }

    public short getT_tmsi_realloc() {
        return t_tmsi_realloc;
    }

    public void setT_tmsi_realloc(short t_tmsi_realloc) {
        this.t_tmsi_realloc = t_tmsi_realloc;
    }

    public short getT_release() {
        return t_release;
    }

    public void setT_release(short t_release) {
        this.t_release = t_release;
    }

    public short getRr_cause() {
        return rr_cause;
    }

    public void setRr_cause(short rr_cause) {
        this.rr_cause = rr_cause;
    }

    public short getT_gprs() {
        return t_gprs;
    }

    public void setT_gprs(short t_gprs) {
        this.t_gprs = t_gprs;
    }

    public short getIden_imsi_ac() {
        return iden_imsi_ac;
    }

    public void setIden_imsi_ac(short iden_imsi_ac) {
        this.iden_imsi_ac = iden_imsi_ac;
    }

    public short getIden_imsi_bc() {
        return iden_imsi_bc;
    }

    public void setIden_imsi_bc(short iden_imsi_bc) {
        this.iden_imsi_bc = iden_imsi_bc;
    }

    public short getIden_imei_ac() {
        return iden_imei_ac;
    }

    public void setIden_imei_ac(short iden_imei_ac) {
        this.iden_imei_ac = iden_imei_ac;
    }

    public short getIden_imei_bc() {
        return iden_imei_bc;
    }

    public void setIden_imei_bc(short iden_imei_bc) {
        this.iden_imei_bc = iden_imei_bc;
    }

    public short getAssign() {
        return assign;
    }

    public void setAssign(short assign) {
        this.assign = assign;
    }

    public short getAssign_cmpl() {
        return assign_cmpl;
    }

    public void setAssign_cmpl(short assign_cmpl) {
        this.assign_cmpl = assign_cmpl;
    }

    public short getHandover() {
        return handover;
    }

    public void setHandover(short handover) {
        this.handover = handover;
    }

    public short getForced_ho() {
        return forced_ho;
    }

    public void setForced_ho(short forced_ho) {
        this.forced_ho = forced_ho;
    }

    public short getA_timeslot() {
        return a_timeslot;
    }

    public void setA_timeslot(short a_timeslot) {
        this.a_timeslot = a_timeslot;
    }

    public short getA_chan_type() {
        return a_chan_type;
    }

    public void setA_chan_type(short a_chan_type) {
        this.a_chan_type = a_chan_type;
    }

    public short getA_tsc() {
        return a_tsc;
    }

    public void setA_tsc(short a_tsc) {
        this.a_tsc = a_tsc;
    }

    public short getA_hopping() {
        return a_hopping;
    }

    public void setA_hopping(short a_hopping) {
        this.a_hopping = a_hopping;
    }

    public short getA_arfcn() {
        return a_arfcn;
    }

    public void setA_arfcn(short a_arfcn) {
        this.a_arfcn = a_arfcn;
    }

    public short getA_hsn() {
        return a_hsn;
    }

    public void setA_hsn(short a_hsn) {
        this.a_hsn = a_hsn;
    }

    public short getA_maio() {
        return a_maio;
    }

    public void setA_maio(short a_maio) {
        this.a_maio = a_maio;
    }

    public short getA_ma_len() {
        return a_ma_len;
    }

    public void setA_ma_len(short a_ma_len) {
        this.a_ma_len = a_ma_len;
    }

    public short getA_chan_mode() {
        return a_chan_mode;
    }

    public void setA_chan_mode(short a_chan_mode) {
        this.a_chan_mode = a_chan_mode;
    }

    public short getA_multirate() {
        return a_multirate;
    }

    public void setA_multirate(short a_multirate) {
        this.a_multirate = a_multirate;
    }

    public short getCall_presence() {
        return call_presence;
    }

    public void setCall_presence(short call_presence) {
        this.call_presence = call_presence;
    }

    public short getSms_presence() {
        return sms_presence;
    }

    public void setSms_presence(short sms_presence) {
        this.sms_presence = sms_presence;
    }

    public short getService_req() {
        return service_req;
    }

    public void setService_req(short service_req) {
        this.service_req = service_req;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTmsi() {
        return tmsi;
    }

    public void setTmsi(String tmsi) {
        this.tmsi = tmsi;
    }

    public String getNew_tmsi() {
        return new_tmsi;
    }

    public void setNew_tmsi(String new_tmsi) {
        this.new_tmsi = new_tmsi;
    }

    public String getTlli() {
        return tlli;
    }

    public void setTlli(String tlli) {
        this.tlli = tlli;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public short getMs_cipher_mask() {
        return ms_cipher_mask;
    }

    public void setMs_cipher_mask(short ms_cipher_mask) {
        this.ms_cipher_mask = ms_cipher_mask;
    }

    public short getUe_cipher_cap() {
        return ue_cipher_cap;
    }

    public void setUe_cipher_cap(short ue_cipher_cap) {
        this.ue_cipher_cap = ue_cipher_cap;
    }

    public short getUe_integrity_cap() {
        return ue_integrity_cap;
    }

    public void setUe_integrity_cap(short ue_integrity_cap) {
        this.ue_integrity_cap = ue_integrity_cap;
    }
//
//    @Override
//    public String toString() {
//        return "SessionInfo{" +
//                "id=" + id +
//                ", timestamp='" + timestamp + '\'' +
//                ", rat=" + rat +
//                ", domain=" + domain +
//                ", mcc=" + mcc +
//                ", mnc=" + mnc +
//                ", lac=" + lac +
//                ", cid=" + cid +
//                ", arfcn=" + arfcn +
//                ", psc=" + psc +
//                ", cracked=" + cracked +
//                ", neigh_count=" + neigh_count +
//                ", unenc=" + unenc +
//                ", unenc_rand=" + unenc_rand +
//                ", enc=" + enc +
//                ", enc_rand=" + enc_rand +
//                ", enc_null=" + enc_null +
//                ", enc_null_rand=" + enc_null_rand +
//                ", enc_si=" + enc_si +
//                ", enc_si_rand=" + enc_si_rand +
//                ", predict=" + predict +
//                ", avg_power=" + avg_power +
//                ", uplink_avail=" + uplink_avail +
//                ", initial_seq=" + initial_seq +
//                ", cipher_seq=" + cipher_seq +
//                ", auth=" + auth +
//                ", auth_req_fn=" + auth_req_fn +
//                ", auth_resp_fn=" + auth_resp_fn +
//                ", auth_delta=" + auth_delta +
//                ", cipher_missing=" + cipher_missing +
//                ", cipher_comp_first=" + cipher_comp_first +
//                ", cipher_comp_last=" + cipher_comp_last +
//                ", cipher_comp_count=" + cipher_comp_count +
//                ", cipher_delta=" + cipher_delta +
//                ", cipher=" + cipher +
//                ", cmc_imeisv=" + cmc_imeisv +
//                ", integrity=" + integrity +
//                ", first_fn=" + first_fn +
//                ", last_fn=" + last_fn +
//                ", duration=" + duration +
//                ", mobile_orig=" + mobile_orig +
//                ", mobile_term=" + mobile_term +
//                ", paging_mi=" + paging_mi +
//                ", t_unknown=" + t_unknown +
//                ", t_detach=" + t_detach +
//                ", t_locupd=" + t_locupd +
//                ", lu_acc=" + lu_acc +
//                ", lu_type=" + lu_type +
//                ", lu_reject=" + lu_reject +
//                ", lu_rej_cause=" + lu_rej_cause +
//                ", lu_mcc=" + lu_mcc +
//                ", lu_mnc=" + lu_mnc +
//                ", lu_lac=" + lu_lac +
//                ", t_abort=" + t_abort +
//                ", t_raupd=" + t_raupd +
//                ", t_attach=" + t_attach +
//                ", att_acc=" + att_acc +
//                ", t_pdp=" + t_pdp +
//                ", pdp_ip='" + pdp_ip + '\'' +
//                ", t_call=" + t_call +
//                ", t_sms=" + t_sms +
//                ", t_ss=" + t_ss +
//                ", t_tmsi_realloc=" + t_tmsi_realloc +
//                ", t_release=" + t_release +
//                ", rr_cause=" + rr_cause +
//                ", t_gprs=" + t_gprs +
//                ", iden_imsi_ac=" + iden_imsi_ac +
//                ", iden_imsi_bc=" + iden_imsi_bc +
//                ", iden_imei_ac=" + iden_imei_ac +
//                ", iden_imei_bc=" + iden_imei_bc +
//                ", assign=" + assign +
//                ", assign_cmpl=" + assign_cmpl +
//                ", handover=" + handover +
//                ", forced_ho=" + forced_ho +
//                ", a_timeslot=" + a_timeslot +
//                ", a_chan_type=" + a_chan_type +
//                ", a_tsc=" + a_tsc +
//                ", a_hopping=" + a_hopping +
//                ", a_arfcn=" + a_arfcn +
//                ", a_hsn=" + a_hsn +
//                ", a_maio=" + a_maio +
//                ", a_ma_len=" + a_ma_len +
//                ", a_chan_mode=" + a_chan_mode +
//                ", a_multirate=" + a_multirate +
//                ", call_presence=" + call_presence +
//                ", sms_presence=" + sms_presence +
//                ", service_req=" + service_req +
//                ", imsi='" + imsi + '\'' +
//                ", imei='" + imei + '\'' +
//                ", tmsi='" + tmsi + '\'' +
//                ", new_tmsi='" + new_tmsi + '\'' +
//                ", tlli='" + tlli + '\'' +
//                ", msisdn='" + msisdn + '\'' +
//                ", ms_cipher_mask=" + ms_cipher_mask +
//                ", ue_cipher_cap=" + ue_cipher_cap +
//                ", ue_integrity_cap=" + ue_integrity_cap +
//                '}';
//    }
//
//
//    public String toJson() {
//        return "{" +
//                "\"timestamp\"='" + timestamp + '\'' +
//                ", \" rat\"=" + rat +
//                ", \"domain\"=" + domain +
//                ", \"mcc\"=" + mcc +
//                ", \"mnc\"=" + mnc +
//                ", \"lac\"=" + lac +
//                ", \"cid\"=" + cid +
//                ", \"arfcn\"=" + arfcn +
//                ", \"psc\"=" + psc +
//                ", \"cracked\"=" + cracked +
//                ", \"neigh_count\"=" + neigh_count +
//                ", \"unenc\"=" + unenc +
//                ", \"unenc_rand\"=" + unenc_rand +
//                ", \"enc\"=" + enc +
//                ", \"enc_rand\"=" + enc_rand +
//                ", \"enc_null\"=" + enc_null +
//                ", \"enc_null_rand\"=" + enc_null_rand +
//                ", \"enc_si\"=" + enc_si +
//                ", \"enc_si_rand\"=" + enc_si_rand +
//                ", \"predict\"=" + predict +
//                ", \"avg_power\"=" + avg_power +
//                ", \"uplink_avail\"=" + uplink_avail +
//                ", \"initial_seq\"=" + initial_seq +
//                ", \"cipher_seq\"=" + cipher_seq +
//                ", \"auth\"=" + auth +
//                ", \"auth_req_fn\"=" + auth_req_fn +
//                ", \"auth_resp_fn\"=" + auth_resp_fn +
//                ", \"auth_delta\"=" + auth_delta +
//                ", \"cipher_missing\"=" + cipher_missing +
//                ", \"cipher_comp_first\"=" + cipher_comp_first +
//                ", \"cipher_comp_last\"=" + cipher_comp_last +
//                ", \"cipher_comp_count\"=" + cipher_comp_count +
//                ", \"cipher_delta\"=" + cipher_delta +
//                ", \"cipher\"=" + cipher +
//                ", \"cmc_imeisv\"=" + cmc_imeisv +
//                ", \"integrity\"=" + integrity +
//                ", \"first_fn\"=" + first_fn +
//                ", \"last_fn\"=" + last_fn +
//                ", \"duration\"=" + duration +
//                ", \"mobile_orig\"=" + mobile_orig +
//                ", \"mobile_term\"=" + mobile_term +
//                ", \"paging_mi\"=" + paging_mi +
//                ", \"t_unknown\"=" + t_unknown +
//                ", \"t_detach\"=" + t_detach +
//                ", \"t_locupd\"=" + t_locupd +
//                ", \"lu_acc\"=" + lu_acc +
//                ", \"lu_type\"=" + lu_type +
//                ", \"lu_reject\"=" + lu_reject +
//                ", \"lu_rej_cause\"=" + lu_rej_cause +
//                ", \"lu_mcc\"=" + lu_mcc +
//                ", \"lu_mnc\"=" + lu_mnc +
//                ", \"lu_lac\"=" + lu_lac +
//                ", \"t_abort\"=" + t_abort +
//                ", \"t_raupd\"=" + t_raupd +
//                ", \"t_attach\"=" + t_attach +
//                ", \"att_acc\"=" + att_acc +
//                ", \"t_pdp\"=" + t_pdp +
//                ", \"pdp_ip\"='" + pdp_ip + '\'' +
//                ", \"t_call\"=" + t_call +
//                ", \"t_sms\"=" + t_sms +
//                ", \"t_ss\"=" + t_ss +
//                ", \"t_tmsi_realloc\"=" + t_tmsi_realloc +
//                ", \"t_release\"=" + t_release +
//                ", \"rr_cause\"=" + rr_cause +
//                ", \"t_gprs\"=" + t_gprs +
//                ", \"iden_imsi_ac\"=" + iden_imsi_ac +
//                ", \"iden_imsi_bc\"=" + iden_imsi_bc +
//                ", \"iden_imei_ac\"=" + iden_imei_ac +
//                ", \"iden_imei_bc\"=" + iden_imei_bc +
//                ", \"assign\"=" + assign +
//                ", \"assign_cmpl\"=" + assign_cmpl +
//                ", \"handover\"=" + handover +
//                ", \"forced_ho\"=" + forced_ho +
//                ", \"a_timeslot\"=" + a_timeslot +
//                ", \"a_chan_type\"=" + a_chan_type +
//                ", \"a_tsc\"=" + a_tsc +
//                ", \"a_hopping\"=" + a_hopping +
//                ", \"a_arfcn\"=" + a_arfcn +
//                ", \"a_hsn\"=" + a_hsn +
//                ", \"a_maio\"=" + a_maio +
//                ", \"a_ma_len\"=" + a_ma_len +
//                ", \"a_chan_mode\"=" + a_chan_mode +
//                ", \"a_multirate\"=" + a_multirate +
//                ", \"call_presence\"=" + call_presence +
//                ", \"sms_presence\"=" + sms_presence +
//                ", \"service_req\"=" + service_req +
//                ", \"imsi\"='" + imsi + '\'' +
//                ", \"imei\"='" + imei + '\'' +
//                ", \"tmsi\"='" + tmsi + '\'' +
//                ", \"new_tmsi\"='" + new_tmsi + '\'' +
//                ", \"tlli\"='" + tlli + '\'' +
//                ", \"msisdn\"='" + msisdn + '\'' +
//                ", \"ms_cipher_mask\"=" + ms_cipher_mask +
//                ", \"ue_cipher_cap\"=" + ue_cipher_cap +
//                ", \"ue_integrity_cap\"=" + ue_integrity_cap +
//                '}';
//    }
}