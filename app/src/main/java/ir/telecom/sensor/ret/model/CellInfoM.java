package ir.telecom.sensor.ret.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CellInfoM {

    @SerializedName("lastSeen")
    @Expose
    private String lastSeen;
    @SerializedName("neigh_2")
    @Expose
    private short neigh2;
    @SerializedName("mcc")
    @Expose
    private short mcc;
    @SerializedName("si6")
    @Expose
    private String si6;
    @SerializedName("ba_len")
    @Expose
    private short baLen;
    @SerializedName("neigh_5")
    @Expose
    private short neigh5;
    @SerializedName("si5t")
    @Expose
    private String si5t;
    @SerializedName("gprs")
    @Expose
    private short gprs;
    @SerializedName("agch_blocks")
    @Expose
    private short agchBlocks;
    @SerializedName("count_si2t")
    @Expose
    private Integer countSi2t;
    @SerializedName("t3212")
    @Expose
    private short t3212;
    @SerializedName("si5b")
    @Expose
    private String si5b;
    @SerializedName("pwr_offset")
    @Expose
    private short pwrOffset;
    @SerializedName("count_si2q")
    @Expose
    private Integer countSi2q;
    @SerializedName("count_si1")
    @Expose
    private Integer countSi1;
    @SerializedName("pen_time")
    @Expose
    private short penTime;
    @SerializedName("count_si5")
    @Expose
    private Integer countSi5;
    @SerializedName("dtx")
    @Expose
    private short dtx;
    @SerializedName("count_si4")
    @Expose
    private Integer countSi4;
    @SerializedName("count_si3")
    @Expose
    private Integer countSi3;
    @SerializedName("count_si2")
    @Expose
    private Integer countSi2;
    @SerializedName("cid")
    @Expose
    private Integer cid;
    @SerializedName("si1")
    @Expose
    private String si1;
    @SerializedName("si4")
    @Expose
    private String si4;
    @SerializedName("si5")
    @Expose
    private String si5;
    @SerializedName("si2")
    @Expose
    private String si2;
    @SerializedName("si3")
    @Expose
    private String si3;
    @SerializedName("count_si13")
    @Expose
    private Integer countSi13;
    @SerializedName("neigh_2t")
    @Expose
    private short neigh2t;
    @SerializedName("power_count")
    @Expose
    private Integer powerCount;
    @SerializedName("count_si6")
    @Expose
    private Integer countSi6;
    @SerializedName("neigh_2q")
    @Expose
    private short neigh2q;
    @SerializedName("gps_lat")
    @Expose
    private float gpsLat;
    @SerializedName("count_si5b")
    @Expose
    private Integer countSi5b;
    @SerializedName("neigh_2b")
    @Expose
    private short neigh2b;
    @SerializedName("rat")
    @Expose
    private short rat;
    @SerializedName("lac")
    @Expose
    private Integer lac;
    @SerializedName("neigh_5t")
    @Expose
    private short neigh5t;
    @SerializedName("gps_lon")
    @Expose
    private float gpsLon;
    @SerializedName("si13")
    @Expose
    private String si13;
    @SerializedName("msc_ver")
    @Expose
    private short mscVer;
    @SerializedName("temp_offset")
    @Expose
    private short tempOffset;
    @SerializedName("mnc")
    @Expose
    private short mnc;
    @SerializedName("firstSeen")
    @Expose
    private String firstSeen;
    @SerializedName("si2q")
    @Expose
    private String si2q;
    @SerializedName("count_si2b")
    @Expose
    private Integer countSi2b;
    @SerializedName("neigh_5b")
    @Expose
    private short neigh5b;
    @SerializedName("cro")
    @Expose
    private short cro;
    @SerializedName("power_sum")
    @Expose
    private Integer powerSum;
    @SerializedName("bcch_arfcn")
    @Expose
    private Integer bcchArfcn;
    @SerializedName("si2t")
    @Expose
    private String si2t;
    @SerializedName("combined")
    @Expose
    private short combined;
    @SerializedName("si2b")
    @Expose
    private String si2b;
    @SerializedName("c1")
    @Expose
    private short c1;
    @SerializedName("c2")
    @Expose
    private short c2;
    @SerializedName("count_si5t")
    @Expose
    private Integer countSi5t;
    @SerializedName("pag_mframes")
    @Expose
    private short pagMframes;

    public CellInfoM(String first_seen, String last_seen, short mcc,
                     short mnc, int lac, int cid, short rat,
                     int bcch_arfcn, short c1, short c2,
                     int power_sum, int power_count, float gps_lon,
                     float gps_lat, short msc_ver, short combined, short agch_blocks,
                     short pag_mframes, short t3212, short dtx,
                     short cro, short temp_offset, short pen_time,
                     short pwr_offset, short gprs, short ba_len,
                     short neigh_2, short neigh_2b, short neigh_2t,
                     short neigh_2q, short neigh_5, short neigh_5b,
                     short neigh_5t, int count_si1, int count_si2,
                     int count_si2b, int count_si2t, int count_si2q,
                     int count_si3, int count_si4, int count_si5,
                     int count_si5b, int count_si5t, int count_si6,
                     int count_si13, String si1, String si2,
                     String si2b, String si2t, String si2q,
                     String si3, String si4, String si5,
                     String si5b, String si5t, String si6,
                     String si13) {
        this.firstSeen = first_seen;
        this.lastSeen = last_seen;
        this.mcc = mcc;
        this.mnc = mnc;
        this.lac = lac;
        this.cid = cid;
        this.rat = rat;
        this.bcchArfcn = bcch_arfcn;
        this.c1 = c1;
        this.c2 = c2;
        this.powerSum = power_sum;
        this.powerCount = power_count;
        this.gpsLon = gps_lon;
        this.gpsLat = gps_lat;
        this.mscVer = msc_ver;
        this.combined = combined;
        this.agchBlocks = agch_blocks;
        this.pagMframes = pag_mframes;
        this.t3212 = t3212;
        this.dtx = dtx;
        this.cro = cro;
        this.tempOffset = temp_offset;
        this.penTime = pen_time;
        this.pwrOffset = pwr_offset;
        this.gprs = gprs;
        this.baLen = ba_len;
        this.neigh2 = neigh_2;
        this.neigh2b = neigh_2b;
        this.neigh2t = neigh_2t;
        this.neigh2q = neigh_2q;
        this.neigh5 = neigh_5;
        this.neigh5b = neigh_5b;
        this.neigh5t = neigh_5t;
        this.countSi1 = count_si1;
        this.countSi2 = count_si2;
        this.countSi2b = count_si2b;
        this.countSi2t = count_si2t;
        this.countSi2q = count_si2q;
        this.countSi3 = count_si3;
        this.countSi4 = count_si4;
        this.countSi5 = count_si5;
        this.countSi5b = count_si5b;
        this.countSi5t = count_si5t;
        this.countSi6 = count_si6;
        this.countSi13 = count_si13;
        this.si1 = si1;
        this.si2 = si2;
        this.si2b = si2b;
        this.si2t = si2t;
        this.si2q = si2q;
        this.si3 = si3;
        this.si4 = si4;
        this.si5 = si5;
        this.si5b = si5b;
        this.si5t = si5t;
        this.si6 = si6;
        this.si13 = si13;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public short getNeigh2() {
        return neigh2;
    }

    public void setNeigh2(short neigh2) {
        this.neigh2 = neigh2;
    }

    public short getMcc() {
        return mcc;
    }

    public void setMcc(short mcc) {
        this.mcc = mcc;
    }

    public String getSi6() {
        return si6;
    }

    public void setSi6(String si6) {
        this.si6 = si6;
    }

    public short getBaLen() {
        return baLen;
    }

    public void setBaLen(short baLen) {
        this.baLen = baLen;
    }

    public short getNeigh5() {
        return neigh5;
    }

    public void setNeigh5(short neigh5) {
        this.neigh5 = neigh5;
    }

    public String getSi5t() {
        return si5t;
    }

    public void setSi5t(String si5t) {
        this.si5t = si5t;
    }

    public short getGprs() {
        return gprs;
    }

    public void setGprs(short gprs) {
        this.gprs = gprs;
    }

    public short getAgchBlocks() {
        return agchBlocks;
    }

    public void setAgchBlocks(short agchBlocks) {
        this.agchBlocks = agchBlocks;
    }

    public Integer getCountSi2t() {
        return countSi2t;
    }

    public void setCountSi2t(Integer countSi2t) {
        this.countSi2t = countSi2t;
    }

    public short getT3212() {
        return t3212;
    }

    public void setT3212(short t3212) {
        this.t3212 = t3212;
    }

    public String getSi5b() {
        return si5b;
    }

    public void setSi5b(String si5b) {
        this.si5b = si5b;
    }

    public short getPwrOffset() {
        return pwrOffset;
    }

    public void setPwrOffset(short pwrOffset) {
        this.pwrOffset = pwrOffset;
    }

    public Integer getCountSi2q() {
        return countSi2q;
    }

    public void setCountSi2q(Integer countSi2q) {
        this.countSi2q = countSi2q;
    }

    public Integer getCountSi1() {
        return countSi1;
    }

    public void setCountSi1(Integer countSi1) {
        this.countSi1 = countSi1;
    }

    public short getPenTime() {
        return penTime;
    }

    public void setPenTime(short penTime) {
        this.penTime = penTime;
    }

    public Integer getCountSi5() {
        return countSi5;
    }

    public void setCountSi5(Integer countSi5) {
        this.countSi5 = countSi5;
    }

    public short getDtx() {
        return dtx;
    }

    public void setDtx(short dtx) {
        this.dtx = dtx;
    }

    public Integer getCountSi4() {
        return countSi4;
    }

    public void setCountSi4(Integer countSi4) {
        this.countSi4 = countSi4;
    }

    public Integer getCountSi3() {
        return countSi3;
    }

    public void setCountSi3(Integer countSi3) {
        this.countSi3 = countSi3;
    }

    public Integer getCountSi2() {
        return countSi2;
    }

    public void setCountSi2(Integer countSi2) {
        this.countSi2 = countSi2;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getSi1() {
        return si1;
    }

    public void setSi1(String si1) {
        this.si1 = si1;
    }

    public String getSi4() {
        return si4;
    }

    public void setSi4(String si4) {
        this.si4 = si4;
    }

    public String getSi5() {
        return si5;
    }

    public void setSi5(String si5) {
        this.si5 = si5;
    }

    public String getSi2() {
        return si2;
    }

    public void setSi2(String si2) {
        this.si2 = si2;
    }

    public String getSi3() {
        return si3;
    }

    public void setSi3(String si3) {
        this.si3 = si3;
    }

    public Integer getCountSi13() {
        return countSi13;
    }

    public void setCountSi13(Integer countSi13) {
        this.countSi13 = countSi13;
    }

    public short getNeigh2t() {
        return neigh2t;
    }

    public void setNeigh2t(short neigh2t) {
        this.neigh2t = neigh2t;
    }

    public Integer getPowerCount() {
        return powerCount;
    }

    public void setPowerCount(Integer powerCount) {
        this.powerCount = powerCount;
    }

    public Integer getCountSi6() {
        return countSi6;
    }

    public void setCountSi6(Integer countSi6) {
        this.countSi6 = countSi6;
    }

    public short getNeigh2q() {
        return neigh2q;
    }

    public void setNeigh2q(short neigh2q) {
        this.neigh2q = neigh2q;
    }

    public float getGpsLat() {
        return gpsLat;
    }

    public void setGpsLat(float gpsLat) {
        this.gpsLat = gpsLat;
    }

    public Integer getCountSi5b() {
        return countSi5b;
    }

    public void setCountSi5b(Integer countSi5b) {
        this.countSi5b = countSi5b;
    }

    public short getNeigh2b() {
        return neigh2b;
    }

    public void setNeigh2b(short neigh2b) {
        this.neigh2b = neigh2b;
    }

    public short getRat() {
        return rat;
    }

    public void setRat(short rat) {
        this.rat = rat;
    }

    public Integer getLac() {
        return lac;
    }

    public void setLac(Integer lac) {
        this.lac = lac;
    }

    public short getNeigh5t() {
        return neigh5t;
    }

    public void setNeigh5t(short neigh5t) {
        this.neigh5t = neigh5t;
    }

    public float getGpsLon() {
        return gpsLon;
    }

    public void setGpsLon(float gpsLon) {
        this.gpsLon = gpsLon;
    }

    public String getSi13() {
        return si13;
    }

    public void setSi13(String si13) {
        this.si13 = si13;
    }

    public short getMscVer() {
        return mscVer;
    }

    public void setMscVer(short mscVer) {
        this.mscVer = mscVer;
    }

    public short getTempOffset() {
        return tempOffset;
    }

    public void setTempOffset(short tempOffset) {
        this.tempOffset = tempOffset;
    }

    public short getMnc() {
        return mnc;
    }

    public void setMnc(short mnc) {
        this.mnc = mnc;
    }

    public String getFirstSeen() {
        return firstSeen;
    }

    public void setFirstSeen(String firstSeen) {
        this.firstSeen = firstSeen;
    }

    public String getSi2q() {
        return si2q;
    }

    public void setSi2q(String si2q) {
        this.si2q = si2q;
    }

    public Integer getCountSi2b() {
        return countSi2b;
    }

    public void setCountSi2b(Integer countSi2b) {
        this.countSi2b = countSi2b;
    }

    public short getNeigh5b() {
        return neigh5b;
    }

    public void setNeigh5b(short neigh5b) {
        this.neigh5b = neigh5b;
    }

    public short getCro() {
        return cro;
    }

    public void setCro(short cro) {
        this.cro = cro;
    }

    public Integer getPowerSum() {
        return powerSum;
    }

    public void setPowerSum(Integer powerSum) {
        this.powerSum = powerSum;
    }

    public Integer getBcchArfcn() {
        return bcchArfcn;
    }

    public void setBcchArfcn(Integer bcchArfcn) {
        this.bcchArfcn = bcchArfcn;
    }

    public String getSi2t() {
        return si2t;
    }

    public void setSi2t(String si2t) {
        this.si2t = si2t;
    }

    public short getCombined() {
        return combined;
    }

    public void setCombined(short combined) {
        this.combined = combined;
    }

    public String getSi2b() {
        return si2b;
    }

    public void setSi2b(String si2b) {
        this.si2b = si2b;
    }

    public short getC1() {
        return c1;
    }

    public void setC1(short c1) {
        this.c1 = c1;
    }

    public short getC2() {
        return c2;
    }

    public void setC2(short c2) {
        this.c2 = c2;
    }

    public Integer getCountSi5t() {
        return countSi5t;
    }

    public void setCountSi5t(Integer countSi5t) {
        this.countSi5t = countSi5t;
    }

    public short getPagMframes() {
        return pagMframes;
    }

    public void setPagMframes(short pagMframes) {
        this.pagMframes = pagMframes;
    }
    public String toJson() {
        return "{" +
                "\"firstSeen\"='" + firstSeen + '\'' +
                ", \"lastSeen\"='" + lastSeen + '\'' +
                ", \"mcc\"=" + mcc +
                ", \"mnc\"=" + mnc +
                ", \"mnc\"=" + mnc +
                ", \"lac\"=" + lac +
                ", \"cid\"=" + cid +
                ", \"rat\"=" + rat +
                ", \"bcch_arfcn\"=" + bcchArfcn +
                ", \"c1\"=" + c1 +
                ", \"c2\"=" + c2 +
                ", \"power_sum\"=" + powerSum +
                ", \"power_count\"=" + powerCount +
                ", \"gps_lon\"=" + gpsLon +
                ", \"gps_lat\"=" + gpsLat +
                ", \"msc_ver\"=" + mscVer +
                ", \"combined\"=" + combined +
                ", \"agch_blocks\"=" + agchBlocks +
                ", \"pag_mframes\"=" + pagMframes +
                ", \"t3212\"=" + t3212 +
                ", \"dtx\"=" + dtx +
                ", \"cro\"=" + cro +
                ", \"temp_offset\"=" + tempOffset +
                ", \"pen_time\"=" + penTime +
                ", \"pwr_offset\"=" + pwrOffset +
                ", \"gprs\"=" + gprs +
                ", \"ba_len\"=" + baLen +
                ", \"neigh_2\"=" + neigh2 +
                ", \"neigh_2b\"=" + neigh2b +
                ", \"neigh_2t\"=" + neigh2t +
                ", \"neigh_2q\"=" + neigh2q +
                ", \"neigh_5\"=" + neigh5 +
                ", \"neigh_5b\"=" + neigh5b +
                ", \"neigh_5t\"=" + neigh5t +
                ", \"count_si1\"=" + countSi1 +
                ", \"count_si2\"=" + countSi2 +
                ", \"count_si2b\"=" + countSi2b +
                ", \"count_si2t\"=" + countSi2t +
                ", \"count_si2q\"=" + countSi2q +
                ", \"count_si3\"=" + countSi3 +
                ", \"count_si4\"=" + countSi4 +
                ", \"count_si5\"=" + countSi5 +
                ", \"count_si5b\"=" + countSi5b +
                ", \"count_si5t\"=" + countSi5t +
                ", \"count_si6\"=" + countSi6 +
                ", \"count_si13\"=" + countSi13 +
                ", \"si1\"=" + si1 +
                ", \"si2\"=" + si2 +
                ", \"si2b\"=" + si2b +
                ", \"si2t\"=" + si2t +
                ", \"si2q\"=" + si2q +
                ", \"si3\"=" + si3 +
                ", \"si4\"=" + si4 +
                ", \"si5\"=" + si5 +
                ", \"si5b\"=" + si5b +
                ", \"si5t\"=" + si5t +
                ", \"si6\"=" + si6 +
                ", \"si13\"=" + si13 +
                '}';
    }

}
