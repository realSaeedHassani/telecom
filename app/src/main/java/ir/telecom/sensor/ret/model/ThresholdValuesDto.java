package ir.telecom.sensor.ret.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThresholdValuesDto {

    @Expose
    @SerializedName("t3212")
    private short T3212;

    @Expose
    @SerializedName("min_Cell_Reselection_Offset")
    private short Min_Cell_Reselection_Offset;

    @Expose
    @SerializedName("max_Cell_Reselection_Offset")
    private short Max_Cell_Reselection_Offset;

    @Expose
    @SerializedName("delay_Between_Command_And_Complete")
    private int Delay_Between_Command_And_Complete;

    @Expose
    @SerializedName("paging_Group_Number")
    private int Paging_Group_Number;

    @Expose
    @SerializedName("traffic_Channel_Idle_Milliseconds")
    private int Traffic_Channel_Idle_Milliseconds;

    public short getT3212() {
        return T3212;
    }

    public void setT3212(short t3212) {
        T3212 = t3212;
    }

    public short getMin_Cell_Reselection_Offset() {
        return Min_Cell_Reselection_Offset;
    }

    public void setMin_Cell_Reselection_Offset(short min_Cell_Reselection_Offset) {
        Min_Cell_Reselection_Offset = min_Cell_Reselection_Offset;
    }

    public short getMax_Cell_Reselection_Offset() {
        return Max_Cell_Reselection_Offset;
    }

    public void setMax_Cell_Reselection_Offset(short max_Cell_Reselection_Offset) {
        Max_Cell_Reselection_Offset = max_Cell_Reselection_Offset;
    }

    public int getDelay_Between_Command_And_Complete() {
        return Delay_Between_Command_And_Complete;
    }

    public void setDelay_Between_Command_And_Complete(int delay_Between_Command_And_Complete) {
        Delay_Between_Command_And_Complete = delay_Between_Command_And_Complete;
    }

    public int getPaging_Group_Number() {
        return Paging_Group_Number;
    }

    public void setPaging_Group_Number(int paging_Group_Number) {
        Paging_Group_Number = paging_Group_Number;
    }

    public int getTraffic_Channel_Idle_Milliseconds() {
        return Traffic_Channel_Idle_Milliseconds;
    }

    public void setTraffic_Channel_Idle_Milliseconds(int traffic_Channel_Idle_Milliseconds) {
        Traffic_Channel_Idle_Milliseconds = traffic_Channel_Idle_Milliseconds;
    }
}