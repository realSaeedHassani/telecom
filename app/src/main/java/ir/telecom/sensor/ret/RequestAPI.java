package ir.telecom.sensor.ret;
import androidx.annotation.NonNull;

import java.util.ArrayList;

import ir.telecom.sensor.model.Sensor;
import ir.telecom.sensor.ret.model.CatcherM;
import ir.telecom.sensor.ret.model.CellInfoM;
import ir.telecom.sensor.ret.model.SensorM;
import ir.telecom.sensor.ret.model.SessionInfoM;
import ir.telecom.sensor.ret.model.ThresholdValuesDto;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestAPI {
    @Headers({"Content-type: application/json"})
    @POST("{token}/cell-info")
    Call<ResponseBody> sendCellinfoTable(@NonNull @Path("token") String token, @Query ("list") int value, @Body ArrayList<CellInfoM> list);

    @Headers({"Content-type: application/json"})
    @POST("{token}/sensor-status")
    Call<ResponseBody> sendSensorStatus(@NonNull @Path("token") String token, @Query ("list") int value, @Body ArrayList<SensorM> list);

    @Headers({"Content-type: application/json"})
    @POST("{token}/catchers")
    Call<ResponseBody> sendCatcher(@NonNull @Path("token") String token, @Query ("list") int value, @Body ArrayList<CatcherM> list);

    @Headers({"Content-type: application/json"})
    @POST("{token}/session-info")
    Call<ResponseBody> sendSessionInfo(@NonNull @Path("token") String token, @Query ("list") int value, @Body ArrayList<SessionInfoM> list);

//    @Headers({"Content-type: application/json"})
//    @POST("{token}/config/threshold-values")
//    Call<ResponseBody> sendConfig(@NonNull @Path("token") String token, @Query ("list") int value, @Body ArrayList<ThresholdValuesDto> list);

//    void getConfig(@NonNull @Path("token") String token,Callback<Response> cb);

    @GET("{token}/config/threshold-values")
    Call < ThresholdValuesDto > getConfig(@NonNull @Path("token") String token);
}
