package ir.telecom.sensor.ret;

/**
 * @author Saeed
 * @date 13-12-1397
 */

import android.text.TextUtils;
import android.util.Log;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static String BASE_URL = "";

    public static <S> S createService(Class<S> serviceClass, String protocol, String serverInfo) throws KeyManagementException, NoSuchAlgorithmException {

        BASE_URL = protocol + "://"+serverInfo+"/sensors/";
        if (protocol.equals("https"))
            return createServiceHttps(serviceClass, null, null);
        else
            return createServiceHttp(serviceClass, null, null);
    }

    private static <S> S createServiceHttp(Class<S> serviceClass, Interceptor interceptor, Converter.Factory[] factories) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder = new Retrofit.Builder();

        boolean containsDefaultConverter = false;
        if (factories != null && factories.length > 0) {
            for (Converter.Factory factory : factories) {
                builder.addConverterFactory(factory);
                if (factory instanceof GsonConverterFactory)
                    containsDefaultConverter = true;
            }
        }

        builder.baseUrl(BASE_URL);
        if (!containsDefaultConverter)
            builder.addConverterFactory(GsonConverterFactory.create());

        if (interceptor != null && !httpClient.interceptors().contains(interceptor))
            httpClient.addInterceptor(interceptor);

        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }


    public static <S> S createServiceHttps(Class<S> serviceClass, Interceptor interceptor, Converter.Factory[] factories) throws NoSuchAlgorithmException, KeyManagementException {

        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };

        // Install the all-trusting trust manager
        final SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


        // In retrofit 1.9 Retrofit class is RestAdapter, baseUrl(String) is setEndpoint(String); addInterceptor(Interceptor) is setRequestInterceptor(RequestInterceptor)
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
        httpClient.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });


        Retrofit.Builder builder = new Retrofit.Builder();

        //check if contains default converter(JSON)
        boolean containsDefaultConverter = false;
        if (factories != null && factories.length > 0) {
            for (Converter.Factory factory : factories) {
                //The factory who takes care for serialization and deserialization of objects
                builder.addConverterFactory(factory);
                if (factory instanceof GsonConverterFactory)
                    containsDefaultConverter = true;
            }
        }

        //if it does not contain default converter, then add it
        if (!containsDefaultConverter)
            builder.addConverterFactory(GsonConverterFactory.create());

        builder.baseUrl(BASE_URL);

        if (interceptor != null && !httpClient.interceptors().contains(interceptor))
            httpClient.addInterceptor(interceptor);

        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }
}