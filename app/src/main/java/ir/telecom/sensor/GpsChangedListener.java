package ir.telecom.sensor;

public interface GpsChangedListener {
    void setOnLocationChanged(double latitude, double longitude);
}
