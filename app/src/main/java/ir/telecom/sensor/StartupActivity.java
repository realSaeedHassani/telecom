package ir.telecom.sensor;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.geniusforapp.fancydialog.FancyAlertDialog;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

import ir.telecom.sensor.error.handling.ErrorControlObject;
import ir.telecom.sensor.error.handling.UncaughtException;
import ir.telecom.sensor.model.Sensor;
import ir.telecom.sensor.qdmon.MsdSQLiteOpenHelper;
import ir.telecom.sensor.toasty.Toasty;
import ir.telecom.sensor.util.DeviceCompatibilityChecker;
import ir.telecom.sensor.util.MsdConfig;
import ir.telecom.sensor.util.MsdDatabaseManager;
import ir.telecom.sensor.util.MsdDialog;
import ir.telecom.sensor.util.PermissionChecker;

/**
 * This class is launched when starting the App. It will display a dialog if the
 * device is not compatible or if it is the first run abstractof the App (so that the
 * user has to confirm to continue). If the device is compatible and the user
 * has already confirmed the first run dialog, it will directly switch over to
 * DashboardActivity.
 */
public class StartupActivity extends Activity {
    private static final String TAG = "StartupActivity";
    private MsdSQLiteOpenHelper helper;
    private boolean alreadyClicked = false;
    private ProgressDialog progressDialog;
    private static boolean isSNSNCompatible = false;
    private static boolean isAppInitialized = false;
    public static String snsnIncompatibilityReason;
    private final SensorUpdateAlarmManager alarmManager = SensorUpdateAlarmManager.getInstance();
    private final SendToServerAlarmManager alarmManager2 = SendToServerAlarmManager.getInstance();
    private ErrorControlObject errorControlObject = new ErrorControlObject();

    private final SensorInfoAlarmManager infoalarmManager = SensorInfoAlarmManager.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deleteCache(this);

        /*
          ....................
          >> Error Handling. |
          ....................
         */
//        errorControlObject.setmCrashText(this.getLocalClassName());
//        errorControlObject.setmDestinationActivity(StartupActivity.class);
//        Thread.setDefaultUncaughtExceptionHandler(new UncaughtException(this, errorControlObject));

        snsnIncompatibilityReason = DeviceCompatibilityChecker.checkDeviceCompatibility(this.getApplicationContext());
        Log.e(">> Check Root: ", DeviceCompatibilityChecker.checkDeviceCompatibility(this.getApplicationContext()) + "  ");
//        Log.i("incom3",snsnIncompatibilityReason);

        if (snsnIncompatibilityReason == null) {
            isSNSNCompatible = true;
            Log.e("setStartBoot", "No");
        } else {
            //disable starting MsdService by BootCompletedIntentReceiver on next boot
            Log.e("setStartBoot", "Yes");
            MsdConfig.setStartOnBoot(this, false);
        }

//        FancyAlertDialog.Builder alert = new FancyAlertDialog.Builder(StartupActivity.this)
//                .setTextSubTitle(" (Android "+ Build.VERSION.RELEASE+")")
//                .setBody("Hardware: "+ "\n" +
//                        "Root    : " + root)
//                .setPositiveButtonText("Ok")
//                .setPositiveColor(R.color.dialog_okay)
//                .setOnPositiveClicked(new FancyAlertDialog.OnPositiveClicked() {
//                    @Override
//                    public void OnClick(View view, Dialog dialog) {
////                                        System.exit(0);
//                    }
//                })
//                .setBodyGravity(FancyAlertDialog.TextGravity.LEFT)
//                .setTitleGravity(FancyAlertDialog.TextGravity.CENTER)
//                .setSubtitleGravity(FancyAlertDialog.TextGravity.CENTER)
//                .setCancelable(false)
//                .build();
//        alert.show();
        isAppInitialized = true;
        createDatabaseAndStartDashboard();
    }


    public int checkrootcommand() {

        Process exec = null;
        try {

            exec = Runtime.getRuntime().exec(new String[]{"su"});

            final OutputStreamWriter out = new OutputStreamWriter(exec.getOutputStream());
            out.write("exit");
            out.flush();

            Log.e(">> ROOT: ", "su command executed successfully");
            return 0; // returns zero when the command is executed successfully
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (exec != null) {
                try {
                    exec.destroy();
                } catch (Exception ignored) {
                }
            }
        }
        Log.e(">> ROOT: ", "su command failed");
        return 1; //returns one when the command execution fails
    }

    public static boolean isAppInitialized() {
        return isAppInitialized;
    }

    // BEWARE: The return value from this is only sane if isAppInitialized() returns true
    public static boolean isSNSNCompatible() {
        return isSNSNCompatible;
    }

    protected void quitApplication() {
        finish();
//        System.exit(0);
    }

    private void createDatabaseAndStartDashboard() {
        progressDialog = ProgressDialog.show(this, "Initializing database", "Please wait...", true);
        progressDialog.show();
        final Handler handler = new Handler();
        Thread t = new Thread() {
            @Override
            public void run() {

                try {
                    MsdDatabaseManager.initializeInstance(new MsdSQLiteOpenHelper(StartupActivity.this));
                    MsdDatabaseManager msdDatabaseManager = MsdDatabaseManager.getInstance();
                    SQLiteDatabase db = msdDatabaseManager.openDatabase();
                    //testing DB init
                    if (db == null)
                        throw new SQLException("Failed to create instance of database object");
                    db.rawQuery("SELECT * FROM config", null).close();

                    msdDatabaseManager.closeDatabase();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            if (isSNSNCompatible) {
                                //Check for ACCESS_COARSE_PERMISSION neccessary for Recoding in MsdService to function
                                if (PermissionChecker.checkAndRequestPermissionForMsdService(StartupActivity.this)) {
                                    Log.e(">> ACCESS: ", "YES");
                                    startDashboard();
                                }
                            } else {
                                Log.e(">> ACCESS: ", "NO");
                                startDashboard();
                            }

                        }
                    });
                } catch (SQLException e) {
                    // Testing if the DB creation worked successfully failed
//                    Log.e(TAG, "DB creation failed, maybe App assets are corrupted: " + e.getMessage());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showDBCreationFailedDialog();
                        }
                    });

                }
            }
        };
        t.start();
    }

    private void startDashboard() {

        alarmManager.setReminder(StartupActivity.this);
        alarmManager2.setReminder(StartupActivity.this);
//        infoalarmManager.setReminder(StartupActivity.this);
        Intent killService = new Intent(this, KillService.class);
        startService(killService);
        Intent i = new Intent(StartupActivity.this, DashboardActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        StartupActivity.this.startActivity(i);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PermissionChecker.REQUEST_MSDSERVICE_PERMISSIONS) {
            if (grantResults.length > 0) {
                //find all neccessary permissions not granted
                List<String> notGrantedPermissions = new LinkedList<>();
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        notGrantedPermissions.add(permissions[i]);
                    }
                }

                if (notGrantedPermissions.isEmpty()) {
                    //Success: All neccessary permissions granted
                    startDashboard();
                } else {

                    //ask again for all not granted permissions
                    boolean showDialog = false;
                    for (String notGrantedPermission : notGrantedPermissions) {
                        showDialog = showDialog || ActivityCompat.shouldShowRequestPermissionRationale(this, notGrantedPermission);
                    }

                    if (showDialog) {
                        showDialogAskingForAllPermissions(getResources().getString(R.string.alert_msdservice_permissions_not_granted));
                    } else {
                        // IF permission is denied (and "never ask again" is checked)
                        // Log.e(TAG, mTAG + ": Permission FAILURE: some permissions are not granted. Asking again.");
                        showDialogPersistentDeniedPermissions(getResources().getString(R.string.alert_msdservice_permissions_not_granted_persistent));

                    }

                }

            }
        }
    }

    private void showDialogAskingForAllPermissions(String message) {
        MsdDialog.makeConfirmationDialog(this, message, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PermissionChecker.checkAndRequestPermissionForMsdService(StartupActivity.this);
            }
        }, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDialogExplainNoRecording(getResources().getString(R.string.alert_msdservice_recording_not_possible));
            }
        }, false).show();
    }

    private void showDialogPersistentDeniedPermissions(String message) {

        MsdDialog.makeConfirmationDialog(this, message, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startDashboard();
            }
        }, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startDashboard();
            }
        }, false).show();
    }

    private void showDialogExplainNoRecording(String message) {

        MsdDialog.makeConfirmationDialog(this, message, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDashboard();
                    }
                },
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startDashboard();
                    }
                }, false).show();
    }

    private void showDBCreationFailedDialog() {
        MsdDialog.makeFatalConditionDialog(this, getResources().getString(R.string.alert_db_creation_failed_detail), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                quitApplication();
            }
        }, null, false).show();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
