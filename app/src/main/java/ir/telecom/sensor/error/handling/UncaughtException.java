package ir.telecom.sensor.error.handling;

import android.app.Activity;
import android.content.Intent;
import android.os.Process;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class UncaughtException implements Thread.UncaughtExceptionHandler {
    private final Activity mContext;
    public ErrorControlObject to;
    public UncaughtException(Activity context, ErrorControlObject transferObject) {
        mContext = context;
        to = transferObject;
    }
    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        throwable.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        Log.d("stacktrace", stacktrace);
        Intent intent = new Intent(mContext, CollapseActivity.class);
        intent.putExtra("stacktrace",stacktrace);
        intent.putExtra("errorControlObject",to);
        mContext.startActivity(intent);
        Process.killProcess(Process.myPid());
//        System.exit(0);
    }
}
