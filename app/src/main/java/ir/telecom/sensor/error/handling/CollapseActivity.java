package ir.telecom.sensor.error.handling;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ir.telecom.sensor.R;
import mehdi.sakout.fancybuttons.FancyButton;

public class CollapseActivity extends AppCompatActivity {
    private FancyButton btnDetaylarC;
    private FancyButton btnDonC;
    private TextView textViewC;
    ErrorControlObject errorControlObject;

    private void init() {
        textViewC = (TextView) findViewById(R.id.textViewC);
        btnDetaylarC = (FancyButton) findViewById(R.id.btnDetaylarC);
        btnDonC = (FancyButton) findViewById(R.id.btnDonC);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collapse);
        init();


//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", new Locale("fa_IR"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        Date now = new Date();
        final String fileName = "Sensor_Report_" + formatter.format(now)+".txt";

        Intent i = getIntent();
        errorControlObject = (ErrorControlObject) i.getSerializableExtra("errorControlObject");
        textViewC.setText(errorControlObject.getmCrashText());
        textViewC.setTextColor(Color.parseColor(errorControlObject.getmCrashTextColor()));
        btnDetaylarC.setText(errorControlObject.getmDetailsButtonText());
        btnDonC.setText(errorControlObject.getmRestartAppButtonText());
        final Bundle extraExp = getIntent().getExtras();
        final String hata = extraExp.getString("stacktrace");
        writeFileOnInternalStorage(this, fileName, hata);

        final MaterialStyledDialog dialog = new MaterialStyledDialog.Builder(this)
                .setTitle(errorControlObject.getmCrashText())
                .setDescription(hata)
                .setScrollable(true)
                .withDialogAnimation(true)
                .setStyle(Style.HEADER_WITH_TITLE)
                .setPositiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }})
                .setCancelable(false)
                .build();

        btnDetaylarC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (extraExp != null) {
                    dialog.show();
                }
            }
        });

        btnDonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mStartActivity = new Intent(getApplicationContext(), errorControlObject.getmDestinationActivity());
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//                System.exit(0);

                Intent intent = new Intent(getApplicationContext(), errorControlObject.getmDestinationActivity());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent mStartActivity = new Intent(getApplicationContext(), errorControlObject.getmDestinationActivity());
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
//        System.exit(0);
    }

    public void writeFileOnInternalStorage(Context context, String sFileName, String sBody) {
        String mFolder = Environment.getExternalStorageDirectory() + "/" + "sensor/Reports";
        File file = new File(mFolder);
        if (!file.exists()){
            file.mkdir();
        }
        try {
            FileWriter writer = new FileWriter(new File(file, sFileName));
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved error in: " + mFolder, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
