package ir.telecom.sensor.error.handling;
import java.io.Serializable;

public class ErrorControlObject implements Serializable {

    private Class mDestinationActivity;
    private String mCrashText;
    private String mCrashTextColor;
    private String mDetailsButtonText;
    private String mRestartAppButtonText;

    public ErrorControlObject() {
        mCrashText = "Oops! Crash...";
        mCrashTextColor = "#000000";
        mDetailsButtonText = "Show Details";
        mRestartAppButtonText = "Continue...";
    }

    public Class getmDestinationActivity() {
        return mDestinationActivity;
    }

    public void setmDestinationActivity(Class mDestinationActivity) {
        this.mDestinationActivity = mDestinationActivity;
    }

    public String getmCrashTextColor() {
        return mCrashTextColor;
    }

    public void setmCrashTextColor(String mCrashTextColor) {
        this.mCrashTextColor = mCrashTextColor;
    }

    public String getmCrashText() {
        return mCrashText;
    }

    public void setmCrashText(String mCrashText) {
        this.mCrashText = mCrashText;
    }

    public String getmDetailsButtonText() {
        return mDetailsButtonText;
    }

    public void setmDetailsButtonText(String mDetailsButtonText) {
        this.mDetailsButtonText = mDetailsButtonText;
    }

    public String getmRestartAppButtonText() {
        return mRestartAppButtonText;
    }

    public void setmRestartAppButtonText(String mRestartAppButtonText) {
        this.mRestartAppButtonText = mRestartAppButtonText;
    }
}