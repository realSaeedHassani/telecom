
------------------------------------------------------------
DROP VIEW IF EXISTS lonesome_lacs;
CREATE VIEW lonesome_lacs AS
SELECT
	mcc,
	mnc,
	lac,
	count(distinct cid) as cells
FROM
	session_info
WHERE
	mcc > 0 AND
	lac > 0 AND
	cid > 0 AND
	mcc < 1000 AND
	mnc < 1000 AND
	domain = 0
GROUP BY
	mcc, mnc, lac
HAVING
	cells = 1;

--  Match all sessions with 'lonesome LACs'
DROP VIEW IF EXISTS a5;
CREATE VIEW a5 as
SELECT
    si.id,
    si.mcc,
    si.mnc,
    si.lac,
    si.cid,
	1.0 AS score
FROM
	session_info as si, lonesome_lacs as ll
ON
	si.mcc = ll.mcc AND
	si.mnc = ll.mnc AND
	si.lac = ll.lac
WHERE
	si.domain = 0;

DROP VIEW IF EXISTS a2;
CREATE VIEW a2 AS
SELECT
	cell.id,
	cell.mcc,
	cell.mnc,
	cell.lac,
	cell.cid,
	--  If none of the observed neighbors has the same LAC
	--  as this cell the score is 0.5. Once ARFCN correlation
	--  is more reliable/tested we could increase that to 1.0
	((count(*) - sum(case when cell.lac != neig.lac then 1 else 0 end)) = 0) * 0.5 as score
FROM
	cell_info  AS cell,
	arfcn_list AS al,
	cell_info  AS neig,
	config
ON
	cell.id = al.id AND
	al.arfcn = neig.bcch_arfcn AND
	cell.bcch_arfcn != neig.bcch_arfcn AND
	cell.mcc = neig.mcc AND
	cell.mnc = neig.mnc AND
	--  Consider only neighboring information collected within the last
	--  config.neig_max_delta seconds as valid (ARFCNs are reused!).
	abs(strftime('%s', cell.last_seen) - strftime('%s', neig.last_seen)) < config.neig_max_delta
WHERE
	cell.mcc > 0 AND
	cell.lac > 0 AND
	cell.cid > 0 AND
	neig.mcc > 0 AND
	neig.lac > 0 AND
	neig.cid > 0
GROUP BY
	cell.mcc,
	cell.mnc,
	cell.lac,
	cell.cid
HAVING
	--  At least 3 neighbors should be known
	count(*) > 2;
-- (unavailable) .read sql/a_03.sql
--  A4

--  Join cell info with itself on all entries that have the same
--  MCC/MNC/LAC/CID, but were observed on different ARFCNs. This
--  my happen when cells are reconfigured, but this should be rare.
--  FIXME: When more test data is available we could decide to make
--  the score dependent on the timestamp delta between the occurrences
--  of the conflicting cell infos.
DROP VIEW IF EXISTS a4;
CREATE VIEW a4 as
SELECT
        l.id,
        l.mcc,
        l.mnc,
        l.lac,
        l.cid,
        count(*) AS value,
        CASE WHEN count(*) > 1 THEN 1 ELSE 0 END AS score
FROM cell_info AS l LEFT JOIN cell_info AS r
ON
        l.mcc = r.mcc AND
        l.mnc = r.mnc AND
        l.lac = r.lac AND
        l.cid = r.cid AND
        l.bcch_arfcn != r.bcch_arfcn AND
        l.first_seen < r.first_seen
WHERE
        l.mcc > 0 AND
        l.lac > 0 AND
        l.cid > 0 AND
        r.bcch_arfcn is not null
GROUP BY l.mcc, l.mnc, l.lac, l.cid;

--  Keep
--  K1
DROP VIEW IF EXISTS k1;
CREATE VIEW k1 AS
SELECT
	id,
	mcc,
	mnc,
	lac,
	cid,
	CASE
		WHEN (neigh_5 + neigh_5b + neigh_5t) > 1 THEN 0.0
		ELSE 1.0
	END as score
	FROM cell_info
	WHERE si5 OR si5b OR si5t;

DROP VIEW IF EXISTS cells_with_neig_arfcn;
CREATE VIEW cells_with_neig_arfcn AS
SELECT
        ci.last_seen,
        ci.id,
        ci.mcc,
        ci.mnc,
        ci.lac,
        ci.cid,
        ci.bcch_arfcn,
        al.arfcn as neig_arfcn
FROM cell_info AS ci LEFT JOIN arfcn_list AS al
ON ci.id = al.id
WHERE ci.mcc > 0 AND ci.lac > 0 AND ci.cid > 0;

--  Join the cells_with_neig_arfcn table above with itself, such
--  that that for every cell in cell_info we get the respective
--  neighboring cell information, if present. This is the case
--  when the neighbor ARFCN (neig_arfcn) equals the main ARFCN
--  (bcch_arfcn) of the joined table.
DROP VIEW IF EXISTS cells_with_neig_cell;
CREATE VIEW cells_with_neig_cell AS
SELECT DISTINCT
        c.id,
        c.mcc as cell_mcc,
        c.mnc as cell_mnc,
        c.lac as cell_lac,
        c.cid as cell_cid,
        c.bcch_arfcn as cell_arfcn,
        n.id as neig_id
FROM cells_with_neig_arfcn as c, cell_info as n, config
ON
	n.mcc>0 AND n.lac>0 AND n.cid>0 AND
	c.neig_arfcn = n.bcch_arfcn AND
	abs(julianday(c.last_seen)-julianday(n.last_seen)) < (config.neig_max_delta / 86400.0)
WHERE c.bcch_arfcn != c.neig_arfcn;

--  Count the number of neighboring cells recorded for every cell
--  in the table. Note, that this is the number of neighboring
--  cells we actually received neighbor information for (as an inner
--  join is used above). This count will later be used as the
--  number of neighboring cells that should have the cell in their
--  own ARFCN list.
--  This makes sense, as you may not receive BCCH info from a
--  neighboring cell on the far other end of you current cell.
--  This is normal and should not lead to a high score. This is
--  only the case if a (supposed) neighboring cell that we
--  receive SI messages from does not announce the current cell as
--  their own neighbor.
DROP VIEW IF EXISTS cells_neig_count;
CREATE VIEW cells_neig_count AS
SELECT *, count(*) as count
FROM cells_with_neig_cell
GROUP BY id, cell_mcc, cell_mnc, cell_lac, cell_cid;

--  This is the list neighboring cells that reference a cell.
--  It is similar to cells_with_neig_cell, just that we also
--  join on the neigboring cells ARFCN equaling the current
--  cells main ARFCN.
DROP VIEW IF EXISTS cells_ref_by_neig_cell;
CREATE VIEW cells_ref_by_neig_cell AS
SELECT DISTINCT
        c.id,
        c.mcc as cell_mcc,
        c.mnc as cell_mnc,
        c.lac as cell_lac,
        c.cid as cell_cid,
        c.bcch_arfcn as cell_arfcn,
        n.id as neig_id
FROM cells_with_neig_arfcn as c, cells_with_neig_arfcn as n, config
ON
	c.neig_arfcn = n.bcch_arfcn AND
	c.bcch_arfcn = n.neig_arfcn AND
	abs(strftime('%s', c.last_seen) - strftime('%s', n.last_seen)) < config.neig_max_delta;

--  Count by how many neigboring cells a cell is announced
--  as neighboring cell.
DROP VIEW IF EXISTS cells_ref_by_neig_count;
CREATE VIEW cells_ref_by_neig_count AS
SELECT id, count(*) as count
FROM cells_ref_by_neig_cell
GROUP BY cell_mcc, cell_mnc, cell_lac, cell_cid, cell_arfcn
ORDER BY id;

--  Join both counts. If none of the neighbors announce
--  this cell, assign a 1.0 score
DROP VIEW IF EXISTS r1;
CREATE VIEW r1 AS
SELECT
        cn.id,
        cn.cell_mcc AS mcc,
        cn.cell_mnc AS mnc,
        cn.cell_lac AS lac,
        cn.cell_cid AS cid,
		CASE
			WHEN cn.count - crbn.count = 0 THEN 1.0
										   ELSE 0.0
		END as score
FROM cells_neig_count as cn, cells_ref_by_neig_count as crbn
ON cn.id = crbn.id;


DROP VIEW IF EXISTS si;
CREATE VIEW si AS
SELECT
        si.id,
        si.timestamp,
        si.duration/1000 as duration,
        si.mcc,
        si.mnc,
        si.lac,
        si.cid,
        ifnull(a5.score, 0) as a5,
        0 as c1,
        0 as c2,
        0 as c3,
        0 as c4,
        0 as c5,
        0 as t3,
        0 as t4
FROM session_info as si LEFT JOIN
    a5 ON si.id = a5.id

WHERE si.domain = 0;

DROP VIEW IF EXISTS ci;
CREATE VIEW ci AS
SELECT DISTINCT
        ci.first_seen as first_seen,
        ci.last_seen as last_seen,
        ci.mcc,
        ci.mnc,
        ci.lac,
        ci.cid,
        0 as a1,
        ifnull(a2.score, 0) as a2,
        ifnull(a4.score, 0) as a4,
        ifnull(k1.score, 0) as k1,
        0 as k2,
        0 as t1,
        ifnull(r1.score, 0) as r1,
        0 as r2,
        0 as f1
FROM cell_info as ci LEFT JOIN
 a2 ON ci.id = a2.id LEFT JOIN
 a4 ON ci.id = a4.id LEFT JOIN
 k1 ON ci.id = k1.id LEFT JOIN
 r1 ON ci.id = r1.id
WHERE
	ci.mcc > 0 AND ci.lac > 0 AND ci.cid > 0;

DELETE FROM catcher;
INSERT INTO catcher
SELECT
	si.id,
	si.mcc,
	si.mnc,
	si.lac,
	si.cid,
	si.timestamp,
	si.duration,
	 0.0,
	ifnull(max(ci.a2), 0.0),
	ifnull(max(ci.a4), 0.0),
	ifnull(max(si.a5), 0.0),
	ifnull(max(ci.k1), 0.0),
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	 0.0,
	ifnull(max(ci.r1), 0.0),
	 0.0,
	 0.0,
	ifnull(si_loc.longitude, 0.0),
	ifnull(si_loc.latitude, 0.0),
	ifnull(si_loc.valid, 0),
	ifnull(max(ci.a1), 0.0) +
	ifnull(max(ci.a2), 0.0) +
	ifnull(max(ci.a4), 0.0) +
	ifnull(max(si.a5), 0.0) +
	ifnull(max(ci.k1), 0.0) +
	ifnull(max(ci.k2), 0.0) +
	ifnull(max(si.c1), 0.0) +
	ifnull(max(si.c2), 0.0) +
	ifnull(max(si.c3), 0.0) +
	ifnull(max(si.c4), 0.0) +
	ifnull(max(si.c5), 0.0) +
	ifnull(max(ci.t1), 0.0) +
	ifnull(max(si.t3), 0.0) +
	ifnull(max(si.t4), 0.0) +
	ifnull(max(ci.r1), 0.0) +
	ifnull(max(ci.r2), 0.0) +
	ifnull(max(ci.f1), 0.0) as score,
	0.0 as pwr

FROM config, si LEFT OUTER JOIN ci
ON
	ci.mcc = si.mcc AND
	ci.mnc = si.mnc AND
	ci.lac = si.lac AND
	ci.cid = si.cid AND
	abs(strftime('%s', ci.last_seen) - strftime('%s', si.timestamp)) < config.cell_info_max_delta
LEFT OUTER JOIN si_loc
ON
	si.id = si_loc.id
GROUP BY
	si.id
HAVING
	score > config.catcher_min_score;
