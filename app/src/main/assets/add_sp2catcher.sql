DROP VIEW IF EXISTS sp;
create view sp as
select *
from catcher
where timestamp > (SELECT max(timestamp) FROM catcher_with_signal_power) or (select count(*) from catcher_with_signal_power)=0;

insert  into catcher_with_signal_power
select * from sp;