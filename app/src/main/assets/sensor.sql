/*!40101 SET storage_engine=MyISAM */;
/*!40101 SET character_set_client = utf8 */;
--added  by sensor

DROP TABLE IF EXISTS sensor_status;
CREATE TABLE sensor_status (
    service_status boolean,
  _id INTEGER PRIMARY KEY AUTOINCREMENT,
  timestamp  datetime not null,
  battery_level   INTEGER NOT NULL,
  longitude   DOUBLE NOT NULL,
  latitude  DOUBLE NOT NULL,
  cpu_usage DOUBLE NOT NULL,
  memory_usage DOUBLE NOT NULL,
  disk_usage DOUBLE NOT NULL
);