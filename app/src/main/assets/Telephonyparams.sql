/*!40101 SET storage_engine=MyISAM */;
/*!40101 SET character_set_client = utf8 */;
--added  by sensor

DROP TABLE IF EXISTS sensor_info;
CREATE TABLE sensor_info(

  id INTEGER PRIMARY KEY AUTOINCREMENT,
  timestamp  datetime not null,
  longitude   DOUBLE NOT NULL,
  latitude  DOUBLE NOT NULL,
  Mcc    INTEGER NOT NULL,
  Mnc    INTEGER NOT NULL,
  Lac    INTEGER NOT NULL,
  Cid INTEGER NOT NULL,
  SignalStrength   INTEGER NOT NULL,
  SizeNeighboring   INTEGER NOT NULL,
  ARFCN    INTEGER NOT NULL,
  RAT   INTEGER NOT NULL

);

DROP TABLE IF EXISTS neighbor_info;
CREATE TABLE neighbor_info(

  id INTEGER PRIMARY KEY AUTOINCREMENT,
  timestamp  datetime not null,
  Mcc    INTEGER NOT NULL,
  Mnc    INTEGER NOT NULL,
  Lac    INTEGER NOT NULL,
  Cid INTEGER NOT NULL,
  SignalStrength   INTEGER NOT NULL,
  ARFCN    INTEGER NOT NULL,
  cellId INTEGER NOT NULL,
  FOREIGN KEY (cellId) REFERENCES sensor_info(id) ON DELETE CASCADE

);

DROP TABLE IF EXISTS neighborcatcher;
CREATE TABLE neighborcatcher
(
	id integer PRIMARY KEY,
	mcc integer,
	mnc integer,
	lac integer,
	cid integer,
	timestamp datetime,
	ma2 FLOAT,
	ma4 FLOAT,
	mk1 FLOAT,
	mk2 FLOAT,
	mt1 FLOAT,
	mr1 FLOAT,
	mr2 FLOAT,
--	longitude DOUBLE NOT NULL,
--	latitude DOUBLE NOT NULL,
	score FLOAT
);
