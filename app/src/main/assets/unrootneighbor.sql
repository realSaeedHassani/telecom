
---------------------------------------------------------
DROP VIEW IF EXISTS mk1;
CREATE VIEW mk1 AS
SELECT
	id,
	mcc,
	mnc,
	lac,
	cid,
	CASE
		WHEN (neigh_5 + neigh_5b + neigh_5t) > 1 THEN 0.0
		ELSE 1.0
	END as score
	FROM (SELECT
                      l.id,
                      l.mcc,
                      l.mnc,
                      l.lac,
                      l.cid,
                      l.neigh_5,
                      l.neigh_5b,
                      l.neigh_5t,
                      l.si5t,
                      l.si5,
                      l.si5b

              FROM cell_info AS l
              WHERE NOT EXISTS (SELECT
                      r.mcc,
                      r.mnc,
                      r.lac,
                      r.cid
                      FROM session_info As r
                      WHERE   l.mcc = r.mcc AND
                      l.mnc = r.mnc AND
                      l.lac = r.lac AND
                      l.cid = r.cid))
	WHERE si5 OR si5b OR si5t;
-------
DROP VIEW IF EXISTS ma2;
CREATE VIEW ma2 AS
SELECT
	cell.id,
	cell.mcc,
	cell.mnc,
	cell.lac,
	((count(*) - sum(case when cell.lac != neig.lac then 1 else 0 end)) = 0) * 0.5 as score
FROM
	(SELECT
                      l.id,
                      l.mcc,
                      l.mnc,
                      l.lac,
                      l.cid,
                    l.cro,
                    l.bcch_arfcn,
                    l.last_seen

              FROM cell_info AS l
              WHERE NOT EXISTS (SELECT
                      r.mcc,
                      r.mnc,
                      r.lac,
                      r.cid
                      FROM session_info As r
                      WHERE   l.mcc = r.mcc AND
                      l.mnc = r.mnc AND
                      l.lac = r.lac AND
                      l.cid = r.cid))  AS cell,
	arfcn_list AS al,
	(SELECT
                      l.id,
                      l.mcc,
                      l.mnc,
                      l.lac,
                      l.cid,
                    l.cro,
                    l.bcch_arfcn,
                      l.last_seen
              FROM cell_info AS l
              WHERE NOT EXISTS (SELECT
                      r.mcc,
                      r.mnc,
                      r.lac,
                      r.cid
                      FROM session_info As r
                      WHERE   l.mcc = r.mcc AND
                      l.mnc = r.mnc AND
                      l.lac = r.lac AND
                      l.cid = r.cid))  AS neig,
	config
ON
	cell.id = al.id AND
	al.arfcn = neig.bcch_arfcn AND
	cell.bcch_arfcn != neig.bcch_arfcn AND
	cell.mcc = neig.mcc AND
	cell.mnc = neig.mnc AND
	abs(strftime('%s', cell.last_seen) - strftime('%s', neig.last_seen)) < config.neig_max_delta
WHERE
	cell.mcc > 0 AND
	cell.lac > 0 AND
	cell.cid > 0 AND
	neig.mcc > 0 AND
	neig.lac > 0 AND
	neig.cid > 0
GROUP BY
	cell.mcc,
	cell.mnc,
	cell.lac,
	cell.cid
HAVING
	count(*) > 2;

DROP VIEW IF EXISTS ma4;
CREATE VIEW ma4 as
SELECT
        l.id,
        l.mcc,
        l.mnc,
        l.lac,
        l.cid,
        count(*) AS value,
        CASE WHEN count(*) > 1 THEN 1 ELSE 0 END AS score
FROM (SELECT
                       l.id,
                       l.mcc,
                       l.mnc,
                       l.lac,
                       l.cid,
                     l.cro,
                     l.bcch_arfcn,
                     l.first_seen
               FROM cell_info AS l
               WHERE NOT EXISTS (SELECT
                       r.mcc,
                       r.mnc,
                       r.lac,
                       r.cid
                       FROM session_info As r
                       WHERE   l.mcc = r.mcc AND
                       l.mnc = r.mnc AND
                       l.lac = r.lac AND
                       l.cid = r.cid)) AS l LEFT JOIN (SELECT
                                                                        l.id,
                                                                        l.mcc,
                                                                        l.mnc,
                                                                        l.lac,
                                                                        l.cid,
                                                                      l.cro,
                                                                      l.bcch_arfcn,
                                                                      l.first_seen
                                                                FROM cell_info AS l
                                                                WHERE NOT EXISTS (SELECT
                                                                        r.mcc,
                                                                        r.mnc,
                                                                        r.lac,
                                                                        r.cid
                                                                        FROM session_info As r
                                                                        WHERE   l.mcc = r.mcc AND
                                                                        l.mnc = r.mnc AND
                                                                        l.lac = r.lac AND
                                                                        l.cid = r.cid)) AS r
ON
        l.mcc = r.mcc AND
        l.mnc = r.mnc AND
        l.lac = r.lac AND
        l.cid = r.cid AND
        l.bcch_arfcn != r.bcch_arfcn AND
        l.first_seen < r.first_seen
WHERE
        l.mcc > 0 AND
        l.lac > 0 AND
        l.cid > 0 AND
        r.bcch_arfcn != null
GROUP BY l.mcc, l.mnc, l.lac, l.cid;


------------------

DROP VIEW IF EXISTS mcells_with_neig_arfcn;
CREATE VIEW mcells_with_neig_arfcn AS
SELECT
        ci.last_seen,
        ci.id,
        ci.mcc,
        ci.mnc,
        ci.lac,
        ci.cid,
        ci.bcch_arfcn,
        al.arfcn as neig_arfcn
FROM (SELECT
                                                                             l.id,
                                                                             l.mcc,
                                                                             l.mnc,
                                                                             l.lac,
                                                                             l.cid,
                                                                           l.cro,
                                                                           l.bcch_arfcn,
                                                                           l.last_seen
                                                                     FROM cell_info AS l
                                                                     WHERE NOT EXISTS (SELECT
                                                                             r.mcc,
                                                                             r.mnc,
                                                                             r.lac,
                                                                             r.cid
                                                                             FROM session_info As r
                                                                             WHERE   l.mcc = r.mcc AND
                                                                             l.mnc = r.mnc AND
                                                                             l.lac = r.lac AND
                                                                             l.cid = r.cid))  AS ci LEFT JOIN arfcn_list AS al
ON ci.id = al.id
WHERE ci.mcc > 0 AND ci.lac > 0 AND ci.cid > 0;


DROP VIEW IF EXISTS mcells_with_neig_cell;
CREATE VIEW mcells_with_neig_cell AS
SELECT DISTINCT
        c.id,
        c.mcc as cell_mcc,
        c.mnc as cell_mnc,
        c.lac as cell_lac,
        c.cid as cell_cid,
        c.bcch_arfcn as cell_arfcn,
        n.id as neig_id
FROM mcells_with_neig_arfcn as c, cell_info as n, config
ON
	n.mcc>0 AND n.lac>0 AND n.cid>0 AND
	c.neig_arfcn = n.bcch_arfcn AND
	abs(julianday(c.last_seen)-julianday(n.last_seen)) < (config.neig_max_delta / 86400.0)
WHERE c.bcch_arfcn != c.neig_arfcn;

DROP VIEW IF EXISTS mcells_neig_count;
CREATE VIEW mcells_neig_count AS
SELECT *, count(*) as count
FROM mcells_with_neig_cell
GROUP BY id, cell_mcc, cell_mnc, cell_lac, cell_cid;


DROP VIEW IF EXISTS mcells_ref_by_neig_cell;
CREATE VIEW mcells_ref_by_neig_cell AS
SELECT DISTINCT
        c.id,
        c.mcc as cell_mcc,
        c.mnc as cell_mnc,
        c.lac as cell_lac,
        c.cid as cell_cid,
        c.bcch_arfcn as cell_arfcn,
        n.id as neig_id
FROM mcells_with_neig_arfcn as c, mcells_with_neig_arfcn as n, config
ON
	c.neig_arfcn = n.bcch_arfcn AND
	c.bcch_arfcn = n.neig_arfcn AND
	abs(strftime('%s', c.last_seen) - strftime('%s', n.last_seen)) < config.neig_max_delta;

DROP VIEW IF EXISTS mcells_ref_by_neig_count;
CREATE VIEW mcells_ref_by_neig_count AS
SELECT id, count(*) as count
FROM mcells_ref_by_neig_cell
GROUP BY cell_mcc, cell_mnc, cell_lac, cell_cid, cell_arfcn
ORDER BY id;


DROP VIEW IF EXISTS mr1;
CREATE VIEW mr1 AS
SELECT
        cn.id,
        cn.cell_mcc AS mcc,
        cn.cell_mnc AS mnc,
        cn.cell_lac AS lac,
        cn.cell_cid AS cid,
		CASE
			WHEN cn.count - crbn.count = 0 THEN 1.0
										   ELSE 0.0
		END as score
FROM mcells_neig_count as cn, mcells_ref_by_neig_count as crbn
ON cn.id = crbn.id;


DROP VIEW IF EXISTS ni;
CREATE VIEW ni AS
SELECT DISTINCT
        ci.id,
        ci.first_seen as first_seen,
        ci.last_seen as last_seen,
        ci.mcc,
        ci.mnc,
        ci.lac,
        ci.cid,
        ifnull(ma2.score, 0) as ma2,
        ifnull(ma4.score, 0) as ma4,
        ifnull(mk1.score, 0) as mk1,
        ifnull(mr1.score, 0) as mr1
FROM cell_info as ci LEFT JOIN
 ma2 ON ci.id = ma2.id LEFT JOIN
 ma4 ON ci.id = ma4.id LEFT JOIN
 mk1 ON ci.id = mk1.id LEFT JOIN
 mr1 ON ci.id = mr1.id
WHERE
	ci.mcc > 0 AND ci.lac > 0 AND ci.cid > 0;


DELETE FROM neighborcatcher;
INSERT INTO neighborcatcher
  SELECT
  	ni.id,
  	ni.mcc,
  	ni.mnc,
  	ni.lac,
  	ni.cid,
  	ni.last_seen,
    ifnull(max(ni.ma2), 0.0),
    ifnull(max(ni.ma4), 0.0),
    ifnull(max(ni.mk1), 0.0),
    0.0,
    0.0,
  	ifnull(max(ni.mr1), 0.0),
  	0.0,
  	ifnull(max(ni.ma2), 0.0) +
  	ifnull(max(ni.ma4), 0.0) +
  	ifnull(max(ni.mk1), 0.0) +
    ifnull(max(ni.mr1), 0.0)  as score
  FROM config,ni
WHERE 1==1
  GROUP BY
  	ni.id
  having  score > config.catcher_min_score;